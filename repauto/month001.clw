

   MEMBER('monthly.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('MONTH001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:EXEName          STRING(20)
  CODE
    x# = 1
    Loop
        tmp:EXEName  = GETINI('MONTHLYREPORT','EXE '& x#,,CLIP(Path()) & '\REPAUTO.INI')
        If tmp:EXEName <> ''
            PUTINI(Clip(tmp:EXEName),'Started',Format(Today(),@d6) & ' at ' & Format(Clock(),@t1),CLIP(Path()) & '\MONTHLYLOG.INI')
            Run(Clip(tmp:EXEName) & ' % /MONTHLY',1)
            PUTINI(Clip(tmp:EXEName),'Stopped',Format(Today(),@d6) & ' at ' & Format(Clock(),@t1),CLIP(Path()) & '\MONTHLYLOG.INI')
        Else
            Break
        End !If tmp:EXEName <> ''
        x# += 1
    End !Loop



   MEMBER('vodr0011.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

OrderStatusReport PROCEDURE                           !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Parameters_Group     GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
LOC:DayLeawayCount   LONG
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
Courier              STRING(30)
CompanyName          STRING(30)
DeskTopPath          STRING(255)
FileName             STRING(255)
JobsExtendedInSync   BYTE(false)
JobNumber            LONG
LineCost             DECIMAL(7,2)
MaxTabNumber         BYTE(2)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
TabNumber            BYTE(1)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
Misc_Group           GROUP,PRE()
debug:Count          LONG
OPTION1              SHORT
Progress:Text        STRING(100)
RecordCount          LONG
Result               BYTE
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:CommentText    STRING(100)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        REAL
                     END
Count_Group          GROUP,PRE()
CountOrders          LONG
CountLines           LONG
CountParts           LONG
CountDelivered       LONG
                     END
DASTAG_Group         GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
                     END
OverdueTally         GROUP,PRE(tg)
Overdue00            LONG
Overdue01_05         LONG
Overdue06_10         LONG
Overdue11            LONG
Undelivered          LONG
Overdue00Parts       LONG
Overdue01_05Parts    LONG
Overdue06_10Parts    LONG
Overdue11Parts       LONG
UndeliveredParts     LONG
Overdue00Amount      DECIMAL(7,2)
Overdue01_05Amount   DECIMAL(7,2)
Overdue06_10Amount   DECIMAL(7,2)
Overdue11Amount      DECIMAL(7,2)
UndeliveredAmount    DECIMAL(7,2)
                     END
SupplierQueue        QUEUE,PRE(sq)
CompanyName          STRING(30)
NormalSupplyPeriod   REAL
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('F')
DataLastCol          STRING('P')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
LocalTag             STRING(1)
LocalTimeOut         LONG
GUIMode              BYTE
DoAll                STRING(1)
LocalHeadAccount     STRING(30)
BRW1::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Order Status Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Status Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Engineer Performance Report Criteria'),USE(?Tab1)
                           STRING('Start Date'),AT(263,89,40,8),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(323,89,64,10),USE(LOC:StartDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest Order date'),REQ
                           BUTTON,AT(395,86),USE(?StartPopCalendar),IMM,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('&Rev tags'),AT(288,198,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           STRING('End Date'),AT(264,110,40,8),USE(?String2:2),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(324,110,64,10),USE(LOC:EndDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest Order date'),REQ
                           BUTTON,AT(396,105),USE(?EndPopCalendar),IMM,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(288,214,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           CHECK('All Repair Centres'),AT(192,126),USE(DoAll),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(432,126),USE(excel:Visible),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,140,296,192),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(496,217),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,249),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,281),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?reportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::5:QUEUE = glo:Queue2
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue2)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF    LOC:DayLeawayCount < 0
            LOC:DayLeawayCount = 1
        ELSIF LOC:DayLeawayCount > 365
            LOC:DayLeawayCount = 365
        END !IF

        IF LOC:StartDate > LOC:EndDate
            temp          = LOC:EndDate
            LOC:EndDate   = LOC:StartDate
            LOC:StartDate = temp
        END !IF
        !-----------------------------------------------------------------
!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO ProgressBar_Setup
           !-----------------------------------------------------------------
           DO XL_Setup

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        !
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 30
        Excel{'ActiveSheet.Columns("B:G").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader

        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
 !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t4), 'start', 'debug.ini')
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
        DO XL_SetWorksheetLandscape

        LOC:SectionName = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        excel:CommentText = ''

        DO CreateSectionHeader
        !-----------------------------------------------------------------
        DO SetColumns

        CountOrders    = 0
        CountLines     = 0
        CountParts     = 0
        CountDelivered = 0
        CLEAR(OverdueTally)
        FREE(SupplierQueue)
        CLEAR(SupplierQueue)
        FREE(HeaderQueue)
        CLEAR(HeaderQueue)
        !------------------------------------------
        Access:ORDERS.ClearKey(ord:DateKey)
        ord:Date = LOC:StartDate
        SET(ord:DateKey, ord:DateKey)
        !------------------------------------------
        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = RECORDS(ORDERS)
        RecordsProcessed = 0
        RecordCount      = 0


        DO ProgressBar_LoopPre
 !message('Start of main loop')
 !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t4), 'before loop', 'debug.ini')
        !-----------------------------------------------------------------
        LOOP UNTIL Access:ORDERS.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF ord:Date > LOC:EndDate
                BREAK
            END !IF

            DO Print_AllPartsOrdered
            !-------------------------------------------------------------
        END !LOOP
 !message('End of main loop')
 !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t4), 'after loop', 'debug.ini')

        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        ! summary details
        !
        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'} = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'} = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.NumberFormat'}  = '0'
            Excel{'ActiveCell.Formula'}       = '=SUBTOTAL(2, C' & sheet:DataHeaderRow+1 & ':C' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("L' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Totals:'

        Excel{'Range("M' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.NumberFormat'}  = '0'
            Excel{'ActiveCell.Formula'}       = '=SUBTOTAL(9, M' & sheet:DataHeaderRow+1 & ':M' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("N' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.NumberFormat'}  = '#,##0.00'
            Excel{'ActiveCell.Formula'}       = '=SUBTOTAL(9, N' & sheet:DataHeaderRow+1 & ':N' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("o' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.NumberFormat'}  = '#,##0.00'
            Excel{'ActiveCell.Formula'}       = '=SUBTOTAL(9, O' & sheet:DataHeaderRow+1 & ':o' & sheet:DataHeaderRow+RecordCount & ')'

        DO FormatColumns

        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        DO WriteSummary
        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------       
        ! putini('CreateSectionHeader_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
        !-----------------------------------------------       
        LOC:Text          = CLIP(LOC:SectionName)
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}     = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'} = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}     = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'} = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(4)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & '").Select'}
        Excel{'ActiveCell.Formula'}           = 'Ordered Date From:'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}   = LEFT(FORMAT(LOC:StartDate, @D8))

        CurrentRow += 1
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}       = 'Ordered Date To:'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}   = LEFT(FORMAT(LOC:EndDate, @D8))

        CurrentRow += 1
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}       = 'Created By:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}   = LOC:UserName
                DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}       = 'Created Date:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}   = LEFT(FORMAT(TODAY(), @D8))
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
    EXIT
Print_AllPartsOrdered                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
 !putini('Print_AllPartsOrdered', ord:Order_Number, format(CLOCK(), @t4), 'debug.ini')
        IF CancelPressed
            EXIT
        END !IF

        CountOrders    += 1

        DO GetSupplierFromQueue
        !------------------------------------------
        !
        Access:ORDPARTS.ClearKey(orp:Order_Number_Key)
        orp:Order_Number = ord:Order_Number
        SET(orp:Order_Number_Key, orp:Order_Number_Key)
        !------------------------------------------
        Progress:Text = CLIP(LOC:SectionName) & ' Order(' & ord:Order_Number & ')'
        !-----------------------------------------------------------------
        LOOP UNTIL Access:ORDPARTS.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF orp:Order_Number <> ord:Order_Number
                BREAK
            END !IF
            !look up stock
            Access:STOCK.CLEARKEY(sto:Ref_Number_Key)
            STO:Ref_Number = orp:Part_Ref_Number
            IF ~Access:STOCK.Fetch(sto:Ref_Number_Key) THEN
               IF STO:Location <> tra_ali:SiteLocation THEN CYCLE.
            ELSE
                CYCLE
            END !IF
            !-------------------------------------------------------------
            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

!        DO SetColumn_Supplier
        head:ColumnName   = 'Supplier'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_Overdue
        head:ColumnName   = 'Overdue'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_OrderNumber        ! Order Number
        head:ColumnName   = 'Order Number'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

!        DO SetColumn_OrderDate          ! Order Date
        head:ColumnName   = 'Order Date'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_OrderedBy          ! Ordered By
        head:ColumnName   = 'Ordered By'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_OrderStatus        ! Order Status
        head:ColumnName   = 'Order Closed'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_OrderPartStatus    ! Order Status
        head:ColumnName   = 'Part Received'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_ReceivedDate       ! Date Received
        head:ColumnName   = 'Received Date'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_DaysToDeliver      ! Days To Deliver
        head:ColumnName   = 'Days To Deliver'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0'
            ADD(HeaderQueue)

!        DO SetColumn_PartNumber         ! Part Number
        head:ColumnName   = 'Part Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_Description        !
        head:ColumnName   = 'Description'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_DeliveryNoteNumber ! Delivery Note Number
        head:ColumnName   = 'Delivery Note Number'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_Quantity           !
        head:ColumnName   = 'Quantity'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

!        DO SetColumn_PurchaseCost       ! Purchase Cost
        head:ColumnName   = 'Purchase Cost'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

!        DO SetColumn_LineCost           ! Line Cost
        head:ColumnName   = 'Line Cost'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

!        DO SetColumn_Type               ! Type
        head:ColumnName   = 'Type'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
ColumnPos LONG(1)
    CODE
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !PutIniCount += 1
        !PUTINI(CLIP(LOC:ProgramName),PutIniCount, 'SetColumns','C:\' & LOC:ApplicationName & '_Debug.INI')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        DO InitColumns

        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
DaysToDeliver    LONG
WriteColumn_Type STRING(6)
    CODE
        !-----------------------------------------------
! putini('WriteColumns_AllPartsOrdered', ord:Order_Number & ', ' & CLIP(orp:Part_Number), orp:Record_Number & ', ' & FORMAT(orp:Date_Received,@D8), 'debug.ini')

        RecordCount        += 1
        CountLines         += 1

        CountParts         += orp:Quantity

        LOC:LineCost = (orp:Quantity * orp:Purchase_Cost)
        !-----------------------------------------------
        IF (orp:All_Received = 'YES')
            !-------------------------------------------
            CountDelivered += orp:Quantity

            DaysToDeliver = (orp:Date_Received - ord:Date - sq:NormalSupplyPeriod)
            IF DaysToDeliver < 1
                tg:Overdue00              += 1
                tg:Overdue00Parts         += orp:Quantity
                tg:Overdue00Amount        += LOC:LineCost
            ELSE
                CASE DaysToDeliver
                OF 1 TO 5
                    tg:Overdue01_05       += 1
                    tg:Overdue01_05Parts  += orp:Quantity
                    tg:Overdue01_05Amount += LOC:LineCost
                OF 6 TO 10
                    tg:Overdue06_10       += 1
                    tg:Overdue06_10Parts  += orp:Quantity
                    tg:Overdue06_10Amount += LOC:LineCost
                ELSE
                    tg:Overdue11          += 1
                    tg:Overdue11Parts     += orp:Quantity
                    tg:Overdue11Amount    += LOC:LineCost
                END !CASE        
            END !IF
            !-------------------------------------------
        ELSE !
            !-------------------------------------------
            tg:Undelivered       += 1
            tg:UndeliveredParts  += orp:Quantity
            tg:UndeliveredAmount += LOC:LineCost
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        If orp:job_number <> ''
            Case orp:part_type
                Of 'RET'
                    WriteColumn_Type  = 'RETAIL'
                Else
                    WriteColumn_Type  = 'JOB'
            End!Case orp:part_type
        Else!If orp:job_number <> ''
            WriteColumn_Type  = 'STOCK'
        End!If orp:job_number <> ''
        !===============================================
        debug:Count += 1

 LINEPRINT(                                                |
        debug:Count                                & ',' & |
        CLIP(ord:Supplier)                         & ',' & |
        TODAY() - ord:Date - sq:NormalSupplyPeriod & ',' & |
        CLIP(ord:Order_Number)                     & ',' & |
        LEFT(FORMAT(ord:Date, @D8))                & ',' & |
        CLIP(ord:User)                             & ',' & |
        CLIP(ord:All_Received)                     & ',' & |
        CLIP(orp:All_Received)                     & ',' & |
        LEFT(FORMAT(orp:Date_Received, @D8))       & ',' & |
        (orp:Date_Received - ord:Date)             & ',' & |
        CLIP(orp:Part_Number)                      & ',' & |
        CLIP(orp:Description)                      & ',' & |
        CLIP(orp:DespatchNoteNumber)               & ',' & |
        orp:Quantity                               & ',' & |
        orp:Purchase_Cost                          & ',' & |
        LOC:LineCost                               & ',' & |
        clip(WriteColumn_Type),                            |
        'C:\debug.CSV')
        !-----------------------------------------------
        DO WriteColumn_Supplier           !
        DO WriteColumn_Overdue            !
        DO WriteColumn_OrderNumber        ! Order Number
        DO WriteColumn_OrderDate          ! Order Date
        DO WriteColumn_OrderedBy          ! Ordered By
        DO WriteColumn_OrderStatus        ! OrderStatus
        DO WriteColumn_OrderPartStatus    ! Part Received
        DO WriteColumn_ReceivedDate       ! Received Date
        DO WriteColumn_DaysToDeliver      ! Days To Deliver
        DO WriteColumn_PartNumber         ! Part Number
        DO WriteColumn_Description        !
        DO WriteColumn_DeliveryNoteNumber ! Delivery Note Number
        DO WriteColumn_Quantity           !
        DO WriteColumn_PurchaseCost       ! Purchase Cost
        DO WriteColumn_LineCost           ! Line Cost
        DO WriteColumn_Type               ! Type

        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_Description                      ROUTINE ! Description
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & orp:Description

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(orp:Description)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DaysOver                  ROUTINE ! Days Over
    DATA
DaysOver LONG
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_DaysOver', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', '(TODAY() - ord:Date)(' & (TODAY() - ord:Date) & ')', 'debug.ini')
        !-----------------------------------------------
        DaysOver = (TODAY() - ord:Date - sq:NormalSupplyPeriod)
        IF DaysOver < 0
            clip:Value = 0
        END !IF

        IF excel:OperatingSystem < 5
            IF DaysOver > 0
                clip:Value = CLIP(clip:Value) & '<09>' & DaysOver
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>'
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        IF DaysOver > 0
            Excel{'ActiveCell.Formula'} = DaysOver
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DaysSinceOrder                         ROUTINE ! Days Since Order
    DATA
DaysSinceOrder LONG
    CODE
        !-----------------------------------------------
        ! Days Since Order
        ! putini('WriteColumn_DaysSinceOrder', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', '(TODAY() - ord:Date)(' & (TODAY() - ord:Date) & ')', 'debug.ini')
        !-----------------------------------------------
        DaysSinceOrder = (TODAY() - ord:Date)

        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & DaysSinceOrder

            EXIT
        END !IF

        Excel{'ActiveCell.Formula'}  = DaysSinceOrder

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DaysToDeliver                       ROUTINE ! Days To Deliver
    DATA
DaysToDeliver LONG
    CODE
        !-----------------------------------------------
        ! Days To Deliver
        ! putini('WriteColumn_DaysToDeliver', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'DaysToDeliver(' & (orp:Date_Received - ord:Date) & ')', 'debug.ini')
        !-----------------------------------------------
        DaysToDeliver = (orp:Date_Received - ord:Date)

        IF excel:OperatingSystem < 5
            IF (orp:All_Received = 'YES')
                clip:Value = CLIP(clip:Value) & '<09>' & DaysToDeliver
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' 
            END !IF

            EXIT
        END !IF

        IF (orp:All_Received = 'YES')
            Excel{'ActiveCell.Formula'} = DaysToDeliver
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DeliveryNoteNumber                            ROUTINE ! Delivery Note Number
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_DeliveryNoteNumber', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:DespatchNoteNumber(' & orp:DespatchNoteNumber & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & orp:DespatchNoteNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = CLIP(orp:DespatchNoteNumber)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_LineCost                    ROUTINE ! Line Cost
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_LineCost', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'LOC:LineCost(' & LOC:LineCost & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:LineCost

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = LOC:LineCost

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Overdue                    ROUTINE ! Overdue
    DATA
Overdue LONG
temp    STRING(3)
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_Overdue', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'Overdue(' & TODAY() - ord:Date & ')', 'debug.ini')
        !-----------------------------------------------
        IF orp:All_Received = 'YES'
            temp = 'NO'
        ELSE
            Overdue = TODAY() - ord:Date - sq:NormalSupplyPeriod

            IF Overdue > 0
                temp = 'YES'
            ELSE
                temp = 'NO'
            END !IF
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & temp

            EXIT
        END !IF

        Excel{'ActiveCell.Formula'}  = CLIP(temp)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_OrderDate                           ROUTINE ! Order Date
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_OrderDate', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'ord:Date(' & LEFT(FORMAT(ord:Date, @D8)) & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(ord:Date, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(ord:Date, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_OrderNumber            ROUTINE ! Order Number
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_OrderNumber', ord:Order_Number & ', ' & orp:Part_Number, ord:Order_Number & ',' &CLIP(orp:Reason)&CLIP(orp:DespatchNoteNumber), 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & ord:Order_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = ord:Order_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_OrderedBy           ROUTINE ! Ordered By
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_OrderedBy', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'ord:User(' & ord:User & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & ord:User

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(ord:User)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_OrderStatus                     ROUTINE ! Order Status
    DATA
OrderStatus STRING(3)
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_OrderStatus', ord:Order_Number & ', ' & orp:Part_Number, CLIP(ord:All_Received), 'debug.ini')
        !-----------------------------------------------
        IF ord:All_Received = 'YES'
            OrderStatus = 'YES' ! 'RECEIVED'
        ELSE
            OrderStatus = 'NO' ! T RECEIVED'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & OrderStatus

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(OrderStatus)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_OrderPartStatus             ROUTINE ! Order Part Status
    DATA
OrderPartStatus STRING(3)
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_OrderPartStatus', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:All_Received(' & orp:All_Received & ')', 'debug.ini')
        !-----------------------------------------------
        IF orp:All_Received = 'YES'
            OrderPartStatus = 'YES'
        ELSE
            OrderPartStatus = 'NO'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & OrderPartStatus

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(OrderPartStatus)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_PartNumber                       ROUTINE ! Part Number
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_PartNumber', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:Part_Number(' & orp:Part_Number & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & orp:Part_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}       = CLIP(orp:Part_Number)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_PurchaseCost                      ROUTINE ! Purchase Cost
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_PurchaseCost', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:Purchase_Cost(' & orp:Purchase_Cost & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & orp:Purchase_Cost

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = orp:Purchase_Cost

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Quantity                      ROUTINE ! Quantity
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_Quantity', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:Quantity(' & orp:Quantity & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & orp:Quantity

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = orp:Quantity

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ReceivedDate                           ROUTINE ! Received Date
    DATA
    CODE
        !-----------------------------------------------
        ! putini('WriteColumn_ReceivedDate', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:Date_Received(' & LEFT(FORMAT(orp:Date_Received, @D8)) & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF orp:All_Received = 'YES'
                clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(orp:Date_Received, @D8))
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' 
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        IF orp:All_Received = 'YES'
            Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(orp:Date_Received, @D8))
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Supplier                      ROUTINE ! Supplier
    DATA
    CODE
        !-----------------------------------------------
        !  putini('WriteColumn_Supplier', ord:Order_Number & ', ' & orp:Part_Number, 'ord:Supplier(' & CLIP(ord:Supplier) & ')', 'debug.ini')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            !clip:Value = CLIP(clip:Value) & '<09>' & ord:Supplier
            clip:Value = ord:Supplier

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(ord:Supplier)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Type                  ROUTINE ! Type (JOB/RETAILSTOCK)
    DATA
temp STRING(6)
    CODE
        !-----------------------------------------------
        !  putini('WriteColumn_Type', 'ord:Order_Number(' & ord:Order_Number & '), orp:Part_Number(' & orp:Part_Number & ')', 'orp:job_number(' & orp:job_number & ')', 'debug.ini')
        !-----------------------------------------------
        If orp:job_number <> ''
            Case orp:part_type
                Of 'RET'
                    temp  = 'RETAIL'
                Else
                    temp  = 'JOB'
            End!Case orp:part_type
        Else!If orp:job_number <> ''
            temp  = 'STOCK'
        End!If orp:job_number <> ''
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(temp) ! LEFT(FORMAT(job:Date_Despatched, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteSummary                                              ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
TopRow       LONG
CurrentRow   LONG
    CODE
        !-----------------------------------------------------------------
        !  putini('WriteSummary_AllPartsOrdered', CLIP(LOC:SectionName), left(format(clock(), @t4)), 'debug.ini')
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
            DO XL_ColFirst
        !-----------------------------------------------------------------
        ! Totals
        !
        CurrentRow = Excel{'ActiveCell.Row'}

        Excel{'ActiveCell.Formula'}                  = 'Overdue Analysis'
            DO XL_ColRight
            Excel{'ActiveCell.Formula'}              = 'Lines'
            DO XL_ColRight
            Excel{'ActiveCell.Formula'}              = 'Parts'
            DO XL_ColRight
            Excel{'ActiveCell.Formula'}              = 'Percent'
            DO XL_ColRight
            Excel{'ActiveCell.Formula'}              = 'Amount'
            DO XL_ColRight
            Excel{'ActiveCell.Formula'}              = 'Percent'

        Excel{'Range("A' & CurrentRow & ':' & sheet:HeadLastCol & CurrentRow & '").Select'}
                DO XL_SetTitle
                DO XL_SetGrid
        !-----------------------------------------------------------------
        CurrentRow += 1
        TopRow      = CurrentRow

        DO XL_RowDown
            Excel{'ActiveCell.Formula'}              = 'Parts Ordered'
        DO XL_RowDown
            Excel{'ActiveCell.Formula'}              = 'Delivered within suppliers time frame'
        DO XL_RowDown
            Excel{'ActiveCell.Formula'}              = 'Delivered 1-5 days overdue'
        DO XL_RowDown
            Excel{'ActiveCell.Formula'}              = 'Delivered 6-10 days overdue'
        DO XL_RowDown
            Excel{'ActiveCell.Formula'}              = 'Delivered over 10 days overdue'
        DO XL_RowDown
            Excel{'ActiveCell.Formula'}              = 'Undelivered'
        !-----------------------------------------------------------------
        Excel{'Range("B' & CurrentRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = '=SUM(B' & CurrentRow+1 & ':B' & CurrentRow+5 & ')'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue00
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue01_05
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue06_10
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue11
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Undelivered
        !-----------------------------------------------------------------
        Excel{'Range("C' & CurrentRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = '=SUM(C' & CurrentRow+1 & ':C' & CurrentRow+5 & ')'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue00Parts
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue01_05Parts
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue06_10Parts
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:Overdue11Parts
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0'
            Excel{'ActiveCell.Formula'}              = tg:UndeliveredParts
        !-----------------------------------------------------------------
        Excel{'Range("D' & CurrentRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.Formula'}              = '=SUM(D' & CurrentRow+1 & ':D' & CurrentRow+5 & ')'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-1]C[-1] = 0, 0, rC[-1]/R[-1]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-2]C[-1] = 0, 0, rC[-1]/R[-2]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-3]C[-1] = 0, 0, rC[-1]/R[-3]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-4]C[-1] = 0, 0, rC[-1]/R[-4]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-5]C[-1] = 0, 0, rC[-1]/R[-5]C[-1])'
        !-----------------------------------------------------------------
        Excel{'Range("E' & CurrentRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}         = '#,##0.00'
            Excel{'ActiveCell.Formula'}              = '=SUM(E' & CurrentRow+1 & ':E' & CurrentRow+5 & ')'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0.00'
            Excel{'ActiveCell.Formula'}              = tg:Overdue00Amount
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0.00'
            Excel{'ActiveCell.Formula'}              = tg:Overdue01_05Amount
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0.00'
            Excel{'ActiveCell.Formula'}              = tg:Overdue06_10Amount
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0.00'
            Excel{'ActiveCell.Formula'}              = tg:Overdue11Amount
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '#,##0.00'
            Excel{'ActiveCell.Formula'}              = tg:UndeliveredAmount
        !-----------------------------------------------------------------
        Excel{'Range("F' & CurrentRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.Formula'}              = '=SUM(F' & CurrentRow+1 & ':F' & CurrentRow+5 & ')'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-1]C[-1] = 0, 0, rC[-1]/R[-1]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-2]C[-1] = 0, 0, rC[-1]/R[-2]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-3]C[-1] = 0, 0, rC[-1]/R[-3]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-4]C[-1] = 0, 0, rC[-1]/R[-4]C[-1])'
        DO XL_RowDown
            Excel{'ActiveCell.NumberFormat'}         = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}          = '=IF(R[-5]C[-1] = 0, 0, rC[-1]/R[-5]C[-1])'
        !-----------------------------------------------------------------
        Excel{'Range("A' & TopRow & ':' & sheet:HeadLastCol & TopRow+5 & '").Select'}
            DO XL_SetBorder

        Excel{'Range("A' & TopRow+7 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
ActiveWorkSheet CSTRING(20)
!ActiveCell      CSTRING(20)
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))

        ActiveWorkSheet = Excel{'ActiveSheet'}
            Excel{ActiveWorkSheet & '.Paste()'}
        Excel{prop:Release} = ActiveWorkSheet
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
LoadTradeAccount                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Account_Number_Key       KEY(                        sub:Account_Number),NOCASE
        ! Branch_Key               KEY(                        sub:Branch),DUP,NOCASE
        ! Company_Name_Key         KEY(                        sub:Company_Name),DUP,NOCASE
        ! Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE,PRIMARY
        ! Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
        ! Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        !--------------------------------------------------------------------------------------
        IF Access:SUBTRACC.Fetch(sub:Account_Number_Key)
            LOC:Account_Name = '<Not Found>'
        ELSE
            LOC:Account_Name = CLIP(sub:Company_Name)
        END !IF
        !-----------------------------------------------
    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mquest.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mquest.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Order Status Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0011 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
GetSupplierFromQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! on entry LOC:HeadAccountNumber = Account Number to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        LOC:CompanyName = ord:Supplier
        sq:CompanyName  = ord:Supplier
        GET(SupplierQueue, +sq:CompanyName)
        IF ERRORCODE()
            ! Not in queue - ADD
            DO LoadSUPPLIER
            IF Result
                sq:CompanyName        = ord:Supplier
                sq:NormalSupplyPeriod = sup:Normal_Supply_Period
            ELSE
                sq:CompanyName        = ord:Supplier
                sq:NormalSupplyPeriod = 0 ! FORCE unfound supplier to be more visible
            END ! IF

            ADD(SupplierQueue, +sq:CompanyName)

            EXIT ! 
        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
LoadSUPPLIER                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Result = False

        Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
        sup:Company_Name = LOC:CompanyName
        SET(sup:Company_Name_Key, sup:Company_Name_Key)

        IF Access:SUPPLIER.NEXT()
            EXIT
        END !IF

        IF sup:Company_Name <> LOC:CompanyName
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncJOBNOTES                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Synchronise JOBNOTES (Notes Attached To Jobs) to current JOBS file
        !-----------------------------------------------
        Result = False

        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        SET(jbn:RefNumberKey, jbn:RefNumberKey)

        IF Access:JOBSE.NEXT()
            EXIT
        END !IF

        IF jbn:RefNumber <> job:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncJOBSE                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Synchronise JOBSE (Jobs Extension) to current JOBS file
        !-----------------------------------------------
        Result = False

        Access:JOBSE.ClearKey(jobe:RecordNumberKey)
        jobe:RefNumber = job:Ref_Number
        SET(jobe:RecordNumberKey, jobe:RecordNumberKey)

        IF Access:JOBSE.NEXT()
            EXIT
        END !IF

        IF jobe:RefNumber <> job:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadJOBS                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Result = False

        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = LOC:JobNumber
        SET(job:Ref_Number_Key, job:Ref_Number_Key)

        IF Access:JOBS.NEXT()
            EXIT
        END !IF

        IF job:Ref_Number <> LOC:JobNumber
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        IF Access:USERS.NEXT()
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            recordspercycle      = 25
            recordsprocessed     = 0
            recordstoprocess     = 10 !***The Number Of Records, or at least a guess***
            percentprogress      = 0
            progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)
        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT
ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        Result            = False

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed = True
            CLOSE(ProgressWindow)
        End!If tmp:cancel = 1
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT
ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT
ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted
ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False  ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A10").Select'}
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("A11"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A11").Select'}
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT

XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT
XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        OperatingSystem     = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP

        excel:OperatingSystem = 0.0
        !-----------------------------------------------
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020725'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OrderStatusReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBBATCH.Open
  Relate:JOBS.Open
  Relate:TRADEACC_ALIAS.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRDPARTY.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:ORDERS.UseFile
  Access:ORDPEND.UseFile
  Access:ORDPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName = 'Order Status Report' !               Job=1207        Cust=
  
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
      LOC:DayLeawayCount = 90
  
      excel:Visible      = False
  
      DO GetUserName
      IF GUIMode = 1 THEN
         MainWindow{PROP:ICONIZE} = TRUE
         LocalTimeOut = 500
         DoAll = 'Y'
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,tra:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,tra:Account_Number,1,BRW1)
  BRW1.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(LocalTag,BRW1.Q.LocalTag)
  BRW1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  BRW1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)
  BRW1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:JOBBATCH.Close
    Relate:JOBS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
          SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)
          LOOP
            IF Access:TRADEACC_ALIAS.NEXT() THEN BREAK.
            IF tra_ali:Account_Number <> LocalHeadAccount THEN
               IF tra_ali:RemoteRepairCentre = 0 THEN CYCLE.
            END !IF
            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.
            IF DoAll <> 'Y' THEN
               glo:Queue2.Pointer2 = tra_ali:RecordNumber
               GET(glo:Queue2,glo:Queue2.Pointer2)
               IF ERROR() THEN CYCLE.
            END !IF
            LOC:FileName = ''
            DO ExportSetup
            IF LOC:FileName = '' THEN CYCLE.
            DO ExportBody
            DO ExportFinalize
          END !LOOP
          DO ExportFinalize
          IF NOT CancelPressed
              POST(Event:CloseWindow)
          END !IF
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020725'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020725'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020725'&'0')
      ***
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW1) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue




   MEMBER('vodr0085.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


VettingTATReport PROCEDURE                            !Generated from procedure template - Window

StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
tmp:VersionNumber    STRING(30)
tmp:ExcelFileName    CSTRING(255)
tmp:Desktop          CSTRING(255)
tmp:AllManufacturers BYTE(1)
tmp:Manufacturer     STRING(30)
tmp:AccountNumber    STRING(30)
tmp:DateType         BYTE(0)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:LineNumber       LONG(8)
tmp:CountJobs        LONG
tmp:AllAccounts      BYTE(0)
tmp:WarrantyType     BYTE(0)
Progress:Thermometer BYTE
window               WINDOW('Vetting TAT Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       GROUP('Manufacturer'),AT(216,138,252,42),USE(?Group:Manufacturer),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         CHECK('All Manufacturers'),AT(224,148),USE(tmp:AllManufacturers),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                         PROMPT('Select Manufacturer'),AT(224,162),USE(?tmp:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(312,162,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),REQ,UPR
                         BUTTON,AT(439,158),USE(?LookupManufacturer),TRN,FLAT,ICON('lookupp.jpg')
                       END
                       GROUP('RRC Account Number'),AT(216,180,252,42),USE(?Group:RRC),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         CHECK('All Accounts'),AT(224,190),USE(tmp:AllAccounts),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                         PROMPT('Account Number'),AT(224,204),USE(?tmp:AccountNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(312,204,124,10),USE(tmp:AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Account Number'),TIP('Account Number'),REQ,UPR
                         BUTTON,AT(439,200),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       END
                       OPTION('Warranty Type'),AT(216,222,252,24),USE(tmp:WarrantyType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('First Year'),AT(224,232),USE(?tmp:WarrantyType:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('2nd Year'),AT(318,232),USE(?tmp:WarrantyType:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                         RADIO('Both'),AT(412,232),USE(?tmp:WarrantyType:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                       END
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(168,88,344,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(172,104,300,),USE(SRN:TipText),TRN
                       PANEL,AT(164,132,352,198),USE(?Panel55),FILL(09A6A7CH)
                       OPTION('Date Type'),AT(216,246,252,24),USE(tmp:DateType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Date Type'),TIP('Date Type')
                         RADIO('<< 48 Hrs'),AT(224,257),USE(?tmp:DateType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('<< 72 Hrs'),AT(318,257),USE(?tmp:DateType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('>72 Hrs'),AT(412,257),USE(?tmp:DateType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                       END
                       GROUP('Completed Date Range'),AT(215,272,252,53),USE(?Group:DateRange),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Start Date'),AT(224,285),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(312,285,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                         BUTTON,AT(379,282),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('End Date'),AT(224,306),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(312,306,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),REQ,UPR
                         BUTTON,AT(380,301),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                       END
                       BUTTON,AT(476,92,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Vetting Report'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Verison:'),AT(168,342),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(372,332),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

local       Class
UpdateProgressWindow            Procedure(String func:Text)
            End ! local       Class
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:AccountNumber                Like(tmp:AccountNumber)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Reporting       Routine
Data
local:ReportStartDate       Date()
local:ReportStartTime       Time()
local:Desktop               Cstring(255)
excel:ProgramName           Cstring(255)
excel:Filename              Cstring(255)

Code
    ! Create Folder In My Documents
    excel:ProgramName = 'Vetting TAT Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)


    ! ____________________________________________________________________

    !Open Program Window 
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'

    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    If E1.Init(0, 0) = 0
        Case Missive('An error has occurred creating the excel document.'&|
          '|Please try again.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Exit
    End ! If E1.Init(0,0,1) = 0

    E1.NewWorkBook()
    E1.SaveAs(excel:Filename)
    E1.CloseWorkBook(2)

    E1.OpenWorkbook(excel:Filename)
    E1.RenameWorksheet('Vetting TAT Report')
    E1.WriteToCell('Vetting TAT Report','A1')
    E1.WriteToCell('Manufacturer','A2')
    If tmp:AllManufacturers = True
        E1.WriteToCell('All','B2')
    Else ! If tmp:AllManufacturers = True
        E1.WriteToCell(tmp:Manufacturer,'B2')
    End ! If tmp:AllManufacturers = True
    E1.WriteToCell('Account','A3')
    E1.WriteToCell(tmp:AccountNumber,'B3')
    E1.WriteToCell('Date Type','A4')
    Case tmp:DateType
    Of 0
        E1.WriteToCell('< 48 Hrs','B4')
    Of 1
        E1.WriteToCell('< 72 Hrs','B4')
    Of 2
        E1.WriteToCell('> 72 Hrs','B4')
    End ! Case tmp:DateType
    E1.WriteToCell('Start Date','A5')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B5')
    E1.WriteToCell('End Date','A6')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B6')
    E1.WriteToCell(tmp:VersionNumber,'D2')

    E1.WriteToCell('Job Number','A8')
    E1.WriteToCell('RRC ID','B8')
    E1.WriteToCell('RRC Job No','C8')
    E1.WriteToCell('Repaired By','D8')
    E1.WriteToCell('Account Number','E8')
    E1.WriteToCell('Completed Date','F8')
    E1.WriteToCell('Model Number','G8')
    E1.WriteToCell('IMEI Number','H8')
    E1.WriteToCell('Charge Type','I8')
    E1.WriteToCell('Repair Type','J8')
    E1.WriteToCell('Exchange Cost','K8')
    E1.WriteToCell('Labour Cost','L8')
    E1.WriteToCell('Parts Cost','M8')
    E1.WriteToCell('Total Cost','N8')

    tmp:CountJobs = 0
    If tmp:AllManufacturers = False
        local.UpdateProgressWindow('')
        local.UpdateProgressWindow('Checking Manufacturer: ' & Clip(tmp:Manufacturer))

        Access:JOBS.Clearkey(job:EDI_Key)
        job:Manufacturer = tmp:Manufacturer
        job:EDI = 'NO'
        Set(job:EDI_Key,job:EDI_Key)
        Loop ! Begin JOBS Loop
            If Access:JOBS.Next()
                Break
            End ! If !Access
            If job:Manufacturer <> tmp:Manufacturer
                Break
            End ! If
            If job:EDI <> 'NO'
                Break
            End ! If
            !Inside Loop
            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel
            Do WriteLine
        End ! End JOBS Loop
        local.UpdateProgressWindow('Jobs Found: ' & tmp:CountJobs)
    Else ! If tmp:AllManufacturers = False
        Set(man:Manufacturer_Key)
        Loop
            If Access:MANUFACT.NEXT()
               Break
            End !If
            local.UpdateProgressWindow('')
            local.UpdateProgressWindow('Checking Manufacturer: ' & Clip(man:Manufacturer))

            tmp:Cancel = 0
            tmp:CountJobs = 0
            Access:JOBS.Clearkey(job:EDI_Key)
            job:Manufacturer = man:Manufacturer
            job:EDI = 'NO'
            Set(job:EDI_Key,job:EDI_Key)
            Loop ! Begin JOBS Loop
                If Access:JOBS.Next()
                    Break
                End ! If !Access
                If job:Manufacturer <> man:Manufacturer
                    Break
                End ! If
                If job:EDI <> 'NO'
                    Break
                End ! If
                !Inside Loop
                Do GetNextRecord2
                Do CancelCheck
                If tmp:Cancel
                    Break
                End !If tmp:Cancel
                Do WriteLine
            End ! End JOBS Loop
            local.UpdateProgressWindow('Jobs Found: ' & tmp:CountJobs)
            If tmp:Cancel
                Break
            End ! If tmp:Cancel
        End !Loop
    End ! If tmp:AllManufacturers = False

    E1.SetCellFontName('Tahoma','A1','N' & tmp:LineNumber + 1)
    E1.SetCellFontSize('12','A1')
    E1.SetCellFontStyle('Bold','A1','A6')
    E1.SetCellFontStyle('Bold','A8','N8')
    E1.SetCellBackgroundColor(color:silver,'A1','D6')
    E1.SetCellBackgroundColor(color:silver,'A8','N8')
    E1.SetCellFontSize('8''A2','N' & tmp:LineNumber + 1)
    E1.SetCellBorders('A1','D6',oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders('A1','D6',oix:BorderEdgeRight,oix:LineStyleContinuous)
    E1.SetCellBorders('A1','D6',oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders('A1','D6',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders('A8','N8',oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders('A8','N8',oix:BorderEdgeRight,oix:LineStyleContinuous)
    E1.SetCellBorders('A8','N8',oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders('A8','N8',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetColumnWidth('A','N')
    E1.SelectCells('B9')
    E1.FreezePanes()


    E1.Save
    E1.CloseWorkBook(2)



    ! _____________________________________________________________________

    !Finish

    E1.Kill()

    If tmp:Cancel = 0 Or tmp:Cancel = 2
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))
    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    Post(Event:CloseWindow)
WriteLine       Routine
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
        Exit
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Found

    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Error
        Exit
    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
        Exit
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
        ! Error
        Exit
    End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign

    ! Inserting (DBH 03/04/2007) # 8883 - Add "All Accounts" option
    If tmp:AllAccounts = 0
    ! End (DBH 03/04/2007) #8883
        If wob:HeadAccountNumber <> tmp:AccountNumber
            Exit
        End ! If wob:HeadAccountNumber <> tmp:AccountNumber
    End ! If tmp:AllAccounts = 0

    If job:Date_Completed < tmp:StartDate Or job:Date_Completed > tmp:EndDate
        Exit
    End ! If job:DateCompleted < tmp:StartDate Or job:Date_Completed > tmp:EndDate

    If jobe2:InPendingDate = ''
        Exit
    End ! If jobe2:InPendingDate = ''

    ! Inserting (DBH 03/04/2007) # 8832 - Add 1st/2nd Year Warranty Filter
    If tmp:WarrantyType > 0
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty = 'YES'
        cha:Charge_Type = job:Warranty_Charge_Type
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If tmp:WarrantyType = 1
                If cha:SecondYearWarranty = 1
                    Exit
                End ! If cha:SecondYearWarranty = 1
            End ! If tmp:WarrantyType = 1
            If tmp:WarrantyType = 2
                If cha:SecondYearWarranty <> 1
                    Exit
                End ! If cha:SecondYearWarranty <> 1
            End ! If tmp:WarrantyType = 2
        Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
        End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
    End ! If tmp:WarrantyType > 0
    ! End (DBH 03/04/2007) #8832


    Case tmp:DateType
    Of 0 ! < 48
        If jobe2:InPendingDate >= Today() - 1

        Else ! If jobe2:InPendingDate >= Today() - 1
            Exit
        End ! If jobe2:InPendingDate >= Today() - 1
    Of 1 ! < 72
        If jobe2:InPendingDate >= Today() - 2 And jobe2:InPendingDate < Today() - 1

        Else ! If jobe2:InPendingDate > Today() - 3 And jobe2:InPendingDate < Today - 2
            Exit
        End ! If jobe2:InPendingDate > Today() - 3 And jobe2:InPendingDate < Today - 2
    Of 2 ! > 72
        If jobe2:InPendingDate < Today() - 2

        Else ! If jobe2:InPendingDate < Today() - 3
            Exit
        End ! If jobe2:InPendingDate < Today() - 3
    End ! Case tmp:DateType

    tmp:LineNumber += 1
    !Job Number
    E1.WriteToCell(job:Ref_Number           ,'A' & tmp:LineNumber)
    !RRC ID
    E1.WriteToCell(tra:BranchIdentification ,'B' & tmp:LineNumber)
    !RRC Job Number
    E1.WriteToCell(wob:JobNumber            ,'C' & tmp:LineNumber)
    !Repaired By
    If SentToHub(job:Ref_Number)
        E1.WriteToCell('ARC'                ,'D' & tmp:LineNumber)
    Else ! If SentToHub(job:Ref_Number)
        E1.WriteToCell('RRC'                ,'D' & tmp:LineNumber)
    End ! If SentToHub(job:Ref_Number)
    !Account Number
    E1.WriteToCell(job:Account_Number       ,'E' & tmp:LineNumber)
    !Completed Date
    E1.WriteToCell(Format(job:Date_Completed,@d06)  ,'F' & tmp:LineNumber)
    !Model Number
    E1.WriteToCell(job:Model_Number         ,'G' & tmp:LineNumber)
    !IMEI Number
    E1.WriteToCell('''' & job:ESN                  ,'H' & tmp:LineNumber)
    !Charge Type
    E1.WriteToCell(job:Warranty_Charge_Type ,'I' & tmp:LineNumber)
    !Repair Type
    E1.WriteToCell(job:Repair_Type_Warranty ,'J' & tmp:LineNumber)
    !Exchange
    If jobe:ConfirmClaimAdjustment
        E1.WriteToCell(Format(jobe:ExchangeAdjustment,@n_14.2)  ,'K' & tmp:LineNumber)
        E1.WriteToCell(Format(jobe:LabourAdjustment,@n_14.2)    ,'L' & tmp:LineNumber)
        E1.WriteToCell(Format(jobe:PartsAdjustment,@n_14.2)     ,'M' & tmp:LineNumber)
        E1.WriteToCell(Format(jobe:SubTotalAdjustment,@n_14.2)  ,'N' & tmp:LineNumber)
    Else ! If jobe:ConfirmClaimAdjustment
        E1.WriteToCell(Format(job:Courier_Cost_Warranty,@n_14.2)  ,'K' & tmp:LineNumber)
        E1.WriteToCell(Format(jobe:ClaimValue,@n_14.2)    ,'L' & tmp:LineNumber)
        E1.WriteToCell(Format(jobe:ClaimPartsCost,@n_14.2)     ,'M' & tmp:LineNumber)
        E1.WriteToCell(Format(jobe:ClaimValue + jobe:ClaimPartsCost + job:Courier_Cost_Warranty,@n_14.2)  ,'N' & tmp:LineNumber)
    End ! If jobe:ConfirmClaimAdjustment

    E1.SetCellNumberFormat(oix:NumberFormatCurrency,oix:CurrencySymbolSARand,2,,'K' & tmp:LineNumber,'N' & tmp:LineNumber)

    tmp:CountJobs += 1
getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020646'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('VettingTATReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:AllManufacturers
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:WEBJOB.Open
  Access:MANUFACT.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:JOBSE2.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  !============== Version Number =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = Clip(tmp:VersionNumber) & '5002'
  ?ReportVersion{prop:Text} = 'Report Version: ' & Clip(tmp:VersionNumber)
  tmp:StartDate = Today()
  tmp:EndDate = Today()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?LookupManufacturer{Prop:Tip}
     ?LookupManufacturer{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?LookupManufacturer{Prop:Msg}
     ?LookupManufacturer{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:AccountNumber{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:AccountNumber{Prop:Tip}
  END
  IF ?tmp:AccountNumber{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:AccountNumber{Prop:Msg}
  END
  IF ?tmp:AllManufacturers{Prop:Checked} = True
    DISABLE(?tmp:Manufacturer)
    DISABLE(?LookupManufacturer)
  END
  IF ?tmp:AllManufacturers{Prop:Checked} = False
    ENABLE(?LookupManufacturer)
    ENABLE(?tmp:Manufacturer)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?tmp:AccountNumber)
    DISABLE(?CallLookup)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?tmp:AccountNumber)
    ENABLE(?CallLookup)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      PickRRCAccounts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:AllManufacturers
      IF ?tmp:AllManufacturers{Prop:Checked} = True
        DISABLE(?tmp:Manufacturer)
        DISABLE(?LookupManufacturer)
      END
      IF ?tmp:AllManufacturers{Prop:Checked} = False
        ENABLE(?LookupManufacturer)
        ENABLE(?tmp:Manufacturer)
      END
      ThisWindow.Reset
    OF ?tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupManufacturer
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?tmp:AccountNumber)
        DISABLE(?CallLookup)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?tmp:AccountNumber)
        ENABLE(?CallLookup)
      END
      ThisWindow.Reset
    OF ?tmp:AccountNumber
      IF tmp:AccountNumber OR ?tmp:AccountNumber{Prop:Req}
        tra:Account_Number = tmp:AccountNumber
        !Save Lookup Field Incase Of error
        look:tmp:AccountNumber        = tmp:AccountNumber
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:AccountNumber = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:AccountNumber = look:tmp:AccountNumber
            SELECT(?tmp:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      tra:Account_Number = tmp:AccountNumber
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:AccountNumber = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:AccountNumber)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020646'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020646'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020646'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      If tmp:AccountNumber = '' And tmp:AllAccounts = 0
          Case Missive('You must select an Account Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:AccountNumber)
          Cycle
      End ! If tmp:AccountNumber = ''
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

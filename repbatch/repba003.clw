

   MEMBER('repbatch.clw')                                  ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('REPBA003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('REPBA002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('REPBA004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('REPBA005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('REPBA006.INC'),ONCE        !Req'd for module callout resolution
                     END


Startup PROCEDURE                                          ! Generated from procedure template - Window

! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
local       CLASS                                 !
PORMessage      Procedure(Byte  func:Type)            !
            END
save_grnotes_id      USHORT                                !
RepositoryDir        CSTRING(255)                          !
seq                  LONG                                  !
tmp:SavedOrderNumber LONG                                  !Order Number
tmp:GRNSuffix        BYTE(0)                               !GRN Suffix
tmp:CompanyName      STRING(30)                            !Third Party Company Name
tmp:PurchaseOrderNumber STRING(20)                         !Purchase Order Number
PurchaseOrderQueue   QUEUE,PRE(purque)                     !
PurchaseOrderNumber  LONG                                  !Purchase Order Number
Cost                 REAL                                  !Cost
                     END                                   !
ResubmitJobQueue     QUEUE,PRE(resque)                     !
RecordNumber         LONG                                  !Record Number
PurchaseOrderNumber  LONG                                  !Purchase Order Number
CompanyName          STRING(30)                            !Company Name
Cost                 REAL                                  !Cost
                     END                                   !
NewJobQueue          QUEUE,PRE(newque)                     !
RecordNumber         LONG                                  !Record Number
CompanyName          STRING(30)                            !Company Name
Cost                 REAL                                  !Cost
                     END                                   !
tmp:TestRecord       STRING(255),STATIC                    !
Window               WINDOW('Caption'),AT(,,260,100),GRAY
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
TestRecord    File,Driver('ASCII'),Pre(test),Name(tmp:TestRecord),Create,Bindable,Thread
Record                  Record
TestLine                String(1)
                        End
                    End
XmlData group,type
ServiceCode string(3)
REC         group
SB_PO_NO          string(10)
ORACLE_PO_NO      string(10)
URL               string(256)
TYPE              string(3)
            end
POR         group,over(REC)
CURRENCY_CODE       string(3)
CURRENCY_UNIT_PRICE string(19)
DELIV_TO_REQ        string(10)
DEST_ORG_ID         string(20)
GL_DATE             string(10)
INTERFACE_SRC       string(20)
ITEM_ID             string(20)  ! Length ?????
ORG_ID              string(2)
PREPARER_ID         string(10)
QTY                 string(10)
SUPPLIER_NUMBER     string(15)
SB_PO_NUMBER        string(13)
TYPE                string(3)
            end
        end

    map
SendXML         procedure(const *XmlData aXmlData, string aRepositoryDir)
    end

XmlValues GROUP(XmlData)
           END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Startup')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:GRNOTESAlias.Open                                 ! File GRNOTESAlias used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Relate:TRDBATCH_ALIAS.Open                               ! File TRDBATCH_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:TRDPONO.Open                                      ! File TRDPONO used by this procedure, so make sure it's RelationManager is open
  Access:TRDBATCH.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ORDERS.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ORDPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUPPLIER.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  !before doing any export - make sure the sales are up to date - create sales from ARC requests
      0{Prop:Hide} = True
      !message('This is a truncated version - it only does the create sales from ARC part')
      CreateOrdersFromARCRequest
      !halt()
  
      glo:PDFExportPath = Clip(GETINI('PDF','ExportPath',,Clip(Path()) & '\SB2KDEF.INI')) & '\PARTS ORDERS ' & Format(Today(),@d12) & '.PDF'
  
      ! Start - Create a small test file in PDF folder - TrkBs: 5110 (DBH: 30-08-2005)
      tmp:TestRecord = Clip(GETINI('PDF','ExportPath',,Clip(Path()) & '\SB2KDEF.INI'))
  
      If Sub(Clip(tmp:TestRecord),-1,1) = '\'
          tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
      Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
          tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
      End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
  
      Remove(TestRecord)
      Create(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      Open(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      test:TestLine = '1'
      Add(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      Close(TestRecord)
      Remove(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      ! End   - Create a small test file in PDF folder - TrkBs: 5110 (DBH: 30-08-2005)
  
      ! Start - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
      tmp:TestRecord = Clip(GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI'))
  
      If Sub(Clip(tmp:TestRecord),-1,1) = '\'
          tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
      Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
          tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
      End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
  
      Remove(TestRecord)
      Create(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      Open(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      test:TestLine = '1'
      Add(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      Close(TestRecord)
      Remove(TestRecord)
      If Error()
          Return Level:Fatal
      End ! If Error()
      ! End   - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
  
      !Parts Order _________________________________-___________________________________
      PUTINI('Parts Orders','Start Date',Format(Today(),@d17),Clip(Path()) & '\REPBATCHLOG.INI')
      PUTINI('Parts Orders','Start Time',Format(Clock(),@t1),Clip(Path()) & '\REPBATCHLOG.INI')
  
  
  
  PartsOrder()
      ! GRN  _________________________________________-___________________________
  
      PUTINI('Goods Received Note', 'Start Date', Format(Today(), @d17), Clip(Path()) & '\REPBATCHLOG.INI')
      PUTINI('Goods Received Note', 'Start Time', Format(Clock(), @t1), Clip(Path()) & '\REPBATCHLOG.INI')
  
      Access:GRNOTESAlias.ClearKey(grnali:NotPrintedOrderKey)
      grnali:BatchRunNotPrinted    = True
      grnali:Order_Number          = 0
      Set(grnali:NotPrintedOrderKey,grnali:NotPrintedOrderKey)
      Loop
          If Access:GRNOTESAlias.NEXT()
             Break
          End !If
          If grnali:BatchRunNotPrinted    <> True
              Break
          End ! If grnali:BatchRunNotPrinted    <> True
  
          purque:PurchaseOrderNumber = grnali:Order_Number
          Get(PurchaseOrderQueue,purque:PurchaseOrderNumber)
          If Error()
              purque:PurchaseOrderNumber = grnali:Order_Number
              Add(PurchaseOrderQueue)
          End ! If Error()
      End !Loop
  
      Loop x# = 1 To Records(PurchaseOrderQueue)
          Get(PurchaseOrderQueue,x#)
  
          tmp:GRNSuffix = 0
          Access:GRNOTESAlias.ClearKey(grnali:Order_Number_Key)
          grnali:Order_Number        = purque:PurchaseOrderNumber
          grnali:Goods_Received_Date = 0
          Set(grnali:Order_Number_Key,grnali:Order_Number_Key)
          Loop
              If Access:GRNOTESAlias.NEXT()
                 Break
              End !If
              If grnali:Order_Number   <> purque:PurchaseOrderNumber      |
                  Then Break.  ! End If#
  
              !Does this GRN Have Parts Attached?
              PartsAttached# = False
              FoundValue# = False
              Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
              orp:Order_Number = grnali:Order_Number
              Set(orp:Part_Number_Key,orp:Part_Number_Key)
              Loop
                  If Access:ORDPARTS.NEXT()
                     Break
                  End !If
                  If orp:Order_Number <> grnali:Order_Number      |
                      Then Break.  ! End If
                  If orp:GRN_Number = grnali:Goods_Received_Number
                      PartsAttached# = True
                  Else
                      Cycle
                  End ! If orp:GRN_Number = grnali:Goods_Received_Number
  
                  If orp:Date_Received = ''
                      Cycle
                  End ! If orp:Date_Received = ''
  
                  ! Only include if the batch will have a value - TrkBs: 6264 (DBH: 07-09-2005)
                  If orp:Purchase_Cost * orp:Number_Received > 0
                      FoundValue# = True
                  End ! If orp:Purchase_Cost * orp:Quantity > 0
              End !Loop
  
              If PartsAttached# = False
                  Cycle
              End ! If PartsAttached# = False
  
              ! Count the GRN even if it's zero value. - TrkBs: 6697 (DBH: 11-11-2005)
  
              tmp:GRNSuffix += 1
  
              If FoundValue# = False
                  Cycle
              End ! If PartsAttached# = False
  
              If grnali:BatchRunNotPrinted = True
                  glo:PDFExportPath = Clip(GETINI('PDF', 'ExportPath',, Clip(Path()) & '\SB2KDEF.INI')) & '\SS' & Format(grnali:Order_Number, @n08) & '-' & Format(tmp:GRNSuffix, @n02) & '.PDF'
                  tmp:PurchaseOrderNumber = 'SS' & Format(grnali:Order_Number, @n08) & '/' & Format(tmp:GRNSuffix, @n02)
                  GoodReceivedNote(grnali:Goods_Received_Number,tmp:GRNSuffix)
                  ! Inserting (DBH 15/03/2006) #7357 - Only create XML if PDF exists
                  If Exists(Clip(glo:PDFExportPath))
                  ! End (DBH 15/03/2006) #7357
                      local.PORMessage(0) ! Stock POR
                  End ! If Exists(Clip(glo:PDFExportPath))
              End ! If grnali:BatchRunNotPrinted = True
          End !Loop
      End ! Loop x# = 1 To Records(PurchaseOrderQueue)
  ! this is completely commented_____________________________________________________________________
  !    !Third Party Receipt _____________________________________________________________________
  !    PUTINI('Third Party Receipt','Start Date',Format(Today(),@d17),Clip(Path()) & '\REPBATCHLOG.INI')
  !    PUTINI('Third Party Receipt','Start Time',Format(Clock(),@t1),Clip(Path()) & '\REPBATCHLOG.INI')
  !
  !    Clear(PurchaseOrderQueue)
  !    Free(PurchaseOrderQueue)
  !
  !    tmp:CompanyName = ''
  !    Access:TRDBATCH_ALIAS.ClearKey(trb_ali:NotPrintedManJobKey)
  !    trb_ali:BatchRunNotPrinted = True
  !    trb_ali:Status             = 'IN'
  !    Set(trb_ali:NotPrintedManJobKey,trb_ali:NotPrintedManJobKey)
  !    Loop
  !        If Access:TRDBATCH_ALIAS.NEXT()
  !           Break
  !        End !If
  !        If trb_ali:BatchRunNotPrinted <> True      |
  !        Or trb_ali:Status             <> 'IN'      |
  !            Then Break.  ! End If
  !
  !        Update# = False
  !        If trb_ali:Company_Name <> tmp:CompanyName
  !            ! New company. Create new purchase order and save it - TrkBs: 5110 (DBH: 02-08-2005)
  !            If trb_ali:PurchaseOrderNumber > 0
  !                ! This entry already has a purchase order, so let's assume it's a resubmit - TrkBs: 5110 (DBH: 08-08-2005)
  !                tmp:PurchaseOrderNumber = trb_ali:PurchaseOrderNumber
  !                Update# = True
  !            Else ! If trb_ali:PurchaseOrderNumber > 0
  !                If Access:TRDPONO.PrimeRecord() = Level:Benign
  !                    tmp:PurchaseOrderNumber = tro:RecordNumber
  !                    tro:CompanyName = trb_ali:Company_Name
  !                    If Access:TRDPONO.TryInsert() = Level:Benign
  !                        ! Insert Successful
  !                        Update# = True
  !                    Else ! If Access:TRPOBO.TryInsert() = Level:Benign
  !                        ! Insert Failed
  !                        Access:TRDPONO.CancelAutoInc()
  !                    End ! If Access:TRPOBO.TryInsert() = Level:Benign
  !                End !If Access:TRPOBO.PrimeRecord() = Level:Benign
  !            End ! If trb_ali:PurchaseOrderNumber > 0
  !            tmp:CompanyName = trb_ali:Company_Name
  !
  !        Else ! If trb_ali:Company_Name <> tmp:CompanyName
  !            Update# = True
  !            ! Same company. Use existing purchase order number - TrkBs: 5110 (DBH: 02-08-2005)
  !        End ! If trb_ali:Company_Name <> tmp:CompanyName
  !
  !        Access:TRDBATCH.Clearkey(trb:RecordNumberKey)
  !        trb:RecordNumber    = trb_ali:RecordNumber
  !        If Access:TRDBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
  !            ! Found
  !            trb:PurchaseOrderNumber = tmp:PurchaseOrderNumber
  !            If Access:TRDBATCH.TryUpdate() = Level:Benign
  !                Access:JOBS.Clearkey(job:Ref_Number_Key)
  !                job:Ref_Number  = trb:Ref_Number
  !                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  !                    ! Found
  !                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
  !                    jobe:RefNumber  = job:Ref_Number
  !                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  !                        ! Found
  !                        purque:PurchaseOrderNumber = tmp:PurchaseOrderNumber
  !                        Get(PurchaseOrderQueue,purque:PurchaseOrderNumber)
  !                        If Error()
  !                            purque:PurchaseOrderNumber = tmp:PurchaseOrderNumber
  !                            ! Changing (DBH 23/02/2006) #7247 - Use the TRDBATCH cost. THe job cost may change
  !                            ! purque:Cost = jobe:ARC3rdPartyCost
  !                            ! to (DBH 23/02/2006) #7247
  !                            purque:Cost = trb:ThirdPartyInvoiceCharge
  !                            ! End (DBH 23/02/2006) #7247
  !                            Add(PurchaseOrderQueue)
  !                        Else ! If Error()
  !                            ! Changing (DBH 23/02/2006) #7247 - Use the TRDBATCH cost. The job cost may change
  !                            ! purque:Cost += jobe:ARC3rdPartyCost
  !                            ! to (DBH 23/02/2006) #7247
  !                            purque:Cost += trb:ThirdPartyInvoiceCharge
  !                            ! End (DBH 23/02/2006) #7247
  !                            Put(PurchaseOrderQueue)
  !                        End ! If Error()
  !                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  !                        ! Error
  !                    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  !                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  !                    ! Error
  !                End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  !
  !            Else ! If Access:TRDBATCH.TryUpdate() = Level:Benign
  !            End ! If Access:TRDBATCH.TryUpdate() = Level:Benign
  !        Else ! If Access:TRDBATCH.Tryfetch(trd:RecordNumberKey) = Level:Benign
  !            ! Error
  !        End ! If Access:TRDBATCH.Tryfetch(trd:RecordNumberKey) = Level:Benign
  !    End !Loop
  !
  !    ! Create a PDF for each new purchase order number - TrkBs: 5110 (DBH: 02-08-2005)
  !    Loop x# = 1 To Records(PurchaseOrderQueue)
  !        Get(PurchaseOrderQueue,x#)
  !        ! Don't export a zero valued batch - TrkBs: 6264 (DBH: 07-09-2005)
  !        If purque:Cost = 0
  !            Cycle
  !        End ! If purque:Cost = 0
  !
  !        glo:PDFExportPath = Clip(GETINI('PDF','ExportPath',,Clip(Path()) & '\SB2KDEF.INI')) & '\SS3' & Format(purque:PurchaseOrderNumber,@n07) & '-01' & '.PDF'
  !
  !        ThirdPartyReceiptReport(purque:PurchaseOrderNumber)
  !        tmp:PurchaseOrderNumber = 'SS3' & Format(purque:PurchaseOrderNumber,@n07) & '/01'
  !        ! Inserting (DBH 15/03/2006) #7357 - Only create XML if PDF exists
  !        If Exists(Clip(glo:PDFExportPath))
  !        ! End (DBH 15/03/2006) #7357
  !            local.PORMessage(1)
  !        End ! If Exists(Clip(glo:PDFExportPath))
  !
  !    End ! Loop x# = 1 To Records(PurchaseOrderQueue)
      !Third Party Receipt _____________________________________________________________________
      PUTINI('Third Party Receipt','Start Date',Format(Today(),@d17),Clip(Path()) & '\REPBATCHLOG.INI')
      PUTINI('Third Party Receipt','Start Time',Format(Clock(),@t1),Clip(Path()) & '\REPBATCHLOG.INI')
  
      Clear(PurchaseOrderQueue)
      Free(PurchaseOrderQueue)
      Free(ResubmitJobQueue)
      Free(NewJobQueue)
  
      Access:TRDBATCH.Clearkey(trb:NotPrintedManJobKey)
      trb:BatchRunNotPrinted = True
      trb:Status = 'IN'
      Set(trb:NotPrintedManJobKey,trb:NotPrintedManJobKey)
      Loop ! Begin TRDBATCH Loop
          If Access:TRDBATCH.Next()
              Break
          End ! If !Access
          If trb:BatchRunNotPrinted <> True
              Break
          End ! If
          If trb:Status <> 'IN'
              Break
          End ! If
          If trb:PurchaseOrderNumber > 0
              ! Already has a purchase order, so this must be a resubmission (DBH: 23-03-2006)
              resque:RecordNumber   = trb:RecordNumber
              resque:PurchaseOrderNumber = trb:PurchaseOrderNumber
              resque:CompanyName    = trb:Company_Name
              resque:Cost         = trb:ThirdPartyInvoiceCharge
              Add(ResubmitJobQueue)
          Else ! If trb:PurchaseOrderNumber > 0
              ! No purchase order: new job (DBH: 23-03-2006)
              newque:RecordNumber = trb:RecordNumber
              newque:CompanyName = trb:Company_Name
              newque:Cost = trb:ThirdPartyInvoiceCharge
              Add(NewJobQueue)
          End ! If trb:PurchaseOrderNumber > 0
      End ! End TRDBATCH Loop
  
      tmp:PurchaseOrderNumber = 0
      Sort(ResubmitJobQueue,resque:PurchaseOrderNumber)
      Loop x# = 1 To Records(ResubmitJobQueue)
          Get(ResubmitJobQueue,x#)
          If tmp:PurchaseOrderNumber <> resque:PurchaseOrderNumber
              ! First record, or new Purchase Order Number (DBH: 23-03-2006)
              tmp:PurchaseOrderNumber = resque:PurchaseOrderNumber
              purque:PurchaseOrderNumber = resque:PurchaseOrderNumber
              purque:Cost = resque:Cost
              Add(PurchaseOrderQueue)
              ! Add record to XML queue (DBH: 23-03-2006)
              Cycle
          Else ! If PurchaseOrderNumber# <> resque:PurchaseOrderNumber
              ! Still on the same Purchase Order Number (DBH: 23-03-2006)
              purque:PurchaseORderNumber = resque:PurchaseOrderNumber
              Get(PurchaseORderQueue,purque:PurchaseOrderNumber)
              If ~Error()
                  purque:Cost += resque:Cost
                  Put(PurchaseOrderQueue)
                  ! Update XML Queue (DBH: 23-03-2006)
              End ! If ~Error()
          End ! If PurchaseOrderNumber# <> resque:PurchaseOrderNumber
      End ! Loop x# = 1 To Records(ResubmitJobQueue)
  
      tmp:PurchaseOrderNumber = 0
      tmp:CompanyName = ''
      Sort(NewJobQueue,newque:CompanyName)
      Loop x# = 1 To Records(NewJobQueue)
          Get(NewJobQueue,x#)
          If tmp:CompanyName <> newque:CompanyName
              ! First record, or new company (DBH: 23-03-2006)
              tmp:CompanyName = newque:CompanyName
              Access:TRDBATCH.Clearkey(trb:RecordNumberKey)
              trb:RecordNumber    = newque:RecordNumber
              If Access:TRDBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
                  ! Lookup TRDBATCH entry, and prime a new purchase order number (DBH: 23-03-2006)
                  If Access:TRDPONO.PrimeRecord() = Level:Benign
                      tro:CompanyName = newque:CompanyName
                      If Access:TRDPONO.TryInsert() = Level:Benign
                          ! Insert Successful
                          ! New Purchase Order. Write to batch entry, and save for future (DBH: 23-03-2006)
                          tmp:PurchaseOrderNumber = tro:RecordNumber
                          trb:PurchaseOrderNumber = tro:RecordNumber
                          If Access:TRDBATCH.Update() = Level:Benign
                              ! Update XML Queue (DBH: 23-03-2006)
                              purque:PurchaseOrderNumber = trb:PurchaseOrderNumber
                              purque:Cost = newque:Cost
                              Add(PurchaseOrderQueue)
                              Cycle
                          End ! If Access:TRDBATCH.Update() = Level:Benign
                      Else ! If Access:TRDPONO.TryInsert() = Level:Benign
                          ! Insert Failed
                          Access:TRDPONO.CancelAutoInc()
                      End ! If Access:TRDPONO.TryInsert() = Level:Benign
                  End !If Access:TRDPONO.PrimeRecord() = Level:Benign
              Else ! If Access:TRBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
  
              End ! If Access:TRBATCH.Tryfetch(trb:RecordNum
          Else ! If tmp:CompanyName <> newque:CompanyName
              If tmp:PurchaseOrderNumber = 0
                  Cycle
              End ! If tmp:PurchaseOrderNumber = 0
              ! Still looking at same company. Lookup record (DBH: 23-03-2006)
              Access:TRDBATCH.Clearkey(trb:RecordNumberKey)
              trb:RecordNumber    = newque:RecordNumber
              If Access:TRDBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
                  ! Found
                  ! Assign the saved new batch number, and update XML queue (DBH: 23-03-2006)
                  trb:PurchaseOrderNumber = tmp:PurchaseOrderNumber
                  If Access:TRDBATCH.Update() = Level:Benign
                      purque:PurchaseOrderNumber = trb:PurchaseOrderNumber
                      Get(PurchaseOrderQueue,purque:PurchaseOrderNumber)
                      If ~Error()
                          purque:Cost += newque:Cost
                          Put(PurchaseOrderQueue)
                          Cycle
                      End ! If ~Error()e
                  End ! If Access:TRDBATCH.Update = Level:Benign
              Else ! If Access:TRDBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
                  ! Error
              End ! If Access:TRDBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
          End ! If tmp:CompanyName <> newque:CompanyName
      End ! Loop x# = 1 To Records(NewJobQueue)
  
  
      ! Create a PDF for each new purchase order number - TrkBs: 5110 (DBH: 02-08-2005)
      Loop x# = 1 To Records(PurchaseOrderQueue)
          Get(PurchaseOrderQueue,x#)
          ! Don't export a zero valued batch - TrkBs: 6264 (DBH: 07-09-2005)
          If purque:Cost = 0
              Cycle
          End ! If purque:Cost = 0
  
          glo:PDFExportPath = Clip(GETINI('PDF','ExportPath',,Clip(Path()) & '\SB2KDEF.INI')) & '\SS3' & Format(purque:PurchaseOrderNumber,@n07) & '-01' & '.PDF'
  
          ThirdPartyReceiptReport(purque:PurchaseOrderNumber)
          tmp:PurchaseOrderNumber = 'SS3' & Format(purque:PurchaseOrderNumber,@n07) & '/01'
          ! Inserting (DBH 15/03/2006) #7357 - Only create XML message if PDF has been created
          If Exists(Clip(glo:PDFExportPath))
          ! End (DBH 15/03/2006) #7357
              local.PORMessage(1)
          End ! If Exists(Clip(glo:PDFExportPath))
      End ! Loop x# = 1 To Records(PurchaseOrderQueue)
      PUTINI('Batch Run Finished','Start Date',Format(Today(),@d17),Clip(Path()) & '\REPBATCHLOG.INI')
      PUTINI('Batch Run Finished','Start Time',Format(Clock(),@t1),Clip(Path()) & '\REPBATCHLOG.INI')
  
      !TB12777 - evo posting - done after purchase orders have been batched
      if Getini('EVO','CREATE',0 ,clip(path())&'\SB2KDEF.INI') = true then
          run('EVOBATCH.EXE',0)
      END !if evo is turned on
  
  
      Post(Event:CloseWindow)
  Do DefineListboxStyle
  INIMgr.Fetch('Startup',Window)                           ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:GRNOTESAlias.Close
    Relate:JOBS.Close
    Relate:TRDBATCH_ALIAS.Close
    Relate:TRDPONO.Close
  END
  IF SELF.Opened
    INIMgr.Update('Startup',Window)                        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ================================================================
  ! Initialise Xml Export Object
    objXmlExport.FInit(fileXmlExport)
  ! ================================================================
  ReturnValue = PARENT.Run()
  RETURN ReturnValue

local.PORMessage                   Procedure(Byte     func:Type)
local:PartsTotal         Real()
local:TotalGRNValue      Real()
local:CurrencyCode        String(30)
Code
    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    clear(XmlValues)
    ! Generic values - TrkBs: 5110 (DBH: 03-08-2005)
    XmlValues.ServiceCode             = 'POR'
    XmlValues.POR.DELIV_TO_REQ        = '5102'   ! string(10)
    XmlValues.POR.PREPARER_ID         = XmlValues.POR.DELIV_TO_REQ   ! string(10)
    XmlValues.POR.DEST_ORG_ID         = '81'   ! string(20)
    XmlValues.POR.INTERFACE_SRC       = 'VSP SERVICEBASE'   ! string(20)
    XmlValues.POR.ORG_ID              = '81'   ! string(2)
    XmlValues.POR.QTY                 = '1'   ! string(10)
    XmlValues.POR.GL_DATE             = format(today(), @d06)   ! string(10)
    XmlValues.POR.SB_PO_NUMBER        = Clip(tmp:PurchaseOrderNumber)

    Case func:Type
    Of 0 ! Stock
        local:PartsTotal = 0
        local:TotalGRNValue = 0
        Access:ORDERS.ClearKey(ord:Order_Number_Key)
        ord:Order_Number = grnali:Order_Number
        If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
            !Found

            Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
            sup:Company_Name = ord:Supplier
            If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                !Found

            Else !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                !Error
            End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign

            local:PartsTotal = 0
            Access:ORDPARTS.Clearkey(orp:Part_Number_Key)
            orp:Order_Number = ord:Order_Number
            Set(orp:Part_Number_Key,orp:Part_Number_Key)
            Loop ! Begin ORDPARTS Loop
                If Access:ORDPARTS.Next()
                    Break
                End ! If !Access
                If orp:Order_Number <> ord:Order_Number
                    Break
                End ! If

                If orp:GRN_Number <> ''
                    If orp:GRN_Number <> grnali:Goods_Received_Number
                        Cycle
                    End ! If orp:GRN_Number <> grnali:Goods_Received_Number
                ! Inserting (DBH 11/01/2006) #6979 - Do not count lines with no GRN
                Else  ! If orp:GRN_Number <> ''
                    Cycle
                ! End (DBH 11/01/2006) #6979
                End ! If orp:GRN_Number <> ''

                If orp:Date_Received = ''
                    Cycle
                End ! If ord:Date_Received = ''


                ! Use the total of each part, rather than applying the currency at the end - TrkBs: 6264 (DBH: 07-09-2005)
! Changing (DBH 11/25/2005) #6789 - Use the daily rate at reciept, instead of order
!                 If grnali:CurrencyCode <> ''
!                     Case grnali:DivideMultiply
!                     Of '*'
!                         local:PartsTotal += ((orp:Purchase_Cost / grnali:DailyRate) * orp:Number_Received)
!                     Of '/'
!                         local:PartsTotal += ((orp:Purchase_Cost * grnali:DailyRate) * orp:Number_Received)
!                     End ! Case grnali:DivideMultiply
!                 Else ! If grnali:CurrencyCode <> ''
!                     local:PartsTotal += (orp:Purchase_Cost * orp:Number_Received)
!                 End ! If grnali:CurrencyCode <> ''
! to (DBH 11/25/2005) #6789
                ! Changing (DBH 31/01/2006) #7106 - Double check that no currency was set at point of order
                !                 ! Changing (DBH 12/01/2006) #7002 - Allow for "0" to be in the received currency field
                !                 ! If orp:ReceivedCurrency <> ''
                !                 ! to (DBH 12/01/2006) #7002
                !                 If Clip(orp:ReceivedCurrency) <> '' And Clip(orp:ReceivedCurrency) <> '0'
                !                 ! End (DBH 12/01/2006) #7002
                ! to (DBH 31/01/2006) #7106
                If Clip(orp:ReceivedCurrency) <> '' And Clip(orp:ReceivedCurrency) <> '0' And Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
                ! End (DBH 31/01/2006) #7106
                    Case orp:ReceivedDivideMultiply
                    Of '*'
                        local:PartsTotal += ((orp:Purchase_Cost / orp:ReceivedDailyRate) * orp:Number_Received)
                    Of '/'
                        local:PartsTotal += ((orp:Purchase_Cost * orp:ReceivedDailyRate) * orp:Number_Received)
                    End ! Case grnali:DivideMultiply
                Else ! If grnali:CurrencyCode <> ''
                    local:PartsTotal += (orp:Purchase_Cost * orp:Number_Received)
                End ! If grnali:CurrencyCode <> ''
! End (DBH 11/25/2005) #6789

            End !
            local:TotalGRNValue = local:PartsTotal

            ! Changing (DBH 12/01/2006) #7002 - Just in case, just check for "0" here too
            ! If grnali:CurrencyCode <> ''
            ! to (DBH 12/01/2006) #7002
            If Clip(grnali:CurrencyCode) <> '' And Clip(grnali:CurrencyCode) <> '0'
            ! End (DBH 12/01/2006) #7002
                local:CurrencyCode = grnali:CurrencyCode
            Else ! If grnali:CurrenyCode <> ''
                local:CurrencyCode = 'ZAR'
            End ! If grnali:CurrenyCode <> ''

        Else !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
            !Error
        End !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
        XmlValues.POR.CURRENCY_CODE       = Clip(local:CurrencyCode)   ! string(3)
        XmlValues.POR.CURRENCY_UNIT_PRICE = Round(local:TotalGRNValue,.01)   ! string(19)
        XmlValues.POR.ITEM_ID             = Clip(sup:MOPItemID)   ! string(20)  ! Length ?????
        XmlValues.POR.SUPPLIER_NUMBER     = Clip(sup:MOPSupplierNumber)   ! string(15)

        XmlValues.POR.TYPE                = 'SPR'   ! string(3)
    Of 1 ! Third Party
        Access:TRDPONO.ClearKey(tro:RecordNumberKey)
        tro:RecordNumber = purque:PurchaseOrderNumber
        If Access:TRDPONO.TryFetch(tro:RecordNumberKey) = Level:Benign
            !Found
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = tro:CompanyName
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found

            Else !If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
            End !If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        Else !If Access:TRDPONO.TryFetch(tro:RecordNumberKey) = Level:Benign
            !Error
        End !If Access:TRDPONO.TryFetch(tro:RecordNumberKey) = Level:Benign

        XmlValues.POR.CURRENCY_CODE       = 'ZAR'   ! string(3)
        XmlValues.POR.CURRENCY_UNIT_PRICE = Round(purque:Cost,.01)   ! string(19)
        XmlValues.POR.ITEM_ID             = Clip(trd:MOPItemID)   ! string(20)  ! Length ?????
        XmlValues.POR.SUPPLIER_NUMBER     = Clip(trd:MOPSupplierNumber)   ! string(15)
        XmlValues.POR.TYPE                = '3OW'   ! string(3)
    End ! Case func:Type

    SendXml(XmlValues, RepositoryDir)
SendXML                      procedure(aXmlData, aRepositoryDir)

TheDate                      date
TheTime                      time
MsgId                        string(24)

   code

   TheDate  = today()
   TheTime  = clock()
   MsgId    = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
   seq     += 1
   if seq > 99999 then
       seq = 1
   end ! if

   if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
       objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
       objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
       objXmlExport.WriteTag('SRC_SYSTEM', '')
       objXmlExport.WriteTag('SRC_APPLICATION', '')
       objXmlExport.WriteTag('SERVICING_APPLICATION', '')
       objXmlExport.WriteTag('ACTION_CODE', 'UPD')
       objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
       objXmlExport.WriteTag('MESSAGE_ID', '')
       objXmlExport.WriteTag('USER_NAME', '')
       objXmlExport.WriteTag('TOKEN', '')
       objXmlExport.CloseTag('MESSAGE_HEADER')
       objXmlExport.OpenTag('MESSAGE_BODY')

       case aXmlData.ServiceCode

       of 'POR'
           objXmlExport.OpenTag('POR_UPDATE_REQUEST', 'version="1.0"')
           objXmlExport.WriteTag('CURRENCY_CODE', clip(aXmlData.POR.CURRENCY_CODE))
           objXmlExport.WriteTag('CURRENCY_UNIT_PRICE', clip(aXmlData.POR.CURRENCY_UNIT_PRICE))
           objXmlExport.WriteTag('DELIV_TO_REQ', clip(aXmlData.POR.DELIV_TO_REQ))
           objXmlExport.WriteTag('DEST_ORG_ID', clip(aXmlData.POR.DEST_ORG_ID))
           objXmlExport.WriteTag('GL_DATE', clip(aXmlData.POR.GL_DATE))
           objXmlExport.WriteTag('INTERFACE_SRC', clip(aXmlData.POR.INTERFACE_SRC))
           objXmlExport.WriteTag('ITEM_ID', clip(aXmlData.POR.ITEM_ID))
           objXmlExport.WriteTag('ORG_ID', clip(aXmlData.POR.ORG_ID))
           objXmlExport.WriteTag('PREPARER_ID', clip(aXmlData.POR.PREPARER_ID))
           objXmlExport.WriteTag('QTY', clip(aXmlData.POR.QTY))
           objXmlExport.WriteTag('SUPPLIER_NUMBER', clip(aXmlData.POR.SUPPLIER_NUMBER))
           objXmlExport.WriteTag('SB_PO_NUMBER', clip(aXmlData.POR.SB_PO_NUMBER))
           objXmlExport.WriteTag('TYPE', clip(aXmlData.POR.TYPE))
           objXmlExport.WriteTag('RESEND_YN', 'N')
           objXmlExport.CloseTag('POR_UPDATE_REQUEST')


           objXmlExport.CloseTag('MESSAGE_BODY')
           objXmlExport.CloseTag('VODACOM_MESSAGE')
           objXmlExport.FClose()
       end ! case





   end ! if

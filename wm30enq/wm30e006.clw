

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E006.INC'),ONCE        !Local module procedure declarations
                     END


ShowAccessories PROCEDURE (SentJobNo)                 !Generated from procedure template - Window

JobNo                LONG
BRW2::View:Browse    VIEW(JOBACC)
                       PROJECT(jac:Ref_Number)
                       PROJECT(jac:Accessory)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jac:Ref_Number         LIKE(jac:Ref_Number)           !List box control field - type derived from field
jac:Accessory          LIKE(jac:Accessory)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Webmaster Enquiry - Accessories booked with job'),AT(,,317,228),GRAY,DOUBLE
                       LIST,AT(26,18,268,168),USE(?List),IMM,MSG('Browsing Records'),FORMAT('0L(2)|M@p<<<<<<<<<<<<<<<<#p@120L(2)|M~Accessory~@s30@'),FROM(Queue:Browse)
                       BUTTON('Cancel'),AT(238,198,56,16),USE(?CancelButton)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowAccessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBACC.Open
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:JOBACC,SELF)
  OPEN(window)
  SELF.Opened=True
  JobNO = SentJobNo
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ShowAccessories)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,jac:Ref_Number_Key)
  BRW2.AddRange(jac:Ref_Number,JobNo)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,jac:Accessory,1,BRW2)
  BRW2.AddField(jac:Ref_Number,BRW2.Q.jac:Ref_Number)
  BRW2.AddField(jac:Accessory,BRW2.Q.jac:Accessory)
  BRW2.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBACC.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)


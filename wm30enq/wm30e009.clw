

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E009.INC'),ONCE        !Local module procedure declarations
                     END


ContactHistory PROCEDURE (SentJobNo)                  !Generated from procedure template - Window

DisplayString        STRING(255)
JobNo                LONG
BRW2::View:Browse    VIEW(CONTHIST)
                       PROJECT(cht:Date)
                       PROJECT(cht:Time)
                       PROJECT(cht:Action)
                       PROJECT(cht:Notes)
                       PROJECT(cht:Record_Number)
                       PROJECT(cht:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
cht:Date               LIKE(cht:Date)                 !List box control field - type derived from field
cht:Time               LIKE(cht:Time)                 !List box control field - type derived from field
cht:Action             LIKE(cht:Action)               !List box control field - type derived from field
cht:Notes              LIKE(cht:Notes)                !List box control field - type derived from field
cht:Record_Number      LIKE(cht:Record_Number)        !Primary key field - type derived from field
cht:Ref_Number         LIKE(cht:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Webmaster Enquiry - Contact History'),AT(,,318,228),GRAY,DOUBLE
                       LIST,AT(10,10,294,118),USE(?List),IMM,HSCROLL,MSG('Browsing Records'),FORMAT('49L(2)|M~Date~@d6b@27L(2)|M~Time~@t7@320L(2)|M~Action~@s80@1020L(2)|M~Notes~@s25' &|
   '5@'),FROM(Queue:Browse)
                       BUTTON('Cancel'),AT(242,174,56,16),USE(?CancelButton)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ContactHistory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CONTHIST.Open
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:CONTHIST,SELF)
  OPEN(window)
  SELF.Opened=True
  JobNo = SentJobNo
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ContactHistory)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,cht:Ref_Number_Key)
  BRW2.AddRange(cht:Ref_Number,JobNo)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,cht:Date,1,BRW2)
  BRW2.AddField(cht:Date,BRW2.Q.cht:Date)
  BRW2.AddField(cht:Time,BRW2.Q.cht:Time)
  BRW2.AddField(cht:Action,BRW2.Q.cht:Action)
  BRW2.AddField(cht:Notes,BRW2.Q.cht:Notes)
  BRW2.AddField(cht:Record_Number,BRW2.Q.cht:Record_Number)
  BRW2.AddField(cht:Ref_Number,BRW2.Q.cht:Ref_Number)
  BRW2.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTHIST.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)


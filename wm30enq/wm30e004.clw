

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E004.INC'),ONCE        !Local module procedure declarations
                     END


ShowEstimateParts PROCEDURE (SentJobNo)               !Generated from procedure template - Window

JobNo                LONG
BRW2::View:Browse    VIEW(ESTPARTS)
                       PROJECT(epr:Part_Number)
                       PROJECT(epr:Description)
                       PROJECT(epr:Quantity)
                       PROJECT(epr:Sale_Cost)
                       PROJECT(epr:Record_Number)
                       PROJECT(epr:Ref_Number)
                       PROJECT(epr:Part_Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
epr:Part_Number        LIKE(epr:Part_Number)          !List box control field - type derived from field
epr:Description        LIKE(epr:Description)          !List box control field - type derived from field
epr:Quantity           LIKE(epr:Quantity)             !List box control field - type derived from field
epr:Sale_Cost          LIKE(epr:Sale_Cost)            !List box control field - type derived from field
epr:Record_Number      LIKE(epr:Record_Number)        !Primary key field - type derived from field
epr:Ref_Number         LIKE(epr:Ref_Number)           !Browse key field - type derived from field
epr:Part_Ref_Number    LIKE(epr:Part_Ref_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Webmaster Enquiry - Parts included in estimate'),AT(,,318,227),GRAY,DOUBLE
                       LIST,AT(14,14,293,165),USE(?List),IMM,MSG('Browsing Records'),FORMAT('80D|M~Part Number~L(2)@s30@120D|M~Description~L(2)@s30@32D|M~Quantity~L(2)@n8@56' &|
   'R|M~Cost Each~L(2)@n14.2@'),FROM(Queue:Browse)
                       BUTTON('Cancel'),AT(256,204,56,16),USE(?Button1)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowEstimateParts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ESTPARTS.Open
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:ESTPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  JobNo = sentJobNo
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ShowEstimateParts)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,epr:Part_Ref_Number_Key)
  BRW2.AddRange(epr:Ref_Number,JobNo)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,epr:Part_Ref_Number,1,BRW2)
  BRW2.AddField(epr:Part_Number,BRW2.Q.epr:Part_Number)
  BRW2.AddField(epr:Description,BRW2.Q.epr:Description)
  BRW2.AddField(epr:Quantity,BRW2.Q.epr:Quantity)
  BRW2.AddField(epr:Sale_Cost,BRW2.Q.epr:Sale_Cost)
  BRW2.AddField(epr:Record_Number,BRW2.Q.epr:Record_Number)
  BRW2.AddField(epr:Ref_Number,BRW2.Q.epr:Ref_Number)
  BRW2.AddField(epr:Part_Ref_Number,BRW2.Q.epr:Part_Ref_Number)
  BRW2.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESTPARTS.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button1
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)


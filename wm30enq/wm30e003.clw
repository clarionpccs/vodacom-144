

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E003.INC'),ONCE        !Local module procedure declarations
                     END


ShowFailed PROCEDURE                                  !Generated from procedure template - Window

window               WINDOW('WebMaster Search Results'),AT(,,418,177),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),GRAY,DOUBLE
                       IMAGE('10by25.gif'),AT(72,24,150,5),USE(?Image1:7)
                       STRING('Phone details not found'),AT(72,40),USE(?FailString),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       IMAGE('10by25.gif'),AT(32,52,20,5),USE(?Image5:3)
                       IMAGE('10by25.gif'),AT(72,52,150,5),USE(?Image1:5)
                       IMAGE('10by25.gif'),AT(228,52,150,5),USE(?Image1:6)
                       IMAGE('10by25.gif'),AT(228,127,150,5),USE(?Image1:4)
                       BUTTON('Cancel'),AT(228,104,88,16),USE(?CancelButton)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowFailed')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1:7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?CancelButton,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ShowFailed)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)




   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WM30E002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E011.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

TelephoneNumber      STRING(20)
accno                STRING(20)
IMEIString           STRING(20)
OrderNumber          STRING(20)
window               WINDOW('ServiceBase 2000 -WebMaster Enquiry Form'),AT(,,323,259),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),GRAY,DOUBLE
                       IMAGE('10by25.gif'),AT(176,8,135,5),USE(?Image2)
                       BUTTON('Search'),AT(176,200,60,12),USE(?SearchButton),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       IMAGE('10by25.gif'),AT(21,220,135,5),USE(?Image3:2)
                       IMAGE('10by25.gif'),AT(21,40,135,5),USE(?Image3:6)
                       BUTTON('Cancel'),AT(176,228,60,15),USE(?CancelButton),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       IMAGE('10by25.gif'),AT(21,192,135,5),USE(?Image3:4)
                       IMAGE('10by25.gif'),AT(21,140,135,5),USE(?Image3:7)
                       ENTRY(@s20),AT(176,153,64,12),USE(OrderNumber),HIDE,COLOR(080FFFFH),UPR
                       PROMPT('Enter order number in this field'),AT(21,157),USE(?Prompt3),HIDE
                       IMAGE('10by25.gif'),AT(21,244,135,5),USE(?Image3:3)
                       IMAGE('10by25.gif'),AT(21,8,135,5),USE(?Image3)
                       STRING('Enter mobile number to locate in this field'),AT(21,20,135,12),USE(?String1),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       ENTRY(@s20),AT(176,20,64,12),USE(TelephoneNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH)
                       PROMPT('If you wish to search by mobile number then click [Search] now. To search by IME' &|
   'I number leave the mobile number blank.'),AT(176,56,135,36),USE(?Prompt2)
                       PROMPT('Enter IMEI number to locate in this field'),AT(21,108,135,12),USE(?Prompt1)
                       ENTRY(@s20),AT(176,108,64,12),USE(IMEIString),COLOR(080FFFFH)
                       IMAGE('10by25.gif'),AT(21,96,135,5),USE(?Image3:5)
                       STRING('Click on the [Search] to lauch the enquiry'),AT(21,200,135,12),USE(?String2),TRN
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?CancelButton,RequestCancelled)
  !Setup the path
  g_path = GETINI('global','datapath','xx7fail8zz', clip(path()) & '\celweb.ini')
  !message('g_path is ' & clip(g_path))
  if clip(g_path)='xx7fail8zz' then
      message ('Fatal error - ini file not found')
      Halt (0,'Fatal error - ini file not found')
  end
  
  setpath (clip(g_path))
  
  
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  set(defaults)
  access:defaults.next()
  !thiswindow{prop:text}='ServiceBase 2000 -WebMaster Enquiry Form: '&DEF:User_Name
  thiswindow.update()
  display
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (Main)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SearchButton
      ThisWindow.Update
      !Find the latest entry ...
      
      !needed to make sure TelNo gets passed
      thiswindow.update()
      display()
      
      
      !Backdoor check
      if upper(clip(TelephoneNumber)) =round(today()*3.88,1) then
          ShowBackDoor
          cycle
      END
      
      jn#=0
      found# =0
      
      IF TelephoneNumber <> ''
          !Loop through file to find most recent job number
          access:jobs.clearkey(job:MobileNumberKey)
          job:mobile_number = TelephoneNumber
          set(job:MobileNumberKey,job:MobileNumberKey)
          loop until access:jobs.next()
             if job:mobile_number <> TelephoneNumber then break.
                 found# += 1
                 if Job:ref_number > jn# then
                     jn#     = JOB:Ref_Number
                     accno   = job:Account_Number
                 END
          end !loop
          !message('Found = '&found#&'Job '&jn#)
      ELSE
          IF IMEIString <> ''   !Telephone number is empty, try for IMEI
              Access:jobs.clearkey(job:ESN_Key)
              job:ESN = IMEIString
              set(job:ESN_Key,job:ESN_Key)
              loop until access:jobs.next()
                  if job:ESN <> IMEIString then break.
                  found# += 1
                  if job:ref_number > jn# then
                      jn#     = JOB:Ref_Number
                      Accno   = job:Account_Number
                  END
              END !Loop
      
          ELSE
              !empty tel no and imei must be order number
              !bother I will have to go through the whole lot!
              found#=0
              set(subtracc)
              loop
                  if access:subtracc.next() then break.  !End of traders
      !            IF CLIP(UPPER(sub:Main_Account_Number)) = 'VR (HEAD)' then  found# = 2.
      !            IF CLIP(UPPER(sub:Main_Account_Number)) = 'VODA B2B'  then  found# = 2.
      !            IF CLIP(UPPER(sub:Main_Account_Number)) = 'VODA TLC (HEAD)' then  found# = 2.
      !            if found#=0 then cycle.   !If not in one of above then look for next one
                  !Now to look and see if this trader has the right order number
                  !found# = 0
                  access:jobs.clearkey(job:AccOrdNoKey)
                  job:Account_Number = sub:Account_Number
                  job:Order_Number   = upper(OrderNumber)
                  access:jobs.fetch(job:AccOrdNoKey)
                  if JOB:Order_Number <> upper(OrderNumber) then
                      found#  = 0
                  else !If job not found
                      found#  = 1
                      jn#     = JOB:Ref_Number
                      Accno   = job:Account_Number
                      break
                  end !if job not found
              END !Loop through all subtraders ...
          END !IMEI number is empty
      END !Telephone number is empty
      
      !message('About to show window - job no '&jn#)
      !Decide which window to show
      If found# = 0 then
             ShowFailed
      ELSE
      !    !Check the account details
      !    Access:SubTracc.ClearKey(sub:Account_Number_Key)
      !    sub:Account_Number = clip(accno)    !job:Account_Number
      !    IF Access:SubTracc.Fetch(sub:Account_Number_Key)
      !        found#=0      !mark it up as a fail
      !        !message('Failed at lookup Traders accounts '&accno)
      !
      !    ELSE
      !        found#=0
      !        IF CLIP(UPPER(sub:Main_Account_Number)) = 'VR (HEAD)' then  found# = 1.
      !        IF CLIP(UPPER(sub:Main_Account_Number)) = 'VODA B2B'  then  found# = 2.
      !        IF CLIP(UPPER(sub:Main_Account_Number)) = 'VODA TLC (HEAD)' then  found# = 3.
      !        !message('found main acc '& sub:Main_Account_Number)
      !   END
      !   if found#=0 then
      !        message(jn#)
      !        showFailed
      !   ELSE
              ShowDetails(jn#)
      !   END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)


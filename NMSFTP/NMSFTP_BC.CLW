  MEMBER('NMSFTP.clw')

  PRAGMA('define(init_priority=>3)')

  MAP
    MODULE('NMSFTP_BC0.CLW')
NMSFTP_BC0:DctInit             PROCEDURE
NMSFTP_BC0:DctKill             PROCEDURE
NMSFTP_BC0:FilesInit           PROCEDURE
    END
    MODULE('NMSFTP_BC1.CLW')
NMSFTP_BC1:DctInit             PROCEDURE
NMSFTP_BC1:DctKill             PROCEDURE
NMSFTP_BC1:FilesInit           PROCEDURE
    END
    MODULE('NMSFTP_BC2.CLW')
NMSFTP_BC2:DctInit             PROCEDURE
NMSFTP_BC2:DctKill             PROCEDURE
NMSFTP_BC2:FilesInit           PROCEDURE
    END
    MODULE('NMSFTP_BC3.CLW')
NMSFTP_BC3:DctInit             PROCEDURE
NMSFTP_BC3:DctKill             PROCEDURE
NMSFTP_BC3:FilesInit           PROCEDURE
    END
    MODULE('NMSFTP_BC4.CLW')
NMSFTP_BC4:DctInit             PROCEDURE
NMSFTP_BC4:DctKill             PROCEDURE
NMSFTP_BC4:FilesInit           PROCEDURE
    END
    MODULE('NMSFTP_BC5.CLW')
NMSFTP_BC5:DctInit             PROCEDURE
NMSFTP_BC5:DctKill             PROCEDURE
NMSFTP_BC5:FilesInit           PROCEDURE
    END
  END

DctInit PROCEDURE
  CODE
  NMSFTP_BC0:DctInit
  NMSFTP_BC1:DctInit
  NMSFTP_BC2:DctInit
  NMSFTP_BC3:DctInit
  NMSFTP_BC4:DctInit
  NMSFTP_BC5:DctInit
  NMSFTP_BC0:FilesInit
  NMSFTP_BC1:FilesInit
  NMSFTP_BC2:FilesInit
  NMSFTP_BC3:FilesInit
  NMSFTP_BC4:FilesInit
  NMSFTP_BC5:FilesInit


DctKill PROCEDURE
  CODE
  NMSFTP_BC0:DctKill
  NMSFTP_BC1:DctKill
  NMSFTP_BC2:DctKill
  NMSFTP_BC3:DctKill
  NMSFTP_BC4:DctKill
  NMSFTP_BC5:DctKill


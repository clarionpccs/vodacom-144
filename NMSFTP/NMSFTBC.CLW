  MEMBER('NMSFTP.clw')

  PRAGMA('define(init_priority=>3)')

  MAP
    MODULE('NMSFTBC0.CLW')
NMSFTBC0:DctInit             PROCEDURE
NMSFTBC0:DctKill             PROCEDURE
NMSFTBC0:FilesInit           PROCEDURE
    END
    MODULE('NMSFTBC1.CLW')
NMSFTBC1:DctInit             PROCEDURE
NMSFTBC1:DctKill             PROCEDURE
NMSFTBC1:FilesInit           PROCEDURE
    END
    MODULE('NMSFTBC2.CLW')
NMSFTBC2:DctInit             PROCEDURE
NMSFTBC2:DctKill             PROCEDURE
NMSFTBC2:FilesInit           PROCEDURE
    END
    MODULE('NMSFTBC3.CLW')
NMSFTBC3:DctInit             PROCEDURE
NMSFTBC3:DctKill             PROCEDURE
NMSFTBC3:FilesInit           PROCEDURE
    END
    MODULE('NMSFTBC4.CLW')
NMSFTBC4:DctInit             PROCEDURE
NMSFTBC4:DctKill             PROCEDURE
NMSFTBC4:FilesInit           PROCEDURE
    END
    MODULE('NMSFTBC5.CLW')
NMSFTBC5:DctInit             PROCEDURE
NMSFTBC5:DctKill             PROCEDURE
NMSFTBC5:FilesInit           PROCEDURE
    END
  END

DctInit PROCEDURE
  CODE
  NMSFTBC0:DctInit
  NMSFTBC1:DctInit
  NMSFTBC2:DctInit
  NMSFTBC3:DctInit
  NMSFTBC4:DctInit
  NMSFTBC5:DctInit
  NMSFTBC0:FilesInit
  NMSFTBC1:FilesInit
  NMSFTBC2:FilesInit
  NMSFTBC3:FilesInit
  NMSFTBC4:FilesInit
  NMSFTBC5:FilesInit


DctKill PROCEDURE
  CODE
  NMSFTBC0:DctKill
  NMSFTBC1:DctKill
  NMSFTBC2:DctKill
  NMSFTBC3:DctKill
  NMSFTBC4:DctKill
  NMSFTBC5:DctKill


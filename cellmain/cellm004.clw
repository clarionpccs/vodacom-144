

   MEMBER('cellmain.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('CELLM004.INC'),ONCE        !Local module procedure declarations
                     END


UpdateDateTimeStamp  PROCEDURE  (f:RefNumber)         ! Declare Procedure
save_jobstamp_id     USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBSTAMP.Open
    ! Inserting (DBH 05/08/2008) # 10253 - Update date/time stamp everytime a job changes
    Save_JOBSTAMP_ID = Access:JOBSTAMP.SaveFile()
    Access:JOBSTAMP.ClearKey(jos:JOBSRefNumberKey)
    jos:JOBSRefNumber = f:RefNumber
    If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Found
        jos:DateStamp = Today()
        jos:TimeStamp = Clock()
        Access:JOBSTAMP.TryUpdate()
    Else ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Error
        If Access:JOBSTAMP.PrimeRecord() = Level:Benign
            jos:JOBSRefNumber = f:RefNumber
            jos:DateStamp = Today()
            jos:TimeStamp = Clock()
            If Access:JOBSTAMP.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:JOBSTAMP.TryInsert() = Level:Benign
                Access:JOBSTAMP.CancelAutoInc()
            End ! If Access:JOBSTAMP.TryInsert() = Level:Benign
        End ! If Access.JOBSTAMP.PrimeRecord() = Level:Benign
    End ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign

    Access:JOBSTAMP.RestoreFile(Save_JOBSTAMP_ID)
    ! End (DBH 05/08/2008) #10253
   Relate:JOBSTAMP.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()

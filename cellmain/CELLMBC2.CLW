  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBC2:DctInit    PROCEDURE
CELLMBC2:DctKill    PROCEDURE
CELLMBC2:FilesInit  PROCEDURE
  END

Hide:Access:AUDSTATS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDSTATS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:KIMPORT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:KIMPORT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDVCPLN CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDVCPLN CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANSAMP  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANSAMP  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:REPMAILP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:REPMAILP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:RAPIDLST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:RAPIDLST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:NETWORKS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:NETWORKS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:REPSCHED CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:REPSCHED CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDTEMP  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ORDTEMP  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:NMSPRE   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:NMSPRE   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDITEMS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ORDITEMS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:KSTAGES  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:KSTAGES  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDHEAD  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ORDHEAD  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:KLOCS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:KLOCS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:IMEISHIP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:IMEISHIP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBTHIRD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
TryUpdate              PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBTHIRD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFAULTV CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:DEFAULTV CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGRETRN CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOGRETRN CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCATLOG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOCATLOG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PRODCODE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PRODCODE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBC2:DctInit PROCEDURE
  CODE
  Relate:AUDSTATS &= Hide:Relate:AUDSTATS
  Relate:KIMPORT &= Hide:Relate:KIMPORT
  Relate:AUDVCPLN &= Hide:Relate:AUDVCPLN
  Relate:MANSAMP &= Hide:Relate:MANSAMP
  Relate:REPMAILP &= Hide:Relate:REPMAILP
  Relate:RAPIDLST &= Hide:Relate:RAPIDLST
  Relate:NETWORKS &= Hide:Relate:NETWORKS
  Relate:REPSCHED &= Hide:Relate:REPSCHED
  Relate:ORDTEMP &= Hide:Relate:ORDTEMP
  Relate:NMSPRE &= Hide:Relate:NMSPRE
  Relate:ORDITEMS &= Hide:Relate:ORDITEMS
  Relate:KSTAGES &= Hide:Relate:KSTAGES
  Relate:ORDHEAD &= Hide:Relate:ORDHEAD
  Relate:KLOCS &= Hide:Relate:KLOCS
  Relate:IMEISHIP &= Hide:Relate:IMEISHIP
  Relate:JOBTHIRD &= Hide:Relate:JOBTHIRD
  Relate:DEFAULTV &= Hide:Relate:DEFAULTV
  Relate:LOGRETRN &= Hide:Relate:LOGRETRN
  Relate:LOCATLOG &= Hide:Relate:LOCATLOG
  Relate:PRODCODE &= Hide:Relate:PRODCODE

CELLMBC2:FilesInit PROCEDURE
  CODE
  Hide:Relate:AUDSTATS.Init
  Hide:Relate:KIMPORT.Init
  Hide:Relate:AUDVCPLN.Init
  Hide:Relate:MANSAMP.Init
  Hide:Relate:REPMAILP.Init
  Hide:Relate:RAPIDLST.Init
  Hide:Relate:NETWORKS.Init
  Hide:Relate:REPSCHED.Init
  Hide:Relate:ORDTEMP.Init
  Hide:Relate:NMSPRE.Init
  Hide:Relate:ORDITEMS.Init
  Hide:Relate:KSTAGES.Init
  Hide:Relate:ORDHEAD.Init
  Hide:Relate:KLOCS.Init
  Hide:Relate:IMEISHIP.Init
  Hide:Relate:JOBTHIRD.Init
  Hide:Relate:DEFAULTV.Init
  Hide:Relate:LOGRETRN.Init
  Hide:Relate:LOCATLOG.Init
  Hide:Relate:PRODCODE.Init


CELLMBC2:DctKill PROCEDURE
  CODE
  Hide:Relate:AUDSTATS.Kill
  Hide:Relate:KIMPORT.Kill
  Hide:Relate:AUDVCPLN.Kill
  Hide:Relate:MANSAMP.Kill
  Hide:Relate:REPMAILP.Kill
  Hide:Relate:RAPIDLST.Kill
  Hide:Relate:NETWORKS.Kill
  Hide:Relate:REPSCHED.Kill
  Hide:Relate:ORDTEMP.Kill
  Hide:Relate:NMSPRE.Kill
  Hide:Relate:ORDITEMS.Kill
  Hide:Relate:KSTAGES.Kill
  Hide:Relate:ORDHEAD.Kill
  Hide:Relate:KLOCS.Kill
  Hide:Relate:IMEISHIP.Kill
  Hide:Relate:JOBTHIRD.Kill
  Hide:Relate:DEFAULTV.Kill
  Hide:Relate:LOGRETRN.Kill
  Hide:Relate:LOCATLOG.Kill
  Hide:Relate:PRODCODE.Kill


Hide:Access:AUDSTATS.Init PROCEDURE
  CODE
  SELF.Init(AUDSTATS,GlobalErrors)
  SELF.Buffer &= aus:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aus:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(aus:DateChangedKey,'By Date Changed',0)
  SELF.AddKey(aus:NewStatusKey,'By New Status',0)
  SELF.AddKey(aus:StatusTimeKey,'By Time',0)
  SELF.AddKey(aus:StatusDateKey,'By Time',0)
  SELF.AddKey(aus:StatusDateRecordKey,'By Record Number',0)
  SELF.AddKey(aus:StatusTypeRecordKey,'By Record Number',0)
  SELF.AddKey(aus:RefRecordNumberKey,'By Record Number',0)
  SELF.AddKey(aus:RefDateRecordKey,'By Record Number',0)
  SELF.LazyOpen = False
  Access:AUDSTATS &= SELF


Hide:Relate:AUDSTATS.Init PROCEDURE
  CODE
  Hide:Access:AUDSTATS.Init
  SELF.Init(Access:AUDSTATS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:AUDSTATS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDSTATS &= NULL


Hide:Relate:AUDSTATS.Kill PROCEDURE

  CODE
  Hide:Access:AUDSTATS.Kill
  PARENT.Kill
  Relate:AUDSTATS &= NULL


Hide:Access:KIMPORT.Init PROCEDURE
  CODE
  SELF.Init(KIMPORT,GlobalErrors)
  SELF.FileName &= glo:filename
  SELF.Buffer &= kim:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:KIMPORT &= SELF


Hide:Relate:KIMPORT.Init PROCEDURE
  CODE
  Hide:Access:KIMPORT.Init
  SELF.Init(Access:KIMPORT,1)


Hide:Access:KIMPORT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:KIMPORT &= NULL


Hide:Relate:KIMPORT.Kill PROCEDURE

  CODE
  Hide:Access:KIMPORT.Kill
  PARENT.Kill
  Relate:KIMPORT &= NULL


Hide:Access:AUDVCPLN.Init PROCEDURE
  CODE
  SELF.Init(AUDVCPLN,GlobalErrors)
  SELF.Buffer &= avcp:Record
  SELF.Create = 0
  SELF.LockRecover = 10
  SELF.AddKey(avcp:Audit_Number_Key,'By Audit Number',0)
  SELF.AddKey(avcp:StockType_Audit_Key,'By Audit Number',0)
  SELF.LazyOpen = False
  Access:AUDVCPLN &= SELF


Hide:Relate:AUDVCPLN.Init PROCEDURE
  CODE
  Hide:Access:AUDVCPLN.Init
  SELF.Init(Access:AUDVCPLN,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDVCPMS)


Hide:Access:AUDVCPLN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDVCPLN &= NULL


Hide:Relate:AUDVCPLN.Kill PROCEDURE

  CODE
  Hide:Access:AUDVCPLN.Kill
  PARENT.Kill
  Relate:AUDVCPLN &= NULL


Hide:Access:MANSAMP.Init PROCEDURE
  CODE
  SELF.Init(MANSAMP,GlobalErrors)
  SELF.Buffer &= msp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(msp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(msp:PartPrefixKey,'By Part Number/Lab Type',0)
  SELF.LazyOpen = False
  Access:MANSAMP &= SELF


Hide:Relate:MANSAMP.Init PROCEDURE
  CODE
  Hide:Access:MANSAMP.Init
  SELF.Init(Access:MANSAMP,1)


Hide:Access:MANSAMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANSAMP &= NULL


Hide:Relate:MANSAMP.Kill PROCEDURE

  CODE
  Hide:Access:MANSAMP.Kill
  PARENT.Kill
  Relate:MANSAMP &= NULL


Hide:Access:REPMAILP.Init PROCEDURE
  CODE
  SELF.Init(REPMAILP,GlobalErrors)
  SELF.FileNameValue = 'REPMAILP'
  SELF.Buffer &= rpp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(rpp:Record_Number_Key,'rpp:Record_Number_Key',1)
  SELF.AddKey(rpp:Ref_Number_Key,'rpp:Ref_Number_Key',0)
  SELF.LazyOpen = False
  Access:REPMAILP &= SELF


Hide:Relate:REPMAILP.Init PROCEDURE
  CODE
  Hide:Access:REPMAILP.Init
  SELF.Init(Access:REPMAILP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:REPMAIL)


Hide:Access:REPMAILP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:REPMAILP &= NULL


Hide:Relate:REPMAILP.Kill PROCEDURE

  CODE
  Hide:Access:REPMAILP.Kill
  PARENT.Kill
  Relate:REPMAILP &= NULL


Hide:Access:RAPIDLST.Init PROCEDURE
  CODE
  SELF.Init(RAPIDLST,GlobalErrors)
  SELF.Buffer &= RAP:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(RAP:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(RAP:OrderKey,'By Order',0)
  SELF.AddKey(RAP:NormalOrderKey,'By Name',0)
  SELF.AddKey(RAP:WebOrderKey,'By Name',0)
  SELF.LazyOpen = False
  Access:RAPIDLST &= SELF


Hide:Relate:RAPIDLST.Init PROCEDURE
  CODE
  Hide:Access:RAPIDLST.Init
  SELF.Init(Access:RAPIDLST,1)


Hide:Access:RAPIDLST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:RAPIDLST &= NULL


Hide:Access:RAPIDLST.PrimeFields PROCEDURE

  CODE
  RAP:Normal = 1
  RAP:Web = 0
  PARENT.PrimeFields


Hide:Relate:RAPIDLST.Kill PROCEDURE

  CODE
  Hide:Access:RAPIDLST.Kill
  PARENT.Kill
  Relate:RAPIDLST &= NULL


Hide:Access:NETWORKS.Init PROCEDURE
  CODE
  SELF.Init(NETWORKS,GlobalErrors)
  SELF.Buffer &= net:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(net:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(net:NetworkKey,'By Network',0)
  SELF.LazyOpen = False
  Access:NETWORKS &= SELF


Hide:Relate:NETWORKS.Init PROCEDURE
  CODE
  Hide:Access:NETWORKS.Init
  SELF.Init(Access:NETWORKS,1)


Hide:Access:NETWORKS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:NETWORKS &= NULL


Hide:Relate:NETWORKS.Kill PROCEDURE

  CODE
  Hide:Access:NETWORKS.Kill
  PARENT.Kill
  Relate:NETWORKS &= NULL


Hide:Access:REPSCHED.Init PROCEDURE
  CODE
  SELF.Init(REPSCHED,GlobalErrors)
  SELF.Buffer &= rpd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(rpd:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(rpd:ReportNameKey,'By Report Name',0)
  SELF.AddKey(rpd:ReportCriteriaKey,'By Report Criteria',0)
  SELF.AddKey(rpd:MachineNextDateKey,'By Next Report Date',0)
  SELF.AddKey(rpd:ReportNameActiveKey,'By Report Name',0)
  SELF.AddKey(rpd:ActiveMachineNextDateKey,'By Next Report Date',0)
  SELF.AddKey(rpd:ActiveDateKey,'By Next Report Date',0)
  SELF.AddKey(rpd:NextReportDateKey,'By Next Report Date',0)
  SELF.LazyOpen = False
  Access:REPSCHED &= SELF


Hide:Relate:REPSCHED.Init PROCEDURE
  CODE
  Hide:Access:REPSCHED.Init
  SELF.Init(Access:REPSCHED,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:REPSCHLG,RI:CASCADE,RI:CASCADE,rlg:TheDateKey)
  SELF.AddRelationLink(rpd:RecordNumber,rlg:REPSCHEDRecordNumber)
  SELF.AddRelation(Relate:REPSCHCR,RI:CASCADE,RI:CASCADE,rpc:ReportCriteriaKey)
  SELF.AddRelationLink(rpd:ReportName,rpc:ReportName)
  SELF.AddRelationLink(rpd:ReportCriteriaType,rpc:ReportCriteriaType)


Hide:Access:REPSCHED.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:REPSCHED &= NULL


Hide:Access:REPSCHED.PrimeFields PROCEDURE

  CODE
  rpd:Frequency = 0
  rpd:Active = 0
  PARENT.PrimeFields


Hide:Relate:REPSCHED.Kill PROCEDURE

  CODE
  Hide:Access:REPSCHED.Kill
  PARENT.Kill
  Relate:REPSCHED &= NULL


Hide:Access:ORDTEMP.Init PROCEDURE
  CODE
  SELF.Init(ORDTEMP,GlobalErrors)
  SELF.Buffer &= ort:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ort:recordnumberkey,'ort:recordnumberkey',1)
  SELF.AddKey(ort:Keyordhno,'ort:Keyordhno',0)
  SELF.LazyOpen = False
  Access:ORDTEMP &= SELF


Hide:Relate:ORDTEMP.Init PROCEDURE
  CODE
  Hide:Access:ORDTEMP.Init
  SELF.Init(Access:ORDTEMP,1)


Hide:Access:ORDTEMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDTEMP &= NULL


Hide:Relate:ORDTEMP.Kill PROCEDURE

  CODE
  Hide:Access:ORDTEMP.Kill
  PARENT.Kill
  Relate:ORDTEMP &= NULL


Hide:Access:NMSPRE.Init PROCEDURE
  CODE
  SELF.Init(NMSPRE,GlobalErrors)
  SELF.Buffer &= nms:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(nms:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(nms:ProcessedIMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(nms:ProcessedJobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:NMSPRE &= SELF


Hide:Relate:NMSPRE.Init PROCEDURE
  CODE
  Hide:Access:NMSPRE.Init
  SELF.Init(Access:NMSPRE,1)


Hide:Access:NMSPRE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:NMSPRE &= NULL


Hide:Access:NMSPRE.PrimeFields PROCEDURE

  CODE
  nms:ReceivedDate = Today()
  nms:ReceivedTime = Clock()
  nms:Processed = 0
  PARENT.PrimeFields


Hide:Relate:NMSPRE.Kill PROCEDURE

  CODE
  Hide:Access:NMSPRE.Kill
  PARENT.Kill
  Relate:NMSPRE &= NULL


Hide:Access:ORDITEMS.Init PROCEDURE
  CODE
  SELF.Init(ORDITEMS,GlobalErrors)
  SELF.Buffer &= ori:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ori:Keyordhno,'ori:Keyordhno',0)
  SELF.AddKey(ori:recordnumberkey,'ori:recordnumberkey',1)
  SELF.AddKey(ori:OrdHNoPartKey,'By Part Number',0)
  SELF.AddKey(ori:PartNoKey,'By Part Number',0)
  SELF.LazyOpen = False
  Access:ORDITEMS &= SELF


Hide:Relate:ORDITEMS.Init PROCEDURE
  CODE
  Hide:Access:ORDITEMS.Init
  SELF.Init(Access:ORDITEMS,1)


Hide:Access:ORDITEMS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDITEMS &= NULL


Hide:Relate:ORDITEMS.Kill PROCEDURE

  CODE
  Hide:Access:ORDITEMS.Kill
  PARENT.Kill
  Relate:ORDITEMS &= NULL


Hide:Access:KSTAGES.Init PROCEDURE
  CODE
  SELF.Init(KSTAGES,GlobalErrors)
  SELF.Buffer &= ksta:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ksta:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(ksta:KeyStageKey,'By Key Stage',0)
  SELF.LazyOpen = False
  Access:KSTAGES &= SELF


Hide:Relate:KSTAGES.Init PROCEDURE
  CODE
  Hide:Access:KSTAGES.Init
  SELF.Init(Access:KSTAGES,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:KLOCS,RI:CASCADE,RI:CASCADE,kloc:KeyLocationKey)
  SELF.AddRelationLink(ksta:KeyStage,kloc:KeyStage)


Hide:Access:KSTAGES.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:KSTAGES &= NULL


Hide:Relate:KSTAGES.Kill PROCEDURE

  CODE
  Hide:Access:KSTAGES.Kill
  PARENT.Kill
  Relate:KSTAGES &= NULL


Hide:Access:ORDHEAD.Init PROCEDURE
  CODE
  SELF.Init(ORDHEAD,GlobalErrors)
  SELF.Buffer &= orh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(orh:KeyOrder_no,'orh:KeyOrder_no',1)
  SELF.AddKey(orh:book_date_key,'orh:book_date_key',0)
  SELF.AddKey(orh:pro_date_key,'orh:pro_date_key',0)
  SELF.AddKey(orh:pro_ord_no_key,'orh:pro_ord_no_key',0)
  SELF.AddKey(orh:pro_cust_name_key,'orh:pro_cust_name_key',0)
  SELF.AddKey(orh:KeyCustName,'orh:KeyCustName',0)
  SELF.AddKey(orh:AccountDateKey,'By Date',0)
  SELF.AddKey(orh:SalesNumberKey,'By Sales Number',0)
  SELF.AddKey(orh:ProcessSaleNoKey,'By Sales Number',0)
  SELF.AddKey(orh:DateDespatchedKey,'By Date Despatched',0)
  SELF.LazyOpen = False
  Access:ORDHEAD &= SELF


Hide:Relate:ORDHEAD.Init PROCEDURE
  CODE
  Hide:Access:ORDHEAD.Init
  SELF.Init(Access:ORDHEAD,1)


Hide:Access:ORDHEAD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDHEAD &= NULL


Hide:Relate:ORDHEAD.Kill PROCEDURE

  CODE
  Hide:Access:ORDHEAD.Kill
  PARENT.Kill
  Relate:ORDHEAD &= NULL


Hide:Access:KLOCS.Init PROCEDURE
  CODE
  SELF.Init(KLOCS,GlobalErrors)
  SELF.Buffer &= kloc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(kloc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(kloc:KeyLocationKey,'By Unit Location',0)
  SELF.AddKey(kloc:RefKeyLocKey,'By Unit Location',2)
  SELF.LazyOpen = False
  Access:KLOCS &= SELF


Hide:Relate:KLOCS.Init PROCEDURE
  CODE
  Hide:Access:KLOCS.Init
  SELF.Init(Access:KLOCS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:KSTAGES)


Hide:Access:KLOCS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:KLOCS &= NULL


Hide:Relate:KLOCS.Kill PROCEDURE

  CODE
  Hide:Access:KLOCS.Kill
  PARENT.Kill
  Relate:KLOCS &= NULL


Hide:Access:IMEISHIP.Init PROCEDURE
  CODE
  SELF.Init(IMEISHIP,GlobalErrors)
  SELF.Buffer &= IMEI:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(IMEI:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(IMEI:IMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(IMEI:ShipDateKey,'By Shipment Date',0)
  SELF.LazyOpen = False
  Access:IMEISHIP &= SELF


Hide:Relate:IMEISHIP.Init PROCEDURE
  CODE
  Hide:Access:IMEISHIP.Init
  SELF.Init(Access:IMEISHIP,1)


Hide:Access:IMEISHIP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:IMEISHIP &= NULL


Hide:Relate:IMEISHIP.Kill PROCEDURE

  CODE
  Hide:Access:IMEISHIP.Kill
  PARENT.Kill
  Relate:IMEISHIP &= NULL


Hide:Access:JOBTHIRD.Init PROCEDURE
  CODE
  SELF.Init(JOBTHIRD,GlobalErrors)
  SELF.Buffer &= jot:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jot:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jot:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jot:OutIMEIKey,'By Outgoing IMEI Number',0)
  SELF.AddKey(jot:InIMEIKEy,'By Incoming IMEI Number',0)
  SELF.AddKey(jot:OutDateKey,'By Outgoing Date',0)
  SELF.AddKey(jot:ThirdPartyKey,'jot:ThirdPartyKey',0)
  SELF.AddKey(jot:OriginalIMEIKey,'By Original I.M.E.I. No',0)
  SELF.LazyOpen = False
  Access:JOBTHIRD &= SELF


Hide:Relate:JOBTHIRD.Init PROCEDURE
  CODE
  Hide:Access:JOBTHIRD.Init
  SELF.Init(Access:JOBTHIRD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDBATCH)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBTHIRD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBTHIRD &= NULL


Hide:Access:JOBTHIRD.TryUpdate PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TryUpdate()
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(jot:RefNumber)
  ! End (DBH 16/09/2008) #10253
  RETURN ReturnValue


Hide:Access:JOBTHIRD.Update PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Update()
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(jot:RefNumber)
  ! End (DBH 16/09/2008) #10253
  RETURN ReturnValue


Hide:Relate:JOBTHIRD.Kill PROCEDURE

  CODE
  Hide:Access:JOBTHIRD.Kill
  PARENT.Kill
  Relate:JOBTHIRD &= NULL


Hide:Access:DEFAULTV.Init PROCEDURE
  CODE
  SELF.Init(DEFAULTV,GlobalErrors)
  SELF.Buffer &= defv:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(defv:RecordNumberKey,'By Record Number',1)
  SELF.LazyOpen = False
  Access:DEFAULTV &= SELF


Hide:Relate:DEFAULTV.Init PROCEDURE
  CODE
  Hide:Access:DEFAULTV.Init
  SELF.Init(Access:DEFAULTV,1)


Hide:Access:DEFAULTV.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFAULTV &= NULL


Hide:Access:DEFAULTV.PrimeFields PROCEDURE

  CODE
  defv:ReUpdate = 0
  PARENT.PrimeFields


Hide:Relate:DEFAULTV.Kill PROCEDURE

  CODE
  Hide:Access:DEFAULTV.Kill
  PARENT.Kill
  Relate:DEFAULTV &= NULL


Hide:Access:LOGRETRN.Init PROCEDURE
  CODE
  SELF.Init(LOGRETRN,GlobalErrors)
  SELF.Buffer &= lrtn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lrtn:RefNumberKey,'By Ref Number',1)
  SELF.AddKey(lrtn:ClubNokiaKey,'By Club Nokia Site',0)
  SELF.AddKey(lrtn:DateKey,'By Date',0)
  SELF.AddKey(lrtn:ReturnsNoKey,'By Returns Number',0)
  SELF.LazyOpen = False
  Access:LOGRETRN &= SELF


Hide:Relate:LOGRETRN.Init PROCEDURE
  CODE
  Hide:Access:LOGRETRN.Init
  SELF.Init(Access:LOGRETRN,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGRTHIS,RI:CASCADE,RI:CASCADE,lsrh:RefNumberKey)
  SELF.AddRelationLink(lrtn:RefNumber,lsrh:RefNumber)


Hide:Access:LOGRETRN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGRETRN &= NULL


Hide:Access:LOGRETRN.PrimeFields PROCEDURE

  CODE
  lrtn:Date = Today()
  lrtn:ReturnType = 'SCN'
  PARENT.PrimeFields


Hide:Relate:LOGRETRN.Kill PROCEDURE

  CODE
  Hide:Access:LOGRETRN.Kill
  PARENT.Kill
  Relate:LOGRETRN &= NULL


Hide:Access:LOCATLOG.Init PROCEDURE
  CODE
  SELF.Init(LOCATLOG,GlobalErrors)
  SELF.Buffer &= lot:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lot:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lot:DateKey,'By Date',0)
  SELF.AddKey(lot:NewLocationKey,'By Location',0)
  SELF.AddKey(lot:DateNewLocationKey,'By New Location',0)
  SELF.LazyOpen = False
  Access:LOCATLOG &= SELF


Hide:Relate:LOCATLOG.Init PROCEDURE
  CODE
  Hide:Access:LOCATLOG.Init
  SELF.Init(Access:LOCATLOG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:LOCATLOG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCATLOG &= NULL


Hide:Access:LOCATLOG.PrimeFields PROCEDURE

  CODE
  lot:TheDate = Today()
  lot:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:LOCATLOG.Kill PROCEDURE

  CODE
  Hide:Access:LOCATLOG.Kill
  PARENT.Kill
  Relate:LOCATLOG &= NULL


Hide:Access:PRODCODE.Init PROCEDURE
  CODE
  SELF.Init(PRODCODE,GlobalErrors)
  SELF.Buffer &= prd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(prd:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(prd:ProductCodeKey,'By Product Code',0)
  SELF.AddKey(prd:ModelProductKey,'By Product Code',0)
  SELF.LazyOpen = False
  Access:PRODCODE &= SELF


Hide:Relate:PRODCODE.Init PROCEDURE
  CODE
  Hide:Access:PRODCODE.Init
  SELF.Init(Access:PRODCODE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:PRODCODE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PRODCODE &= NULL


Hide:Relate:PRODCODE.Kill PROCEDURE

  CODE
  Hide:Access:PRODCODE.Kill
  PARENT.Kill
  Relate:PRODCODE &= NULL


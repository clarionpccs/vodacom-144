  MEMBER('StockHistAdj.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
STOCKBC3:DctInit    PROCEDURE
STOCKBC3:DctKill    PROCEDURE
STOCKBC3:FilesInit  PROCEDURE
  END

Hide:Access:USERS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:USERS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCSHELF CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOCSHELF CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:REPTYCAT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:REPTYCAT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFPALO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFPALO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSTAGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSTAGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAUPA CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAUPA CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCINTER CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOCINTER CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:NOTESFAU CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:NOTESFAU CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAULO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAULO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBEXHIS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBEXHIS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORDPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCATION CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOCATION CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOHISTE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOHISTE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOMPFAU CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOMPFAU CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOMJFAU CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOMJFAU CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOESN   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOESN   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUPPLIER CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:SUPPLIER CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COUBUSHR CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COUBUSHR CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUPVALA  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUPVALA  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

STOCKBC3:DctInit PROCEDURE
  CODE
  Relate:USERS &= Hide:Relate:USERS
  Relate:LOCSHELF &= Hide:Relate:LOCSHELF
  Relate:REPTYCAT &= Hide:Relate:REPTYCAT
  Relate:MANFPALO &= Hide:Relate:MANFPALO
  Relate:JOBSTAGE &= Hide:Relate:JOBSTAGE
  Relate:MANFAUPA &= Hide:Relate:MANFAUPA
  Relate:LOCINTER &= Hide:Relate:LOCINTER
  Relate:NOTESFAU &= Hide:Relate:NOTESFAU
  Relate:MANFAULO &= Hide:Relate:MANFAULO
  Relate:JOBEXHIS &= Hide:Relate:JOBEXHIS
  Relate:STOPARTS &= Hide:Relate:STOPARTS
  Relate:ORDPARTS &= Hide:Relate:ORDPARTS
  Relate:LOCATION &= Hide:Relate:LOCATION
  Relate:STOHISTE &= Hide:Relate:STOHISTE
  Relate:STOMPFAU &= Hide:Relate:STOMPFAU
  Relate:STOMJFAU &= Hide:Relate:STOMJFAU
  Relate:STOESN &= Hide:Relate:STOESN
  Relate:SUPPLIER &= Hide:Relate:SUPPLIER
  Relate:COUBUSHR &= Hide:Relate:COUBUSHR
  Relate:SUPVALA &= Hide:Relate:SUPVALA

STOCKBC3:FilesInit PROCEDURE
  CODE
  Hide:Relate:USERS.Init
  Hide:Relate:LOCSHELF.Init
  Hide:Relate:REPTYCAT.Init
  Hide:Relate:MANFPALO.Init
  Hide:Relate:JOBSTAGE.Init
  Hide:Relate:MANFAUPA.Init
  Hide:Relate:LOCINTER.Init
  Hide:Relate:NOTESFAU.Init
  Hide:Relate:MANFAULO.Init
  Hide:Relate:JOBEXHIS.Init
  Hide:Relate:STOPARTS.Init
  Hide:Relate:ORDPARTS.Init
  Hide:Relate:LOCATION.Init
  Hide:Relate:STOHISTE.Init
  Hide:Relate:STOMPFAU.Init
  Hide:Relate:STOMJFAU.Init
  Hide:Relate:STOESN.Init
  Hide:Relate:SUPPLIER.Init
  Hide:Relate:COUBUSHR.Init
  Hide:Relate:SUPVALA.Init


STOCKBC3:DctKill PROCEDURE
  CODE
  Hide:Relate:USERS.Kill
  Hide:Relate:LOCSHELF.Kill
  Hide:Relate:REPTYCAT.Kill
  Hide:Relate:MANFPALO.Kill
  Hide:Relate:JOBSTAGE.Kill
  Hide:Relate:MANFAUPA.Kill
  Hide:Relate:LOCINTER.Kill
  Hide:Relate:NOTESFAU.Kill
  Hide:Relate:MANFAULO.Kill
  Hide:Relate:JOBEXHIS.Kill
  Hide:Relate:STOPARTS.Kill
  Hide:Relate:ORDPARTS.Kill
  Hide:Relate:LOCATION.Kill
  Hide:Relate:STOHISTE.Kill
  Hide:Relate:STOMPFAU.Kill
  Hide:Relate:STOMJFAU.Kill
  Hide:Relate:STOESN.Kill
  Hide:Relate:SUPPLIER.Kill
  Hide:Relate:COUBUSHR.Kill
  Hide:Relate:SUPVALA.Kill


Hide:Access:USERS.Init PROCEDURE
  CODE
  SELF.Init(USERS,GlobalErrors)
  SELF.Buffer &= use:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(use:User_Code_Key,'By User Code',0)
  SELF.AddKey(use:User_Type_Active_Key,'By Surname',0)
  SELF.AddKey(use:Surname_Active_Key,'By Surname',0)
  SELF.AddKey(use:User_Code_Active_Key,'use:User_Code_Active_Key',0)
  SELF.AddKey(use:User_Type_Key,'By Surname',0)
  SELF.AddKey(use:surname_key,'use:surname_key',0)
  SELF.AddKey(use:password_key,'use:password_key',0)
  SELF.AddKey(use:Logged_In_Key,'By Name',0)
  SELF.AddKey(use:Team_Surname,'Surname',0)
  SELF.AddKey(use:Active_Team_Surname,'By Surname',0)
  SELF.AddKey(use:LocationSurnameKey,'By Surname',0)
  SELF.AddKey(use:LocationForenameKey,'By Forename',0)
  SELF.AddKey(use:LocActiveSurnameKey,'By Surname',0)
  SELF.AddKey(use:LocActiveForenameKey,'By Forename',0)
  SELF.AddKey(use:TeamStatusKey,'By Surname',0)
  Access:USERS &= SELF


Hide:Relate:USERS.Init PROCEDURE
  CODE
  Hide:Access:USERS.Init
  SELF.Init(Access:USERS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TEAMS)
  SELF.AddRelation(Relate:USUASSIG,RI:CASCADE,RI:CASCADE,usu:Unit_Type_Key)
  SELF.AddRelationLink(use:User_Code,usu:User_Code)
  SELF.AddRelation(Relate:USMASSIG,RI:CASCADE,RI:CASCADE,usm:Model_Number_Key)
  SELF.AddRelationLink(use:User_Code,usm:User_Code)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Engineer_Key)
  SELF.AddRelationLink(use:User_Code,job:Engineer)


Hide:Access:USERS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USERS &= NULL


Hide:Access:USERS.PrimeFields PROCEDURE

  CODE
  use:StockFromLocationOnly = 0
  use:RestrictParts = 0
  use:RestrictChargeable = 0
  use:RestrictWarranty = 0
  use:IncludeInEngStatus = 0
  PARENT.PrimeFields


Hide:Relate:USERS.Kill PROCEDURE

  CODE
  Hide:Access:USERS.Kill
  PARENT.Kill
  Relate:USERS &= NULL


Hide:Access:LOCSHELF.Init PROCEDURE
  CODE
  SELF.Init(LOCSHELF,GlobalErrors)
  SELF.Buffer &= los:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(los:RecordNumberKey,'los:RecordNumberKey',1)
  SELF.AddKey(los:Shelf_Location_Key,'By Shelf Location',0)
  Access:LOCSHELF &= SELF


Hide:Relate:LOCSHELF.Init PROCEDURE
  CODE
  Hide:Access:LOCSHELF.Init
  SELF.Init(Access:LOCSHELF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCATION)
  SELF.AddRelation(Relate:STOCK,RI:CASCADE,RI:RESTRICT,sto:Shelf_Location_Key)
  SELF.AddRelationLink(los:Site_Location,sto:Location)
  SELF.AddRelationLink(los:Shelf_Location,sto:Shelf_Location)


Hide:Access:LOCSHELF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCSHELF &= NULL


Hide:Relate:LOCSHELF.Kill PROCEDURE

  CODE
  Hide:Access:LOCSHELF.Kill
  PARENT.Kill
  Relate:LOCSHELF &= NULL


Hide:Access:REPTYCAT.Init PROCEDURE
  CODE
  SELF.Init(REPTYCAT,GlobalErrors)
  SELF.Buffer &= repc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(repc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(repc:RepairTypeKey,'By Repair Type',0)
  SELF.AddKey(repc:CategoryKey,'By Repair Type',0)
  Access:REPTYCAT &= SELF


Hide:Relate:REPTYCAT.Init PROCEDURE
  CODE
  Hide:Access:REPTYCAT.Init
  SELF.Init(Access:REPTYCAT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:REPTYDEF)


Hide:Access:REPTYCAT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:REPTYCAT &= NULL


Hide:Relate:REPTYCAT.Kill PROCEDURE

  CODE
  Hide:Access:REPTYCAT.Kill
  PARENT.Kill
  Relate:REPTYCAT &= NULL


Hide:Access:MANFPALO.Init PROCEDURE
  CODE
  SELF.Init(MANFPALO,GlobalErrors)
  SELF.Buffer &= mfp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mfp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mfp:Field_Key,'By Field',0)
  SELF.AddKey(mfp:DescriptionKey,'By Description.',0)
  SELF.AddKey(mfp:AvailableFieldKey,'By Field',0)
  SELF.AddKey(mfp:AvailableDescriptionKey,'By Description',0)
  Access:MANFPALO &= SELF


Hide:Relate:MANFPALO.Init PROCEDURE
  CODE
  Hide:Access:MANFPALO.Init
  SELF.Init(Access:MANFPALO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFPARL,RI:CASCADE,RI:CASCADE,mpr:FieldKey)
  SELF.AddRelationLink(mfp:RecordNumber,mpr:MANFPALORecordNumber)
  SELF.AddRelation(Relate:STOMPFAU,RI:CASCADE,RI:CASCADE,stu:ManufacturerFieldKey)
  SELF.AddRelationLink(mfp:Manufacturer,stu:Manufacturer)
  SELF.AddRelationLink(mfp:Field_Number,stu:FieldNumber)
  SELF.AddRelationLink(mfp:Field,stu:Field)
  SELF.AddRelation(Relate:MANFAUPA)
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:MANFPALO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFPALO &= NULL


Hide:Access:MANFPALO.PrimeFields PROCEDURE

  CODE
  mfp:RestrictLookup = 0
  mfp:RestrictLookupType = 0
  mfp:NotAvailable = 0
  mfp:ForceJobFaultCode = 0
  mfp:SetPartFaultCode = 0
  mfp:JobTypeAvailability = 0
  PARENT.PrimeFields


Hide:Relate:MANFPALO.Kill PROCEDURE

  CODE
  Hide:Access:MANFPALO.Kill
  PARENT.Kill
  Relate:MANFPALO &= NULL


Hide:Access:JOBSTAGE.Init PROCEDURE
  CODE
  SELF.Init(JOBSTAGE,GlobalErrors)
  SELF.Buffer &= jst:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jst:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(jst:Job_Stage_Key,'jst:Job_Stage_Key',0)
  Access:JOBSTAGE &= SELF


Hide:Relate:JOBSTAGE.Init PROCEDURE
  CODE
  Hide:Access:JOBSTAGE.Init
  SELF.Init(Access:JOBSTAGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSTAGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSTAGE &= NULL


Hide:Relate:JOBSTAGE.Kill PROCEDURE

  CODE
  Hide:Access:JOBSTAGE.Kill
  PARENT.Kill
  Relate:JOBSTAGE &= NULL


Hide:Access:MANFAUPA.Init PROCEDURE
  CODE
  SELF.Init(MANFAUPA,GlobalErrors)
  SELF.Buffer &= map:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(map:Field_Number_Key,'By Field Number',0)
  SELF.AddKey(map:MainFaultKey,'By Main Fault',0)
  SELF.AddKey(map:ScreenOrderKey,'By Screen Order',0)
  SELF.AddKey(map:KeyRepairKey,'By KeyRepair',0)
  Access:MANFAUPA &= SELF


Hide:Relate:MANFAUPA.Init PROCEDURE
  CODE
  Hide:Access:MANFAUPA.Init
  SELF.Init(Access:MANFAUPA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFPALO,RI:CASCADE,RI:CASCADE,mfp:Field_Key)
  SELF.AddRelationLink(map:Manufacturer,mfp:Manufacturer)
  SELF.AddRelationLink(map:Field_Number,mfp:Field_Number)
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:MANFAUPA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAUPA &= NULL


Hide:Access:MANFAUPA.PrimeFields PROCEDURE

  CODE
  map:Compulsory = 'NO'
  map:RestrictLength = 0
  map:ForceFormat = 0
  map:MainFault = 0
  map:UseRelatedJobCode = 0
  map:NotAvailable = 0
  map:CopyFromJobFaultCode = 0
  map:NAForAccessory = 0
  map:CompulsoryForAdjustment = 0
  map:KeyRepair = 0
  map:CCTReferenceFaultCode = 0
  map:NAForSW = 0
  PARENT.PrimeFields


Hide:Relate:MANFAUPA.Kill PROCEDURE

  CODE
  Hide:Access:MANFAUPA.Kill
  PARENT.Kill
  Relate:MANFAUPA &= NULL


Hide:Access:LOCINTER.Init PROCEDURE
  CODE
  SELF.Init(LOCINTER,GlobalErrors)
  SELF.Buffer &= loi:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(loi:Location_Key,'By Location',0)
  SELF.AddKey(loi:Location_Available_Key,'By Location',0)
  Access:LOCINTER &= SELF


Hide:Relate:LOCINTER.Init PROCEDURE
  CODE
  Hide:Access:LOCINTER.Init
  SELF.Init(Access:LOCINTER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Location_Key)
  SELF.AddRelationLink(loi:Location,job:Location)


Hide:Access:LOCINTER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCINTER &= NULL


Hide:Relate:LOCINTER.Kill PROCEDURE

  CODE
  Hide:Access:LOCINTER.Kill
  PARENT.Kill
  Relate:LOCINTER &= NULL


Hide:Access:NOTESFAU.Init PROCEDURE
  CODE
  SELF.Init(NOTESFAU,GlobalErrors)
  SELF.Buffer &= nof:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(nof:Notes_Key,'By Notes',0)
  Access:NOTESFAU &= SELF


Hide:Relate:NOTESFAU.Init PROCEDURE
  CODE
  Hide:Access:NOTESFAU.Init
  SELF.Init(Access:NOTESFAU,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:NOTESFAU.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:NOTESFAU &= NULL


Hide:Relate:NOTESFAU.Kill PROCEDURE

  CODE
  Hide:Access:NOTESFAU.Kill
  PARENT.Kill
  Relate:NOTESFAU &= NULL


Hide:Access:MANFAULO.Init PROCEDURE
  CODE
  SELF.Init(MANFAULO,GlobalErrors)
  SELF.Buffer &= mfo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mfo:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mfo:RelatedFieldKey,'By Field',0)
  SELF.AddKey(mfo:Field_Key,'By Field',0)
  SELF.AddKey(mfo:DescriptionKey,'By Description',0)
  SELF.AddKey(mfo:ManFieldKey,'By Field',0)
  SELF.AddKey(mfo:HideFieldKey,'By Field',0)
  SELF.AddKey(mfo:HideDescriptionKey,'By Description',0)
  SELF.AddKey(mfo:RelatedDescriptionKey,'By Description',0)
  SELF.AddKey(mfo:HideRelatedFieldKey,'By Field',0)
  SELF.AddKey(mfo:HideRelatedDescKey,'By Description',0)
  SELF.AddKey(mfo:FieldNumberKey,'By Field Number',0)
  SELF.AddKey(mfo:PrimaryLookupKey,'By Primary Lookup',0)
  Access:MANFAULO &= SELF


Hide:Relate:MANFAULO.Init PROCEDURE
  CODE
  Hide:Access:MANFAULO.Init
  SELF.Init(Access:MANFAULO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFAURL,RI:CASCADE,RI:CASCADE,mnr:FieldKey)
  SELF.AddRelationLink(mfo:RecordNumber,mnr:MANFAULORecordNumber)
  SELF.AddRelation(Relate:MANFAUEX,RI:CASCADE,RI:CASCADE,max:ModelNumberKey)
  SELF.AddRelationLink(mfo:RecordNumber,max:MANFAULORecordNumber)
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:MANFAULT)


Hide:Access:MANFAULO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAULO &= NULL


Hide:Access:MANFAULO.PrimeFields PROCEDURE

  CODE
  mfo:HideFromEngineer = 0
  mfo:PromptForExchange = 0
  mfo:ExcludeFromBouncer = 0
  mfo:ReturnToRRC = 0
  mfo:RestrictLookup = 0
  mfo:RestrictLookupType = 0
  mfo:NotAvailable = 0
  mfo:PrimaryLookup = 0
  mfo:SetJobFaultCode = 0
  mfo:JobTypeAvailability = 0
  PARENT.PrimeFields


Hide:Relate:MANFAULO.Kill PROCEDURE

  CODE
  Hide:Access:MANFAULO.Kill
  PARENT.Kill
  Relate:MANFAULO &= NULL


Hide:Access:JOBEXHIS.Init PROCEDURE
  CODE
  SELF.Init(JOBEXHIS,GlobalErrors)
  SELF.Buffer &= jxh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jxh:Ref_Number_Key,'By Date',0)
  SELF.AddKey(jxh:record_number_key,'jxh:record_number_key',1)
  Access:JOBEXHIS &= SELF


Hide:Relate:JOBEXHIS.Init PROCEDURE
  CODE
  Hide:Access:JOBEXHIS.Init
  SELF.Init(Access:JOBEXHIS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBEXHIS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBEXHIS &= NULL


Hide:Relate:JOBEXHIS.Kill PROCEDURE

  CODE
  Hide:Access:JOBEXHIS.Kill
  PARENT.Kill
  Relate:JOBEXHIS &= NULL


Hide:Access:STOPARTS.Init PROCEDURE
  CODE
  SELF.Init(STOPARTS,GlobalErrors)
  SELF.Buffer &= spt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(spt:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(spt:DateChangedKey,'By Date Changed',0)
  SELF.AddKey(spt:LocationDateKey,'By Date Changed',0)
  SELF.AddKey(spt:LocationNewPartKey,'By New Part Number',0)
  SELF.AddKey(spt:LocationOldPartKey,'By Old Part Number',0)
  Access:STOPARTS &= SELF


Hide:Relate:STOPARTS.Init PROCEDURE
  CODE
  Hide:Access:STOPARTS.Init
  SELF.Init(Access:STOPARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)


Hide:Access:STOPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOPARTS &= NULL


Hide:Relate:STOPARTS.Kill PROCEDURE

  CODE
  Hide:Access:STOPARTS.Kill
  PARENT.Kill
  Relate:STOPARTS &= NULL


Hide:Access:ORDPARTS.Init PROCEDURE
  CODE
  SELF.Init(ORDPARTS,GlobalErrors)
  SELF.Buffer &= orp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(orp:Order_Number_Key,'By Order Number',0)
  SELF.AddKey(orp:record_number_key,'orp:record_number_key',1)
  SELF.AddKey(orp:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(orp:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(orp:Description_Key,'By Description',0)
  SELF.AddKey(orp:Received_Part_Number_Key,'By Part Number',0)
  SELF.AddKey(orp:Account_Number_Key,'orp:Account_Number_Key',0)
  SELF.AddKey(orp:Allocated_Key,'orp:Allocated_Key',0)
  SELF.AddKey(orp:Allocated_Account_Key,'orp:Allocated_Account_Key',0)
  SELF.AddKey(orp:Order_Type_Received,'By Part Number',0)
  SELF.AddKey(orp:PartRecordNumberKey,'By Record Number',0)
  Access:ORDPARTS &= SELF


Hide:Relate:ORDPARTS.Init PROCEDURE
  CODE
  Hide:Access:ORDPARTS.Init
  SELF.Init(Access:ORDPARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ORDERS)
  SELF.AddRelation(Relate:STOCK)


Hide:Access:ORDPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDPARTS &= NULL


Hide:Access:ORDPARTS.PrimeFields PROCEDURE

  CODE
  orp:All_Received = 'NO'
  orp:Allocated_To_Sale = 'NO'
  PARENT.PrimeFields


Hide:Relate:ORDPARTS.Kill PROCEDURE

  CODE
  Hide:Access:ORDPARTS.Kill
  PARENT.Kill
  Relate:ORDPARTS &= NULL


Hide:Access:LOCATION.Init PROCEDURE
  CODE
  SELF.Init(LOCATION,GlobalErrors)
  SELF.Buffer &= loc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(loc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(loc:Location_Key,'By Location',0)
  SELF.AddKey(loc:Main_Store_Key,'By Location',0)
  SELF.AddKey(loc:ActiveLocationKey,'By Location',0)
  SELF.AddKey(loc:ActiveMainStoreKey,'By Location',0)
  SELF.AddKey(loc:VirtualLocationKey,'By Location',0)
  SELF.AddKey(loc:VirtualMainStoreKey,'By Location',0)
  SELF.AddKey(loc:FaultyLocationKey,'By Location',0)
  Access:LOCATION &= SELF


Hide:Relate:LOCATION.Init PROCEDURE
  CODE
  Hide:Access:LOCATION.Init
  SELF.Init(Access:LOCATION,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANMARK,RI:CASCADE,RI:None,mak:SiteLocationOnlyKey)
  SELF.AddRelationLink(loc:Location,mak:SiteLocation)
  SELF.AddRelation(Relate:LOCVALUE,RI:CASCADE,RI:CASCADE,lov:DateKey)
  SELF.AddRelationLink(loc:Location,lov:Location)
  SELF.AddRelation(Relate:LOCSHELF,RI:CASCADE,RI:CASCADE,los:Shelf_Location_Key)
  SELF.AddRelationLink(loc:Location,los:Site_Location)
  SELF.AddRelation(Relate:STOCK,RI:CASCADE,RI:CASCADE,sto:Location_Key)
  SELF.AddRelationLink(loc:Location,sto:Location)


Hide:Access:LOCATION.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCATION &= NULL


Hide:Access:LOCATION.PrimeFields PROCEDURE

  CODE
  loc:Main_Store = 'NO'
  loc:Active = 0
  loc:VirtualSite = 0
  loc:Level1 = 1
  loc:Level2 = 1
  loc:Level3 = 1
  loc:UseRapidStock = 0
  PARENT.PrimeFields


Hide:Relate:LOCATION.Kill PROCEDURE

  CODE
  Hide:Access:LOCATION.Kill
  PARENT.Kill
  Relate:LOCATION &= NULL


Hide:Access:STOHISTE.Init PROCEDURE
  CODE
  SELF.Init(STOHISTE,GlobalErrors)
  SELF.Buffer &= stoe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stoe:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(stoe:SHIRecordNumberKey,'stoe:SHIRecordNumberKey',0)
  SELF.AddKey(stoe:KeyArcStatus,'stoe:KeyArcStatus',0)
  Access:STOHISTE &= SELF


Hide:Relate:STOHISTE.Init PROCEDURE
  CODE
  Hide:Access:STOHISTE.Init
  SELF.Init(Access:STOHISTE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOHIST)


Hide:Access:STOHISTE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOHISTE &= NULL


Hide:Relate:STOHISTE.Kill PROCEDURE

  CODE
  Hide:Access:STOHISTE.Kill
  PARENT.Kill
  Relate:STOHISTE &= NULL


Hide:Access:STOMPFAU.Init PROCEDURE
  CODE
  SELF.Init(STOMPFAU,GlobalErrors)
  SELF.Buffer &= stu:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stu:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(stu:FieldKey,'By Field',0)
  SELF.AddKey(stu:DescriptionKey,'By Description',0)
  SELF.AddKey(stu:ManufacturerFieldKey,'By Field',0)
  Access:STOMPFAU &= SELF


Hide:Relate:STOMPFAU.Init PROCEDURE
  CODE
  Hide:Access:STOMPFAU.Init
  SELF.Init(Access:STOMPFAU,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOMODEL)
  SELF.AddRelation(Relate:MANFPALO)


Hide:Access:STOMPFAU.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOMPFAU &= NULL


Hide:Relate:STOMPFAU.Kill PROCEDURE

  CODE
  Hide:Access:STOMPFAU.Kill
  PARENT.Kill
  Relate:STOMPFAU &= NULL


Hide:Access:STOMJFAU.Init PROCEDURE
  CODE
  SELF.Init(STOMJFAU,GlobalErrors)
  SELF.Buffer &= stj:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stj:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(stj:FieldKey,'By Field',0)
  Access:STOMJFAU &= SELF


Hide:Relate:STOMJFAU.Init PROCEDURE
  CODE
  Hide:Access:STOMJFAU.Init
  SELF.Init(Access:STOMJFAU,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOMODEL)


Hide:Access:STOMJFAU.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOMJFAU &= NULL


Hide:Relate:STOMJFAU.Kill PROCEDURE

  CODE
  Hide:Access:STOMJFAU.Kill
  PARENT.Kill
  Relate:STOMJFAU &= NULL


Hide:Access:STOESN.Init PROCEDURE
  CODE
  SELF.Init(STOESN,GlobalErrors)
  SELF.Buffer &= ste:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ste:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(ste:Sold_Key,'By Serial Number',0)
  Access:STOESN &= SELF


Hide:Relate:STOESN.Init PROCEDURE
  CODE
  Hide:Access:STOESN.Init
  SELF.Init(Access:STOESN,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)


Hide:Access:STOESN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOESN &= NULL


Hide:Access:STOESN.PrimeFields PROCEDURE

  CODE
  ste:Sold = 'NO'
  PARENT.PrimeFields


Hide:Relate:STOESN.Kill PROCEDURE

  CODE
  Hide:Access:STOESN.Kill
  PARENT.Kill
  Relate:STOESN &= NULL


Hide:Access:SUPPLIER.Init PROCEDURE
  CODE
  SELF.Init(SUPPLIER,GlobalErrors)
  SELF.Buffer &= sup:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sup:RecordNumberKey,'sup:RecordNumberKey',1)
  SELF.AddKey(sup:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(sup:Company_Name_Key,'By Company Name',0)
  Access:SUPPLIER &= SELF


Hide:Relate:SUPPLIER.Init PROCEDURE
  CODE
  Hide:Access:SUPPLIER.Init
  SELF.Init(Access:SUPPLIER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPVALA,RI:CASCADE,RI:CASCADE,suva:RunDateKey)
  SELF.AddRelationLink(sup:Company_Name,suva:Supplier)
  SELF.AddRelation(Relate:SUPVALB,RI:CASCADE,RI:CASCADE,suvb:RunDateKey)
  SELF.AddRelationLink(sup:Company_Name,suvb:Supplier)
  SELF.AddRelation(Relate:ORDPEND,RI:CASCADE,RI:None,ope:Supplier_Name_Key)
  SELF.AddRelationLink(sup:Company_Name,ope:Supplier)
  SELF.AddRelation(Relate:ESTPARTS,RI:CASCADE,RI:None,epr:Supplier_Key)
  SELF.AddRelationLink(sup:Company_Name,epr:Supplier)
  SELF.AddRelation(Relate:PARTS,RI:CASCADE,RI:None,par:Supplier_Key)
  SELF.AddRelationLink(sup:Company_Name,par:Supplier)
  SELF.AddRelation(Relate:WARPARTS,RI:CASCADE,RI:None,wpr:Supplier_Key)
  SELF.AddRelationLink(sup:Company_Name,wpr:Supplier)


Hide:Access:SUPPLIER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUPPLIER &= NULL


Hide:Access:SUPPLIER.PrimeFields PROCEDURE

  CODE
  sup:UseForeignCurrency = 0
  sup:EVO_Excluded = 0
  PARENT.PrimeFields


Hide:Relate:SUPPLIER.Kill PROCEDURE

  CODE
  Hide:Access:SUPPLIER.Kill
  PARENT.Kill
  Relate:SUPPLIER &= NULL


Hide:Access:COUBUSHR.Init PROCEDURE
  CODE
  SELF.Init(COUBUSHR,GlobalErrors)
  SELF.Buffer &= cbh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cbh:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(cbh:TypeDateKey,'By Date',0)
  SELF.AddKey(cbh:EndTimeKey,'By End Time',0)
  Access:COUBUSHR &= SELF


Hide:Relate:COUBUSHR.Init PROCEDURE
  CODE
  Hide:Access:COUBUSHR.Init
  SELF.Init(Access:COUBUSHR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COURIER)


Hide:Access:COUBUSHR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COUBUSHR &= NULL


Hide:Access:COUBUSHR.PrimeFields PROCEDURE

  CODE
  cbh:ExceptionType = 0
  PARENT.PrimeFields


Hide:Relate:COUBUSHR.Kill PROCEDURE

  CODE
  Hide:Access:COUBUSHR.Kill
  PARENT.Kill
  Relate:COUBUSHR &= NULL


Hide:Access:SUPVALA.Init PROCEDURE
  CODE
  SELF.Init(SUPVALA,GlobalErrors)
  SELF.Buffer &= suva:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(suva:RecordNumberKey,'suva:RecordNumberKey',1)
  SELF.AddKey(suva:RunDateKey,'By Run Date',0)
  SELF.AddKey(suva:DateOnly,'suva:DateOnly',0)
  Access:SUPVALA &= SELF


Hide:Relate:SUPVALA.Init PROCEDURE
  CODE
  Hide:Access:SUPVALA.Init
  SELF.Init(Access:SUPVALA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)


Hide:Access:SUPVALA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUPVALA &= NULL


Hide:Relate:SUPVALA.Kill PROCEDURE

  CODE
  Hide:Access:SUPVALA.Kill
  PARENT.Kill
  Relate:SUPVALA &= NULL


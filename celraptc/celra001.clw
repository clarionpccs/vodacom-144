

   MEMBER('celraptc.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


Startup              PROCEDURE                        ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('pcancel.ico'),HIDE
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:DEFAULTS.Open
   Relate:REPSCHLG.Open
   Relate:REPSCHCR.Open
   Relate:MANUFACT_ALIAS.Open
    If Command('/SCHEDULE')

        x# = Instring('%',Command(),1,1)
        Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
        rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
        If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
            !Found
            Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
            rpc:ReportName = rpd:ReportName
            rpc:ReportCriteriaType = rpd:ReportCriteriaType
            If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
                !Found
            End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
        Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
            !Error
        End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
    End ! If Command('/SCHEDULE')


    Do Prog:ProgressSetup
    Prog:TotalRecords = Records(MANUFACT_ALIAS)
    Prog:ShowPercentage = 1 !Show Percentage Figure
    Access:MANUFACT_ALIAS.Clearkey(man_ali:RecordNumberKey)
    man_ali:RecordNumber = 1
    Set(man_ali:RecordNumberKey,man_ali:RecordNumberKey)
    Accept
        Case Event()
        Of Event:Timer
            Loop 25 Times
                !Inside Loop
                If Access:MANUFACT_ALIAS.Next()
                    Prog:Exit = 1
                    Break
                End ! If Access:MANUFACT.Next()
                Do Prog:UpdateScreen
                Prog:RecordCount += 1
                If ~man_ali:AutoTechnicalReports
                    Cycle
                End ! If ~man_ali:AutoTechnicalReports
                ?Prog:UserString{Prop:Text} = 'Manufacturer: ' & man_ali:Manufacturer

                If GenericEDI(man_ali:Manufacturer,Date(Month(Today()),1,2000),Today(),1,'','',str")
                    SendEmail()
                    If Command('/SCHEDULE')
                        If Access:REPSCHLG.PrimeRecord() = Level:Benign
                            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                              '<13,10>' & Clip(man_ali:Manufacturer) & ' failed.'
                            If Access:REPSCHLG.TryInsert() = Level:Benign
                                !Insert
                            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                                Access:REPSCHLG.CancelAutoInc()
                            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
                        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
                    End ! If Command('/SCHEDULE')

                    Cycle
                End ! If GenericEDI(man_ali:Manufacturer,0,Date(Month(Today()),1,Year(Today())),Today(),1,'','')
            End ! Loop 25 Times
        Of Event:CloseWindow
            Prog:Exit = 1
            Prog:Cancelled = 1
            Break
        Of Event:Accepted
            If Field() = ?Prog:Cancel
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                Of 1 ! Yes
                    Prog:Exit = 1
                    Prog:Cancelled = 1
                    Break
                Of 2 ! No
                End ! Case Message
            End! If FIeld()
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    End ! If Command('/SCHEDULE')
   Relate:DEFAULTS.Close
   Relate:REPSCHLG.Close
   Relate:REPSCHCR.Close
   Relate:MANUFACT_ALIAS.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SendEmail PROCEDURE                                   !Generated from procedure template - Window

FilesOpened          BYTE
MyText               STRING(30000)
MyHTML               STRING(30000)
MyTo                 STRING(255)
MyFrom               STRING(255)
MyServer             STRING(255)
MySubject            STRING(255)
MyHelo               STRING(80)
Window               WINDOW('NetTalk Email Send Jump Start Example'),AT(,,63,27),FONT('MS Sans Serif',8,,FONT:regular),IMM,SYSTEM,GRAY,DOUBLE
                       BUTTON('Send'),AT(8,4,50,14),USE(?SendMail)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisEmailSend        CLASS(NetEmailSend)              !Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SendEmail')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SendMail
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
                                               ! Generated by NetTalk Extension (Start)
  ThisEmailSend.init()
  if ThisEmailSend.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  SELF.SetAlerts()
  0{prop:Hide} = 1
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  Post(Event:Accepted,?SendMail)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisEmailSend.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SendMail
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SendMail, Accepted)
      ThisEmailSend.Server = Clip(def:EmailServerAddress)
      ThisEmailSend.Port = Clip(def:EmailServerPort)
      ThisEmailSend.ToList = Clip(man:AlertEmailAddress)
      ThisEmailSend.From = def:EmailAddress
      ThisEmailSend.Subject = Clip(Man:Manufacturer) & ' Technical Report'
      ThisEmailSend.AttachmentList = '' ! Could be 'c:\test.txt'
      
      ThisEmailSend.AuthUser = Clip(de2:EmailUserName)
      ThisEmailSend.AuthPassword = Clip(de2:EmailPassword)
      
      MyText = 'Please note that the technical report failed on ' & Format(Today(),@d06)
      
      ThisEmailSend.SetRequiredMessageSize (0, len(clip(MyText)), len(clip(MyHTML)))
            !You must call this previous function
            !before populating self.MesageText
      if ThisEmailSend.Error = 0 ! Check for error
        ThisEmailSend.MessageText = clip (MyText)
        setcursor (CURSOR:WAIT)
        display()
        ThisEmailSend.SendMail(NET:EMailMadeFromPartsMode) ! Put email in queue and start sending it
        setcursor
        display()
      end 
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SendMail, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisEmailSend.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisEmailSend.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
  ! Before Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, ErrorTrap, (string errorStr,string functionName))
  ! This is the method that is called when an error occurs while 
  ! sending an email. If you do not suppress Errors (see the setting  
  ! in the procedure extension), NetTalk will automatically display  
  ! an error message for you. When you send an email using the  
  ! SendMail() method the message is queued in the DataQueue (the  
  ! outgoing queue). If the message is sent successfully the  
  ! MessageSent method is called, if an error occurs the ErrorTrap  
  ! method is called. 
  !Message ('Message was not sent')
  
  Post(Event:CloseWindow)
  PARENT.ErrorTrap(errorStr,functionName)
  ! After Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, ErrorTrap, (string errorStr,string functionName))


ThisEmailSend.MessageSent PROCEDURE


  CODE
  ! Before Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, MessageSent, ())
  ! This is the method that is called when a message is sent
  ! The first thing that we do here is work out how many emails
  ! are sitting in the OutGoing Queue. (When you call the SendMail()
  ! method the message is put into the OutGoing Queue (called 
  ! self.DataQueue)). 
  ! The last message sent is still in self.DataQueue, so when 
  ! records (self.DataQueue) = 1, you know there are no more 
  ! emails in the out queue and all the emails have been sent.
  if records (self.DataQueue) = 1  ! Last sent email still in queue
    !Message (Clip(man:Manufacturer) & 'Email Sent Successfully', 'Email Send', ICON:ASTERISK)
  end
  
  Post(Event:CloseWindow)
  PARENT.MessageSent
  ! After Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, MessageSent, ())


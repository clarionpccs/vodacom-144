

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01019.INC'),ONCE        !Local module procedure declarations
                     END


IsIMEIExchangeLoan   PROCEDURE  (fIMEINumber,fShowError) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:EXCHANGE.Open
   Relate:LOAN.Open
    error# = 0
    Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
    xch:ESN    = fIMEINumber
    if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
        ! Found
        if (xch:Available = 'AVL')
            if (fShowError = 1)
                Beep(Beep:SystemHand)  ;  Yield()
                Case Missive('Error! The selected unit is available in another location.','ServiceBase',|
                               'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                End!Case Message
            end !if (fShowError = 1)
            error# = 1
        end ! if (xch:Available <> 'AVL')
    else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
        ! Error
    end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)

    if (error# = 0)
        Access:LOAN.Clearkey(loa:ESN_Only_Key)
        loa:ESN    = fIMEINumber
        if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
            ! Found
            if (loa:Available = 'AVL')
                if (fShowError = 1)
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('Error! The selected unit is available in another location.','ServiceBase',|
                                   'mstop.jpg','/&OK')
                        Of 1 ! &OK Button
                    End!Case Message
                end !if (fShowError = 1)
                error# = 1
            end ! if (loa:Available = 'AVL')
        else ! if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
            ! Error
        end ! if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
    end !if (error# = 0)

    return error#
   Relate:EXCHANGE.Close
   Relate:LOAN.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ExcludeModelFromRebooking PROCEDURE  (fManufacturer,fModelNumber) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_MODELNUM_id            USHORT(),AUTO
save_MANUFACTURER_id        USHORT(),AUTO
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:MANUFACT.Open
   Relate:MODELNUM.Open
    save_MODELNUM_ID = Access:MODELNUM.SaveFile()
    save_MANUFACTURER_ID = Access:MANUFACT.SaveFile()

    return# = 0

    if (fManufacturer <> '')
        Access:MANUFACT.Clearkey(man:Manufacturer_Key)
        man:Manufacturer    = fManufacturer
        if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            ! Found
            if (man:ExcludeAutomaticRebookingProcess)
                ! If manufacturer is set to exclude, then ALL models will be excluded (DBH: 05/05/2009) #10795
                return# = 1
            end ! if (man:ExcludeAutomaticRebookingProcess)
        else ! if (Access:MANUFACTURER.TryFetch(man:Manufacturer_Key) = Level:Benign)
            ! Error
        end ! if (Access:MANUFACTURER.TryFetch(man:Manufacturer_Key) = Level:Benign)
    end ! if (fManufacturer <> '')

    if (fModelNumber <> '' and return# = 0)
        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
        mod:Model_Number    = fModelNumber
        if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = mod:Manufacturer
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:ExcludeAutomaticRebookingProcess)
                    ! If manufacturer is set to exclude, then ALL models will be excluded (DBH: 05/05/2009) #10795
                    return# = 1
                end ! if (man:ExcludeAutomaticRebookingProcess)
            else ! if (Access:MANUFACTURER.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACTURER.TryFetch(man:Manufacturer_Key) = Level:Benign)

            if (return# = 0)
                if (mod:ExcludeAutomaticRebookingProcess)
                    return# = 1
                end ! if (mod:ExcludeAutomaticRebookingProcess)
            end ! if (return# = 0)
        else ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
    end ! if (fModelNumber <> '')

    Access:MANUFACT.RestoreFile(save_MANUFACTURER_ID)
    Access:MODELNUM.RestoreFile(save_MODELNUM_ID)

    return return#
   Relate:MANUFACT.Close
   Relate:MODELNUM.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CustCollectionValidated PROCEDURE  (fJobNumber)       ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return (VodacomClass.CustomerCollectionValidated(fJobNumber))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CustClassificationFields PROCEDURE  (LONG refNumber,*STRING lifeTimeValue,*STRING averageSpend,*STRING loyaltyStatus,*DATE upgradeDate,*STRING IDNumber) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    VodacomClass.CustomerClassificationFields(refNumber,|
                                    lifeTimeValue,|
                                    averageSpend,|
                                    loyaltyStatus,|
                                    upgradeDate,|
                                    IDNumber)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetTheMainOutFault   PROCEDURE  (LONG jobNumber,*STRING outFault,*STRING description) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    VodacomClass.GetMainOutFault(jobNumber,outFault,description)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IsOneYearWarranty    PROCEDURE  (STRING manufacturer,STRING modelNumber) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return (VodacomClass.OneYearWarranty(manufacturer,modelNumber))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
MainStoreSuspended   PROCEDURE  (String fPartNumber)  ! Declare Procedure
retValue             BYTE
save_stock_id        USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.MainStoreSuspended(fPartNumber))
!    relate:STOCK_ALIAS.open()
!
!    save_stock_id = Access:STOCK_ALIAS.SaveFile()
!
!    retValue = False
!
!    Access:STOCK_ALIAS.Clearkey(sto_ali:Location_Key)
!    sto_ali:Location = MainStoreLocation()
!    sto_ali:Part_Number = fPartNumber
!    If (Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign)
!        if (sto_ali:Suspend)
!            retValue = True
!        end
!    end !If (Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign)
!
!    Access:STOCK_ALIAS.RestoreFile(save_stock_id)
!
!    relate:STOCK_ALIAS.Close()
!
!    Return retValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StockAliasInUse      PROCEDURE  (func:ShowError)      ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Pointer# = Pointer(STOCK_ALIAS)
    Hold(STOCK_ALIAS,1)
    Get(STOCK_ALIAS,Pointer#)
    If Errorcode() = 43
        If func:ShowError
            Case Missive('The selected item is currently in use by another station.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If func:ShowError
        Return Level:Fatal
    End !If Errorcode() = 43
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SuspendedPartCheck   PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    ! Assumes we are in the stock record
    If (sto:Location <> MainStoreLocation())
        If (MainStoreSuspended(sto:Part_Number))
            if (sto:Quantity_Stock = 0)
                ! Suspend Item
                Beep(Beep:SystemExclamation)  ;  Yield()
                Case Missive('There are no more items in stock. '&|
                    '|This part has now been suspended.','ServiceBase',|
                               'mexclam.jpg','/&OK')
                Of 1 ! &OK Button
                End!Case Message
                sto:Suspend = 1
                If (Access:STOCK.TryUpdate() = Level:Benign)
                    if (AddToStockHistory(sto:Ref_Number, |
                                            'ADD',|
                                            '',|
                                            0, |
                                            0, |
                                            0, |
                                            sto:Purchase_Cost,|
                                            sto:Sale_Cost, |
                                            sto:Retail_Cost, |
                                            'PART SUSPENDED',|
                                            ''))
                    end
                ENd ! If (Access:STOCK.TryUpdate() = Level:Benign)
            else
                ! Show warning
                Beep(Beep:SystemExclamation)  ;  Yield()
                Case Missive('This part has been suspended at Main Store. It will not be available for ordering.'&|
                    '|'&|
                    '|Current Stock Level: ' & Clip(sto:Quantity_stock) & '.','ServiceBase',|
                               'mexclam.jpg','/&OK')
                Of 1 ! &OK Button
                End!Case Message
            end
        end ! If (MainStoreSuspened(sto:Part_Number))
    End
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SetupAccessLevels    PROCEDURE                        ! Declare Procedure
AccessLevel_Q        QUEUE,PRE()
LevelName            STRING(30)
                     END
RenameLevel_Q        QUEUE,PRE()
OldLevelName         STRING(30)
NewLevelName         STRING(30)
                     END
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:ACCAREAS.Open
   Relate:ALLLEVEL.Open
! Rename Levels & Remove any that dont exist
    Free(RenameLevel_Q)

    RenameLevel_Q.OldLevelName = 'RAPID LETTER PRINTING'
    RenameLevel_Q.NewLevelName = 'RAPID - LETTER PRINTING'
    Add(RenameLevel_Q)
    RenameLevel_Q.OldLevelName = 'RAPID EXCHANGE INSERT WIZARD'
    RenameLevel_Q.NewLevelName = 'RAPID - EXCHANGE INSERT WIZARD'
    Add(RenameLevel_Q)
    RenameLevel_Q.OldLevelName = 'RAPID EXCHANGE TRANSFER'
    RenameLevel_Q.NewLevelName = 'RAPID - EXCHANGE TRANSFER'
    Add(RenameLevel_Q)

    Prog.ProgressSetup(Records(ACCAREAS))

    Access:ACCAREAS.Clearkey(acc:AccessOnlyKey)
    acc:Access_Area = ''
    Set(acc:AccessOnlyKey,acc:AccessOnlyKey)
    Loop Until Access:ACCAREAS.Next()
        RenameLevel_Q.OldLevelName = acc:Access_Area
        Get(RenameLevel_Q,RenameLevel_Q.OldLevelName)
        If (NOT Error())
            acc:Access_Area = RenameLevel_Q.NewLevelName
            Access:ACCAREAS.TryUpdate()
        Else
            Access:ALLLEVEL.Clearkey(all:Access_Level_Key)
            all:Access_Level = acc:Access_Area
            If (Access:ALLLEVEL.TryFetch(all:Access_LEvel_Key))
                Access:ACCAREAS.DeleteRecord(0)
            End
        End
        if Prog.InsideLoop()
            Break
        End
    End

    Prog.ProgressFinish()
   Relate:ACCAREAS.Close
   Relate:ALLLEVEL.Close
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Source
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Open(Prog:ProgressWindow)
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()

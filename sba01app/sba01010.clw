

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01010.INC'),ONCE        !Local module procedure declarations
                     END


ExcludeHandlingFee   PROCEDURE  (func:Type,func:Manufacturer,func:RepairType) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Return Fatal if the Handling Fee should be excluded
    Case func:Type
        Of 'C'
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Chargeable   = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Found
                IF rtd:ExcludeHandlingFee
                    Return Level:Fatal
                End !IF rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
        Of 'W'
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Warranty     = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Found
                If rtd:ExcludeHandlingFee
                    Return Level:Fatal
                End !If rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
    End !Case func:Type
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobPricingRoutine    PROCEDURE  (func:ForceWarranty)  ! Declare Procedure
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_epr_id          USHORT,AUTO
tmp:ARCCLabourCost   REAL
tmp:RRCCLabourCost   REAL
tmp:ARCCPartsCost    REAL
tmp:RRCCPartsCost    REAL
tmp:ARCWLabourCost   REAL
tmp:RRCWLabourCost   REAL
tmp:ARCWPartsCost    REAL
tmp:RRCWPartsCost    REAL
tmp:ARCELabourCost   REAL
tmp:RRCELabourCost   REAL
tmp:ARCEPartsCost    REAL
tmp:RRCEPartsCost    REAL
tmp:ClaimPartsCost   REAL
tmp:UseStandardRate  BYTE(0)
tmp:ClaimValue       REAL
tmp:FixedCharge      BYTE(0)
tmp:ExcludeHandlingFee BYTE(0)
tmp:ManufacturerExchangeFee REAL
tmp:FixedChargeARC   BYTE(0)
tmp:FixedChargeRRC   BYTE(0)
tmp:ARCLocation      STRING(30)
tmp:RRCLocation      STRING(30)
tmp:SecondYearWarranty BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    ! Insert --- To ensure that I don't have to open files everytime I call this (DBH: 19/03/2009) #10473
    Relate:PARTS.Open()
    Relate:STOCK.Open()
    Relate:ESTPARTS.Open()
    Relate:WARPARTS.Open()
    Relate:CHARTYPE.Open()
    Relate:SUBCHRGE.Open()
    Relate:TRACHRGE.Open()
    Relate:STDCHRGE.Open()
    Relate:MANUFACT.Open()
    Relate:TRDPARTY.Open()
    Relate:WEBJOB.Open()
    Relate:JOBSE2.Open()

    !** Assumes JOBS and JOBSE are open and are in the right record **!

    ! end --- (DBH: 19/03/2009) #10473

    tmp:FixedCharge = 0
    tmp:ExcludeHandlingFee = 0
    tmp:ManufacturerExchangeFee = 0
    tmp:FixedChargeARC = 0
    tmp:FixedChargeRRC = 0
    tmp:ClaimPartsCost = 0
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    if glo:RelocateStore then   !JC 28/05/2012 - TB12031 - need to match the location to "MAIN STORE"
        tra:Account_number = 'AA10'
    ELSE
        tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    END
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        tmp:ARCLocation = tra:SiteLocation
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber  = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            tmp:RRCLocation = tra:SiteLocation
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign

    If job:Chargeable_Job <> 'YES'
        !If not chargeable job, blank chargeable costs - L874 (DBH: 16-07-2003)
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        job:Ignore_Chargeable_Charges = 'NO'
        jobe:IgnoreRRCChaCosts = 0

    Else !If job:Chargeable_Job <> 'YES'
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        jobe:HandlingFee    = 0
        jobe:ExchangeRate   = 0

        If ExcludeHandlingFee('C',job:Manufacturer,job:Repair_Type)
            tmp:ExcludeHandlingFee = 1
        End !If ExcludeHandingFee('C',job:Manfacturer,job:Repair_Type)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type = job:Charge_Type
        If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Found
            If cha:Zero_Parts_ARC
                tmp:FixedChargeARC = 1
            End !If cha:Zero_Parts_ARC = 'YES'
            If cha:Zero_Parts = 'YES'
                tmp:FixedChargeRRC = 1
            End !If cha:Zero_Parts = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If


                If ~cha:Zero_Parts_ARC
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            If job:Estimate = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = job:Ref_Number
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:ARCCPartsCost += epr:Sale_Cost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If job:Estimate = 'YES'
                                tmp:ARCCPartsCost += par:Sale_Cost * par:Quantity
                            End !If job:Estimate = 'YES'
                        End !If sto:Location = tmp:ARCLocatin
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts_ARC <> 'YES'
                    If job:Invoice_Number = 0
                        par:Sale_Cost = 0
                        Access:PARTS.Update()
                    End !If job:Invoice_Number = 0
                End !If cha:Zero_Parts_ARC <> 'YES'
                If cha:Zero_Parts <> 'YES'
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            If job:Estimate = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = job:Ref_Number
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:RRCCPartsCost += epr:RRCSaleCost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If job:Estimate = 'YES'
                                tmp:RRCCPartsCost += par:RRCSaleCost * par:Quantity
                            End !If job:Estimate = 'YES'

                        End !If sto:Location <> tmp:RRCLocation
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts <> 'YES'
                    If job:Invoice_Number = 0
                        par:RRCSaleCost = 0
                        Access:PARTS.Update()
                    End !If job:Invoice_Number = 0
                End !If cha:Zero_Parts <> 'YES'
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(job:Account_Number)
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = job:account_number
                suc:model_number   = job:model_number
                suc:charge_type    = job:charge_type
                suc:unit_type      = job:unit_type
                suc:repair_type    = job:repair_type
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = job:model_number
                    trc:charge_type    = job:charge_type
                    trc:unit_type      = job:unit_type
                    trc:repair_type    = job:repair_type
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCCLabourCost      = trc:Cost
                        tmp:RRCCLabourCost      = trc:RRCRate
                        jobe:HandlingFee    = trc:HandlingFee
                        jobe:ExchangeRate   = trc:Exchange
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCCLabourCost      = suc:Cost
                    tmp:RRCCLabourCost      = suc:RRCRate
                    jobe:HandlingFee    = suc:HandlingFee
                    jobe:ExchangeRate   = suc:Exchange

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = job:model_number
                trc:charge_type    = job:charge_type
                trc:unit_type      = job:unit_type
                trc:repair_type    = job:repair_type
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCCLabourCost      = trc:Cost
                    tmp:RRCCLabourCost      = trc:RRCRate
                    jobe:HandlingFee    = trc:HandlingFee
                    jobe:ExchangeRate   = trc:Exchange
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = job:model_number
                sta:charge_type  = job:charge_type
                sta:unit_type    = job:unit_type
                sta:repair_type  = job:repair_type
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCCLabourCost      = sta:Cost
                    tmp:RRCCLabourCost      = sta:RRCRate
                    jobe:HandlingFee    = sta:HandlingFee
                    jobe:ExchangeRate   = sta:Exchange
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Error
        End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Dont blank the cost because of their cock up .
        !They used this field for other costs, and now want to see it
        !even if it's not a loan
!        If ~LoanAttachedToJob(job:Ref_Number)
!            job:Courier_Cost = 0
!            job:Courier_Cost_Estimate = 0
!        End !If LoanAttachedToJob(job:Ref_Number)
    End !job:Chargeable_Job = 'YES'

    tmp:SecondYearWarranty = 0
    If job:Warranty_Job <> 'YES'
        !Blank costs if not a Warranty Job - L874 (DBH: 16-07-2003)
        tmp:ARCWLabourCost  = 0
        tmp:ARCWPartsCost   = 0
        tmp:RRCWLabourCost  = 0
        tmp:RRCWPartsCost   = 0
        tmp:ClaimValue      = 0
        job:Ignore_Warranty_Charges = 'NO'
        jobe:IgnoreRRCWarCosts = 0
        jobe:IgnoreClaimCosts = 0
    Else !If job:Warranty_Job = 'YES'
        !Do not reprice Warranty Completed Jobs - L945  (DBH: 03-09-2003)
        !Allow to force the reprice if the job conditions change - L945 (DBH: 04-09-2003)
        If job:Date_Completed = 0 Or func:ForceWarranty
            tmp:ARCWLabourCost  = 0
            tmp:ARCWPartsCost   = 0
            tmp:RRCWLabourCost  = 0
            tmp:RRCWPartsCost   = 0
            jobe:HandlingFee    = 0
            jobe:ExchangeRate   = 0

            If ExcludeHandlingFee('W',job:Manufacturer,job:Repair_Type_Warranty)
                tmp:ExcludeHandlingFee = 1
            End !If ExcludeHandingFee('C',job:Manfacturer,job:Repair_Type)

            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = job:Warranty_Charge_Type
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                tmp:SecondYearWarranty = cha:SecondYearWarranty


                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                        Then Break.  ! End If

                    tmp:ClaimPartsCost += wpr:Purchase_Cost * wpr:Quantity

                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            tmp:ARCWPartsCost += wpr:Purchase_Cost * wpr:Quantity
                        End !If sto:Location = tmp:ARCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            tmp:RRCWPartsCost += wpr:RRCPurchaseCost * wpr:Quantity
                        End !If sto:Location = tmp:RRCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)

                tmp:UseStandardRate = 1
                If InvoiceSubAccounts(job:Account_Number)
                    access:subchrge.clearkey(suc:model_repair_type_key)
                    suc:account_number = job:account_number
                    suc:model_number   = job:model_number
                    suc:charge_type    = job:Warranty_charge_type
                    suc:unit_type      = job:unit_type
                    suc:repair_type    = job:repair_type_Warranty
                    if access:subchrge.fetch(suc:model_repair_type_key)
                        access:trachrge.clearkey(trc:account_charge_key)
                        trc:account_number = sub:main_account_number
                        trc:model_number   = job:model_number
                        trc:charge_type    = job:Warranty_charge_type
                        trc:unit_type      = job:unit_type
                        trc:repair_type    = job:repair_type_Warranty
                        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                            tmp:ARCWLabourCost      = trc:Cost
                            tmp:RRCWLabourCost      = trc:RRCRate
                            tmp:ClaimValue          = trc:WarrantyClaimRate
                            jobe:HandlingFee    = trc:HandlingFee
                            jobe:ExchangeRate   = trc:Exchange
                            tmp:useStandardRate = 0
                        End!if access:trachrge.fetch(trc:account_charge_key)
                    Else
                        tmp:ARCWLabourCost      = suc:Cost
                        tmp:RRCWLabourCost      = suc:RRCRate
                        tmp:ClaimValue          = suc:WarrantyClaimRate
                        jobe:HandlingFee    = suc:HandlingFee
                        jobe:ExchangeRate   = suc:Exchange

                        tmp:useStandardRate = 0
                    End!if access:subchrge.fetch(suc:model_repair_type_key)
                Else !If InvoiceSubAccounts(job:Account_Number)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = job:model_number
                    trc:charge_type    = job:Warranty_charge_type
                    trc:unit_type      = job:unit_type
                    trc:repair_type    = job:repair_type_Warranty
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCWLabourCost      = trc:Cost
                        tmp:RRCWLabourCost      = trc:RRCRate
                        tmp:ClaimValue          = trc:WarrantyClaimRate
                        jobe:HandlingFee    = trc:HandlingFee
                        jobe:ExchangeRate   = trc:Exchange
                        tmp:useStandardRate = 0

                    End!if access:trachrge.fetch(trc:account_charge_key)

                End !If InvoiceSubAccounts(job:Account_Number)

                If tmp:useStandardRate
                    access:stdchrge.clearkey(sta:model_number_charge_key)
                    sta:model_number = job:model_number
                    sta:charge_type  = job:Warranty_charge_type
                    sta:unit_type    = job:unit_type
                    sta:repair_type  = job:repair_type_Warranty
                    if access:stdchrge.fetch(sta:model_number_charge_key)
                    Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                        tmp:ARCWLabourCost      = sta:Cost
                        tmp:RRCWLabourCost      = sta:RRCRate
                        tmp:ClaimValue          = sta:WarrantyClaimRate
                        jobe:HandlingFee    = sta:HandlingFee
                        jobe:ExchangeRate   = sta:Exchange
                    end !if access:stdchrge.fetch(sta:model_number_charge_key)
                End !If tmp:useStandardRate
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = job:Manufacturer
            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Found
                If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:SecondYrExchangeFee
                Else !If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:ExchangeFee
                End !If tmp:SecondYearWarranty

            Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Error
            End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        End !If job:Date_Completed = 0
    End !job:Warranty_Job = 'YES'

    tmp:ARCELabourCost = 0
    tmp:ARCEPartsCost  = 0
    tmp:RRCELabourCost = 0
    tmp:RRCEPartsCost   = 0
    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type = job:Charge_Type
    If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Found
        Save_epr_ID = Access:ESTPARTS.SaveFile()
        Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
        epr:Ref_Number  = job:Ref_Number
        Set(epr:Part_Number_Key,epr:Part_Number_Key)
        Loop
            If Access:ESTPARTS.NEXT()
               Break
            End !If
            If epr:Ref_Number  <> job:Ref_Number      |
                Then Break.  ! End If
            If ~cha:Zero_Parts_ARC
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:ARCLocation
                        tmp:ARCEPartsCost += epr:Sale_Cost * epr:Quantity
                    End !If sto:Location = tmp:ARCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts_ARC <> 'YES'
                If job:Invoice_Number = 0
                    epr:Sale_Cost = 0
                    Access:ESTPARTS.Update()
                End !If job:Invoice_Number = 0
            End !If cha:Zero_Parts_ARC <> 'YES'

            If cha:Zero_Parts <> 'YES'
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:RRCLocation
                        tmp:RRCEPartsCost += epr:RRCSaleCost * epr:Quantity
                    End !If sto:Location = tmp:RRCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts <> 'YES'
                If job:Invoice_Number = 0
                    epr:RRCSaleCost = 0
                    Access:ESTPARTS.Update()
                End !If job:Invoice_Number = 0
            End !If cha:Zero_Parts <> 'YES'
        End !Loop
        Access:ESTPARTS.RestoreFile(Save_epr_ID)
        If job:Estimate = 'YES' And job:Estimate_Accepted = 'YES'
                !If estimate, then use the estimate cost.
                !it doesn't matter what the current costs are
                tmp:ARCELabourCost  = job:Labour_Cost_Estimate
                tmp:RRCELabourCost  = jobe:RRCELabourCost
                tmp:ARCCLabourCost  = job:Labour_Cost_Estimate
                tmp:RRCCLabourCost  = jobe:RRCELabourCost

                ! Inserting (DBH 03/12/2007) # 8218 - For RRC-ARC estimate, the RRC costs must mirror the ARC costs
                If SentToHub(job:Ref_Number) And jobe:WebJob
                    tmp:RRCCLabourCost = tmp:ARCCLabourCost
                    tmp:RRCCPartsCost = tmp:ARCCPartsCost
                End ! If SentToHub(job:Ref_Number) And jobe:WebJob
                ! End (DBH 03/12/2007) #8218
        Else !If job:Estimate = 'YES' And job:Estimate_Accepted = 'YES'

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(job:Account_Number)
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = job:account_number
                suc:model_number   = job:model_number
                suc:charge_type    = job:charge_type
                suc:unit_type      = job:unit_type
                suc:repair_type    = job:repair_type
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = job:model_number
                    trc:charge_type    = job:charge_type
                    trc:unit_type      = job:unit_type
                    trc:repair_type    = job:repair_type
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCELabourCost = trc:Cost
                        tmp:RRCELabourCost  = trc:RRCRate
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCELabourCost = suc:Cost
                    tmp:RRCELabourCost = suc:RRCRate

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = job:model_number
                trc:charge_type    = job:charge_type
                trc:unit_type      = job:unit_type
                trc:repair_type    = job:repair_type
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCELabourCost     = trc:Cost
                    tmp:RRCELabourCost  = trc:RRCRate
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = job:model_number
                sta:charge_type  = job:charge_type
                sta:unit_type    = job:unit_type
                sta:repair_type  = job:repair_type
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCELabourCost = sta:Cost
                    tmp:RRCELabourCost = sta:RRCRate
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        End !If job:Estimate = 'YES' And job:Estimate_Accepted = 'YES'
    Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Error
    End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

    If job:Exchange_Unit_Number <> 0
        If jobe:ExchangedAtRRC
            jobe:HandlingFee = 0
        Else !If jobe:ExchangedAtRRC
            jobe:ExchangeRate = 0
        End !If jobe:ExchangedAtRRC
    Else !job:Exchange_Unit_Number <> 0
        If ~SentToHub(job:Ref_Number)
            jobe:HandlingFee = 0
        End !If ~SentToHub(job:Ref_Number)
        jobe:ExchangeRate = 0
    End !job:Exchange_Unit_Number <> 0

    If tmp:ExcludeHandlingFee
        jobe:HandlingFee = 0
    End !If tmp:ExcludeHandlingFee

    If job:Ignore_Chargeable_Charges <> 'YES'
        job:Labour_Cost     = tmp:ARCCLabourCost
        job:Parts_Cost      = tmp:ARCCPartsCost
        If job:Third_Party_Site <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = job:Third_Party_Site
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If ~jobe:Ignore3rdPartyCosts
                    If jobe:ARC3rdPartyMarkup = 0
                        jobe:ARC3rdPartyMarkup = trd:Markup
                    End !If jobe:ARC3rdPartyMarkup = 0
                End !If ~jobe:Ignore3rdPartyCosts
                jobe:ARC3rdPartyVAT = jobe:ARC3rdPartyCost * (trd:VatRate/100)
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            If tmp:FixedChargeARC = 0
                job:Labour_Cost = jobe:ARC3rdPartyCost + jobe:ARC3rdPartyMarkup
            End !If tmp:FixedChargeARC = 0
        End !If job:Third_Party_Site <> ''
    End !job:Ignore_Chargeable_Charges <> 'YES'

    If ~jobe:IgnoreRRCChaCosts
        jobe:RRCCLabourCost = tmp:RRCCLabourCost

        If job:Third_Party_Site <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = job:Third_Party_Site
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If ~jobe:Ignore3rdPartyCosts
                    If jobe:ARC3rdPartyMarkup = 0
                        jobe:ARC3rdPartyMarkup = trd:Markup
                    End !If jobe:ARC3rdPartyMarkup = 0
                End !If ~jobe:Ignore3rdPartyCosts
                jobe:ARC3rdPartyVAT = jobe:ARC3rdPartyCost * (trd:VatRate/100)
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

            If tmp:FixedChargeRRC = 0
                jobe:RRCCLabourCost = jobe:ARC3rdPartyCost + jobe:ARC3rdPartyMarkup
            End !If tmp:FixedCharge = 0
        End !If job:Third_Party_Site <> ''
    End !~jobe:IgnoreRRCChaCosts
    jobe:RRCCPartsCost  = tmp:RRCCPartsCost

    !Only update prices on incomplete jobs - L945 (DBH: 03-09-2003)
    !Allow to reprice if the job conditions change - L945 (DBH: 04-09-2003)
    If job:Date_Completed = 0 Or func:ForceWarranty
        If job:Ignore_Warranty_Charges <> 'YES'
            job:Labour_Cost_Warranty    = tmp:ARCWLabourCost
            job:Courier_Cost_Warranty = 0

            If jobe:ExchangedAtRRC = True
                If jobe:SecondExchangeNumber <> 0
                    !Exchange attached at RRC
                    !Don't claim, unless the ARC has added a second exchange - 3788 (DBH: 07-04-2004)
                    job:Courier_Cost_Warranty = tmp:ManufacturerExchangeFee
                Else !If jobe:SecondExchangeNumber <> 0
                    !Exchange attached at RRC.
                    !Do not claim, unless sent to ARC, and job is RTM - 3788 (DBH: 07-04-2004)
                    If jobe:Engineer48HourOption <> 1
                        If job:Repair_Type_Warranty = 'R.T.M.'
                            job:Courier_Cost_Warranty = tmp:ManufacturerExchangeFee
                        End !If job:Repair_Type_Warranty = 'R.T.M.'
                    End !If jobe:Engineer48HourOption = 1
                End !If jobe:SecondExchangeNumber <> 0
            Else !If jobe:ExchangedAtRRC = True
                !Exchange attached at ARC. Claim for it - 3788 (DBH: 07-04-2004)
                If job:Exchange_Unit_Number <> 0
                    job:Courier_Cost_Warranty = tmp:ManufacturerExchangeFee
                End !If job:Exchange_Unit_Number <> 0
            End !If jobe:ExchangedAtRRC = True
        End !job:Invoice_Warranty_Charges <> 'YES'

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If job:Exchange_Unit_Number > 0
            If jobe:HandsetReplacmentValue > 0
                tmp:ARCWPartsCost += jobe:HandsetReplacmentValue
            End ! If jobe:HandsetReplacmentValue > 0
        End ! If job:Exchange_Unit_Number > 0
        If jobe:SecondExchangeNumber > 0
            If jobe:SecondHandsetRepValue > 0
                tmp:ARCWPartsCost += jobe:SecondHandsetRepValue
            End ! If jobe:SecondHandsetReplacementValue > 0
        End ! If jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        job:Parts_Cost_Warranty     = tmp:ARCWPartsCost

        If ~jobe:IgnoreRRCWarCosts
            jobe:RRCWLabourCost         = tmp:RRCWLabourCost
        End !~jobe:IgnoreRRCWarCosts

        jobe:RRCWPartsCost          = tmp:RRCWPartsCost

        If ~jobe:IgnoreClaimCosts
            jobe:ClaimValue         = tmp:ClaimValue
        End !If ~jobe:IgnoreClaimCosts

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If job:Exchange_Unit_Number > 0
            If jobe:HandsetReplacmentValue > 0
                tmp:ClaimPartsCost += jobe:HandsetReplacmentValue
            End ! If jobe:HandsetReplacmentValue > 0
        End ! If job:Exchange_Unit_Number > 0
        If jobe:SecondExchangeNumber > 0
            If jobe:SecondHandsetRepValue > 0
                tmp:ClaimPartsCost += jobe:SecondHandsetRepValue
            End ! If jobe:SecondHandsetReplacementValue > 0
        End ! If jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        jobe:ClaimPartsCost         = tmp:ClaimPartsCost
    End !If job:Date_Completed = 0

    If job:Ignore_Estimate_Charges <> 'YES'
        job:Labour_Cost_Estimate    = tmp:ARCELabourCost
    End !job:Ignore_Estimate_Charges <> 'YES'
    job:Parts_Cost_Estimate     = tmp:ARCEPartsCost

    If ~jobe:IgnoreRRCEstCosts
        jobe:RRCELabourCost         = tmp:RRCELabourCost
    End !~jobe:IgnoreRRCEstCosts

    jobe:RRCEPartsCost          = tmp:RRCEPartsCost

    !Fixed Pricing

    If ~SentToHub(job:Ref_Number)
        !Not a hub job
        If tmp:FixedChargeRRC
            jobe:RRCCPartsCost = 0
            jobe:RRCEPartsCost = 0
        End !If tmp:FixedChargeRRC
    Else !If ~SentToHub(job:Ref_Number)
        If jobe:WebJob = 1
            If tmp:FixedChargeARC
                job:Parts_Cost = 0
                job:Parts_Cost_Estimate = 0
            End !If tmp:FixedChargeRRC

            If tmp:FixedChargeRRC
                jobe:RRCCPartsCost = 0
                jobe:RRCEPartsCost = 0
            Else !If tmp:FixedChargeRRC
                If ~jobe:IgnoreRRCChaCosts
                    jobe:RRCCLabourCost = job:Labour_Cost
                End !If ~jobe:IgnoreRRCChaCosts
            End !If tmp:FixedChargeRRC
        Else !If jobe:WebJob = 1
            If tmp:FixedChargeARC
                job:Parts_Cost = 0
                job:Parts_Cost_Estimate = 0
            End !If tmp:FixedChargeARC
        End !If jobe:WebJob = 1
    End !If ~SentToHub(job:Ref_Number)

    !Totals
! Inserting (DBH 03/12/2007) # 8218 - RRC has the same costs as the ARC for an estimate
    If SentToHub(job:Ref_Number) And jobe:WebJob
        jobe:RRCELabourCost = job:Labour_Cost_Estimate
        jobe:RRCEPartsCost = job:Parts_Cost_Estimate
    End ! If SentToHub(job:Ref_Number) And jobe:WebJob
! End (DBH 03/12/2007) #8218

    IF (_PreCLaimThirdParty(job:Third_Party_Site,job:Manufacturer))
        ! #12363 Assign the 3rd party handling fee from the manufacturer (DBH: 16/02/2012)
        IF (job:Warranty_Job = 'YES')
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer = job:Manufacturer
            IF (Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign)
                Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                jobe2:RefNumber = job:Ref_Number
                IF (Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign)
                    jobe2:ThirdPartyHandlingFee = man:ThirdPartyHandlingFee
                    Access:JOBSE2.TryUpdate()
                END
            END
        END ! IF (job:Warranty_Job = 'YES')
    END ! IF (_PreThirdPartyClaim(job:Third_Party_Site,job:Manufacturer))

    job:Sub_Total           = job:Courier_Cost + job:Labour_Cost + job:Parts_Cost
    jobe:RRCCSubTotal       = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost
    job:Sub_Total_Warranty  = job:Labour_Cost_Warranty + job:Parts_Cost_Warranty + job:Courier_Cost_Warranty
    jobe:RRCWSubTotal       = jobe:RRCWLabourCost + jobe:RRCWPartsCost
    job:Sub_Total_Estimate  = job:Labour_Cost_Estimate + job:Parts_Cost_Estimate + job:Courier_Cost_Estimate
    jobe:RRCESubTotal       = jobe:RRCELabourCost + jobe:RRCEPartsCost + job:Courier_Cost_Estimate


    Relate:PARTS.Close()
    Relate:STOCK.Close()
    Relate:ESTPARTS.Close()
    Relate:WARPARTS.Close()
    Relate:CHARTYPE.Close()
    Relate:SUBCHRGE.Close()
    Relate:TRACHRGE.Close()
    Relate:STDCHRGE.Close()
    Relate:MANUFACT.Close()
    Relate:TRDPARTY.Close()
    Relate:JOBSE2.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
NoAccess             PROCEDURE  (func:Level)          ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !This routine used to do something clever, but not anymore - TrkBs: N/A (DBH: 14-12-2004)
    Case Missive('You do not have access to this option.','ServiceBase 3g',|
               'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetDays              PROCEDURE  (func:Sat,func:Sun,func:Start) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
loc:Count       Long
loc:Days        Long
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    loc:Days = 0
    loc:Count = 0
    Loop
        loc:Count += 1
        If (func:Start + loc:Count) %7 = 0 And ~func:Sun
            Cycle
        End !If (aus:DateChanged + Count#) %7 = 0 And ~sun#
        If (func:Start + loc:Count) %7 = 6 And ~func:Sat
            Cycle
        End !If (aus:DateChanged + Count#) %7 = 0 And ~sun#
        loc:Days += 1
        If func:Start + loc:Count >= Today()
            Break
        End !If aus:DateChanged + Count# = Today()
    End !Loop
    Return loc:Days
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetDaysToDate        PROCEDURE  (func:Sat,func:Sun,func:Start,func:End) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
loc:Count       Long
loc:Days        Long
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    loc:Days = 0
    loc:Count = 0
    Loop
        loc:Count += 1
        If (func:Start + loc:Count) %7 = 0 And ~func:Sun
            Cycle
        End !If (aus:DateChanged + Count#) %7 = 0 And ~sun#
        If (func:Start + loc:Count) %7 = 6 And ~func:Sat
            Cycle
        End !If (aus:DateChanged + Count#) %7 = 0 And ~sun#
        loc:Days += 1
        If func:Start + loc:Count >= func:End
            Break
        End !If aus:DateChanged + Count# = Today()
    End !Loop
    Return loc:Days
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
WOBEDIStatus         PROCEDURE  (func:Status)         ! Declare Procedure
save_aud_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Case func:Status
        Of 'NO'
            Return 'PENDING'
        Of 'YES'
! Changing (DBH 07/02/2007) # 8707 - Don't think this is necessary and is slowing the process anyway
!            !PENDING, or RESUBMITTED?
!            Found# = 0
!            Save_aud_ID = Access:AUDIT.SaveFile()
!            Access:AUDIT.ClearKey(aud:Ref_Number_Key)
!            aud:Ref_Number = wob:RefNumber
!            aud:Date       = Today()
!            Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
!            Loop
!                If Access:AUDIT.NEXT()
!                   Break
!                End !If
!                If aud:Ref_Number <> wob:RefNumber      |
!                    Then Break.  ! End If
!                If aud:Action = 'WARRANTY CLAIM SUBMITTED'
!                    Found# = 1
!                    Break
!                End !If aud:Action = 'WARRANTY CLAIM SUBMITTED'
!
!                x# = Instring('EDI FILE CREATED',aud:Action,1,1)
!                If x# <> 0
!                    Found# = 1
!                    Break
!                End !If x# <> 0
!            End !Loop
!            Access:AUDIT.RestoreFile(Save_aud_ID)
!            If Found# = 1
!                Return 'RESUBMITTED'
!            Else !If Found# = 1
!                Return 'IN PROGRESS'
!            End !Else !If Found# = 1
! to (DBH 07/02/2007) # 8707
            Return 'SUBMITTED'
! End (DBH 07/02/2007) #8707

        Of 'APP'
            Return 'APPROVED'
        Of 'PAY'
            Return 'PAID'
        Of 'EXC'
            Return 'REJECTED'
        Of 'EXM'
            Return 'REJECTED'
        Of 'AAJ'
            Return 'ACCEPTED REJECTION'
        Of 'REJ'
            Return 'FINAL REJECTION'
    End !func:Status
    Return ''
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CanWOBEDIBeChanged   PROCEDURE  (func:EDI)            ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If func:EDI = 'AAJ' Or |
        func:EDI = 'PAY' Or|
        func:EDI = 'REJ'
        !Can't change EDI
        Return Level:Benign
    End !func:EDI = 'EXC'
    Return Level:Fatal
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
LoanAttachedToJob    PROCEDURE  (func:JobNumber)      ! Declare Procedure
save_aud_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If (job:Courier_Cost <> 0 or job:Courier_Cost_Estimate <> 0 Or |
        job:Invoice_Courier_Cost <> 0) And jobe:Engineer48HourOption <> 1
        Return 1
    End !If job:Courier_Cost <> 0 or job:Courier_Cost_Estimate <> 0
    If job:Loan_Unit_Number <> 0
        Return 1
    Else !If job:Loan_Unit_Number <> 0
        Found# = 0
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeActionKey)
        aud:Ref_Number = func:JobNumber
        aud:Type       = 'LOA'
        aud:Action     = 'LOAN UNIT ATTACHED TO JOB'
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Ref_Number <> func:JobNumber      |
            Or aud:Type       <> 'LOA'      |
            Or aud:Action     <> 'LOAN UNIT ATTACHED TO JOB'      |
                Then Break.  ! End If
            Found# = 1
            Break
        End !Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)
        Return Found#
    End !If job:Loan_Unit_Number <> 0
    Return 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddToConsignmentHistory PROCEDURE  (func:JobNumber,func:DespatchFrom,func:DespatchTo,func:Courier,func:ConsignmentNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:JOBSCONS.Open()
    Access:JOBSCONS.UseFile()

    If Access:JOBSCONS.PrimeRecord() = Level:Benign

        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Found

        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        joc:UserCode          = use:User_Code
        joc:RefNumber         = func:JobNumber
        joc:DespatchFrom      = func:DespatchFrom
        joc:DespatchTo        = func:DespatchTo
        joc:Courier           = func:Courier
        joc:ConsignmentNumber = func:ConsignmentNumber
        joc:DespatchType      = func:Type
        If Access:JOBSCONS.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:JOBSCONS.TryInsert() = Level:Benign
            !Insert Failed
            Access:JOBSCONS.CancelAutoInc()
        End !If Access:JOBSCONS.TryInsert() = Level:Benign
    End !If Access:JOBSCONS.PrimeRecord() = Level:Benign

    Access:JOBSCONS.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Markups              PROCEDURE  (func:OriginalCost,func:Cost,func:MarkUp) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN VodacomClass.Markup(func:OriginalCost,func:Cost,func:MarkUp)
!
!
!    If func:MarkUp <> 0
!    !round added to make this round off properly
!        Return round(func:Cost + (func:Cost * (func:MarkUp/100)),.01)
!    Else !If func:MarkUp <> 0
!        Return func:OriginalCost
!    End !If func:MarkUp <> 0
!
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()

    MEMBER
    INCLUDE('BHSimpleExportFile.inc'),ONCE
    INCLUDE('KEYCODES.CLW'),ONCE
    INCLUDE('ABUTIL.INC'),ONCE
!!! <summary>
!!! Procedure to initalize an ASCII export
!!! </summary>
!!! <param name="pUseFilename">Name of the export file</param>
!!! <param name="fDel">(Optional: Defaults to commana)  String(@s1) containing the field delimiter</param>
!!! <param name="pQuotesOnFields">(Optional) TRUE/FALSE to put quotes around the fields.</param>
!!! <remarks>
!!! This procedure will remove the existing, and create a new file. It will return a value if an error occurs.
!!! </remarks>
SimpleExportClass.Init      PROCEDURE(STRING pUseFilename,<STRING pRecordDelimiter>,<BYTE pQuotesOnFields>)
SimpleExportFileName            STRING(255),AUTO
retValue                        BYTE(Level:Benign)
    CODE
		
        SELF.LastError = ''
        LOOP 1 TIMES
            IF (pUseFilename = '')
                SELF.LastError = 'Filename Is Missing'
                retValue = Level:Fatal
                BREAK
            END ! IF
            
            IF (SELF.FileOpen = TRUE)
                ! File has already been opened!
                SELF.LastError = 'File is already open'
                retValue = Level:Fatal
                BREAK
            END ! IF
            
            REMOVE(pUseFilename)
            IF (EXISTS(pUseFilename))
                SELF.LastError = 'File is locked'
                retValue =  Level:Fatal
                BREAK
            END

            SimpleExportFileName = pUseFilename
            IF (pRecordDelimiter = '')
                SELF.Delimeter = ','
            ELSE
                SELF.Delimeter = pRecordDelimiter
            END

            IF (pQuotesOnFields = 1)
                SELF.UseQuotes = TRUE
            ELSE
                SELF.UseQuotes = FALSE
            END

            SimpleExportFile{PROP:Name} = SimpleExportFileName
            
            CREATE(SimpleExportFile)
            OPEN(SimpleExportFile)
            IF (ERROR())
                SELF.LastError = 'Unable to open file: ' & ERROR() & '<13,10>' & CLIP(SimpleExportFileName)
                retValue =  Level:Fatal
                BREAK
            END


            SELF.FileOpen = TRUE
        END ! LOOP

        RETURN retValue

    !!! <summary>
!!! Procedure to close the open ASCII file.
!!! </summary>
SimpleExportClass.Kill PROCEDURE()
    CODE
        IF (SELF.FileOpen = FALSE)
            ! File has already been closed
            RETURN
        END

        CLOSE(SimpleExportFile)

        SELF.FileOpen = FALSE
!!! <summary>
!!! Procedure to populate an export line
!!! </summary>
!!! <param name="pFieldText">String to populate</param>
!!! <param name="pFirstFieldInRow">(Optional) To signify the first record</param>
!!! <param name="pLastFieldInRow">(Optional) To signify the last record and ADD to the file</param>
!!! <param name="pMultipleRows">(Optional) Add the same field a number of times</param>
SimpleExportClass.AddField  PROCEDURE(<STRING pFieldText>,<BYTE pFirstFieldInRow>,<BYTE pLastFieldInRow>,<BYTE pMultipleRows>,<BYTE pIgnoreQuotes>)
locField                        CSTRING(10000)
multiple                        LONG,AUTO
m                               LONG,AUTO
    CODE
        IF (SELF.FileOpen = FALSE)
            !File isn't open
            SELF.LastError = 'File Is Not Open'
            RETURN
        END
        

        IF (OMITTED(pMultipleRows))
            multiple = 1
        ELSE
            multiple = pMultipleRows
        END ! IF

        IF (pFieldText <> '')
            locField = pFieldText
        ELSE
            locField = ''
        END
		
		
		IF (pFirstFieldInRow = 1 AND pLastFieldInRow = 1)
			IF (SELF.UseQuotes AND pIgnoreQuotes <> 1)
				SimpleExportFile.ExportLine = '"' & CLIP(locField) & '"'
				ADD(SimpleExportFile)
			ELSE
				SimpleExportFile.ExportLine = CLIP(locField)
				ADD(SimpleExportFile)
			END
        ELSIF (pFirstFieldInRow = 1)
            IF (SELF.UseQuotes AND pIgnoreQuotes <> 1)
                SimpleExportFile.ExportLine = '"' & CLIP(locField)
            ELSE
                SimpleExportFile.ExportLine = locField
            END
        ELSIF (pLastFieldInRow = 1)
            IF (SELF.UseQuotes AND pIgnoreQuotes <> 1)
                SimpleExportFile.ExportLine = CLIP(SimpleExportFile.ExportLine) & '"' & CLIP(SELF.Delimeter) & '"' & CLIP(locField) & '"'
            ELSE
                SimpleExportFile.ExportLine = CLIP(SimpleExportFile.ExportLine) & SELF.Delimeter & locField
            END
            ADD(SimpleExportFile)
        ELSE
            LOOP m = 1 TO multiple
                IF (SELF.UseQuotes AND pIgnoreQuotes <> 1)
                    SimpleExportFile.ExportLine = CLIP(SimpleExportFile.ExportLine) & '"' & CLIP(SELF.Delimeter) & '"' & locField
                ELSE
                    SimpleExportFile.ExportLine = CLIP(SimpleExportFile.ExportLine) & SELF.Delimeter & locField
                END
            END
        END


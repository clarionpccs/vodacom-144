!region SBOnline Log File Declaration
sbonlineLogName     STRING(255),STATIC
SBOnlineLog         File,Driver('ASCII'),Pre(sbolog),NAME(sbonlineLogName),Create,Bindable,THREAD
RECORD                  RECORD
DataLine                    STRING(2000000)
                        END ! 
                    END ! I
!endregion

!region Skyline Response Codes
Skyline:Success     EQUATE('SC0001')
Skyline:Error       EQUATE('SC0002')
Skyline:NotFounderror       EQUATE('SC0003')
Skyline:InsertError EQUATE('SC0004')
Skyline:UpdateError EQUATE('SC0005')
Skyline:AccountMatchError   EQUATE('SC0006')
Skyline:NoRecordsError      EQUATE('SC0010')
!endregion

!region Skyline Default Equates
SkylineKey          EQUATE('|sKyLiNe|') ! Used as part of the MD5 encoding of Skyline password
SkylinePostString   STRING(2000000),AUTO
SkylinePostCString  CSTRING(2000000),AUTO

qSkylinePutJobDetails       QUEUE,TYPE,PRE(qSkylinePutJobDetails)
Accessories                     STRING(10000),NAME('Accessories')
AgentChargeableInvoiceDate      STRING(30),NAME('AgentChargeableInvoiceDate')
AgentChargeableInvoiceNo        STRING(30),NAME('AgentChargeableInvoiceNo')
AgentRefNo                      STRING(30),NAME('AgentRefNo') ! Order Number
AgentStatus                     STRING(30),NAME('AgentStatus')
Appointments                    STRING(10000),NAME('Appointments')
AuthorisationNumber             STRING(20),NAME('AuthorisationNumber')
BookingAccountNumber            STRING(30),NAME('BookingAccountNumber') ! RRC Account Number
ChargeableDeliveryCost          STRING(30),NAME('ChargeableDeliveryCost')
ChargeableDeliveryVATCost       STRING(30),NAME('ChargeableDeliveryVATCost')
ChargeableLabourCost            STRING(30),NAME('ChargeableLabourCost')
ChargeableLabourVATCost         STRING(30),NAME('ChargeableLabourVATCost')
ChargeablePartsCost             STRING(30),NAME('ChargeablePartsCost')
ChargeableSubTotal              STRING(30),NAME('ChargeableSubTotal')
ChargeableTotalCost             STRING(30),NAME('ChargeableTotalCost')
ChargeableVATCost               STRING(30),NAME('ChargeableVATCost')
ClaimNumber                     STRING(20),NAME('ClaimNumber')
ClaimTransmitStatus             STRING(4),NAME('ClaimTransmitStatus')
Client                          STRING(30),NAME('Client')
ClientAccountNo                 STRING(20),NAME('ClientAccountNo') ! Trade Account Number
ClosedDate                      STRING(10),NAME('ClosedDate')
ColAddCompanyName               STRING(30),NAME('ColAddCompanyName')
ColAddBuildingName              STRING(30),NAME('ColAddBuildingName')
ColAddStreet                    STRING(30),NAME('ColAddString')
ColAddArea                      STRING(30),NAME('ColAddArea')
ColAddPostcode                  STRING(30),NAME('ColAddPostcode')
ColAddPhoneNo                   STRING(30),NAME('ColAddPhoneNo')
ColAddTown                      STRING(30),NAME('ColAddTown')
ColAddCounty                    STRING(30),NAME('ColAddCounty')  
Colour                          STRING(30),NAME('Colour') ! Colour
Courier                         STRING(30),NAME('Courier') ! Courier
CompletionStatus                STRING(30),NAME('CompletionStatus')
ConditionCode                   STRING(4),NAME('ConditionCode')
ConsumerType                    STRING(30),NAME('ConsumerType')
ConsignmentNo                   STRING(30),NAME('ConsignmentNo') ! Courier Waybill No
ContactHistory                  STRING(10000),NAME('ContactHistory')
CustomerAddressArea             STRING(50),NAME('CustomerAddressArea')
CustomerAddressTown             STRING(50),NAME('CustomerAddressTown')
CustomerBuildingName            STRING(40),NAME('CustomerBuildingName')
CustomerCompanyName             STRING(30),NAME('CustomerCompanyName')
CustomerCounty                  STRING(30),NAME('CustomerCounty')
CustomerEmailAddress            STRING(50),NAME('CustomerEmailAddress')
CustomerForename                STRING(30),NAME('CustomerForename')
CustomerHomeTelNo               STRING(20),NAME('CustomerHomeTelNo')
CustomerMobileNo                STRING(20),NAME('CustomerMobileNo')
CustomerPostcode                STRING(15),NAME('CustomerPostcode')
CustomerStreet                  STRING(50),NAME('CustomerStreet')
CustomerSurname                 STRING(50),NAME('CustomerSurname')
CustomerTitle                   STRING(5),NAME('CustomerTitle')
CustomerWorkTelNo               STRING(20),NAME('CustomerWorkTelNo')
DamageCheckListItems            STRING(1000),NAME('DamageCheckListItems') ! External Damage List
DataConsent                     STRING(1),NAME('DataConsent')
DateBooked                      STRING(10),NAME('DateBooked')
DateRepairComplete              STRING(10),NAME('DateRepairComplete')
DateUnitReceived                STRING(10),NAME('DateUnitReceived')
DefectType                      STRING(4),NAME('DefectType')
DelAddCompanyName               STRING(30),NAME('DelAddCompanyName')
DelAddBuildingName              STRING(30),NAME('DelAddBuildingName')
DelAddStreet                    STRING(30),NAME('DelAddString')
DelAddArea                      STRING(30),NAME('DelAddArea')
DelAddPostcode                  STRING(30),NAME('DelAddPostcode')
DelAddPhoneNo                   STRING(30),NAME('DelAddPhoneNo')
DelAddTown                      STRING(30),NAME('DelAddTown')
DelAddCounty                    STRING(30),NAME('DelAddCounty')
DespatchConsignmentNo           STRING(30),NAME('DespatchConsignmentNo')
DespatchCourier                 STRING(30),NAME('DespatchCourier')
DOP                             STRING(10),NAME('DOP')
EInvoiceStatus                  STRING(4),NAME('EInvoiceStatus')
EmailAlertAddress               STRING(255),NAME('EmailAddressAddress')    
EmailNotificationRequired       STRING(1),NAME('EmailNotificationRequired')
EngineerCode                    STRING(20),NAME('EngineerCode')
EngineerName                    STRING(50),NAME('EngineerName')
EstimateRequired                STRING(1),NAME('EstimateRequired')
EstimateStatus                  STRING(30),NAME('EstimateStatus')
ETDDate                         STRING(10),NAME('ETDDate')
ExtendedWarrantyNo              STRING(30),NAME('ExtendedWarrantyNo')
GuaranteeCode                   STRING(4),NAME('GuaranteeCode')
CustomerIDNumber                STRING(30),NAME('CustomerIDNo')
IMEINo                          STRING(30),NAME('IMEINo') ! IMEI Number
Insurer                         STRING(30),NAME('Insurer')
JobSite                         STRING(1),NAME('JobSite')
JobType                         STRING(30),NAME('JobType') ! Transit Type
Manufacturer                    STRING(50),NAME('Manufacturer')
ModelNo                         STRING(50),NAME('ModelNo')
Network                         STRING(30),NAME('Network') !Phone Network
NetworkReferenceNo              STRING(30),NAME('NetworkReferenceNo')
NewFirmwareVersion              STRING(30),NAME('NewFirmwareVersion')
Notes                           STRING(4000),NAME('Notes')
OBFIMEINumber                   STRING(30),NAME('OBFIMEINumber')
OBFReturnDate                   STRING(10),NAME('OBFReturnDate')
OBFTalkTime                     STRING(30),NAME('OBFTalkTime')
OBFOriginalDealer               STRING(30),NAME('OBFOriginalDealer')
OBFBranchOfReturn               STRING(30),NAME('OBFBranchOfReturn')
OBFStoreReferenceNumber         STRING(30),NAME('OBFStoreReferenceNumber')
OBFProofOfPurchase              STRING(1),NAME('OBFProofOfPurchase')
OBFOriginalAccessories          STRING(1),NAME('OBFOriginalAccessoriesDa')
OBFOriginalCharger              STRING(1),NAME('OBFOriginalCharger')    
OBFOriginalPackaging            STRING(1),NAME('OBFOriginalPackaging')
OBFOriginalAntenna              STRING(1),NAME('OBFOriginalAntenna')
OBFOriginalManuals              STRING(1),NAME('OBFOriginalManuals')
OBFPhysicalDamage               STRING(1),NAME('OBFPhysicalDamage')
OBFReplacement                  STRING(1),NAME('OBFReplacement')
OBFLAccountNo                   STRING(30),NAME('OBFLAccountNo')
OBFReplacementIMEINo            STRING(30),NAME('OBFReplacementIMEINo')    
OriginalRetailer                STRING(30),NAME('OriginalRetailer')
Originator                      STRING(30),NAME('Originator')
Parts                           STRING(10000),NAME('Parts')
PolicyNo                        STRING(20),NAME('PolicyNo')
ProductCode                     STRING(30),NAME('ProductCode')
ReceiptNo                       STRING(30),NAME('ReceiptNo')
ReferralNumber                  STRING(20),NAME('ReferralNumber')
RefreshAll                      STRING(4),NAME('RefreshAll')
RepairDescription               STRING(4000),NAME('RepairDescription')
ReportedFault                   STRING(2000),NAME('ReportedFault')
RetailerLocation                STRING(100),NAME('RetailerLocation')
RetailerRef                     STRING(30),NAME('RetailerRef')
RMANumber                       STRING(30),NAME('RMANumber')
SamsungRefNo                    STRING(30),NAME('SamsungRefNo')
SamsungSubServiceType           STRING(30),NAME('SamsungSubServiceType')
SBLicenceNo                     STRING(15),NAME('SBLicenceNo')
SBVersion                       STRING(10),NAME('SBVersion')
SCJobNo                         STRING(30),NAME('SCJobNo')
SerialNo                        STRING(50),NAME('SerialNo') !MSN
ServiceType                     STRING(1),NAME('ServiceType')
ServiceTypeName                 STRING(30),NAME('ServiceTypeNames')
SLNumber                        STRING(30),NAME('SLNumber')
SMSAlertNo                      STRING(30),NAME('SMSAlertNo')
SMSNotificationRequired         STRING(1),NAME('SMSNotificationRequired')
Status                          STRING(30),NAME('Status')
StatusUpdates                   STRING(10000),NAME('StatusUpdates')
SymptomCategory1                STRING(10),NAME('SymptomCategory1')
SymptomCategory2                STRING(10),NAME('SymptomCategory2')
SymptomCategory3                STRING(10),NAME('SymptomCategory3')
SymptomCode                     STRING(4),NAME('SymptomCode')
TimeBooked                      STRING(10),NAME('TimeBooked')
UnitCondition                   STRING(50),NAME('UnitCondition')
UnitType                        STRING(50),NAME('UnitType')
Username                        STRING(30),NAME('Username') ! Usercode
VATNumber                       STRING(30),NAME('VATNumber') ! Vat Number
VestelManufactureDate           STRING(30),NAME('VestelManufactureDate')
VestelModelCode                 STRING(30),NAME('VestelModelCode')
WarrantyNotes                   STRING(100),NAME('WarrantyNotes')
                            END
                    
SkylinePJDAccessories       QUEUE(),PRE(SkylinePutJobDetailsAccessories),TYPE
Accessory                       STRING(30)
                            END
                        
SkylinePJDDamageCheckListItems      QUEUE(),TYPE,PRE(SkylinePJDDamageCheckListItems)
DamageCheckListItem                     STRING(30) ! Damage Check List Item
                                    END ! 

!Skyline             CLASS
!Init                    PROCEDURE(NetWebServerWorker p_web,STRING pHeaderAuthString,*QUEUE Q, LONG pNullEmptyFields=0),LONG
!Scramble                PROCEDURE(*STRING pString)
!CreateResponse          PROCEDURE(NetwebServerWorker p_web,STRING pCode,STRING pDescription,<STRING pDetails>)        
!PutNewJob               PROCEDURE(NetwebServerWorker p_web,qSkylinePutJobDetails Q,*STRING pErrorStr),LONG
!                    END ! CLASS
            
            
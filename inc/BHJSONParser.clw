    MEMBER
    INCLUDE('BHJSONParser.inc'),ONCE
    INCLUDE('KEYCODES.CLW'),ONCE
    INCLUDE('ABUTIL.INC'),ONCE    
!!! <summary>
!!! Procedure to convert JSON string into a Q
!!!
!!! <param name="Q">Q to fill from JSON data</param>
!!! <param name="inputString">JSON string to fill Queue with</param>
!!! </summary>
!!! <remarks>
!!! Q names must match the JSON field names
!!! </remarks>
JSONParser.AddStringToQueue  PROCEDURE(*QUEUE Q,STRING inputString,LONG pNullSkippedFields=0)
lenString                       LONG(),AUTO
i                               LONG(),AUTO
j                               LONG(),AUTO
k                               LONG(),AUTO

colonPos                        LONG(),AUTO
quoteLeftPos                    LONG(),AUTO
quoteRightPos                   LONG(),AUTO
braceLeftPos                    LONG(),AUTO


fieldName                       STRING(100),DIM(100)
variableValue                   ANY,DIM(100)
totalFields                     LONG,AUTO
currentFieldName                STRING(100),AUTO

    CODE
        i = 1
        totalFields = 0
        LOOP
            IF (i > 100)
                HALT(,'Error. Cannot parse a string with over 100 fields.')
                BREAK
            END

            fieldName[i] = WHO(Q,i)
            IF (fieldName[i] = '')
                ! Blank, so there can't be anymore fields
                totalFields = i - 1
                BREAK
            END

            ! Incase the Q has a prefix, remove it.
            col# = INSTRING(':',fieldName[i],1,1)
            IF (col# > 0)
                fieldName[i] = CLIP(SUB(fieldName[i],col# + 1,100))
            END

            i += 1
        END

        ! Assign variables to Q variables
        LOOP i = 1 TO totalFields
            variableValue[i] &= WHAT(Q,i)
        END

        lenString = LEN(CLIP(inputString))

        braceLeftPos = 0
        DO ResetVariables

        FREE(Q)
        CLEAR(Q)

        LOOP i = 1 TO lenString
            IF (inputString[i] = '}')
                ! End Of Record
                
                IF (pNullSkippedFields = 1)
                    ! Full renaming Q Fields will a NULL
                    LOOP k = 1 TO totalFields
                        IF (INSTRING('"' & UPPER(CLIP(fieldName[k])) & '":',UPPER(inputString),1,1) = 0)
                            variableValue[k] = 'NULL'
                        END ! IF
                    END ! LOOP
                END ! IF                
                ADD(Q)
                DO ResetVariables
                braceLeftPos = 0
                CYCLE
            END

            IF (braceLeftPos = 0)
                IF (inputString[i] = '{{')
                    ! Start of Record
                    braceLeftPos = i
                    CYCLE
                END
            END

            IF (braceLeftPos > 0)
                IF (colonPos = 0)
                    IF (inputString[i] = ':')
                        ! Done Field Name, now doing data
                        colonPos = i
                        CYCLE
                    END
                END

                IF (quoteLeftPos = 0)
                    IF (inputString[i] = '"')
                        ! Start of quote
                        quoteLeftPos = i
                        CYCLE
                    END
                END ! IF

                IF (quoteLeftPos > 0)
                    IF (inputString[i] = '"')
                        ! End of quote
                        quoteRightPos = i

                        IF (colonPos = 0)
                            ! This is a field name
                            currentFieldName = inputString[quoteLeftPos + 1:quoteRightPos - 1]
                            !STOP('1: ' & inputString[quoteLeftPos + 1:quoteRightPos - 1])
                            quoteLeftPos = 0
                            quoteRightPos = 0
                            CYCLE
                        ELSE
                            ! This is the data
                            IF (currentFieldName <> '')
                                LOOP j = 1 TO totalFields
                                    !STOP(UPPER(fieldName[i]) & '<13,10>' & UPPER(CLIP(currentFieldName)))
                                    IF (UPPER(fieldName[j]) = UPPER(CLIP(currentFieldName)))
                                        variableValue[j] = SELF.DecodeJSONString(inputString[quoteLeftPos + 1:quoteRightPos - 1])
                                        !STOP(UPPER(fieldName[j]) & '<13,10,13,10>2: ' & inputString[quoteLeftPos + 1:quoteRightPos - 1])
                                        BREAK
                                    END
                                END!  LOOP
                            END
                            Do ResetVariables
                            CYCLE
                        END

                        CYCLE
                    END
                END

            END ! IF

        END ! LOOP

ResetVariables      ROUTINE
    currentFieldName = ''
    quoteLeftPos = 0
    quoteRightPos = 0
    colonPos = 0


!!! <summary>
!!! Procedure to convert Q to JSON String
!!!
!!! <param name="Q">Q to convert to JSON data</param>
!!! <param name="outputString">JSON String created from Q</param>
!!! </summary>
!!! <remarks>
!!! Q field names will be used as field names
!!! </remarks>
JSONParser.AddQueueToString  PROCEDURE(*STRING outputString,QUEUE inQ)
fieldName                       STRING(100),DIM(100)
totalFields                     LONG,AUTO
i                               LONG,AUTO
j                               LONG,AUTO
fieldValue                      ANY
finalString                     STRING(10000),AUTO
    CODE
        i = 1
        totalFields = 0
        LOOP
            IF (i > 100)
                HALT(,'Error. Cannot parse a string with over 100 fields.')
                BREAK
            END

            fieldName[i] = WHO(inQ,i)
            IF (fieldName[i] = '')
                ! Blank, so there can't be anymore fields
                totalFields = i - 1
                BREAK
            END

            ! Incase the Q has a prefix, remove it.
            col# = INSTRING(':',fieldName[i],1,1)
            IF (col# > 0)
                fieldName[i] = CLIP(SUB(fieldName[i],col# + 1,100))
            END


            i += 1


        END

        finalString = ''

        inFirstLoop# = 0
        ! Loop through the Q records
        LOOP j = 1 TO RECORDS(inQ)
            GET(inQ,j)

            IF (inFirstLoop# = 0)
                finalString = '[{{"'
                inFirstLoop# = 1
            ELSE ! IF
                finalString = CLIP(finalString) & '},{{"'
            END ! IF

            inSecondLoop# = 0
            ! Loop through the fields
            LOOP i = 1 TO totalFields
                fieldValue &= WHAT(inQ,i)

                IF (inSecondLoop# = 0)
                    finalString = CLIP(finalString) & CLIP(fieldName[i]) ! Add field name
                    inSecondLoop# = 1
                ELSE ! IF
                    finalString = CLIP(finalString) & ',"' & CLIP(fieldName[i]) ! Add field name
                END ! IF
                finalString = CLIP(finalString) & '":"' & CLIP(SELF.EncodeJSONString(fieldValue)) & '"'
            END !LOOP

        END !LOOP

        IF (finalString <> '')
            finalString = CLIP(finalString) & '}]'
        END ! IF

        outputString = finalString
        RETURN
JSONParser.EncodeJSONString        PROCEDURE(STRING pJSONString)!,STRING
strLen                  LONG()
strPos                  LONG()
strNewPos               LONG()
strFinal                STRING(100000)
    CODE
        strLen = LEN(CLIP(pJSONString))

        strPos = 0
        strNewPos = 0
        
        LOOP 
            strPos += 1
            
            CASE pJSONString[strPos]
            OF '\'
                strNewPos += 1
                strFinal[strNewPos : strNewPos + 1] = '\\'
                strNewPos += 1
            OF '/'
                strNewPos += 1
                strFinal[strNewPos : strNewPos + 1] = '\/'
                strNewPos += 1                
            OF '<13>'
                strNewPos += 1
                strFinal[strNewPos : strNewPos + 1] = '\r'
                strNewPos += 1
            OF '<10>'
                strNewPos += 1
                strFinal[strNewPos : strNewPos + 1] = '\n'
                strNewPos += 1
            OF '<9>'
                strNewPos += 1
                strFinal[strNewPos : strNewPos + 1] = '\t'
                strNewPos += 1
            OF '"' ! Single quotes allowed now OROF ''''
                strNewPos += 1
                strFinal[strNewPos : strNewPos + 1] = '\' & pJSONString[strPos]
                strNewPos += 1
            ELSE
                strNewPos += 1
                strFinal[strNewPos] = pJSONString[strPos]
            END ! CASE
            
            IF (strPos >= strLen)
                BREAK
            END
        END ! LOOP
    
        RETURN (CLIP(strFinal))     
        
JSONParser.DecodeJSONString        PROCEDURE(STRING pJSONString)!,STRING
strLen                  LONG()
strPos                  LONG()
strNewPos               LONG()
strFinal                STRING(100000)
    CODE

        strLen = LEN(CLIP(pJSONString))     
        
        strPos = 0
        strNewPos = 0
        
        LOOP
            strPos += 1
            
            CASE pJSONString[strPos]
            OF '\'
                strNewPos += 1
                strPos += 1
                IF (strPos > strLen)
                    StrFinal[strNewPos] = ''
                ELSE
                    StrFinal[strNewPos] = pJSONString[strPos]
                END
                
            ELSE
                strNewPos += 1
                StrFinal[strNewPos] = pJSONString[strPos]
            END ! CASE
            
            IF (strPos >= strLen)
                BREAK
            END
        END
        
        RETURN CLIP(StrFinal)        
                    MEMBER('vodr0037.clw')


OMIT('***')
    * Bryan: I have move this code to an external file so I could use C8 to edit it
    * Plus I needed two instanced of the Office Inside Class To pass data back and forth
    * with the csv file (and to not get Clipboard errors)
 ***

PartsUsageReport        PROCEDURE(DATE pStartDate,LONG pNumberOfDays)
!region Declarations
                    MAP
GetLastGRNDetails       PROCEDURE(STRING pPartNumber,STRING pRefNumber,*DATE pLastGRNDate,*REAL pLastGRNPrice)!
GetQuantityOnBackOrder  PROCEDURE(STRING pMainStore,STRING pPartNumber,STRING pLocation),LONG
GetRetailSalesValue     PROCEDURE(LONG pRefNumber,STRING pType,<STRING pSiteLocation>),LONG
GetJobsWithSparesRequested      PROCEDURE(STRING pMainStore,LONG pRefNumber,STRING pPartNumber),LONG
GetStockHistoryValues   PROCEDURE(LONG pRefNumber,STRING pMainStore,*LONG pQtyOnJobs,*LONG pTotalQtySold,*DATE pLastSellingDate)
                    END ! MAP

qFilenames          QUEUE(),PRE(qFileNames)
Location                STRING(30)
CSVFilename             STRING(255)
LastLine                LONG()
TotalRecords            LONG()
MainStore               STRING(3)
                    END ! QUEUE

locQtyOnJobs        LONG()
locTotalQtySold     LONG()
locLastSellingDate  DATE()
locLastGRNDate      DATE()
locLastGRNPrice     REAL()
kReportName         EQUATE('Parts Usage Export')
kTempFolder         CSTRING(255)
kTempFileName       CSTRING(255)
kFilename           CSTRING(255)
kReportFolder       CSTRING(255)
locCountRecords     LONG()
kLastColumnDefaultMS       EQUATE('O')
kLastColumnDefault         EQUATE('L')
kLastColumn         STRING(1)
kStartOfDataCell    EQUATE('A14')
kShowExcel          EQUATE(0) !Show Excel (DEBUG)
tmp:VersionNumber   STRING(100)
pEndDate            DATE()
firstSheet          LONG()

ei                  LONG()

exFinal                     Class(oiExcel)
Init                            PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                            PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent                       PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                            End
exTemp                      Class(oiExcel)
Init                            PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                            PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent                       PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                            End
e                   MyExportClass
!endregion

    CODE
!region Processed Code
        Include('..\ReportVersion.inc')
        tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'

        pEndDate = pStartDate + pNumberOfDays

        kTempFolder = GetTempFolder()

        kTempFileName = CLIP(kTempFolder) & 'PARTSUSAGE_' & CLOCK() & RANDOM(1,1000) & '.CSV'

        kFileName = 'Parts Usage Report ' & FORMAT(TODAY(),@d012) & '.xls'

        IF (kTempFolder = '')
            BEEP(BEEP:SystemHand)  ;  YIELD()
            CASE Missive('An error occurred creating the report.'&|
                '|'&|
                '|Please try again.','ServiceBase',|
                'mstop.jpg','/&OK')
            OF 1 ! &OK Button
            END!CASE MESSAGE
            RETURN
        END ! IF

        e.OpenProgressWindow(250)
        e.UpdateProgressText('')
        e.UpdateProgressText('Report Started: ' & FORMAT(e.StartDate,@d06) & ' ' & FORMAT(e.StartTime,@t01))
        e.UpdateProgressText('')

        IF (exFinal.Init(kShowExcel ,0 ) = 0)
            BEEP(BEEP:SystemHand)  ;  YIELD()
            CASE Missive('An error occurred communicating with Excel.'&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                'mstop.jpg','/&OK')
            OF 1 ! &OK Button
            END!CASE MESSAGE
            RETURN
        END ! IF
        !IF (exTemp.Init(kShowExcel ,0 ) = 0)
        !    BEEP(BEEP:SystemHand)  ;  YIELD()
        !    CASE Missive('An error occurred communicating with Excel.'&|
        !        '|'&|
        !        '|Please quit and try again.','ServiceBase',|
        !        'mstop.jpg','/&OK')
        !    OF 1 ! &OK Button
        !    END!CASE MESSAGE
        !    RETURN
        !END ! IF

        FREE(qFilenames)

        e.UpdateProgressText('Searching Parts...')
        e.UpdateProgressText('')

        LOOP x# = 1 TO RECORDS(glo:Queue)
            GET(glo:Queue,x#)

            Access:LOCATION.ClearKey(loc:Location_Key)
            loc:Location = glo:Pointer
            IF (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ELSE ! IF
                CYCLE
            END ! IF

            !Access:TRADEACC.ClearKey(tra:SiteLocationKey)
            !tra:SiteLocation = loc:Location
            !IF (Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign)
            !    locStoresAccont = tra:StoresAccount
            !ELSE ! IF
            !    CYCLE
            !END ! IF

            kTempFilename = CLIP(kTempFolder) & 'PUR_' & CLIP(glo:Pointer) & CLOCK() & RANDOM(1,1000) & '.CSV'

            IF (e.OpenDataFile(kTempFilename,,1))

            END ! IF

            e.UpdateProgressText('Location: ' & glo:Pointer)

            ! Report Title
            e.AddField(kReportName,1,1)

            e.AddField('',1,1)

            ! Header
            e.AddField('Criteria',1)
            e.AddField()
            e.AddField()
            e.AddField(tmp:VersionNumber,,1)

            e.AddField('Start Date',1)
            e.AddField(FORMAT(pStartDate,@d06b),,1)
            e.AddField('End Date',1)
            e.AddField(FORMAT(pEndDate,@d06b),,1)
            e.AddField('Period',1)
            e.AddField(pNumberOfDays,,1)
            e.AddField('Created By',1)

            Access:USERS.ClearKey(use:Password_Key)
            use:Password = glo:Password
            IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                e.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname),,1)
            ELSE ! IF
                e.AddField('AUTOMATED PROCEDURE',,1)
            END ! IF

            e.AddField('Created Date',1)
            e.AddField(FORMAT(TODAY(),@d06b),,1)
            e.AddField('Created Time',1)
            e.AddField(FORMAT(CLOCK(),@t01b),,1)
            e.AddField('',1,1)

            e.AddField('Section Name',1)
            e.AddField(glo:Pointer,,1)

            e.AddField('',1,1)

            e.AddField('Manufacturer',1)
            e.AddField('Part Number')
            e.AddField('Description')
            e.AddField('Location Bin Name')
            e.AddField('Date Created')
            e.AddField('Qty Used On Jobs')
            e.AddField('Location Average Cost')
            IF (loc:Main_Store = 'YES')
                e.AddField('Total Quantity Sold')
                e.AddField('Last Selling Date')
            END ! IF
            e.AddField('Jobs With Spares Requested')
            IF (loc:Main_Store = 'YES')
                e.AddField('Total Qty On Retail Backorder')
            END ! IF
            e.AddField('Location Stock On Hand')
            e.AddField('Qty On Order')
            e.AddField('Last GRN Date')
            e.AddField('Last GRN Price',,1)

            e.UpdateProgressText()

            cnt# = 0
            locCountRecords = 0
            Access:STOCK.ClearKey(sto:Location_Key)
            sto:Location = loc:Location
            sto:Part_Number = ''
            SET(sto:Location_Key,sto:Location_Key)
            LOOP UNTIL Access:STOCK.Next() <> Level:Benign
                IF (sto:Location <> loc:Location)
                    BREAK
                END ! IF
                cnt# += 1
                IF (cnt# > 1000)
                    e.UpdateProgressText('.....',,1)
                    cnt# = 0
                END ! IF

                IF (e.UpdateProgressWindow())
                    BREAK
                END ! IF
                ! Manufacturer
                e.AddField(sto:Manufacturer,1)
                ! Part Number
                e.AddField(sto:Part_Number)
                ! Description
                e.AddField(sto:Description)
                ! Location Bin Name
                e.AddField(sto:Shelf_Location)
                ! Date Created
                e.AddField(FORMAT(sto:DateBooked,@d06b))

                GetStockHistoryValues(sto:Ref_Number,loc:Main_Store,locQtyOnJobs,locTotalQtySold,locLastSellingDate)
                ! Quantity Used On Jobs
                IF (loc:Main_Store = 'YES')
                    e.AddField('0')
                ELSE
                    e.AddField(locQtyOnJobs)
                END ! IF
                ! Location Average Cost
                IF (loc:Main_Store = 'YES')
                    e.AddField(FORMAT(sto:Purchase_Cost,@n_14.2))
                ELSE ! IF
                    e.AddField(FORMAT(sto:AveragePurchaseCost,@n_14.2))
                END ! IF
                ! Total Quantity Sold (Main Store)
                ! Last Selling Date (Main Store)
                IF (loc:Main_Store = 'YES')
                    e.AddField(locTotalQtySold)
                    e.AddField(FORMAT(locLastSellingDate,@d06b))
                ELSE ! IF
                    ! No column
                END ! IF

                ! Jobs With Spares Requested
                e.AddField(GetJobsWithSparesRequested(loc:Main_Store,sto:Ref_Number,sto:Part_Number))
                ! Total Qty On Retail Backorder (Main Store)
                IF (loc:Main_Store = 'YES')
                    e.AddField(GetRetailSalesValue(sto:Ref_Number,'DEC'))
                ELSE ! IF
                    ! No Column
                END ! IF
                ! Location Stock On Hand
                e.AddField(sto:Quantity_Stock)
                ! Qty On Order
                e.AddField(GetQuantityOnBackOrder(loc:Main_Store,sto:Part_Number,sto:Location))

                GetLastGRNDetails(sto:Part_Number,sto:Ref_Number,locLastGRNDate,locLastGRNPrice)
                ! Last GRN Date
                e.AddField(FORMAT(locLastGRNDate,@d06b))
                ! Last GRN Price
                e.AddField(FORMAT(locLastGRNPrice,@n_14.2),,1)

                locCountRecords += 1
            END ! LOOP
            e.UpdateProgressText('Parts Found: ' & locCountRecords)
            e.UpdateProgressText('=====')

            IF (locCountRecords > 0)
                qFilenames.Location = glo:Pointer
                qFilenames.CSVFilename = kTempFilename
                qFilenames.LastLine = locCountRecords + 13
                qFilenames.TotalRecords = locCountRecords
                qFilenames.MainStore = loc:Main_Store
                ADD(qFilenames,-qFilenames.Location)
            END ! IF

            e.CloseDataFile()

            IF (e.CancelPressed > 0)
                BREAK
            END ! IF
        END ! LOOP

        IF (e.CancelPressed = 2)
            e.UpdateProgressText('===============')
            e.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                ' ' & FORMAT(clock(),@t1))
            e.FinishProgress()
            ! Remove temp files
            LOOP i# = 1 TO RECORDS(qFilenames)
                GET(qFilenames,i#)
                REMOVE(qFilenames.CSVFilename)
            END ! LOOP
        ELSE ! IF
            e.UpdateProgressText()
            e.UpdateProgressText('Formatting Documents....')
            e.UpdateProgressText()

            ! Create Final Worksheet

            firstSheet = 1
            LOOP ei = 1 TO RECORDS(qFilenames)
                GET(qFilenames,ei)
                e.UpdateProgressText('Location: ' & CLIP(qFilenames.Location) & ' (' & ei & '/' & RECORDS(qFilenames) & ')')

                IF (qFilenames.MainStore = 'YES')
                    kLastColumn = kLastColumnDefaultMS
                ELSE ! IF
                    kLastColumn = kLastColumnDefault
                END ! IF                
                
                exTemp.Init(kShowExcel,0)
                exTemp.OpenWorkBook(qFilenames.CSVFilename)
                exTemp.Copy('A1',kLastColumn & qFilenames.LastLine)
                !exTemp.Copy()


                
                IF (firstSheet = 1)
                    exFinal.NewWorkBook()
                ELSE
                    exFinal.InsertWorkSheet()
                END ! IF
                
                !STOP('Copied: ' & kLastColumn & qFilenames.LastLine)
                
                exFinal.RenameWorkSheet(qFilenames.Location)
                exFinal.SelectCells('A1')
                
                exFinal.Paste()
 
                exTemp.Copy('A1')
                exTemp.CloseWorkBook(2)
                exTemp.Kill()


                exFinal.WriteToCell('Total Records','D11')
                exFinal.WriteToCell(qFilenames.TotalRecords,'E11')
                exFinal.WriteToCell('Showing','F11')
                exFinal.WriteToCell('=SUBTOTAL(2, G14:G' & qFilenames.LastLine & ')','G11')

                exFinal.WriteToCell('Sub Totals','A' & qFilenames.LastLine + 1)
                exFinal.WriteToCell('=MIN(E14:E' & qFilenames.LastLine & ')','E' & qFilenames.LastLine + 1)
                IF (qFilenames.MainStore = 'YES')
                    exFinal.WriteToCell('=SUBTOTAL(9,G14:G' & qFilenames.LastLine & ')','G' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=MAX(I14:I' & qFilenames.LastLine & ')','I' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,J14:J' & qFilenames.LastLine & ')','J' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,L14:K' & qFilenames.LastLine & ')','K' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,L14:L' & qFilenames.LastLine & ')','L' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,M14:M' & qFilenames.LastLine & ')','M' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=MAX(N14:N' & qFilenames.LastLine & ')','N' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=MAX(O14:O' & qFilenames.LastLine & ')','O' & qFilenames.LastLine + 1)
                    
                    exFinal.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'J14','M' & qFilenames.LastLine + 1)
                    exFinal.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'O14','O' & qFilenames.LastLine + 1)
                ELSE ! IF
                    exFinal.WriteToCell('=SUBTOTAL(9,G14:G' & qFilenames.LastLine & ')','G' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,H14:H' & qFilenames.LastLine & ')','H' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,I14:I' & qFilenames.LastLine & ')','I' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=SUBTOTAL(9,J14:J' & qFilenames.LastLine & ')','J' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=MAX(K14:K' & qFilenames.LastLine & ')','K' & qFilenames.LastLine + 1)
                    exFinal.WriteToCell('=MAX(L14:L' & qFilenames.LastLine & ')','L' & qFilenames.LastLine + 1)
                    
                    exFinal.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'G14','J' & qFilenames.LastLine + 1)
                    exFinal.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'L14','L' & qFilenames.LastLine + 1)
                END ! IF
                
                exFinal.SetCellNumberFormat(oix:NumberFormatText,,,,'A14','D' & qFilenames.LastLine + 1)

                exFinal.SetCellFontStyle('bold','A1','A11')
                exFinal.SetCellFontStyle('bold','A13',kLastColumn & '13')
                exFinal.SetCellFontStyle('bold','D3')
                exFinal.SetCellFontStyle('bold','D11','G11')
                exFinal.SetCellFontStyle('bold','A' & qFilenames.LastLine + 1,kLastColumn & qFilenames.LastLine + 1)
                exFinal.SetCellBackGroundColor(color:silver,'A1','G9')
                exFinal.SetCellBackGroundColor(color:silver,'A11',kLastColumn & '11')
                exFinal.SetCellBackGroundColor(color:silver,'A13',kLastColumn & '13')
                exFinal.SetCellBackGroundColor(color:silver,'A' & qFilenames.LastLine + 1,kLastColumn & qFilenames.LastLine + 1)

                e.FinishFormat(exFinal,kLastColumn,qFilenames.LastLine + 1,kStartOfDataCell)

                !IF (firstSheet = 1)
                !    REMOVE(CLIP(kTempFolder) & CLIP(kFilename))
                !    exFinal.SaveAs(CLIP(kTempFolder) & CLIP(kFilename))
                !ELSE
                !    exFinal.Save()
                !END ! IF
                !exFinal.Copy('A1') ! Clearing Clipboard
                !exFinal.CloseWorkBook(2)

                !!REMOVE(qFilenames.CSVFilename)

                firstSheet = 0
            END ! LOOP
            exFinal.SaveAs(CLIP(kTempFolder) & CLIP(kFilename))
            exFinal.Copy('A1')
            exFinal.CloseWorkBook(2)

            exFinal.Kill()
            

            e.UpdateProgressText()
            e.UpdateProgressText('===============')
            e.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),0)
            IF (e.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                e.FinishProgress()
            ELSE
                e.FinishProgress(kReportFolder)
            END
        END ! IF
!endregion
!region Procedures
!----------------------------------------------------------
GetStockHistoryValues       PROCEDURE(LONG pRefNumber,STRING pMainStore,*LONG pQtyOnJobs,*LONG pTotalQtySold,*DATE pLastSellingDate)
retValue                        LONG()
    CODE
        pQtyOnJobs = 0
        pTotalQtySold = 0
        pLastSellingDate = 0

        Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
        shi:Ref_Number = pRefNumber
        shi:Transaction_Type = 'DEC'
        shi:Date = pStartDate
        SET(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
        LOOP UNTIL Access:STOHIST.Next() <> Level:Benign
            IF (shi:Ref_Number <> pRefNumber OR|
                shi:Transaction_Type <> 'DEC' OR |
                shi:Date > pEndDate)
                BREAK
            END ! IF

            IF (pMainStore = 'YES')
                IF (shi:Notes = 'RETAIL ITEM SOLD')
                    pTotalQtySold += shi:Quantity
                    IF (pLastSellingDate < shi:Date)
                        pLastSellingDate = shi:Date
                    END ! IF
                END ! IF
            ELSE ! IF
                IF (shi:Job_Number = 0)
                    CYCLE
                END ! IF
                pQtyOnJobs += shi:Quantity
            END ! IF

        END ! LOOP

        IF (pMainStore = 'YES')
            IF (pLastSellingDate = 0)
                ! There is no selling date.
                ! Have to find one somewhere
                Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
                shi:Ref_Number = pRefNumber
                shi:Transaction_Type = 'DEC'
                shi:Date = pStartDate
                SET(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
                LOOP UNTIL Access:STOHIST.Previous() <> Level:Benign
                    IF (shi:Ref_Number <> pRefNumber OR |
                        shi:Transaction_Type <> 'DEC')
                        BREAK
                    END ! IF
                    IF (shi:Notes = 'RETAIL ITEM SOLD')
                        pLastSellingDate = shi:Date
                        BREAK
                    END ! IF
                END ! LOOP
            ELSE ! IF

            END ! IF
        END ! IF

!----------------------------------------------------------
GetJobsWithSparesRequested  PROCEDURE(STRING pMainStore,LONG pRefNumber,STRING pPartNumber)!,LONG
retValue                        LONG()
    CODE
        IF (pMainStore = 'YES')
            Access:ORDPEND.ClearKey(ope:Ref_Number_Key)
            ope:Ref_Number = 0
            SET(ope:Ref_Number_Key,ope:Ref_Number_Key)
            LOOP UNTIL Access:ORDPEND.Next() <> Level:Benign
                IF (ope:Part_Number = pPartNumber)
                    retValue += 1
                END ! IF
            END ! LOOP
        ELSE ! IF
            ! How many parts ordered but not received
            Access:PARTS.ClearKey(par:WebOrderKey)
            par:WebOrder = 1
            par:Part_Number = pPartNumber
            SET(par:WebOrderKey,par:WebOrderKey)
            LOOP UNTIL Access:PARTS.Next() <> Level:Benign
                IF (par:WebOrder <> 1 OR |
                    par:Part_Number <> pPartNumber)
                    BREAK
                END ! IF

                IF (par:Part_Ref_Number = pRefNumber)
                    retValue += 1
                END ! IF
            END ! LOOP

            Access:WARPARTS.ClearKey(wpr:WebOrderKey)
            wpr:WebOrder = 1
            wpr:Part_Number = pPartNumber
            SET(wpr:WebOrderKey,wpr:WebOrderKey)
            LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
                IF (wpr:WebOrder <> 1 OR |
                    wpr:Part_Number <> pPartNumber)
                    BREAK
                END ! IF

                IF (wpr:Part_Ref_Number = pRefNumber)
                    retValue += 1
                END ! IF
            END ! LOOP
        END ! IF

        RETURN retValue
!----------------------------------------------------------
GetRetailSalesValue PROCEDURE(LONG pRefNumber,STRING pType,<STRING pSiteLocation>)!,LONG
retValue                LONG()
    CODE
        Access:RETSTOCK.ClearKey(res:PartRefNumberKey)
        res:Part_Ref_Number = pRefNumber
        SET(res:PartRefNumberKey,res:PartRefNumberKey)
        LOOP UNTIL Access:RETSTOCK.Next() <> Level:Benign
            IF (res:Part_Ref_Number <> pRefNumber)
                BREAK
            END ! IF
            IF (res:Despatched <> pType)
                CYCLE
            END ! IF

            IF (pSiteLocation <> '')
                ! Compare the site's location
                Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                ret:Ref_Number = res:Ref_Number
                IF (Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign)
                    Access:TRADEACC_ALIAS.ClearKey(tra_ali:StoresAccountKey)
                    tra_ali:StoresAccount = ret:Account_Number
                    IF (Access:TRADEACC_ALIAS.TryFetch(tra_ali:StoresAccountKey) = Level:Benign)
                        IF (tra_ali:SiteLocation <> pSiteLocation)
                            CYCLE
                        END ! IF
                    END ! IF
                END ! IF
            END ! IF
            retValue += res:Quantity
        END ! LOOP

        RETURN retValue

!----------------------------------------------------------
GetQuantityOnBackOrder      PROCEDURE(STRING pMainStore,STRING pPartNumber,STRING pLocation)!,LONG
retValue                        LONG()
    CODE
        IF (pMainStore = 'YES')
            Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
            orp:All_Received = 'NO'
            orp:Part_Number = pPartNumber
            SET(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
            LOOP UNTIL Access:ORDPARTS.Next() <> Level:Benign
                IF (orp:All_Received <> 'NO' OR |
                    orp:Part_Number <> pPartNumber)
                    BREAK
                END ! IF
                Access:ORDERS.ClearKey(ord:Order_Number_Key)
                ord:Order_Number = orp:Order_Number
                IF (Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign)
                    IF (ord:All_Received = 'NO')
                        retValue += orp:Quantity
                    END ! IF
                END ! IF
            END ! LOOP
        ELSE ! IF
            Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
            sto_ali:Location = MainStoreLocation()
            sto_ali:Part_Number = pPartNumber
            IF (Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign)
                retValue = GetRetailSalesValue(sto_ali:Ref_Number,'PEN',pLocation)
            ELSE ! IF

            END ! IF
        END ! IF

        RETURN retValue

!----------------------------------------------------------
GetLastGRNDetails   PROCEDURE(STRING pPartNumber,STRING pRefNumber,*DATE pLastGRNDate,*REAL pLastGRNPrice)!
    CODE
        pLastGRNDate = ''
        pLastGRNPrice = ''
        Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
        orp:All_Received = 'YES'
        orp:Part_Number = pPartNumber
        SET(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
        LOOP UNTIL Access:ORDPARTS.Next() <> Level:Benign
            IF (orp:All_Received <> 'YES' OR |
                orp:part_Number <> pPartNumber)
                BREAK
            END ! IF

            IF (orp:Part_Ref_Number <> pRefNumber)
                CYCLE
            END ! IF

            IF (orp:Date_Received > pLastGRNDate)
                pLastGRNDate = orp:Date_Received
                pLastGRNPrice = orp:Purchase_Cost
            END ! IF
        END ! LOOP
        RETURN
!endregion

!region OfficeInside Required Procedures

exFinal.Init             PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue             byte
    CODE
        ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
        self.TakeSnapShotOfWindowPos()
        Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
exFinal.Kill             PROCEDURE (byte pUnloadCOM=1)
ReturnValue             byte
    CODE
        self.RestoreSnapShotOfWindowPos()
        ReturnValue = PARENT.Kill (pUnloadCOM)
        Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
exFinal.TakeEvent        PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
    CODE
        PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
        if pEventType = 0  ! Generated by CapeSoft Office Inside
            case event()
            of event:accepted
                case field()
                end
            end
        end
exTemp.Init             PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue             byte
    CODE
        ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
        self.TakeSnapShotOfWindowPos()
        Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
exTemp.Kill             PROCEDURE (byte pUnloadCOM=1)
ReturnValue             byte
    CODE
        self.RestoreSnapShotOfWindowPos()
        ReturnValue = PARENT.Kill (pUnloadCOM)
        Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
exTemp.TakeEvent        PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
    CODE
        PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
        if pEventType = 0  ! Generated by CapeSoft Office Inside
            case event()
            of event:accepted
                case field()
                end
            end
        end
!endregion


   MEMBER('vodr0051.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


TurnaroundTimeSummaryReport PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Local                CLASS
AddToSummary         Procedure(String func:AccountNumber,String,Byte,Long,Long)
LoanIssued           Procedure(),Byte
                     END
save_tra_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:FileName         STRING(255),STATIC
tmp:CurrentStatus2   STRING(30)
tmp:CurrentStatus    STRING(3)
Local_Group          GROUP,PRE(LOC)
DesktopPath          STRING(255)
Path                 STRING(255)
ProgramName          STRING(50)
                     END
Version              STRING('3.1.0000')
ARC_RRC_Group        GROUP,PRE()
tmp:ARC              BYTE(0)
tmp:WhereIsJob       BYTE(0)
tmp:WhoCompleted     BYTE(0)
                     END
Timing_Group         GROUP,PRE()
tmp:StartClockDate   DATE
tmp:StartClockTime   TIME
tmp:CriteriaStartDate DATE
tmp:CriteriaEndDate  DATE
tmp:TotalRepairTime  LONG
tmp:TotalRepairDays  LONG
tmp:CriteriaDays     LONG
tmp:CustomerRepairDate LONG
tmp:CustomerRepairTime LONG
tmp:RRCTime          LONG
tmp:RRCDate          LONG
tmp:ARCTime          LONG
tmp:ARCDate          LONG
tmp:OOCDate          LONG
tmp:OOCTime          LONG
tmp:RRCCompletedDate LONG
tmp:RRCCompletedTime LONG
tmp:ARCCompletedDate LONG
tmp:ARCCompletedTime LONG
tmp:StartDate        DATE
tmp:EndDate          LONG
tmp:EndTime          LONG
tmp:OOCARCDate       LONG
tmp:OOCARCTime       LONG
tmp:OOCRRCTime       LONG
tmp:OOCRRCDate       LONG
tmp:LoanDate         LONG
tmp:LoanTime         LONG
tmp:DateLoanattached DATE
tmp:TimeLoanAttached TIME
                     END
RejectRecord         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE(0)
RecordStatus         BYTE(0)
Progress:Thermometer BYTE(0)
Misc_Group           GROUP,PRE()
tmp:StartRow         LONG
tmp:FinishRow        LONG
tmp:SheetTotal       LONG
tmp:TotalCompleted   LONG
tmp:TotalBooked      LONG
tmp:TotalOpen        LONG
tmp:TotalLoaned      LONG
tmp:tag              STRING(1)
                     END
Sheet_Group          GROUP,PRE()
tmp:SheetDateRepairTotal LONG
tmp:SheetTimeRepairTotal LONG
tmp:SheetDateInTotal LONG
tmp:SheetTimeInTotal LONG
tmp:SheetDateOutTotal LONG
tmp:SheetTimeOutTotal LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
InvoiceSubAccounts   BYTE
IncludeSaturday      LONG
IncludeSunday        LONG
StartWorkHours       TIME
EndWorkHours         TIME
WorkingHours         LONG
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
INI_Group            GROUP,PRE()
ExamineAudStatsInDetail STRING(200)
ExchangeStatusDespatchToRRC LIKE(aus:NewStatus)
ExchangeStatusReceivedAtRRC LIKE(aus:NewStatus)
StatusARCReceivedQuery LIKE(aus:NewStatus)
StatusDespatchedToARC LIKE(aus:NewStatus)
StatusDespatchedToRRC LIKE(aus:NewStatus)
StatusReceivedAtARC  LIKE(aus:NewStatus)
StatusReceivedAtRRC  LIKE(aus:NewStatus)
StatusSendToARC      LIKE(aus:NewStatus)
StatusSendToRRC      LIKE(aus:NewStatus)
HeadAccount          LIKE(tra:Account_Number)
                     END
Sheet_Queue          QUEUE,PRE(shQ)
AccountName          STRING(30)
SheetName            STRING(30)
RecordCount          LONG
                     END
Account_Queue        QUEUE,PRE(aQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
                     END
suppress_detail      BYTE
tmp:Stop             BYTE(0)
BRW7::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RemoteRepairCentre)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RemoteRepairCentre LIKE(tra:RemoteRepairCentre)   !Browse hot field - type derived from field
HeadAccount            LIKE(HeadAccount)              !Browse hot field - type derived from local data
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('Turnaround Time Summary Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Turnaround Time Summary Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(240,376,200,8),RANGE(0,100),COLOR(09A6A7CH,09A6A7CH,COLOR:White)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Turnaround Time Report'),USE(?Tab1)
                           PROMPT('Select Accounts To Include'),AT(240,180),USE(?Prompt5),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(240,192,200,166),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@65L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(304,280,70,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(304,264,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(452,254),USE(?DASTAG),TRN,FLAT,LEFT,FONT(,,,,CHARSET:ANSI),ICON('tagitemp.jpg')
                           BUTTON,AT(452,288),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(452,320),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           PROMPT('Start Date'),AT(248,114),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(340,114,64,10),USE(tmp:CriteriaStartDate),LEFT,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(407,110),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(248,134),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(340,134,64,10),USE(tmp:CriteriaEndDate),LEFT,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),REQ,UPR
                           BUTTON,AT(407,132),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Summary Only'),AT(340,168),USE(suppress_detail),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           PROMPT('Open Jobs Over Last'),AT(247,152),USE(?tmp:CriteriaDays:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Days'),AT(411,152),USE(?tmp:CriteriaDays:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(340,152,64,10),USE(tmp:CriteriaDays),FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Open Jobs Over Last'),TIP('Open Jobs Over Last'),REQ,UPR
                         END
                       END
                       BUTTON,AT(480,366),USE(?OK),IMM,TRN,FLAT,LEFT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?Cancel),IMM,TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
!------------------------------------------------------------------------
    MAP
CoerceToWorkingWeek     PROCEDURE( TIME, TIME, LONG, LONG, DATE, TIME )
CountStatusTime     PROCEDURE()
DateToString        PROCEDURE( DATE ), STRING
DaysTimeBetween PROCEDURE  (LONG, LONG, LONG, LONG, TIME, TIME, *LONG ), LONG
FormatDaysHoursMins         PROCEDURE( LONG, LONG ), STRING
GetHeadAccount              PROCEDURE( STRING )
GetSubAccount               PROCEDURE( STRING )
HoursBetween                PROCEDURE  (TIME, TIME),LONG
LoadAUDSTATS        PROCEDURE( LONG, DATE, *LONG ), LONG ! BOOL
LoadSUBTRACC                PROCEDURE( STRING ), LONG ! BOOL
LoadSTATUS          PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC                PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB                  PROCEDURE( LONG ), LONG ! BOOL
NormaliseDateTime           PROCEDURE  ( LONG, *LONG, *LONG )

!NormaliseDateTime          PROCEDURE(IN:WorkingHours, INOUT:Date, INOUT:Time)
SetSheetTo              PROCEDURE( STRING )
WorkingDaysBetween          PROCEDURE  (LONG, LONG, LONG, LONG), LONG
WorkingHoursBetween         PROCEDURE  (LONG, LONG, LONG, LONG, LONG, LONG, LONG, LONG),LONG
WorkingTimeBetween          PROCEDURE  ( LONG, LONG, LONG, LONG, LONG, LONG, LONG, LONG, *LONG ), LONG
WriteDebug      PROCEDURE( STRING )
    END !MAP
!------------------------------------------------------------------------
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!File Declaration
!Could put it in the dct, but then would have the hassle of changing it
!Ah.. good old legacy, here we come
TurnReport    File,Driver('TOPSPEED'),Pre(turn),Name(tmp:TurnFileName),Create,Bindable,Thread
AccountNameKey          Key(turn:AccountName),DUP
AccountNumberKey        Key(turn:AccountNumber),DUP
Record                  Record
AccountName             String(30)
AccountNumber           String(30)
ARCAccount              Byte(0)         !1 - This is the ARC Account Number Line
Completed               Long
Booked                  Long
Opened                  Long
Loaned                  Long
TimeGroup               Group
Completed1                  Long
Loaned1                     Long
Completed24                 Long
Loaned24                    Long
Completed48                 Long
Loaned48                    Long
Completed72                 Long
Loaned72                    Long
Completed168                Long
Loaned168                   Long
CompletedOver168            Long
LoanedOver168               Long
                        End !Group
CustomerGroup           Group
CustCompleted1              Long
CustCompleted24             Long
CustCompleted48             Long
CustCompleted72             Long
CustCompleted168            Long
CustCompletedOver168        Long
                        End !Group
                        End !Record
                    End !TurnReport    File,Driver('TOPSPEED'),Pre(turn),Name(tmp:TurnFileName),Create,Bindable,Thread

TurdReport   File,Driver('TOPSPEED'),Pre(turd),Name(tmp:TurdFileName),Create,Bindable,Thread
JobNumberKey        Key(turd:AccountNumber,turd:JobNumber),DUP
Record              Record
AccountName         String(30)
AccountNumber       String(30)
JobNumber           Long
WobNumber           String(30)
SubAccountName      String(30)
Manufacturer        String(30)
ModelNumber         String(30)
Warranty            String(3)
DateBooked          Date
TimeBooked          Time
DateCompleted       Date
TimeCompleted       Time
DateCustPerspective     DATE ! 2 Apr 2003 John
TimeCustPerspective     TIME ! 2 Apr 2003 John
TotalRepairType     String(30)
InControl           String(30)
OutControl          String(30)
Loaned              String(3)
                    End !Record
                    End !TurdReport   File,Driver('TOPSPEED'),Pre(turd),Name(tmp:TurdFileName),Create,Bindable,Thread

!Test file
TestFile   File,Driver('ASCII'),Pre(test),Name(tmp:TestFileName),Create,Bindable,Thread
Record      Record
Line1       String(1000)
            End !Record
    End !File,Driver('ASCII'),Pre(test),Name(tmp:TestFileName),Create,Bindable,Thread
TempFilePath         CSTRING(255)
! CountStatusTime tmp:WhereIsJob equates
JobAtRRC      EQUATE(0)
JobAtARC      EQUATE(1)
JobInTransit  EQUATE(2)
JobAt3rdParty EQUATE(4)
JobAtUnknown  EQUATE(128)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW7.UpdateBuffer
   glo:Queue2.Pointer2 = tra:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::8:QUEUE = glo:Queue2
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue2)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer2 = tra:Account_Number
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!------------------------------------------------------------------------
OKButton_Pressed                ROUTINE
    DATA
QueueIndex LONG
QueueCount LONG
    CODE
    !---------------------------------------------------------------------
    QueueCount = Records(glo:Queue2)

    If QueueCount < 1
        Case Missive('You must select at least one account.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive

        EXIT
    End !If ~Records(glo:Queue)
    !---------------------------------------------------------------------
    !debug:Active = True
    tmp:Stop        = False

    !WriteDebug('OKButton_Pressed(START)')
    !---------------------------------------------------------------------
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            tmp:TurnFileName = Clip(TempFilePath) & 'TURNAROUNDREPORT'       & Clock() & '.TPS'
            tmp:TurdFileName = Clip(TempFilePath) & 'TURNAROUNDREPORTDETAIL' & Clock() & '.TPS'
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:TurnFileName = Clip(TempFilePath) & '\TURNAROUNDREPORT'      & Clock() & '.TPS'
            tmp:TurdFileName = Clip(TempFilePath) & 'TURNAROUNDREPORTDETAIL' & Clock() & '.TPS'
        End !If Sub(TempFilePath,-1,1) = '\'
    End
    !---------------------------------------------------------------------
    ExchangeStatusDespatchToRRC = SUB(GETINI('RRC', 'ExchangeStatusDespatchToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    ExchangeStatusReceivedAtRRC = SUB(GETINI('RRC', 'ExchangeStatusReceivedAtRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusARCReceivedQuery      = SUB(GETINI('RRC',      'StatusARCReceivedQuery', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusDespatchedToARC       = SUB(GETINI('RRC',       'StatusDespatchedToARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusDespatchedToRRC       = SUB(GETINI('RRC',       'StatusDespatchedToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusReceivedAtARC         = SUB(GETINI('RRC',         'StatusReceivedAtARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusReceivedAtRRC         = SUB(GETINI('RRC',         'StatusReceivedAtRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusSendToARC             = SUB(GETINI('RRC',             'StatusSendToARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
    StatusSendToRRC             = SUB(GETINI('RRC',             'StatusSendToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)

    ExamineAudStatsInDetail     = GETINI(    'RRC',      'ExamineAudStatsInDetail', , CLIP(PATH()) & '\SB2KDEF.INI')
    !---------------------------------------------------------------------
    Remove(tmp:TurnFileName)
    Create(TurnReport)
    Open(  TurnReport)

    Remove(tmp:TurdFileName)
    Create(TurdReport)
    Open(  TurdReport)
    !---------------------------------------------------------------------
    FREE(Account_Queue)

    !check summat!
    Clear(glo:Queue2)
    glo:Queue2.Pointer2 = HeadAccount
    Get(glo:Queue2, glo:Queue2.Pointer2)
    IF ERROR()
      glo:Queue2.Pointer2 = HeadAccount
      ADD(glo:Queue2)
    END
    QueueCount = Records(glo:Queue2)
    Prog.ProgressSetup(Records(JOBS))
    Save_tra_ID = Access:TRADEACC.SaveFile()
        Loop QueueIndex = 1 To QueueCount
            !-----------------------------------------------------------------
            Get(glo:Queue2, QueueIndex)

            GetHeadAccount(glo:Queue2.Pointer2)
            !WriteDebug('OKButton_Pressed GetHeadAccount(haQ:AccountNumber="' & CLIP(haQ:AccountNumber) & '", haQ:AccountName = "' & CLIP(haQ:AccountName) & '")')
            !Prog.ProgressText(haQ:AccountName)
            !-------------------------------------------------------------
            If haQ:BranchIdentification = ''
                !WriteDebug('haQ:BranchIdentification = " CYCLE')

                CYCLE
            End !If tra:BranchIdentification = ''
            !-------------------------------------------------------------
            CLEAR(Account_Queue)
                aQ:AccountNumber = haQ:AccountNumber
                aQ:AccountName   = haQ:AccountName
            ADD(Account_Queue, +aQ:AccountName)

            !WriteDebug('OKButton_Pressed() LOOP glo:Queue2.Pointer2="' & CLIP(glo:Queue2.Pointer2) & '", haQ:AccountNumber="' & CLIP(haQ:AccountNumber) & '", haQ:AccountName="' & CLIP(haQ:AccountName) & '"')

            !DO ReportOnRepairCentre

            If tmp:Stop = 1
                Break
            End !If 
            !-----------------------------------------------------------------
        End !Loop
    DO ReportOnRepairCentre
    Access:TRADEACC.RestoreFile(Save_tra_ID)
    !---------------------------------------------------------------------
    Close(TurnReport)
    Close(TurdReport)

    Close(TestFile)

    Prog.ProgressFinish()
    !---------------------------------------------------------------------
    If NOT tmp:Stop
        DO MakeExcelDocument
    End !If tmp:Stop

    Post(Event:CloseWindow)
    !---------------------------------------------------------------------

MakeExcelDocument               ROUTINE
    DATA
QueueCount LONG
QueueIndex LONG
local:Desktop       CString(255)
    CODE
    !-------------------------------------------------------------------
    !Make Excel Document
    !WriteDebug('MakeExcelDocument')
    !-------------------------------------------------------------------
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0

    recordstoprocess    = (Records(TurnReport) * 2) + Records(TurdReport) + 24

    !-------------------------------------------------------------------
    !Excel Bit

    excel:ProgramName = 'Turnaround Time Summary Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    ExcelSetup(0)
    ExcelMakeWorkBook('Turnaround Time Report','ServiceBase 2000 Cellular','')
    ExcelSheetName('Summary')

!    Excel:ProgramName = 'Turnaround Report'
!    If ExcelGetFileName(0)
!        
!    End !If ExcelGetFileName()

    ExcelSelectSheet('Summary')
    Do GetNextRecord2
    !-------------------------------------------------------------------
    !Title
    ExcelSelectRange('A1')

    ExcelFontSize(14)
    ExcelCell('Turnaround Report (Summary)',1)
    ExcelSelectRange('A3')
    ExcelCellWidth(25)
    ExcelCell('Jobs Completed From',0)
    ExcelCell(DateToString(tmp:CriteriaStartDate), 1) ! Format(tmp:CriteriaStartDate,@d06),1)
    ExcelSelectRange('A4')
    ExcelCellWidth(14.5)
    ExcelCell('Jobs Completed To',0)
    ExcelCellWidth(11.5)
    ExcelCell(DateToString(tmp:CriteriaEndDate), 1) ! Format(tmp:CriteriaEndDate,@d06),1)
    ExcelSelectRange('A5')
    ExcelCell('Date Created',0)
    ExcelCell(DateToString(Today()), 1) ! Format(Today(),@d06)

    !-------------------------------------------------------------------
    !Neil
        ExcelSelectRange('E3')
        ExcelHorizontal('Right')
        ExcelCell(Clip(tmp:VersionNumber) & ' ', 1) ! trailing space keeps cell border visible
    !-------------------------------------------------------------------

    Do GetNextRecord2
        ExcelGrid( 'A1:E1',   1,1,1,1,15)
        ExcelGrid( 'A3:E5',   1,1,1,1,15)
        ExcelGrid( 'A8:E10',  1,1,1,1,15)
        ExcelGrid( 'G8:K10',  1,1,1,1,15)
        ExcelGrid( 'M8:Q10',  1,1,1,1,15)
        ExcelGrid( 'S8:W10',  1,1,1,1,15)
        ExcelGrid( 'Y8:AC10', 1,1,1,1,15)
        ExcelGrid('AE8:AI10', 1,1,1,1,15)
        ExcelGrid('AK8:AO10', 1,1,1,1,15)

    Do GetNextRecord2

    !-------------------------------------------------------------------
    !REPAIR CENTER PERSPECTIVE

    ExcelSelectRange('A8')
    ExcelCell('Repair Centre Perspective',1)
    ExcelSelectRange('G8')
    ExcelFontSize(12)
    ExcelCell('1 Hour',1)
    ExcelSelectRange('M8')
    ExcelFontSize(12)
    ExcelCell('24 Hours',1)
    ExcelSelectRange('S8')
    ExcelFontSize(12)
    ExcelCell('48 Hours',1)
    ExcelSelectRange('Y8')
    ExcelFontSize(12)
    ExcelCell('72 Hours',1)
    ExcelSelectRange('AE8')
    ExcelFontSize(12)
    ExcelCell('168 Hours',1)
    ExcelSelectRange('AK8')
    ExcelFontSize(12)
    ExcelCell('Over 168 Hours',1)

    Do GetNextRecord2
        ExcelSelectRange('A10')
        ExcelCell('Account Name',1)
        ExcelCell('Jobs Completed Within Dates',1)
        ExcelCell('Jobs Booked Within Dates',1)
        ExcelCell('Open Jobs',1)
        ExcelCell('Loans Issued On Open Jobs',1)
        ExcelCell('',0)

    Do GetNextRecord2
        Loop QueueIndex = 1 to 6
            Do GetNextRecord2
            ExcelCellWidth(10)
            ExcelCell('Completed',1)
            ExcelCellWidth(12)
            ExcelCell('%Completed',1)
            ExcelCellWidth(10)
            ExcelCell('Loans',1)
            ExcelCellWidth(12)
            ExcelCell('Satsified',1)
            ExcelCellWidth(12)
            ExcelCell('%Satisfied',1)
            ExcelCellWidth(1)
            ExcelCell('',0)
        End !Loop


    tmp:StartRow = 11

    !-------------------------------------------------------------------
    Do SummarySheet
    !-------------------------------------------------------------------

    ExcelSelectRange('A' & tmp:FinishRow -1)
    ExcelCell('Sub Totals',1)
    ExcelCell(tmp:TotalCompleted,1)
    ExcelCell(tmp:TotalBooked,1)
    ExcelCell(tmp:TotalOpen,1)
    ExcelCell(tmp:TotalLoaned,1)
    ExcelGrid('A' & tmp:FinishRow -1 & ':E' & tmp:FinishRow -1,1,1,1,1,15)

    !-------------------------------------------------------------------
    !CUSTOMER PERSPECTIVE

    CustomerRow# = tmp:FinishRow + 2

    ExcelSelectRange('A' & CustomerRow#)
    ExcelCell('Customer Perspective',1)
    ExcelSelectRange('G' & CustomerRow#)
    ExcelFontSize(12)
    ExcelCell('1 Hour',1)
    ExcelSelectRange('M' & CustomerRow#)
    ExcelFontSize(12)
    ExcelCell('24 Hours',1)
    ExcelSelectRange('S' & CustomerRow#)
    ExcelFontSize(12)
    ExcelCell('48 Hours',1)
    ExcelSelectRange('Y' & CustomerRow#)
    ExcelFontSize(12)
    ExcelCell('72 Hours',1)
    ExcelSelectRange('AE' & CustomerRow#)
    ExcelFontSize(12)
    ExcelCell('168 Hours',1)
    ExcelSelectRange('AK' & CustomerRow#)
    ExcelFontSize(12)
    ExcelCell('Over 168 Hours',1)

    Do GetNextRecord2

    ExcelSelectRange('A' & CustomerRow# + 2)
    ExcelCell('Account Name',1)
    ExcelCell('Jobs Completed Within Dates',1)
    ExcelCell('',0)
    ExcelCell('',0)
    ExcelCell('',0)
    ExcelCell('',0)

    Do GetNextRecord2

    Loop QueueIndex = 1 to 6
        Do GetNextRecord2
        ExcelCell('Completed',1)
        ExcelCell('',1)     
        ExcelCell('',1)
        ExcelCell('',1)
        ExcelCell('%Completed',1)
        ExcelCell('',0)
    End !Loop

    ExcelGrid('A'  & CustomerRow# & ':E'  & CustomerRow# + 2,1,1,1,1,15)
    ExcelGrid('G'  & CustomerRow# & ':K'  & CustomerRow# + 2,1,1,1,1,15)
    ExcelGrid('M'  & CustomerRow# & ':Q'  & CustomerRow# + 2,1,1,1,1,15)
    ExcelGrid('S'  & CustomerRow# & ':W'  & CustomerRow# + 2,1,1,1,1,15)
    ExcelGrid('Y'  & CustomerRow# & ':AC' & CustomerRow# + 2,1,1,1,1,15)
    ExcelGrid('AE' & CustomerRow# & ':AI' & CustomerRow# + 2,1,1,1,1,15)
    ExcelGrid('AK' & CustomerRow# & ':AO' & CustomerRow# + 2,1,1,1,1,15)

    Do GetNextRecord2
        tmp:StartRow = tmp:FinishRow + 4

    !-------------------------------------------------------------------
    Do SummarySheetCustomer
    !-------------------------------------------------------------------

    ExcelSelectRange('A' & tmp:FinishRow -1)
    ExcelCell('Sub Totals',1)
    ExcelCell(tmp:TotalCompleted,1)
    ExcelGrid('A' & tmp:FinishRow -1 & ':E' & tmp:FinishRow-1,1,1,1,1,15)

    Do GetNextRecord2
    !-------------------------------------------------------------------

    Open(TurdReport)

  IF suppress_detail = FALSE

    FirstAccount"  = ''
    StartRow#      = 9
    tmp:SheetTotal = 0

    !-------------------------------------------------------------------
    ! 6 Mar 2003 John
    ! Show all selected accounts, currently not showing if no jobs found
    !
    SORT(Account_Queue, +aQ:AccountName)
    QueueCount = Records(Account_Queue)

    Loop QueueIndex = 1 To QueueCount
        !-----------------------------------------------------------------
        Do GetNextRecord2

        Get(Account_Queue, QueueIndex)

        !GetHeadAccount(aQ:AccountNumber)
        !WriteDebug('MakeExcelDocument() LOOP aQ:AccountNumber="' & CLIP(aQ:AccountNumber) & '", aQ:AccountName="' & CLIP(aQ:AccountName) & '"' & QueueIndex & ' / ' & QueueCount)

        SetSheetTo(CLIP(aQ:AccountName))
        !-----------------------------------------------------------------
    End !Loop
    !-------------------------------------------------------------------
    Clear(turd:Record)
    Set(turd:JobNumberKey,0)
    Loop
        Next(TurdReport)
        If Error()
            Break
        End !If Error()
        Do GetNextRecord2

        If FirstAccount" = ''
            FirstAccount"   = turd:AccountNumber
            !-----------------------------------------------------------
            ! 6 Mar 2003 John
            ! Show all selected accounts on sheet, currently only showing if jobs found
            !
            !WriteDebug('FirstAccount="' & CLIP(turd:AccountName) & '"')
            SetSheetTo(CLIP(turd:AccountName))
            !-----------------------------------------------------------
            !ExcelSelectRange('A9')
            !StartRow# = 9

        Else !If FirstAccount" = ''
            If turd:AccountNumber <> FirstAccount"
                ExcelSelectRange('E4')
                ExcelCell(tmp:SheetTotal,1)
                ExcelCell('=SUBTOTAL(2, A9:A' & 9 + tmp:SheetTotal & ')', 1)
                ExcelSelectRange('A' & tmp:SheetTotal + 9)
                ExcelCell('TOTAL',1)
                ExcelSelectRange('L4')
                    ExcelFormatCell( '0') !                                                              ! L
                    ExcelCell('=SUM(L9:L' & tmp:SheetTotal+8 & ')+SUM(M9:M' & tmp:SheetTotal+8 & ')', 1) ! L
                    ExcelHorizontal('Left')                                                              ! M
                    ExcelFormatCell( 'HH:MM') !                                                          ! M
                    ExcelCell('=L4', 1)                                                                  ! M

                    ExcelFormatCell( '0') !                                                              ! N
                    ExcelCell('=SUM(N9:N' & tmp:SheetTotal+8 & ')+SUM(O9:O' & tmp:SheetTotal+8 & ')', 1) ! N
                    ExcelHorizontal('Left')                                                              ! O
                    ExcelFormatCell( 'HH:MM') !                                                          ! O                                                                                                                                                         
                    ExcelCell('=N4', 1)                                                                  ! O

                    ExcelFormatCell( '0') !                                                              ! P
                    ExcelCell('=SUM(P9:P' & tmp:SheetTotal+8 & ')+SUM(Q9:Q' & tmp:SheetTotal+8 & ')', 1) ! P
                    ExcelHorizontal('Left')                                                              ! Q
                    ExcelFormatCell( 'HH:MM') !                                                          ! Q
                    ExcelCell('=P4', 1)                                                                  ! Q

                    ExcelFormatCell( '0') !                                                              ! R
                    ExcelCell('=SUM(R9:R' & tmp:SheetTotal+8 & ')+SUM(S9:S' & tmp:SheetTotal+8 & ')', 1) ! R
                    ExcelHorizontal('Left')                                                              ! S
                    ExcelFormatCell( 'HH:MM') !                                                          ! S
                    ExcelCell('=P4', 1)                                                                  ! S

                ExcelSelectRange('L' & tmp:SheetTotal + 9)
                ExcelCell('=L4',1) ! 02 Apr 2003 John
                ExcelCell('=M4',1) ! 02 Apr 2003 John
                ExcelCell('=N4',1)
                ExcelCell('=O4',1)
                ExcelCell('=P4',1)
                ExcelCell('=Q4',1)
                ExcelCell('=R4',1)
                ExcelCell('=S4',1)

                ExcelGrid('A' & tmp:SheetTotal + 9 & ':T' & tmp:SheetTotal + 9,1,1,1,1,15) ! 02 Apr 2003 John

                ExcelAutoFilter('A8:T8') ! 02 Apr 2003 John

                !tmp:SheetDateRepairTotal = 0
                !tmp:SheetTimeRepairTotal = 0
                tmp:SheetDateInTotal     = 0
                tmp:SheetTimeInTotal     = 0
                tmp:SheetDateOutTotal    = 0
                tmp:SheetTimeOutTotal    = 0

                !-------------------------------------------------------
                ! 6 Mar 2003 John
                ! Show all selected accounts on sheet, currently only showing if jobs found
                !
                !WriteDebug('Subsequent Account="' & CLIP(turd:AccountName) & '"')
                SetSheetTo(CLIP(turd:AccountName))
                !-------------------------------------------------------
                !ExcelSelectRange('A9')
                StartRow# = 9
                FirstAccount"   = turd:AccountNumber
                tmp:SheetTotal = 0
            End !If turd:AccountNumber <> FirstAccount"
        End !If FirstAccount" = ''
        tmp:SheetTotal += 1
        !---------------------------------------------------------------
        ExcelSelectRange('A' & StartRow#)
            ExcelCell( turd:JobNumber,                                 0) ! A
            ExcelCell( turd:WobNumber,                                 0) ! B
            ExcelCell( turd:AccountName,                               0) ! C
            ExcelCell( turd:SubAccountName,                            0) ! D
            ExcelCell( turd:Manufacturer,                              0) ! E
            ExcelCell( turd:ModelNumber,                               0) ! F
            ExcelCell( turd:Warranty,                                  0) ! G
            ExcelCell( DateToString(turd:DateBooked   ),               0) ! H Format(turd:DateBooked,    @d06)
            ExcelCell( Format(turd:TimeBooked,    @t01),               0) ! I Format(turd:TimeBooked,    @t01)
            ExcelCell( DateToString(turd:DateCompleted),               0) ! J Format(turd:DateCompleted, @d06)
            ExcelCell( Format(turd:TimeCompleted, @t01),               0) ! K Format(turd:TimeCompleted, @t01)
            !-----------------------------------------------------------
            ! 2 Apr 2003 John
            ExcelCell( INT(turd:DateCustPerspective),                  0) ! L ! 2 Apr 2003 John
            ExcelCell( FORMAT(turd:TimeCustPerspective, @t01),         0) ! turd:TimeCustPerspective-INT(turd:TimeCustPerspective), 0) ! M ! 2 Apr 2003 John
            !-----------------------------------------------------------
            ExcelCell( INT(turd:TotalRepairType),                      0) ! N
            ExcelCell( turd:TotalRepairType-INT(turd:TotalRepairType), 0) ! O
            ExcelCell( INT(turd:InControl),                            0) ! P
            ExcelCell( turd:InControl-INT(turd:InControl),             0) ! Q
            ExcelCell( INT(turd:OutControl),                           0) ! R
            ExcelCell( turd:OutControl-INT(turd:OutControl),           0) ! S
            ExcelCell( turd:Loaned,                                    0) ! T
        !---------------------------------------------------------------
        ExcelSelectRange('L' & StartRow#) ! 2 Apr 2003 John
            ExcelFormatCell( '0') !       ! 2 Apr 2003 John
        ExcelSelectRange('M' & StartRow#) ! 2 Apr 2003 John
            ExcelFormatCell( 'HH:MM') !   ! 2 Apr 2003 John

        ExcelSelectRange('N' & StartRow#)
            ExcelFormatCell( '0') !
        ExcelSelectRange('O' & StartRow#)
            ExcelFormatCell( 'HH:MM') !

        ExcelSelectRange('P' & StartRow#)
            ExcelFormatCell( '0') !
        ExcelSelectRange('Q' & StartRow#)
            ExcelFormatCell( 'HH:MM') !

        ExcelSelectRange('R' & StartRow#)
            ExcelFormatCell( '0') !
        ExcelSelectRange('S' & StartRow#)
            ExcelFormatCell( 'HH:MM') !
            !-----------------------------------------------------------
        !tmp:SheetDateRepairTotal += Sub(turd:TotalRepairType,               1, 2)
        !tmp:SheetTimeRepairTotal += Sub(Deformat(turd:TotalRepairType,@t1), 3, 5)

        !NormaliseDateTime(8640000, tmp:SheetDateRepairTotal, tmp:SheetTimeRepairTotal)
        !If tmp:SheetTimeRepairTotal > 8640000
        !    tmp:SheetDateRepairTotal -= 1
        !    tmp:SheetTimeRepairTotal = 0
        !End !If tmp:SheetTimeRepairTotal > 8640000

        tmp:SheetDateInTotal    = 0
        tmp:SheetTimeInTotal    = 0
        tmp:SheetDateOutTotal   = 0
        tmp:SheetTimeOutTotal   = 0

        StartRow# += 1
    End !Loop
  END
    Close(TurdReport)

    Remove(TurnReport)
    Remove(TurdReport)
  IF suppress_detail = FALSE
    !-------------------------------------------------------------------
    ExcelSelectRange('E4')
    ExcelCell(tmp:SheetTotal,1)
    ExcelCell('=SUBTOTAL(2,A9:A' & 9 + tmp:SheetTotal,1)
    !-------------------------------------------------------------------
    ExcelSelectRange('A' & tmp:SheetTotal + 9)

    ExcelCell('TOTAL',1)
    ExcelSelectRange('L4')
        ExcelFormatCell( '0') !                                                              ! 02 Apr 2003 John
        ExcelCell('=SUM(L9:L' & tmp:SheetTotal+8 & ')+sum(M9:M' & tmp:SheetTotal+8 & ')', 1) ! 02 Apr 2003 John
        ExcelFormatCell( 'HH:MM') !                                                          ! 02 Apr 2003 John
        ExcelCell('=L4', 1)                                                                  ! 02 Apr 2003 John

        ExcelFormatCell( '0') !
        ExcelCell('=SUM(N9:N' & tmp:SheetTotal+8 & ')+sum(O9:O' & tmp:SheetTotal+8 & ')', 1)
        ExcelFormatCell( 'HH:MM') !
        ExcelCell('=N4', 1)

        ExcelFormatCell( '0') !
        ExcelCell('=SUM(P9:P' & tmp:SheetTotal+8 & ')+sum(Q9:Q' & tmp:SheetTotal+8 & ')', 1)
        ExcelFormatCell( 'HH:MM') !
        ExcelCell('=P4', 1)

        ExcelFormatCell( '0') !
        ExcelCell('=SUM(R9:R' & tmp:SheetTotal+8 & ')+sum(S9:S' & tmp:SheetTotal+8 & ')', 1)
        ExcelFormatCell( 'HH:MM') !
        ExcelCell('=P4', 1)
    !-------------------------------------------------------------------
    ExcelSelectRange('L' & tmp:SheetTotal + 9)
        ExcelCell('=L4',1) ! 02 Apr 2003 John
        ExcelCell('=M4',1) ! 02 Apr 2003 John
        ExcelCell('=N4',1)
        ExcelCell('=O4',1)
        ExcelCell('=P4',1)
        ExcelCell('=Q4',1)
        ExcelCell('=R4',1)
        ExcelCell('=S4',1)

    ExcelGrid('A' & tmp:SheetTotal + 9 & ':t' & tmp:SheetTotal + 9,1,1,1,1,15) ! 02 Apr 2003 John

    ExcelAutoFilter('A8:t8') ! 02 Apr 2003 John

    Do GetNextRecord2
  END
    ExcelSelectSheet('Summary')
    ExcelWrapText('B6:E' & tmp:FinishRow + 10,1)
    ExcelSelectRange('A1')
    ExcelCellWidth(25)

    Do GetNextRecord2

    ExcelSaveWorkBook(excel:FileName)

    Do GetNextRecord2

    ExcelClose()

    Do EndPrintRun

    Case Missive('Export Completed.','ServiceBase 3g',|
                   'midea.jpg','/OK') 
        Of 1 ! OK Button
    End ! Case Missive


                                                                                          
!OKButton_Pressed                ROUTINE
!    DATA
!loc:TheDate      LONG
!loc:TheTime      LONG
!AUDSTATSFirst    LONG(True)
!PreviousLocation LIKE(tmp:WhereIsJob) ! For 3rd party despatches
!
!RecordCount  LONG
!    CODE
!    !---------------------------------------------------------------------
!    If ~Records(glo:Queue)
!        Case MessageEx('You must select at least one account.','ServiceBase 2000',|
!                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!        End!Case MessageEx
!
!        EXIT
!    End !If ~Records(glo:Queue)
!    !---------------------------------------------------------------------
!    debug:Active = True
!    tmp:Stop        = 0
!    !---------------------------------------------------------------------
!    Excel:ProgramName = 'Turnaround Report'
!    If ExcelGetFileName()
!        
!    End !If ExcelGetFileName()
!
!    If GetTempPathA(255,TempFilePath)
!        If Sub(TempFilePath,-1,1) = '\'
!            tmp:TurnFileName = Clip(TempFilePath) & 'TURNAROUNDREPORT'       & Clock() & '.TPS'
!            tmp:TurdFileName = Clip(TempFilePath) & 'TURNAROUNDREPORTDETAIL' & Clock() & '.TPS'
!        Else !If Sub(TempFilePath,-1,1) = '\'
!            tmp:TurnFileName = Clip(TempFilePath) & '\TURNAROUNDREPORT'      & Clock() & '.TPS'
!            tmp:TurdFileName = Clip(TempFilePath) & 'TURNAROUNDREPORTDETAIL' & Clock() & '.TPS'
!        End !If Sub(TempFilePath,-1,1) = '\'
!    End
!    !---------------------------------------------------------------------
!    ExchangeStatusDespatchToRRC = SUB(GETINI('RRC', 'ExchangeStatusDespatchToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    ExchangeStatusReceivedAtRRC = SUB(GETINI('RRC', 'ExchangeStatusReceivedAtRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusARCReceivedQuery      = SUB(GETINI('RRC',      'StatusARCReceivedQuery', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusDespatchedToARC       = SUB(GETINI('RRC',       'StatusDespatchedToARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusDespatchedToRRC       = SUB(GETINI('RRC',       'StatusDespatchedToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusReceivedAtARC         = SUB(GETINI('RRC',         'StatusReceivedAtARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusReceivedAtRRC         = SUB(GETINI('RRC',         'StatusReceivedAtRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusSendToARC             = SUB(GETINI('RRC',             'StatusSendToARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusSendToRRC             = SUB(GETINI('RRC',             'StatusSendToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!
!    HeadAccount                 = GETINI('BOOKING', 'HeadAccount', , CLIP(PATH()) & '\SB2KDEF.INI')
!    !---------------------------------------------------------------------
!    Remove(tmp:TurnFileName)
!    Create(TurnReport)
!    Open(  TurnReport)
!
!    Remove(tmp:TurdFileName)
!    Create(TurdReport)
!    Open(  TurdReport)
!    !---------------------------------------------------------------------
!    Prog.ProgressSetup(Records(glo:Queue) * (Records(JOBS) * 3))
!
!    Loop x# = 1 To Records(glo:Queue)
!        Get(glo:Queue,x#)
!        !-----------------------------------------------------------------
!        GetHeadAccount(glo:Pointer)
!        !-----------------------------------------------------------------
!        Prog.ProgressText(haQ:AccountName)
!
!        If haQ:BranchIdentification = ''
!            Cycle
!        End !If tra:BranchIdentification = ''
!        !-----------------------------------------------------------------
!        If tmp:Stop = 1
!            Break
!        End !If 
!        !-----------------------------------------------------------------
!        !Booked within date range
!        Prog.ProgressText(Clip(haQ:AccountName) & ' - Checking Jobs Booked')
!        Save_job_ID = Access:JOBS.SaveFile()
!        Access:JOBS.ClearKey(job:Date_Booked_Key)
!            job:date_booked = tmp:CriteriaStartDate
!        Set(job:Date_Booked_Key,job:Date_Booked_Key)
!
!        Loop WHILE Access:JOBS.NEXT() = Level:Benign
!            If job:date_booked > tmp:CriteriaEndDate       |
!                Then Break.  ! End If
!
!            If Prog.InsideLoop()
!                tmp:Stop = 1
!                Break
!            End !If Prog.InsideLoop()
!
!            IF LoadWEBJOB(job:Ref_Number)
!                Local.AddToSummary( wob:HeadAccountNumber, 'BOOK', 0, 0, 0)
!            END !IF
!
!        End !Loop
!        Access:JOBS.RestoreFile(Save_job_ID)
!        !-----------------------------------------------------------------
!        If tmp:Stop
!            BREAK
!        End !If tmp:Stop
!        !-----------------------------------------------------------------
!        Prog.ProgressText(Clip(tra:Company_Name) & ' - Checking Open Jobs')
!        !Open Jobs
!        Save_job_ID = Access:JOBS.SaveFile()
!        Access:JOBS.ClearKey(job:Date_Booked_Key)
!            job:date_booked = (Today() - tmp:CriteriaDays)
!        Set(job:Date_Booked_Key,job:Date_Booked_Key)
!
!        LOOP WHILE Access:JOBS.NEXT() = Level:Benign
!            If tmp:Stop = 1
!                Break
!            End !If
!            If job:date_booked > Today()      |
!                Then Break.  ! End If
!            If Prog.InsideLoop()
!                tmp:Stop = 1
!                Break
!            End !If Prog.InsideLoop()
!
!            If job:Date_Completed = 0
!                IF LoadWEBJOB(job:Ref_Number)
!                    Local.AddToSummary(wob:HeadAccountNumber,'OPEN',0,0,0)
!
!                    If local.LoanIssued()
!                        ! It has a loan attached
!                        Local.AddToSummary(wob:HeadAccountNumber,'LOAN',0,0,0)
!                    End !If job:Loan_Unit_Number <> 0
!                END !IF
!
!            End !If job:Date_Completed = 0
!        End !Loop
!        Access:JOBS.RestoreFile(Save_job_ID)
!        !-----------------------------------------------------------------
!        If tmp:Stop
!            BREAK
!        End !If tmp:Stop
!        !-----------------------------------------------------------------
!        Prog.ProgressText(Clip(haQ:AccountName) & ' - Checking Completed Jobs')
!        !Now through all completed jobs in date range
!        Save_job_ID = Access:JOBS.SaveFile()
!        Access:JOBS.ClearKey(job:DateCompletedKey)
!            job:Date_Completed = tmp:CriteriaStartDate
!        Set(job:DateCompletedKey,job:DateCompletedKey)
!
!        LOOP WHILE Access:JOBS.NEXT() = Level:Benign
!            !---------------------------------------------------------
!            If tmp:Stop = 1
!                Break
!            End !If
!            If Prog.InsideLoop()
!                tmp:Stop = 1
!                Break
!            End !If Prog.InsideLoop()
!            !---------------------------------------------------------
!            If job:Date_Completed > tmp:CriteriaEndDate
!              !WriteDebug('EOI job:Date_Completed[' & FORMAT(job:Date_Completed, @d6) & '] > tmp:CriteriaEndDate[' & FORMAT(tmp:CriteriaEndDate, @d6) & ']')
!
!                Break
!             End !If
!            !---------------------------------------------------------
!! CASE job:Ref_Number
!! OF 10167 !OROF 9847 OROF 9910
!!    debug:Active = True
!! ELSE
!!    debug:Active = False
!! END !IF
!            !---------------------------------------------------------
!            IF NOT LoadWEBJOB(job:Ref_Number)
!              !WriteDebug('LoadWEBJOB(job:Ref_Number="' & job:Ref_Number & '")FAIL')
!
!                CYCLE
!            END !IF
!
!            !Found
!            If wob:HeadAccountNumber <> haQ:AccountName ! glo:Pointer
!              !WriteDebug('LoadWEBJOB(If wob:HeadAccountNumber="' & CLIP(wob:HeadAccountNumber) & '" <> haQ:AccountNumber="' & CLIP(haQ:AccountNumber) & '")')
!
!                Cycle
!            End !If wob:HeadAccountNumber <> tra:Account_Number
!
!            Access:JOBSE.Clearkey(jobe:RefNumberKey)
!            jobe:RefNumber = job:Ref_Number
!            If NOT Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!              !WriteDebug('JOBSE.Tryfetch(job:Ref_Number="' & job:Ref_Number & '")FAIL')
!
!                CYCLE
!            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!            !---------------------------------------------------------
!            RecordCount += 1
!            !---------------------------------------------------------
!            ! CountStatusTime tmp:WhereIsJob equates
!            !   JobAtRRC     EQUATE(0)
!            !   JobAtARC     EQUATE(1)
!            !   JobInTransit EQUATE(2)
!            ! Original comment text, disagrees with CountStatusTime()
!            !   tmp:WhereIsJob 0 = AT RRC
!            !                  1 = In the middle
!            !                  2 = AT ARC
!            If jobe:WebJob
!               !WriteDebug('OKButton(RRC Job")')
!
!                tmp:WhereIsJob = JobAtRRC ! 0
!                tmp:ARC        = False
!            Else !jobe:WebJob
!               !WriteDebug('OKButton(ARC Job")')
!
!                tmp:WhereIsJob = JobAtARC ! 1
!                tmp:ARC        = True
!            End !jobe:WebJob
!
!            tmp:RRCTime          = 0
!            tmp:RRCDate          = 0
!            tmp:ARCTime          = 0
!            tmp:ARCDate          = 0
!            tmp:OOCTime          = 0
!            tmp:OOCDate          = 0
!            tmp:OOCRRCDate       = 0
!            tmp:OOCRRCTime       = 0
!            tmp:OOCARCDate       = 0
!            tmp:OOCARCTime       = 0
!            tmp:RRCCompletedTime = 0
!            tmp:RRCCompletedDate = 0
!            tmp:ARCCompletedTime = 0
!            tmp:ARCCompletedDate = 0
!            tmp:EndDate          = 0
!            tmp:EndTime          = 0
!            tmp:CurrentStatus    = ''
!            tmp:CurrentStatus2   = ''
!            tmp:LoanDate         = 0
!            tmp:LoanTime         = 0
!            !---------------------------------------------------------
!            tmp:StartClockDate   = job:Date_Booked
!            tmp:StartClockTime   = job:Time_Booked
!            !---------------------------------------------------------
!            Save_aus_ID = Access:AUDSTATS.SaveFile()
!            AUDSTATSFirst = True
!
!            LOOP WHILE LoadAUDSTATS(job:Ref_Number, job:Date_Booked, AUDSTATSFirst) !Access:AUDSTATS.NEXT() = Level:Benign
!                !-----------------------------------------------------
!               !WriteDebug('Access:AUDSTATS.NEXT(job:Ref_Number="' & job:Ref_Number & '", aus:Type="' & CLIP(aus:Type) & '", aus:NewStatus="' & CLIP(aus:NewStatus) & '", aus:OldStatus="' & CLIP(aus:OldStatus) & '")') ! tmp:CurrentStatus    = ''
!
!                tmp:EndDate        = aus:DateChanged
!                tmp:EndTime        = aus:TimeChanged
!                !-----------------------------------------------------
!                If tmp:Stop = 1
!                    Break
!                End !If
!                CountStatusTime()
!
!                CASE aus:Type
!                OF 'JOB'
!                    !WriteDebug('OKButton_Pressed(CASE aus:Type = "JOB")')
!
!                    CASE SUB(aus:NewStatus, 1, 3)
!                    !F ExchangeStatusDespatchToRRC
!                    !F ExchangeStatusReceivedAtRRC
!                    !F StatusARCReceivedQuery
!
!                    !F StatusDespatchedToARC
!                    OF StatusDespatchedToARC
!                        !Job Despatched To ARC
!                       !WriteDebug('AUDSTATS StatusDespatchedToARC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Despatched to ARC')
!
!                        tmp:WhereIsJob = JobInTransit ! 2
!
!                    OF StatusDespatchedToRRC
!                        !Job Despatched To ARC
!                       !WriteDebug('AUDSTATS StatusDespatchedToRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Despatched to ARC')
!
!                        tmp:WhereIsJob = JobInTransit ! 2
!
!                    OF StatusReceivedAtARC
!                        !Job has arrived at ARC
!                       !WriteDebug('AUDSTATS StatusReceivedAtARC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Arrived at ARC')
!
!                        tmp:WhereIsJob = JobAtARC ! 1
!                        tmp:ARC        = True
!
!                    OF StatusReceivedAtRRC
!                        !Job has Arrived at RRC
!                       !WriteDebug('AUDSTATS StatusReceivedAtRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Arrived at RRC')
!
!                        tmp:WhereIsJob = JobAtRRC ! 0
!
!                    !F StatusSendToARC
!                    !F StatusSendToRRC
!                    OF '705'
!                        !---------------------------------------------
!                        !Has the job been competed?
!
!                       !WriteDebug('AUDSTATS 705-COMPLETED Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" COMPLETED')
!                        !---------------------------------------------
!                        !Where is the job when it was completed?
!
!                        If  tmp:WhereIsJob = JobInTransit ! 2
!                            IF haQ:AccountName = HeadAccount
!                               !WriteDebug('705 Completed Status While in transit = choosing ARC')
!
!                                tmp:WhoCompleted = JobAtARC
!                            ELSE
!                               !WriteDebug('705 Completed Status While in transit = choosing RRC')
!
!                                tmp:WhoCompleted = JobAtRRC ! 0
!                            END !IF
!
!                        Else !If tmp:WhereIsJob = 2
!                            tmp:WhoCompleted = tmp:WhereIsJob
!                        End !If tmp:WhereIsJob = 2
!                        !---------------------------------------------
!                        IF tmp:WhoCompleted = JobAtARC
!                           !WriteDebug('705 Completed ARC')
!
!                            tmp:ARCCompletedDate = loc:TheDate
!                            tmp:ARCCompletedTime = loc:TheTime
!                        ELSE
!                           !WriteDebug('705 Completed RRC')
!
!                            tmp:RRCCompletedDate = loc:TheDate
!                            tmp:RRCCompletedTime = loc:TheTime
!                        END !IF
!                        !---------------------------------------------
!
!                    OF '410' ! Despatched To 3rd Party
!                       !WriteDebug('AUDSTATS StatusReceivedAtRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" 410')
!
!                        PreviousLocation = tmp:WhereIsJob 
!                        tmp:WhereIsJob   = JobAt3rdParty
!
!                    ELSE
!                       !WriteDebug('AUDSTATS StatusReceivedAtRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" ELSE')
!
!                        IF (tmp:WhereIsJob = JobAt3rdParty)
!                            !-----------------------------------------
!                            ! 06 Dec 2002 John
!                            ! *** TESTING *** TESTING *** TESTING *** TESTING *** TESTING *** TESTING ***
!                            ! Status 520 Received from 3rd party not being found in audstats
!                            !   assume any status sets location back at RRC
!                            !
!                            !-----------------------------------------
!                            tmp:WhereIsJob = PreviousLocation
!                        END !IF 
!                        !---------------------------------------------
!                    END !CASE
!                !-----------------------------------------------------
!                !When the loan is issued, then work out how long the job has been
!                !at that point.
!                OF  'LOA'
!                   !WriteDebug('OKButton_Pressed(CASE aus:Type = "LOA")')
!
!                    IF tmp:LoanDate = 0 and tmp:LoanTime = 0
!                        !Is loan unit ready to despatch, or ready for qa
!                        If Sub(aus:NewStatus,1,3) = 106 Or Sub(aus:NewStatus,1,3) = 605
!                            tmp:DateLoanAttached = aus:DateChanged
!                            tmp:TimeLoanAttached = aus:TimeChanged
!                            If jobe:WebJob
!                                tmp:LoanDate    = tmp:RRCDate
!                                tmp:LoanTime    = tmp:RRCTime
!                            Else !If jobe:WebJob
!                                tmp:LoanDate    = tmp:ARCDate
!                                tmp:LoanTime    = tmp:ARCTime
!                            End !If jobe:WebJob
!                        End !If Sub(aus:NewStatus,1,3) = 106 Or Sub(aus:NewStatus,1,3) = 605
!                    END !IF
!
!                ELSE
!                   !WriteDebug('OKButton_Pressed(CASE aus:Type = ELSE)')
!                    ! SKIP Empty For first entry
!                    ! SKIP 'EXC'
!                END !CASE aus:Type
!                !-----------------------------------------------------
!                tmp:StartClockDate = aus:DateChanged
!                tmp:StartClockTime = aus:TimeChanged
!                !-----------------------------------------------------
!            End !Loop
!
!            Access:AUDSTATS.RestoreFile(Save_aus_ID)
!            !---------------------------------------------------------
!            !Whoever completed the job gets added to the summary
!            Case tmp:WhoCompleted
!            Of JobAtRRC ! 0 !RRC
!               !WriteDebug('Case tmp:WhoCompleted Of JobAtRRC')
!
!                Local.AddToSummary(wob:HeadAccountNumber, 'COMP', 0,           0,          0)
!                Local.AddToSummary(wob:HeadAccountNumber, 'TURN', 0, tmp:RRCDate,tmp:RRCTime)
!
!            Of JobAtARC ! 1 !ARC
!               !WriteDebug('Case tmp:WhoCompleted Of JobAtARC')
!
!                Local.AddToSummary(          HeadAccount, 'COMP', 1,           0,          0)
!                Local.AddToSummary(          HeadAccount, 'TURN', 1, tmp:ARCDate,tmp:ARCTime)
!
!                IF jobe:WebJob !
!                   !WriteDebug('RRC job "' & job:Ref_Number & '" completed at ARC')
!
!                END !IF
!
!            ELSE
!              !WriteDebug('Case tmp:WhoCompleted ELSE')
!
!                Local.AddToSummary(wob:HeadAccountNumber, 'COMP', 0,           0,          0)
!                Local.AddToSummary(wob:HeadAccountNumber, 'TURN', 0, tmp:RRCDate,tmp:RRCTime)
!
!            End !tmp:WhoCompleted
!
!            If local.LoanIssued()
!               !WriteDebug('If local.LoanIssued()')
!
!                !If RRC job then we'll assume they issued the loan
!                If jobe:WebJob
!                    Local.AddToSummary(wob:HeadAccountNumber, 'TURNLOAN', 0, tmp:LoanDate, tmp:LoanTime)
!                Else !If jobe:WebJob
!                    Local.AddToSummary(          HeadAccount, 'TURNLOAN', 1, tmp:LoanDate, tmp:LoanTime)
!                End !If jobe:WebJob
!            End !If local.LoanIssed()
!
!
!            !Need to create a line for the RRC, and/or ARC
!            If jobe:WebJob ! RRC
!               !WriteDebug('If jobe:WebJob ! RRC')
!
!                Clear(turd:Record)
!                    !-----------------------------------------------------
!                    turd:AccountName        = haQ:AccountNumber
!                    turd:AccountNumber      = wob:HeadAccountNumber
!                    turd:JobNumber          = job:Ref_Number
!                    turd:WobNumber          = wob:JobNumber
!                    turd:SubAccountName     = job:Account_Number
!                    turd:Manufacturer       = job:Manufacturer
!                    turd:ModelNumber        = job:Model_Number
!                    If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'Yes'
!                    Else !If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'No'
!                    End !If job:Warranty_job = 'YES'
!                    
!                    turd:DateBooked         = job:Date_Booked
!                    turd:TimeBooked         = job:Time_Booked
!                    turd:DateCompleted      = job:Date_Completed
!                    turd:TimeCompleted      = job:Time_Completed
!                    !-----------------------------------------------------
!                    ! 25 Nov 2002 John
!                    !oc:TheDate          = WorkingTimeBetween(   IN:StartDate,  IN:EndDate,    IN:StartTime,  IN:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, loc:TheTime)
!                   !WriteDebug('If jobe:WebJob ! RRC')
!
!                    tmp:TotalRepairDays  = WorkingTimeBetween(job:Date_Booked, tmp:EndDate, job:Time_Booked, tmp:EndDate, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, tmp:TotalRepairTime)
!                    turd:TotalRepairType = FormatDaysHoursMins(tmp:TotalRepairDays, tmp:TotalRepairTime) ! tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime, @t01)
!                    turd:InControl       = FormatDaysHoursMins(tmp:RRCDate,         tmp:RRCTime)         ! tmp:RRCDate         & ':' & Format(tmp:RRCTime,         @t01)
!                    turd:OutControl      = FormatDaysHoursMins(tmp:OOCRRCDate,      tmp:OOCRRCTime)      ! tmp:OOCRRCDate      & ':' & Format(tmp:OOCRRCTime,      @t01)
!                    !-----------------------------------------------------
!                    Case local.LoanIssued()
!                        Of 0
!                            turd:Loaned             = 'No'
!                        Of 1
!                            turd:Loaned             = 'Yes'
!                    End !Case local.LoanIssued()
!                    !-----------------------------------------------------
!                Add(TurdReport)
!            End !If jobe:WebJob
!
!            If tmp:ARC
!               !WriteDebug('If tmp:ARC CLEAR(turd:Record)')
!
!                Clear(turd:Record)
!                    !-----------------------------------------------------
!                    Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
!                    tra_ali:Account_Number  = HeadAccount
!                    If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!                        !Found
!
!                    Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!                        !Error
!                    End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!
!                    turd:AccountName        = tra_ali:Company_Name
!                    turd:AccountNumber      = tra_ali:Account_Number
!                    turd:JobNumber          = job:Ref_Number
!                    turd:WobNumber          = wob:JobNumber
!                    turd:SubAccountName     = job:Account_Number
!                    turd:Manufacturer       = job:Manufacturer
!                    turd:ModelNumber        = job:Model_Number
!                    If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'Yes'
!                    Else !If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'No'
!                    End !If job:Warranty_job = 'YES'
!                    
!                    turd:DateBooked         = job:Date_Booked
!                    turd:TimeBooked         = job:Time_Booked
!                    turd:DateCompleted      = job:Date_Completed
!                    turd:TimeCompleted      = job:Time_Completed
!                    !-----------------------------------------------------
!                    ! 22 Nov 2002 John
!                    ! 25 Nov 2002 John
!                    !oc:TheDate          = WorkingTimeBetween(   IN:StartDate,  IN:EndDate,    IN:StartTime,  IN:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, loc:TheTime)
!                   !WriteDebug('If tmp:ARC')
!
!                    tmp:TotalRepairDays  = WorkingTimeBetween(job:Date_Booked, tmp:EndDate, job:Time_Booked, tmp:EndDate, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, tmp:TotalRepairTime)
!                    turd:TotalRepairType = FormatDaysHoursMins(tmp:TotalRepairDays, tmp:TotalRepairTime) ! tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime, @t01)
!                    turd:InControl       = FormatDaysHoursMins(tmp:ARCDate,         tmp:ARCTime)         ! tmp:ARCDate         & ':' & Format(tmp:ARCTime,         @t01)
!                    turd:OutControl      = FormatDaysHoursMins(tmp:OOCARCDate,      tmp:OOCARCTime)      ! tmp:OOCARCDate      & ':' & Format(tmp:OOCARCTime,      @t01)
!                    !-----------------------------------------------------
!                    Case local.LoanIssued()
!                        Of 0
!                            turd:Loaned             = 'No'
!                        Of 1
!                            turd:Loaned             = 'Yes'
!                    End !Case local.LoanIssued()
!                    !-----------------------------------------------------
!                Add(TurdReport)
!            End !If tmp:ARC
!
!        End !Loop
!        Access:JOBS.RestoreFile(Save_job_ID)
!        !-----------------------------------------------------------------
!        If tmp:Stop
!            Break
!        End !If tmp:Stop
!        !-----------------------------------------------------------------
!    End !Loop
!    Access:TRADEACC.RestoreFile(Save_tra_ID)
!    !---------------------------------------------------------------------
!    Close(TurnReport)
!    Close(TurdReport)
!
!    Close(TestFile)
!
!    Prog.ProgressFinish()
!    !---------------------------------------------------------------------
!   !WriteDebug('RecordCount = ' & RecordCount)
!    !---------------------------------------------------------------------
!    If NOT tmp:Stop
!        DO MakeExcelDocument
!    End !If tmp:Stop
!
!    Post(Event:CloseWindow)
!    !---------------------------------------------------------------------
!
!OKButton_Pressed                ROUTINE
!    DATA
!StatusReceivedAtARC   LIKE(aus:NewStatus)
!StatusReceivedAtRRC   LIKE(aus:NewStatus)
!StatusDespatchedToARC LIKE(aus:NewStatus)
!StatusDespatchedToRRC LIKE(aus:NewStatus)
!
!HeadAccount LIKE(tra:Account_Number)
!    CODE
!    !---------------------------------------------------------------------
!    If ~Records(glo:Queue)
!        Case MessageEx('You must select at least one account.','ServiceBase 2000',|
!                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!        End!Case MessageEx
!
!        EXIT
!    End !If ~Records(glo:Queue)
!    !---------------------------------------------------------------------
!    Excel:ProgramName = 'Turnaround Report'
!    If ExcelGetFileName()
!        
!    End !If ExcelGetFileName()
!
!    If GetTempPathA(255,TempFilePath)
!        If Sub(TempFilePath,-1,1) = '\'
!            tmp:TurnFileName = Clip(TempFilePath) & 'TURNAROUNDREPORT'       & Clock() & '.TPS'
!            tmp:TurdFileName = Clip(TempFilePath) & 'TURNAROUNDREPORTDETAIL' & Clock() & '.TPS'
!        Else !If Sub(TempFilePath,-1,1) = '\'
!            tmp:TurnFileName = Clip(TempFilePath) & '\TURNAROUNDREPORT'      & Clock() & '.TPS'
!            tmp:TurdFileName = Clip(TempFilePath) & 'TURNAROUNDREPORTDETAIL' & Clock() & '.TPS'
!        End !If Sub(TempFilePath,-1,1) = '\'
!    End
!    !---------------------------------------------------------------------
!    StatusReceivedAtARC   = SUB(GETINI('RRC',  'StatusReceivedAtARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusReceivedAtRRC   = SUB(GETINI('RRC',  'StatusReceivedAtRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusDespatchedToARC = SUB(GETINI('RRC','StatusDespatchedToARC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!    StatusDespatchedToRRC = SUB(GETINI('RRC','StatusDespatchedToRRC', , CLIP(PATH()) & '\SB2KDEF.INI'), 1, 3)
!
!    HeadAccount           = GETINI('BOOKING','HeadAccount', , CLIP(PATH()) & '\SB2KDEF.INI')
!    !---------------------------------------------------------------------
!    Remove(tmp:TurnFileName)
!    Create(TurnReport)
!    Open(  TurnReport)
!
!    Remove(tmp:TurdFileName)
!    Create(TurdReport)
!    Open(  TurdReport)
!    !---------------------------------------------------------------------
!    Prog.ProgressSetup(Records(glo:Queue) * (Records(JOBS) * 3))
!
!    Loop x# = 1 To Records(glo:Queue)
!        Get(glo:Queue,x#)
!
!        !-----------------------------------------------------------------
!!        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!!        tra:Account_Number  = glo:Pointer
!!        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!!            !Found
!!
!!        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!!            !Error
!!        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!        GetHeadAccount(glo:Pointer)
!        !-----------------------------------------------------------------
!
!        Prog.ProgressText(haQ:AccountName)
!
!        If haQ:BranchIdentification = ''
!            Cycle
!        End !If tra:BranchIdentification = ''
!        !-----------------------------------------------------------------
!        tmp:Stop = 0
!        !Booked within date range
!        Prog.ProgressText(Clip(haQ:AccountName) & ' - Checking Jobs Booked')
!        Save_job_ID = Access:JOBS.SaveFile()
!        Access:JOBS.ClearKey(job:Date_Booked_Key)
!        job:date_booked = tmp:CriteriaStartDate
!        Set(job:Date_Booked_Key,job:Date_Booked_Key)
!        Loop WHILE Access:JOBS.NEXT() = Level:Benign
!            If job:date_booked > tmp:CriteriaEndDate       |
!                Then Break.  ! End If
!
!            If Prog.InsideLoop()
!                tmp:Stop = 1
!                Break
!            End !If Prog.InsideLoop()
!
!            IF LoadWEBJOB(job:Ref_Number)
!                Local.AddToSummary( wob:HeadAccountNumber, 'BOOK', 0, 0, 0)
!            END !IF
!
!!            Access:WEBJOB.Clearkey(wob:RefNumberKey)
!!            wob:RefNumber   = job:Ref_Number
!!            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!                !Found
!!                If wob:HeadAccountNumber <> tra:Account_Number
!!                    Cycle
!!                End !If wob:HeadAccountNumber <> tra:Account_Number
!!                Local.AddToSummary(wob:HeadAccountNumber,'BOOK',0,0,0)
!!            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!                !Error
!!            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!        End !Loop
!        Access:JOBS.RestoreFile(Save_job_ID)
!        !-----------------------------------------------------------------
!        If tmp:Stop
!            BREAK
!        End !If tmp:Stop
!        !-----------------------------------------------------------------
!        Prog.ProgressText(Clip(tra:Company_Name) & ' - Checking Open Jobs')
!        !Open Jobs
!        Save_job_ID = Access:JOBS.SaveFile()
!        Access:JOBS.ClearKey(job:Date_Booked_Key)
!        job:date_booked = Today() - tmp:CriteriaDays
!        Set(job:Date_Booked_Key,job:Date_Booked_Key)
!        Loop
!            If Access:JOBS.NEXT()
!               Break
!            End !If
!            If job:date_booked > Today()      |
!                Then Break.  ! End If
!            If Prog.InsideLoop()
!                tmp:Stop = 1
!                Break
!            End !If Prog.InsideLoop()
!
!            If job:Date_Completed = 0
!                IF LoadWEBJOB(job:Ref_Number)
!                    Local.AddToSummary(wob:HeadAccountNumber,'OPEN',0,0,0)
!
!                    If local.LoanIssued()
!                        ! It has a loan attached
!                        Local.AddToSummary(wob:HeadAccountNumber,'LOAN',0,0,0)
!                    End !If job:Loan_Unit_Number <> 0
!                END !IF
!
!!                Access:WEBJOB.Clearkey(wob:RefNumberKey)
!!                wob:RefNumber   = job:Ref_Number
!!                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!                    !Found
!!                    If wob:HeadAccountNumber <> tra:Account_Number
!!                        Cycle
!!                    End !If wob:HeadAccountNumber <> tra:Account_Number
!!
!!                    Local.AddToSummary(wob:HeadAccountNumber,'OPEN',0,0,0)
!!
!!                    If local.LoanIssued()
!!                        !It has a loan attached
!!                        Local.AddToSummary(wob:HeadAccountNumber,'LOAN',0,0,0)
!!
!!                    End !If job:Loan_Unit_Number <> 0
!!                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!                    !Error
!!                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!
!            End !If job:Date_Completed = 0
!        End !Loop
!        Access:JOBS.RestoreFile(Save_job_ID)
!        !-----------------------------------------------------------------
!        If tmp:Stop
!            BREAK
!        End !If tmp:Stop
!        !-----------------------------------------------------------------
!        Prog.ProgressText(Clip(haQ:AccountName) & ' - Checking Completed Jobs')
!        !Now through all completed jobs in date range
!        Save_job_ID = Access:JOBS.SaveFile()
!        Access:JOBS.ClearKey(job:DateCompletedKey)
!        job:Date_Completed = tmp:CriteriaStartDate
!        Set(job:DateCompletedKey,job:DateCompletedKey)
!        Loop
!            If Access:JOBS.NEXT()
!               Break
!            End !If
!            If job:Date_Completed > tmp:CriteriaEndDate       |
!                Then Break.  ! End If
!
!            If Prog.InsideLoop()
!                tmp:Stop = 1
!                Break
!            End !If Prog.InsideLoop()
!
!            Access:WEBJOB.Clearkey(wob:RefNumberKey)
!            wob:RefNumber   = job:Ref_Number
!            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                !Found
!                If wob:HeadAccountNumber <> haQ:AccountNumber
!                    Cycle
!                End !If wob:HeadAccountNumber <> tra:Account_Number
!
!
!                Access:JOBSE.Clearkey(jobe:RefNumberKey)
!                jobe:RefNumber  = job:Ref_Number
!                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                    !Found
!
!                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                    !Error
!                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!
!                !rrctot:Completed += 1
!
!                !Right, only count jobs that have been completed within the date range
!
!                ! tmp:WhereIsJob 0 = AT RRC
!                !                1 = In the middle
!                !                2 = AT ARC
!                If jobe:WebJob
!                    tmp:WhereIsJob = 0    
!                    tmp:ARC        = 0
!                Else !jobe:WebJob
!                    tmp:WhereIsJob = 1
!                    tmp:ARC        = 1
!                End !jobe:WebJob
!
!                tmp:RRCTime          = 0
!                tmp:RRCDate          = 0
!                tmp:ARCTime          = 0
!                tmp:ARCDate          = 0
!                tmp:OOCTime          = 0
!                tmp:OOCDate          = 0
!                tmp:OOCRRCDate       = 0
!                tmp:OOCRRCTime       = 0
!                tmp:OOCARCDate       = 0
!                tmp:OOCARCTime       = 0
!                tmp:RRCCompletedTime = 0
!                tmp:RRCCompletedDate = 0
!                tmp:ARCCompletedTime = 0
!                tmp:ARCCompletedDate = 0
!                tmp:EndDate          = 0
!                tmp:EndTime          = 0
!                tmp:CurrentStatus    = ''
!                tmp:CurrentStatus2   = ''
!                tmp:LoanDate         = 0
!                tmp:LoanTime         = 0
!
!                Save_aus_ID = Access:AUDSTATS.SaveFile()
!                Access:AUDSTATS.ClearKey(aus:StatusDateKey)
!                aus:RefNumber   = job:Ref_Number
!                aus:DateChanged = job:Date_Booked
!                aus:TimeChanged = job:Time_Booked
!                Set(aus:StatusDateKey,aus:StatusDateKey)
!                Loop
!                    If Access:AUDSTATS.NEXT()
!                       Break
!                    End !If
!                    If aus:RefNumber   <> job:Ref_Number      |
!                    Or aus:DateChanged < job:Date_Booked      |
!                        Then Break.  ! End If
!
!                    tmp:EndDate = aus:DateChanged
!                    tmp:EndTime = aus:TimeChanged
!
!                    If aus:Type = 'JOB'
!                        CASE SUB(aus:NewStatus, 1, 3)
!                        OF StatusReceivedAtARC
!                            !Job has arrived at ARC
!                            Do CountStatusTime
!                            tmp:WhereIsJob = 1
!                            tmp:ARC        = 1
!
!                        OF StatusReceivedAtRRC
!                            !Job has Arrived at RRC
!                            Do CountStatusTime
!                            tmp:WhereIsJob = 0
!
!                        OF StatusDespatchedToARC
!                            !Job Despatched To ARC
!                            Do CountStatusTime
!                            tmp:WhereIsJob = 2
!
!                        OF StatusDespatchedToRRC
!                            !Job Despatched To ARC
!                            Do CountStatusTime
!                            tmp:WhereIsJob = 2
!
!                        OF '705'
!                            !Has the job been competed?
!                            Do CountStatusTime
!
!                            !Where is the job when it was completed?
!                            If tmp:WhereIsJob = 2
!                                tmp:WhoCompleted = 0
!                            Else !If tmp:WhereIsJob = 2
!                                tmp:WhoCompleted = tmp:WhereIsJob
!                            End !If tmp:WhereIsJob = 2
!
!                            tmp:RRCCompletedDate = tmp:RRCDate
!                            tmp:RRCCompletedTime = tmp:RRCTime
!
!                            tmp:ARCCompletedDate = tmp:ARCDate
!                            tmp:ARCCompletedTime = tmp:ARCTime
!                        END !CASE
!
!!                        !Arrived at ARC
!!                        If Sub(aus:NewStatus,1,3) = Sub(StatusReceivedAtARC,1,3)
!!                            !Job has arrived at ARC
!!                            Do CountStatusTime
!!                            tmp:WhereIsJob = 1
!!                            tmp:ARC        = 1
!!                        End !If Sub(aus:NewStatus,1,3) = Sub(StatusReceivedAtARC),1,3)
!!
!!                        !Arrived at RRC
!!                        If Sub(aus:NewStatus,1,3) = Sub(StatusReceivedAtRRC,1,3)
!!                            !Job has Arrived at RRC
!!                            Do CountStatusTime
!!                            tmp:WhereIsJob = 0
!!                        End !If Sub(aus:NewStatus,1,3) = Sub(StatusReceivedAtRRC,1,3)
!!
!!                        !Despatched To ARC
!!                        If Sub(aus:NewStatus,1,3) = Sub(StatusDespatchedToARC,1,3)
!!                            !Job Despatched To ARC
!!                            Do CountStatusTime
!!                            tmp:WhereIsJob = 2
!!                        End !If Sub(aus:NewStatus,1,3) = Sub(StatusDespatchedToARC,1,3)
!!
!!                        !Despatched To RRC
!!                        If Sub(aus:NewStatus,1,3) = Sub(StatusDespatchedToRRC,1,3)
!!                            !Job Despatched To RRC
!!                            Do CountStatusTime
!!                            tmp:WhereIsJob = 2
!!                        End !If Sub(aus:NewStatus,1,3) = Sub(StatusDespatchedToRRC,1,3)
!!
!!                        !Has the job been competed?
!!                        If Sub(aus:NewStatus,1,3) = '705'
!!                            Do CountStatusTime
!!                            !Where is the job when it was completed?
!!                            If tmp:WhereIsJob = 2
!!                                tmp:WhoCompleted = 0
!!                            Else !If tmp:WhereIsJob = 2
!!                                tmp:WhoCompleted = tmp:WhereIsJob
!!                            End !If tmp:WhereIsJob = 2
!!
!!                            tmp:RRCCompletedDate = tmp:RRCDate
!!                            tmp:RRCCompletedTime = tmp:RRCTime
!!
!!                            tmp:ARCCompletedDate = tmp:ARCDate
!!                            tmp:ARCCompletedTime = tmp:ARCTime
!!                        End !If Sub(aus:NewStatus,1,3) = '705'
!
!                        Do CountStatusTime
!
!                        If tmp:CurrentStatus = '' And tmp:CurrentStatus2 = ''
!                            !Should this status be counted?
!                            Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
!                            sts:Ref_Number = Sub(aus:NewStatus,1,3)
!                            If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!                                !Found
!                                If sts:TurnaroundTimeReport
!                                    tmp:StartClockDate  = aus:DateChanged
!                                    tmp:StartClockTime  = aus:TimeChanged
!                                    tmp:CurrentStatus   = Sub(aus:NewStatus,1,3)
!
!                                    Cycle
!                                Else
!                                    tmp:StartClockDate  = aus:DateChanged
!                                    tmp:StartClockTime  = aus:TimeChanged
!                                    tmp:CurrentStatus2  = Sub(aus:NewStatus,1,3)
!
!                                    Cycle
!                                End !If sts:TurnaroundTimeReport
!
!                            Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End                !If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!                        Else !If tmp:CurrentStatus = ''
!
!                        End !If tmp:CurrentStatus = ''
!                    End !If aus:Type = 'JOB'
!
!                    !When the loan is issued, then work out how long the job has been
!                    !at that point.
!                    If aus:Type = 'LOA' and tmp:LoanDate = 0 and tmp:LoanTime = 0
!                        !Is loan unit ready to despatch, or ready for qa
!                        If Sub(aus:NewStatus,1,3) = 106 Or Sub(aus:NewStatus,1,3) = 605
!                            tmp:DateLoanAttached = aus:DateChanged
!                            tmp:TimeLoanAttached = aus:TimeChanged
!                            If jobe:WebJob
!                                tmp:LoanDate    = tmp:RRCDate
!                                tmp:LoanTime    = tmp:RRCTime
!                            Else !If jobe:WebJob
!                                tmp:LoanDate    = tmp:ARCDate
!                                tmp:LoanTime    = tmp:ARCTime
!                            End !If jobe:WebJob
!                        End !If Sub(aus:NewStatus,1,3) = 106 Or Sub(aus:NewStatus,1,3) = 605
!                    End !If aus:Type = 'LOA'
!
!                End !Loop
!                Access:AUDSTATS.RestoreFile(Save_aus_ID)
!                !---------------------------------------------------------
!                !Whoever completed the job gets added to the summary
!                Case tmp:WhoCompleted
!                    Of 0 !RRC
!                        Local.AddToSummary(wob:HeadAccountNumber, 'COMP', 0,           0,          0)
!                        Local.AddToSummary(wob:HeadAccountNumber, 'TURN', 0, tmp:RRCDate,tmp:RRCTime)
!                    Of 1 !ARC
!                        Local.AddToSummary(          HeadAccount, 'TURN', 1, tmp:ARCDate,tmp:ARCTime)
!                        Local.AddToSummary(          HeadAccount, 'COMP', 1,           0,          0)
!                    Of 2 !
!
!                End !tmp:WhoCompleted
!
!                If local.LoanIssued()
!                    !If RRC job then we'll assume they issued the loan
!                    If jobe:WebJob
!                        Local.AddToSummary(wob:HeadAccountNumber, 'TURNLOAN', 0, tmp:LoanDate, tmp:LoanTime)
!                    Else !If jobe:WebJob
!                        Local.AddToSummary(          HeadAccount, 'TURNLOAN', 1, tmp:LoanDate, tmp:LoanTime)
!                    End !If jobe:WebJob
!                End !If local.LoanIssed()
!
!
!                !Need to create a line for the RRC, and/or ARC
!
!                If jobe:WebJob
!                    Clear(turd:Record)
!
!                    turd:AccountName        = haQ:AccountNumber
!                    turd:AccountNumber      = wob:HeadAccountNumber
!                    turd:JobNumber          = job:Ref_Number
!                    turd:WobNumber          = wob:JobNumber
!                    turd:SubAccountName     = job:Account_Number
!                    turd:Manufacturer       = job:Manufacturer
!                    turd:ModelNumber        = job:Model_Number
!                    If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'Yes'
!                    Else !If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'No'
!                    End !If job:Warranty_job = 'YES'
!                    
!                    turd:DateBooked         = job:Date_Booked
!                    turd:TimeBooked         = job:Time_Booked
!                    turd:DateCompleted      = job:Date_Completed
!                    turd:TimeCompleted      = job:Time_Completed
!                    !-----------------------------------------------------
!                    ! 22 Nov 2002 John
!                    tmp:TotalRepairDays     = WorkingDaysBetween( job:Date_Booked, tmp:EndDate,                                                                     haQ:IncludeSaturday, haQ:IncludeSunday)
!                    tmp:TotalRepairTime     = WorkingHoursBetween(job:Date_Booked, tmp:EndDate, job:Time_Booked, tmp:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday)
!                    !tmp:TotalRepairDays     = tmp:EndDate - job:Date_Booked
!                    !tmp:TotalRepairTime     = tmp:EndTime - job:Time_Booked
!                    !-----------------------------------------------------
!                    If tmp:EndDate = job:Date_Booked
!                        turd:TotalRepairType = '0:' & Format(tmp:TotalRepairTime,@t01)
!                    Else !If job:Date_Completed = job:Time_Completed
!                        If tmp:EndTime < job:Time_Booked
!                            tmp:TotalRepairDays -= 1
!                            tmp:TotalRepairTime = (8640000 - job:Time_Booked + tmp:EndTime)
!                            turd:TotalRepairType = tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime,@t01)
!                        End !If job:Time_Completed < job:Time_Booked
!                        If tmp:Endtime > job:Time_Booked
!                            turd:TotalRepairType = tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime,@t01)
!                        End !If job:Time_Completed > job:Time_Booked
!                        If tmp:EndTime = job:Time_Booked
!                            tmp:TotalRepairTime = 0
!                            turd:TotalRepairType = tmp:TotalRepairDays & ':00:00'
!                        End !If job:Time_Completed = job:Time_Booked
!                    End !If job:Date_Completed = job:Time_Completed
!
!                    turd:InControl          = tmp:RRCDate & ':' & Format(tmp:RRCTime,@t01)
!                    turd:OutControl         = tmp:OOCRRCDate & ':' & Format(tmp:OOCRRCTime,@t01)
!
!                    Case local.LoanIssued()
!                        Of 0
!                            turd:Loaned             = 'No'
!                        Of 1
!                            turd:Loaned             = 'Yes'
!                    End !Case local.LoanIssued()
!
!                    Add(TurdReport)
!                End !If jobe:WebJob
!
!                If tmp:ARC
!                    Clear(turd:Record)
!                    Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
!                    tra_ali:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!                    If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!                        !Found
!
!                    Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!                        !Error
!                    End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!
!                    turd:AccountName        = tra_ali:Company_Name
!                    turd:AccountNumber      = tra_ali:Account_Number
!                    turd:JobNumber          = job:Ref_Number
!                    turd:WobNumber          = wob:JobNumber
!                    turd:SubAccountName     = job:Account_Number
!                    turd:Manufacturer       = job:Manufacturer
!                    turd:ModelNumber        = job:Model_Number
!                    If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'Yes'
!                    Else !If job:Warranty_job = 'YES'
!                        turd:Warranty       = 'No'
!                    End !If job:Warranty_job = 'YES'
!                    
!                    turd:DateBooked         = job:Date_Booked
!                    turd:TimeBooked         = job:Time_Booked
!                    turd:DateCompleted      = job:Date_Completed
!                    turd:TimeCompleted      = job:Time_Completed
!                    !-----------------------------------------------------
!                    ! 22 Nov 2002 John
!                    tmp:TotalRepairDays     = WorkingDaysBetween( job:Date_Booked, tmp:EndDate,                                                                     haQ:IncludeSaturday, haQ:IncludeSunday)
!                    tmp:TotalRepairTime     = WorkingHoursBetween(job:Date_Booked, tmp:EndDate, job:Time_Booked, tmp:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday)
!                    !tmp:TotalRepairDays     = tmp:EndDate - job:Date_Booked
!                    !tmp:TotalRepairTime     = tmp:EndTime - job:Time_Booked
!                    !-----------------------------------------------------
!
!                    If tmp:EndDate = job:Date_Booked
!                        turd:TotalRepairType = '0:' & Format(tmp:TotalRepairTime,@t01)
!                    Else !If job:Date_Completed = job:Time_Completed
!                        If tmp:EndTime < job:Time_Booked
!                            tmp:TotalRepairDays -= 1
!                            tmp:TotalRepairTime = (8640000 - job:Time_Booked + tmp:EndTime)
!                            turd:TotalRepairType = tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime,@t01)
!                        End !If job:Time_Completed < job:Time_Booked
!                        If tmp:EndTime > job:Time_Booked
!                            turd:TotalRepairType = tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime,@t01)
!                        End !If job:Time_Completed > job:Time_Booked
!                        If tmp:EndTime = job:Time_Booked
!                            tmp:TotalRepairTime = 0
!                            turd:TotalRepairType = tmp:TotalRepairDays & ':00:00'
!                        End !If job:Time_Completed = job:Time_Booked
!                    End !If job:Date_Completed = job:Time_Completed
!
!                    turd:InControl          = tmp:ARCDate    & ':' & Format(tmp:ARCTime,    @t01)
!                    turd:OutControl         = tmp:OOCARCDate & ':' & Format(tmp:OOCARCTime, @t01)
!
!                    Case local.LoanIssued()
!                        Of 0
!                            turd:Loaned             = 'No'
!                        Of 1
!                            turd:Loaned             = 'Yes'
!                    End !Case local.LoanIssued()
!
!                    Add(TurdReport)
!                End !If tmp:ARC
!
!
!
!            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                !Error
!            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!        End !Loop
!        Access:JOBS.RestoreFile(Save_job_ID)
!        !-----------------------------------------------------------------
!        If tmp:Stop
!            Break
!        End !If tmp:Stop
!        !-----------------------------------------------------------------
!    End !Loop
!    Access:TRADEACC.RestoreFile(Save_tra_ID)
!    !---------------------------------------------------------------------
!    Close(TurnReport)
!    Close(TurdReport)
!
!    Close(TestFile)
!
!    Prog.ProgressFinish()
!
!    !---------------------------------------------------------------------
!    DO MakeExcelDocument
!    !---------------------------------------------------------------------
!
!MakeExcelDocument               ROUTINE
!    !Make Excel Document
!    recordspercycle         = 25
!    recordsprocessed        = 0
!    percentprogress         = 0
!    progress:thermometer    = 0
!
!    recordstoprocess    = (Records(TurnReport) * 2) + Records(TurdReport) + 24
!
!    !Excel Bit
!    ExcelSetup(0)
!    ExcelMakeWorkBook('Turnaround Time Report','ServiceBase 2000 Cellular','')
!    ExcelSheetName('Summary')
!
!    Do GetNextRecord2
!
!    !Title
!    ExcelSelectRange('A1')
!
!    ExcelFontSize(14)
!    ExcelCell('Turnaround Report (Summary)',1)
!    ExcelSelectRange('A3')
!    ExcelCellWidth(25)
!    ExcelCell('Jobs Completed From',0)
!    ExcelCell(DateToString(tmp:CriteriaStartDate), 1) ! Format(tmp:CriteriaStartDate,@d06),1)
!    ExcelSelectRange('A4')
!    ExcelCellWidth(14.5)
!    ExcelCell('Jobs Completed To',0)
!    ExcelCellWidth(11.5)
!    ExcelCell(DateToString(tmp:CriteriaEndDate), 1) ! Format(tmp:CriteriaEndDate,@d06),1)
!    ExcelSelectRange('A5')
!    ExcelCell('Date Created',0)
!    ExcelCell(DateToString(Today()), 1) ! Format(Today(),@d06)
!
!    !Neil
!        ExcelSelectRange('D3')
!        ExcelCell('VODR0051 - ' & CLIP(LOC:Version), 1)
!
!    Do GetNextRecord2
!
!    ExcelGrid( 'A1:E1',   1,1,1,1,15)
!    ExcelGrid( 'A3:E5',   1,1,1,1,15)
!    ExcelGrid( 'A8:E10',  1,1,1,1,15)
!    ExcelGrid( 'G8:K10',  1,1,1,1,15)
!    ExcelGrid( 'M8:Q10',  1,1,1,1,15)
!    ExcelGrid( 'S8:W10',  1,1,1,1,15)
!    ExcelGrid( 'Y8:AC10', 1,1,1,1,15)
!    ExcelGrid('AE8:AI10', 1,1,1,1,15)
!    ExcelGrid('AK8:AO10', 1,1,1,1,15)
!
!    Do GetNextRecord2
!
!    !REPAIR CENTER PERSPECTIVE
!
!    ExcelSelectRange('A8')
!    ExcelCell('Repair Centre Pespective',1)
!    ExcelSelectRange('G8')
!    ExcelFontSize(12)
!    ExcelCell('1 Hour',1)
!    ExcelSelectRange('M8')
!    ExcelFontSize(12)
!    ExcelCell('24 Hours',1)
!    ExcelSelectRange('S8')
!    ExcelFontSize(12)
!    ExcelCell('48 Hours',1)
!    ExcelSelectRange('Y8')
!    ExcelFontSize(12)
!    ExcelCell('72 Hours',1)
!    ExcelSelectRange('AE8')
!    ExcelFontSize(12)
!    ExcelCell('168 Hours',1)
!    ExcelSelectRange('AK8')
!    ExcelFontSize(12)
!    ExcelCell('Over 168 Hours',1)
!
!    Do GetNextRecord2
!
!
!    ExcelSelectRange('A10')
!    ExcelCell('Account Name',1)
!    ExcelCell('Jobs Completed Within Dates',1)
!    ExcelCell('Jobs Booked Within Dates',1)
!    ExcelCell('Open Jobs',1)
!    ExcelCell('Loans Issued On Open Jobs',1)
!    ExcelCell('',0)
!
!    Do GetNextRecord2
!
!    Loop x# = 1 to 6
!        Do GetNextRecord2
!        ExcelCellWidth(10)
!        ExcelCell('Completed',1)
!        ExcelCellWidth(12)
!        ExcelCell('%Completed',1)
!        ExcelCellWidth(10)
!        ExcelCell('Loans',1)
!        ExcelCellWidth(12)
!        ExcelCell('Satsified',1)
!        ExcelCellWidth(12)
!        ExcelCell('%Satisfied',1)
!        ExcelCellWidth(1)
!        ExcelCell('',0)
!    End !Loop
!
!
!    tmp:StartRow = 11
!
!    Do SummarySheet
!
!    ExcelSelectRange('A' & tmp:FinishRow -1)
!    ExcelCell('Sub Totals',1)
!    ExcelCell(tmp:TotalCompleted,1)
!    ExcelCell(tmp:TotalBooked,1)
!    ExcelCell(tmp:TotalOpen,1)
!    ExcelCell(tmp:TotalLoaned,1)
!    ExcelGrid('A' & tmp:FinishRow -1 & ':E' & tmp:FinishRow -1,1,1,1,1,15)
!
!    !CUSTOMER PERSPECTIVE
!
!    CustomerRow# = tmp:FinishRow + 2
!
!    ExcelSelectRange('A' & CustomerRow#)
!    ExcelCell('Customer Pespective',1)
!    ExcelSelectRange('G' & CustomerRow#)
!    ExcelFontSize(12)
!    ExcelCell('1 Hour',1)
!    ExcelSelectRange('M' & CustomerRow#)
!    ExcelFontSize(12)
!    ExcelCell('24 Hours',1)
!    ExcelSelectRange('S' & CustomerRow#)
!    ExcelFontSize(12)
!    ExcelCell('48 Hours',1)
!    ExcelSelectRange('Y' & CustomerRow#)
!    ExcelFontSize(12)
!    ExcelCell('72 Hours',1)
!    ExcelSelectRange('AE' & CustomerRow#)
!    ExcelFontSize(12)
!    ExcelCell('168 Hours',1)
!    ExcelSelectRange('AK' & CustomerRow#)
!    ExcelFontSize(12)
!    ExcelCell('Over 168 Hours',1)
!
!    Do GetNextRecord2
!
!    ExcelSelectRange('A' & CustomerRow# + 2)
!    ExcelCell('Account Name',1)
!    ExcelCell('Jobs Completed Within Dates',1)
!    ExcelCell('',0)
!    ExcelCell('',0)
!    ExcelCell('',0)
!    ExcelCell('',0)
!
!    Do GetNextRecord2
!
!    Loop x# = 1 to 6
!        Do GetNextRecord2
!        ExcelCell('Completed',1)
!        ExcelCell('',1)
!        ExcelCell('',1)
!        ExcelCell('',1)
!        ExcelCell('%Completed',1)
!        ExcelCell('',0)
!    End !Loop
!
!    ExcelGrid('A'  & CustomerRow# & ':E'  & CustomerRow# + 2,1,1,1,1,15)
!    ExcelGrid('G'  & CustomerRow# & ':K'  & CustomerRow# + 2,1,1,1,1,15)
!    ExcelGrid('M'  & CustomerRow# & ':Q'  & CustomerRow# + 2,1,1,1,1,15)
!    ExcelGrid('S'  & CustomerRow# & ':W'  & CustomerRow# + 2,1,1,1,1,15)
!    ExcelGrid('Y'  & CustomerRow# & ':AC' & CustomerRow# + 2,1,1,1,1,15)
!    ExcelGrid('AE' & CustomerRow# & ':AI' & CustomerRow# + 2,1,1,1,1,15)
!    ExcelGrid('AK' & CustomerRow# & ':AO' & CustomerRow# + 2,1,1,1,1,15)
!
!    Do GetNextRecord2
!
!    tmp:StartRow = tmp:FinishRow + 4
!
!    Do SummarySheetCustomer
!
!    ExcelSelectRange('A' & tmp:FinishRow -1)
!    ExcelCell('Sub Totals',1)
!    ExcelCell(tmp:TotalCompleted,1)
!    ExcelGrid('A' & tmp:FinishRow -1& ':E' & tmp:FinishRow-1,1,1,1,1,15)
!
!
!    Do GetNextRecord2
!
!    Open(TurdReport)
!
!    FirstAccount"  = ''
!    StartRow#      = 9
!    tmp:SheetTotal = 0
!
!    Clear(turd:Record)
!    Set(turd:JobNumberKey,0)
!    Loop
!        Next(TurdReport)
!        If Error()
!            Break
!        End !If Error()
!        Do GetNextRecord2
!
!        If FirstAccount" = ''
!            FirstAccount"   = turd:AccountNumber
!            ExcelMakeSheet()
!            ExcelSheetName(Clip(turd:AccountName))
!            Do DetailHeading
!            ExcelSelectRange('A9')
!            StartRow# = 9
!        Else !If FirstAccount" = ''
!            If turd:AccountNumber <> FirstAccount"
!                ExcelSelectRange('E4')
!                ExcelCell(tmp:SheetTotal,1)
!                ExcelCell('=SUBTOTAL(2,A9:A' & 9 + tmp:SheetTotal,1)
!                ExcelSelectRange('A' & tmp:SheetTotal + 9)
!                ExcelCell('TOTAL',1)
!                ExcelSelectRange('L4')
!                    ExcelCell('=SUM(L9:L' & tmp:SheetTotal+8, 1)
!                    ExcelCell('=SUM(M9:M' & tmp:SheetTotal+8, 1)
!                    ExcelCell('=SUM(N9:N' & tmp:SheetTotal+8, 1)
!
!                ExcelSelectRange('L4')
!                    ExcelFormatCell( 'd HH:MM') !
!                ExcelSelectRange('M4')
!                    ExcelFormatCell( 'd HH:MM') !
!                ExcelSelectRange('N4')
!                    ExcelFormatCell( 'd HH:MM') !
!
!                ExcelSelectRange('L' & tmp:SheetTotal + 9)
!                ExcelCell('=L4',1)
!                ExcelCell('=M4',1)
!                ExcelCell('=N4',1)
!                ExcelGrid('A' & tmp:SheetTotal + 9 & ':O' & tmp:SheetTotal + 9,1,1,1,1,15)
!
!                ExcelAutoFilter('A8:O8')
!
!                tmp:SheetDateRepairTotal = 0
!                tmp:SheetTimeRepairTotal = 0
!                tmp:SheetDateInTotal     = 0
!                tmp:SheetTimeInTotal     = 0
!                tmp:SheetDateOutTotal    = 0
!                tmp:SheetTimeOutTotal    = 0
!
!
!                ExcelMakeSheet()
!                ExcelSheetName(Clip(turd:AccountName))
!                Do DetailHeading
!                ExcelSelectRange('A9')
!                StartRow# = 9
!                FirstAccount"   = turd:AccountNumber
!                tmp:SheetTotal = 0
!            End !If turd:AccountNumber <> FirstAccount"
!        End !If FirstAccount" = ''
!        tmp:SheetTotal += 1
!
!        ExcelSelectRange('A' & StartRow#)
!            ExcelCell( turd:JobNumber,                   0)
!            ExcelCell( turd:WobNumber,                   0)
!            ExcelCell( turd:AccountName,                 0)
!            ExcelCell( turd:SubAccountName,              0)
!            ExcelCell( turd:Manufacturer,                0)
!            ExcelCell( turd:ModelNumber,                 0)
!            ExcelCell( turd:Warranty,                    0)
!            ExcelCell( DateToString(turd:DateBooked   ), 0) ! Format(turd:DateBooked, @d06)
!            ExcelCell( Format(turd:TimeBooked,    @t01), 0) ! Format(turd:TimeBooked, @t01)
!            ExcelCell( DateToString(turd:DateCompleted), 0) ! Format(turd:DateCompleted, @d06)
!            ExcelCell( Format(turd:TimeCompleted, @t01), 0) ! Format(turd:TimeCompleted, @t01)
!            ExcelCell( turd:TotalRepairType,             0) ! 
!            ExcelCell( turd:InControl,                   0) ! 
!            ExcelCell( turd:OutControl,                  0) ! 
!            ExcelCell( turd:Loaned,                      0)
!
!        ExcelSelectRange('L' & StartRow#)
!            ExcelFormatCell( 'd HH:MM') !
!        ExcelSelectRange('M' & StartRow#)
!            ExcelFormatCell( 'd HH:MM') !
!        ExcelSelectRange('N' & StartRow#)
!            ExcelFormatCell( 'd HH:MM') !
!
!        tmp:SheetDateRepairTotal += Sub(turd:TotalRepairType,               1, 2)
!        tmp:SheetTimeRepairTotal += Sub(Deformat(turd:TotalRepairType,@t1), 3, 5)
!
!        If tmp:SheetTimeRepairTotal > 8640000
!            tmp:SheetDateRepairTotal -= 1
!            tmp:SheetTimeRepairTotal = 0
!        End !If tmp:SheetTimeRepairTotal > 8640000
!
!        tmp:SheetDateInTotal    = 0
!        tmp:SheetTimeInTotal    = 0
!        tmp:SheetDateOutTotal   = 0
!        tmp:SheetTimeOutTotal   = 0
!
!        StartRow# += 1
!    End !Loop
!
!    ExcelSelectRange('E4')
!    ExcelCell(tmp:SheetTotal,1)
!    ExcelCell('=SUBTOTAL(2,A9:A' & 9 + tmp:SheetTotal,1)
!    ExcelSelectRange('A' & tmp:SheetTotal + 9)
!    ExcelCell('TOTAL',1)
!    ExcelSelectRange('L4')
!        ExcelCell('=SUM(L9:L' & tmp:SheetTotal+8,1)
!        ExcelCell('=SUM(M9:M' & tmp:SheetTotal+8,1)
!        ExcelCell('=SUM(N9:N' & tmp:SheetTotal+8,1)
!
!    ExcelSelectRange('L4')
!        ExcelFormatCell( 'd HH:MM') ! 
!    ExcelSelectRange('M4')
!        ExcelFormatCell( 'd HH:MM') ! 
!    ExcelSelectRange('N4')
!        ExcelFormatCell( 'd HH:MM') ! 
!
!    ExcelSelectRange('L' & tmp:SheetTotal + 9)
!    ExcelCell('=L4',1)
!    ExcelCell('=M4',1)
!    ExcelCell('=N4',1)
!    ExcelGrid('A' & tmp:SheetTotal + 9 & ':O' & tmp:SheetTotal + 9,1,1,1,1,15)
!
!    ExcelAutoFilter('A8:O8')
!    Close(TurdReport)
!
!    Do GetNextRecord2
!
!    ExcelSelectSheet('Summary')
!    ExcelWrapText('B1:E' & tmp:FinishRow + 10,1)
!    ExcelSelectRange('A1')
!    ExcelCellWidth(25)
!
!    Do GetNextRecord2
!
!    ExcelSaveWorkBook(tmp:FileName)
!
!    Do GetNextRecord2
!
!    ExcelClose()
!
!    Do EndPrintRun
!
!
!    Case MessageEx('Export Completed.','ServiceBase 2000',|
!                   'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!        Of 1 ! &OK Button
!    End!Case MessageEx
!
!    IF debug:Active = True
!        ! 14 Jan 2003 john
!        Remove(TurnReport)
!        Remove(TurdReport)
!    END !IF
!
!------------------------------------------------------------------------
ReportOnRepairCentre        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('ReportOnRepairCentre(' & CLIP(haQ:AccountName) & ')')
            !-------------------------------------------------------------
            DO JobsBookedInDateRange
            DO OpenJobsBookedInDateRange
            DO JobsCompletedInDateRange
            !-------------------------------------------------------------
        !WriteDebug('ReportOnRepairCentre(END)')
        !-----------------------------------------------------------------
    EXIT
JobsBookedInDateRange           ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('JobsBookedInDateRange(START)')

        !Booked within date range
        If tmp:Stop
            EXIT
        End !If tmp:Stop
        !-----------------------------------------------------------------
        Prog.ProgressText('Checking Jobs Booked')

        Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:Date_Booked_Key)
                job:date_booked = tmp:CriteriaStartDate
            Set(job:Date_Booked_Key,job:Date_Booked_Key)

            Loop WHILE Access:JOBS.NEXT() = Level:Benign
                If job:date_booked > tmp:CriteriaEndDate       |
                    Then BREAK.  ! End If

                If Prog.InsideLoop()
                    tmp:Stop = 1
                    BREAK
                End !If Prog.InsideLoop()

                IF LoadWEBJOB(job:Ref_Number)
                   CLEAR(HeadAccount_Queue)
                   haQ:AccountNumber = wob:HeadAccountNumber
                   GET(HeadAccount_Queue,HeadAccount_Queue.haQ:AccountNumber)
                   IF ERROR()
                     CYCLE
                   END
                   !If CLIP(wob:HeadAccountNumber) <> CLIP(haQ:AccountNumber) ! glo:Pointer
                   !  !WriteDebug('LoadWEBJOB(If wob:HeadAccountNumber="' & CLIP(wob:HeadAccountNumber) & '" <> haQ:AccountNumber="' & CLIP(haQ:AccountNumber) & '")')
                   !
                   !    Cycle
                   !  End !If wob:HeadAccountNumber <> tra:Account_Number
                   Local.AddToSummary( wob:HeadAccountNumber, 'BOOK', 0, 0, 0)
                END !IF

            End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)
        !-----------------------------------------------------------------
    EXIT
OpenJobsBookedInDateRange           ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('OpenJobsBookedInDateRange(START)')

        If tmp:Stop
            EXIT
        End !If tmp:Stop
        !-----------------------------------------------------------------
        Prog.ProgressText('Checking Open Jobs')
        !Open Jobs
        Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:date_booked = (Today() - tmp:CriteriaDays)
            Set(job:Date_Booked_Key,job:Date_Booked_Key)

            LOOP WHILE Access:JOBS.NEXT() = Level:Benign
                If job:date_booked > Today()      |
                    Then Break.  ! End If
                If Prog.InsideLoop()
                    tmp:Stop = 1
                    Break
                End !If Prog.InsideLoop()

                If job:Date_Completed = 0
                    IF LoadWEBJOB(job:Ref_Number)
                        CLEAR(HeadAccount_Queue)
                        haQ:AccountNumber = wob:HeadAccountNumber
                        GET(HeadAccount_Queue,HeadAccount_Queue.haQ:AccountNumber)
                        IF ERROR()
                          CYCLE
                        END
!                        If CLIP(wob:HeadAccountNumber) <> CLIP(haQ:AccountNumber) ! glo:Pointer
!                           !WriteDebug('LoadWEBJOB(If wob:HeadAccountNumber="' & CLIP(wob:HeadAccountNumber) & '" <> haQ:AccountNumber="' & CLIP(haQ:AccountNumber) & '")')
!
!                            Cycle
!                        End !If wob:HeadAccountNumber <> tra:Account_Number

                        Local.AddToSummary(wob:HeadAccountNumber,'OPEN',0,0,0)

                        If local.LoanIssued()
                            ! It has a loan attached
                            Local.AddToSummary(wob:HeadAccountNumber,'LOAN',0,0,0)
                        End !If job:Loan_Unit_Number <> 0
                    END !IF
                ELSE
                  CYCLE
                End !If job:Date_Completed = 0
            End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)
        !-----------------------------------------------------------------
    EXIT
JobsCompletedInDateRange            ROUTINE
    DATA
RecordCount   LONG

CustomerRepairDate   DATE
CustomerRepairTime   TIME
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('JobsCompletedInDateRange(START)[' & FORMAT(tmp:CriteriaStartDate, @D6) & '] To [' & FORMAT(tmp:CriteriaEndDate, @D6) & ']')

        If tmp:Stop
            EXIT
        End !If tmp:Stop
        !WriteDebug('start of loop')
        !-----------------------------------------------------------------
        Prog.ProgressText('Checking Completed Jobs')
        !Now through all completed jobs in date range

        Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateCompletedKey)
                job:Date_Completed = tmp:CriteriaStartDate
            Set(job:DateCompletedKey,job:DateCompletedKey)

            LOOP WHILE Access:JOBS.NEXT() = Level:Benign
                !---------------------------------------------------------
                Do GetNextRecord2
                If Prog.InsideLoop()
                    tmp:Stop = 1
                    Break
                End !If Prog.InsideLoop()
                !---------------------------------------------------------
                If job:Date_Completed > tmp:CriteriaEndDate
                    !WriteDebug('EOI job:Date_Completed[' & FORMAT(job:Date_Completed, @d6) & '] > tmp:CriteriaEndDate[' & FORMAT(tmp:CriteriaEndDate, @d6) & ']')
                    Break
                 End !If
                !WriteDebug('after break')
                IF NOT LoadWEBJOB(job:Ref_Number)
                    !WriteDebug('LoadWEBJOB(job:Ref_Number="' & job:Ref_Number & '")FAIL')
                    CYCLE
                END !IF
                !WriteDebug('after loadwebjob')
                !Found
                CLEAR(HeadAccount_Queue)
                haQ:AccountNumber = wob:HeadAccountNumber
                GET(HeadAccount_Queue,HeadAccount_Queue.haQ:AccountNumber)
                IF ERROR()
                  CYCLE
                END
!                If CLIP(wob:HeadAccountNumber) <> CLIP(aQ:AccountNumber) ! glo:Pointer
!                    !WriteDebug('LoadWEBJOB(If wob:HeadAccountNumber="' & CLIP(wob:HeadAccountNumber) & '" <> haQ:AccountNumber="' & CLIP(haQ:AccountNumber) & '")')
!                    Cycle
!                End !If wob:HeadAccountNumber <> tra:Account_Number
                !WriteDebug('after headacc compare')
                !---------------------------------------------------------
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If NOT Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !WriteDebug('JOBSE.Tryfetch(job:Ref_Number="' & job:Ref_Number & '")FAIL')
                    CYCLE
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !---------------------------------------------------------
                RecordCount += 1
                !---------------------------------------------------------
                ! CountStatusTime tmp:WhereIsJob equates
                !   JobAtRRC     EQUATE(0)
                !   JobAtARC     EQUATE(1)
                !   JobInTransit EQUATE(2)
                ! Original comment text, disagrees with CountStatusTime()
                !   tmp:WhereIsJob 0 = AT RRC
                !                  1 = In the middle
                !                  2 = AT ARC
                If jobe:WebJob
                   !WriteDebug('OKButton(RRC Job")')
                    tmp:WhereIsJob = JobAtRRC ! 0
                    tmp:ARC        = False
                Else !jobe:WebJob
                   !WriteDebug('OKButton(ARC Job")')
                    tmp:WhereIsJob = JobAtARC ! 1
                    tmp:ARC        = True
                End !jobe:WebJob

                tmp:RRCTime          = 0
                tmp:RRCDate          = 0
                tmp:ARCTime          = 0
                tmp:ARCDate          = 0
                tmp:OOCTime          = 0
                tmp:OOCDate          = 0
                tmp:OOCRRCDate       = 0
                tmp:OOCRRCTime       = 0
                tmp:OOCARCDate       = 0
                tmp:OOCARCTime       = 0

                tmp:CustomerRepairDate = 0
                tmp:CustomerRepairTime = 0

                tmp:ARCCompletedTime = 0
                tmp:ARCCompletedDate = 0

                tmp:RRCCompletedTime = 0
                tmp:RRCCompletedDate = 0

                tmp:EndDate          = 0
                tmp:EndTime          = 0
                tmp:CurrentStatus    = ''
                tmp:CurrentStatus2   = ''
                tmp:LoanDate         = 0
                tmp:LoanTime         = 0
                !---------------------------------------------------------
                tmp:TotalRepairDays  = WorkingTimeBetween(job:Date_Booked, job:Date_Completed, job:Time_Booked, job:Time_Completed, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, tmp:TotalRepairTime)

                tmp:StartClockDate   = job:Date_Booked
                tmp:StartClockTime   = job:Time_Booked
                !---------------------------------------------------------
                !WriteDebug('before status checking code')
                DO CheckStatusChanges

                If tmp:Stop = 1
                    EXIT
                End !If

                !WriteDebug('Got here - after check status changes')
                !---------------------------------------------------------
                !Whoever completed the job gets added to the summary
                !WriteDebug('CHECK(wob:HeadAccountNumber="' & CLIP(wob:HeadAccountNumber) & '", HeadAccount="' & CLIP(HeadAccount) & '"')
               
                Local.AddToSummary(wob:HeadAccountNumber, 'COMP', tmp:WhoCompleted,                      0,                      0)
                Local.AddToSummary(wob:HeadAccountNumber, 'CUST', tmp:WhoCompleted, tmp:CustomerRepairDate, tmp:CustomerRepairTime) ! Customer perspective
                IF jobe:webjob AND tmp:arc = true
                  Local.AddToSummary(HeadAccount, 'COMP', tmp:WhoCompleted,                      0,                      0)
                  Local.AddToSummary(HeadAccount, 'CUST', tmp:WhoCompleted, tmp:CustomerRepairDate, tmp:CustomerRepairTime) ! Customer perspective
                END
                Case tmp:WhoCompleted
                Of JobAtRRC ! 0 !RRC
                   !WriteDebug('Case tmp:WhoCompleted Of JobAtRRC')

                    Local.AddToSummary(wob:HeadAccountNumber, 'TURN', JobAtRRC,            tmp:RRCDate,            tmp:RRCTime)
                    IF jobe:webjob AND tmp:arc = true
                      Local.AddToSummary(          HeadAccount, 'TURN', JobAtARC,            tmp:ARCDate,            tmp:ARCTime)
                    END
                    !Added by Neil 10/04/03
                    !IF tmp:ARC = True
                    !   Local.AddToSummary(          HeadAccount, 'TURN', JobAtARC,            tmp:ARCDate,            tmp:ARCTime)
                    !END
                Of JobAtARC  ! 1 !ARC
                    !WriteDebug('Case tmp:WhoCompleted Of JobAtARC, ARC[HeadAccount]="' & CLIP(HeadAccount) & '"')

                    Local.AddToSummary(          HeadAccount, 'TURN', JobAtARC,            tmp:ARCDate,            tmp:ARCTime)
                    IF jobe:webjob
                      Local.AddToSummary(wob:HeadAccountNumber, 'TURN', JobAtRRC,            tmp:RRCDate,            tmp:RRCTime)
                    END
                    !IF jobe:WebJob !
                    !   !WriteDebug('RRC job "' & job:Ref_Number & '" completed at ARC')
                    !
                    !END !IF

                ELSE ! JobInTransit ! 2
                   !WriteDebug('Case tmp:WhoCompleted ELSE')
                   Local.AddToSummary(wob:HeadAccountNumber, 'TURN', JobAtRRC,            tmp:RRCDate,            tmp:RRCTime)
                End !tmp:WhoCompleted

                !---------------------------------------------------------
                If local.LoanIssued()
                   !WriteDebug('If local.LoanIssued()')

                    !If RRC job then we'll assume they issued the loan
                    If jobe:WebJob
                      !Local.AddToSummary(wob:HeadAccountNumber, 'LOAN', tmp:WhoCompleted,                      0,                      0)  !Local.AddToSummary(wob:HeadAccountNumber, 'TURNLOAN', JobAtRRC, tmp:LoanDate, tmp:LoanTime)
                      Local.AddToSummary(wob:HeadAccountNumber, 'TURNLOAN', tmp:WhoCompleted,                      tmp:LoanDate,                      tmp:LoanTime)  !Local.AddToSummary(wob:HeadAccountNumber, 'TURNLOAN', JobAtRRC, tmp:LoanDate, tmp:LoanTime)
                    Else !If jobe:WebJob
                      !Local.AddToSummary(HeadAccount, 'LOAN', tmp:WhoCompleted,                      0,                      0)  !Local.AddToSummary(          HeadAccount, 'TURNLOAN', JobAtARC, tmp:LoanDate, tmp:LoanTime)
                      Local.AddToSummary(HeadAccount, 'TURNLOAN', tmp:WhoCompleted,                      tmp:LoanDate,                      tmp:LoanTime)  !Local.AddToSummar
                    End !If jobe:WebJob
                End !If local.LoanIssed()
                !---------------------------------------------------------
                !Need to create a line for the RRC, and/or ARC
                IF suppress_detail = FALSE
                  If jobe:WebJob ! RRC
                      !WriteDebug('If jobe:WebJob ! RRC')
                      Clear(turd:Record)
                          !-----------------------------------------------------
                          turd:AccountNumber      = haQ:AccountNumber ! wob:HeadAccountNumber
                          turd:AccountName        = haQ:AccountName

                          turd:JobNumber          = job:Ref_Number
                          turd:WobNumber          = wob:JobNumber
                          turd:SubAccountName     = job:Account_Number
                          turd:Manufacturer       = job:Manufacturer
                          turd:ModelNumber        = job:Model_Number
                          If job:Warranty_job = 'YES'
                              turd:Warranty       = 'Yes'
                          Else !If job:Warranty_job = 'YES'
                              turd:Warranty       = 'No'
                          End !If job:Warranty_job = 'YES'
                          
                          turd:DateBooked         = job:Date_Booked
                          turd:TimeBooked         = job:Time_Booked
                          turd:DateCompleted      = job:Date_Completed
                          turd:TimeCompleted      = job:Time_Completed
                          !-----------------------------------------------------
                          ! 2 Apr 2003 John
                          !IF (tmp:CustomerRepairDate = 0) AND (tmp:CustomerRepairTime = 0)
                          !    tmp:CustomerRepairDate = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, TODAY(), CLOCK(), DEFORMAT('00:00:01', @T2), DEFORMAT('23:59:59', @T2), tmp:CustomerRepairTime)
                          !END !IF
                          !
                          turd:DateCustPerspective = tmp:CustomerRepairDate
                          turd:TimeCustPerspective = tmp:CustomerRepairTime
                          !-----------------------------------------------------
                          ! 25 Nov 2002 John
                          !oc:TheDate          = WorkingTimeBetween(   IN:StartDate,  IN:EndDate,    IN:StartTime,  IN:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, loc:TheTime)
                          !WriteDebug('If jobe:WebJob ! RRC')

                          !tmp:TotalRepairDays  = WorkingTimeBetween(job:Date_Booked, tmp:EndDate, job:Time_Booked, tmp:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, tmp:TotalRepairTime)
                          turd:TotalRepairType = FormatDaysHoursMins(tmp:TotalRepairDays, tmp:TotalRepairTime) ! tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime, @t01)
                          turd:InControl       = FormatDaysHoursMins(tmp:RRCDate,         tmp:RRCTime)         ! tmp:RRCDate         & ':' & Format(tmp:RRCTime,         @t01)
                          turd:OutControl      = FormatDaysHoursMins(tmp:OOCRRCDate,      tmp:OOCRRCTime)      ! tmp:OOCRRCDate      & ':' & Format(tmp:OOCRRCTime,      @t01)
                          !-----------------------------------------------------
                          Case local.LoanIssued()
                              Of 0
                                  turd:Loaned             = 'No'
                              Of 1
                                  turd:Loaned             = 'Yes'
                          End !Case local.LoanIssued()
                          !-----------------------------------------------------
                      Add(TurdReport)
                  End !If jobe:WebJob

                  If tmp:ARC = True
                     !WriteDebug('If tmp:ARC = True')

                      Clear(turd:Record)
                          !-----------------------------------------------------
                          GetHeadAccount(HeadAccount)
                          turd:AccountNumber      = haQ:AccountNumber
                          turd:AccountName        = haQ:AccountName

  !                        Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
  !                        tra_ali:Account_Number  = HeadAccount
  !                        If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
  !                            !Found
  !                            turd:AccountNumber      = tra_ali:Account_Number
  !                            turd:AccountName        = tra_ali:Company_Name
  !
  !                        Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
  !                            !Error
  !                            turd:AccountNumber      = 'ARC Not Found'
  !                            turd:AccountName        = 'ARC Not Found'
  !
  !                        End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign

                          turd:JobNumber          = job:Ref_Number
                          turd:WobNumber          = wob:JobNumber
                          turd:SubAccountName     = job:Account_Number
                          turd:Manufacturer       = job:Manufacturer
                          turd:ModelNumber        = job:Model_Number
                          If job:Warranty_job = 'YES'
                              turd:Warranty       = 'Yes'
                          Else !If job:Warranty_job = 'YES'
                              turd:Warranty       = 'No'
                          End !If job:Warranty_job = 'YES'
                          
                          turd:DateBooked         = job:Date_Booked
                          turd:TimeBooked         = job:Time_Booked
                          turd:DateCompleted      = job:Date_Completed
                          turd:TimeCompleted      = job:Time_Completed
                          !-----------------------------------------------------
                          ! 2 Apr 2003 John
                          !IF (tmp:CustomerRepairDate = 0) AND (tmp:CustomerRepairTime = 0)
                          !    tmp:CustomerRepairDate = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, TODAY(), CLOCK(), DEFORMAT('00:00:01', @T2), DEFORMAT('23:59:59', @T2), tmp:CustomerRepairTime)
                          !END !IF
                          !
                          turd:DateCustPerspective = tmp:CustomerRepairDate
                          turd:TimeCustPerspective = tmp:CustomerRepairTime
                          !-----------------------------------------------------
                          ! 22 Nov 2002 John
                          ! 25 Nov 2002 John
                          !oc:TheDate          = WorkingTimeBetween(   IN:StartDate,  IN:EndDate,    IN:StartTime,  IN:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, loc:TheTime)
                         !WriteDebug('If tmp:ARC')

                          !tmp:TotalRepairDays  = WorkingTimeBetween(job:Date_Booked, tmp:EndDate, job:Time_Booked, tmp:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, tmp:TotalRepairTime)
                          turd:TotalRepairType = FormatDaysHoursMins(tmp:TotalRepairDays, tmp:TotalRepairTime) ! tmp:TotalRepairDays & ':' & Format(tmp:TotalRepairTime, @t01)
                          turd:InControl       = FormatDaysHoursMins(tmp:ARCDate,         tmp:ARCTime)         ! tmp:ARCDate         & ':' & Format(tmp:ARCTime,         @t01)
                          turd:OutControl      = FormatDaysHoursMins(tmp:OOCARCDate,      tmp:OOCARCTime)      ! tmp:OOCARCDate      & ':' & Format(tmp:OOCARCTime,      @t01)
                          !-----------------------------------------------------
                          Case local.LoanIssued()
                              Of 0
                                  turd:Loaned             = 'No'
                              Of 1
                                  turd:Loaned             = 'Yes'
                          End !Case local.LoanIssued()
                          !-----------------------------------------------------
                      Add(TurdReport)
                  End !If tmp:ARC
                END
            End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)

        !WriteDebug('RecordCount = ' & RecordCount)
        !-----------------------------------------------------------------
    EXIT
CheckStatusChanges          ROUTINE
    DATA
First                        LONG(True)
AUDSTATSFirst                LONG(True)
CustomerPerspective_Complete BYTE(False)
Complete_job                 BYTE(FALSE)

PreviousLocation  LIKE(tmp:WhereIsJob) ! For 3rd party despatches
PreviousEstLocation  LIKE(tmp:WhereIsJob) ! For Estimates
Entry_debugActive LONG
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('A) CheckStatusChanges(job:Ref_Number="' & job:Ref_Number & '", aus:Type="' & CLIP(aus:Type) & '", aus:NewStatus="' & CLIP(aus:NewStatus) & '", aus:OldStatus="' & CLIP(aus:OldStatus) & '")')
        PreviousEstLocation = ''
        If tmp:Stop
            EXIT
        End !If tmp:Stop
        !-----------------------------------------------------------------
        Entry_debugActive = debug:Active

        !IF INSTRING('/' & job:Ref_Number & '/', ExamineAudStatsInDetail, 1, 1)
        IF job:ref_NUMBER = 34291
            debug:Active = True
        ELSE
            debug:Active = FALSE
        END
        !END !IF
            !-------------------------------------------------------------
            Save_aus_ID = Access:AUDSTATS.SaveFile()
                LOOP WHILE LoadAUDSTATS(job:Ref_Number, job:Date_Booked, AUDSTATSFirst) !Access:AUDSTATS.NEXT() = Level:Benign
                    !-----------------------------------------------------
                    IF First = True
                        First = False
                        !WriteDebug('B) CheckStatusChanges(First Loop for job ' & job:ref_Number)

                        ! Expecting "000 New Job Booking"
                        tmp:StartClockDate = aus:DateChanged
                        tmp:StartClockTime = aus:TimeChanged
                        aus:OldStatus      = aus:NewStatus
                        
                    END !IF
                    !-----------------------------------------------------
                    tmp:EndDate = WorkingTimeBetween(tmp:StartClockDate, aus:DateChanged, tmp:StartClockTime, aus:TimeChanged, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, tmp:EndTime)

                    IF debug:Active = True
                        ! Write message so can be pasted into Excel, CHR(9)=tab=Excel moves to new cell same row
                        !
                        WriteDebug(  |
                            ''  & job:Ref_Number                   & ''  & CHR(9) & |
                            '"' & CLIP(aus:Type)                   & '"' & CHR(9) & |
                            ''  & FORMAT(tmp:StartClockDate, @d08) & ''  & CHR(9) & |
                            ''  & FORMAT(tmp:StartClockTime, @T01) & ''  & CHR(9) & |
                            ''  & FORMAT(aus:DateChanged, @d08)    & ''  & CHR(9) & |
                            ''  & FORMAT(aus:TimeChanged, @T01)    & ''  & CHR(9) & |
                            ''  & tmp:EndDate                      & ''  & CHR(9) & |
                            ''  & FORMAT(tmp:EndTime, @T01)        & ''  & CHR(9) & |
                            '"' & CLIP(aus:OldStatus)              & '"' & CHR(9) & |
                            '"' & CLIP(aus:NewStatus)              & '"' & CHR(9) & |
                            '"' & CLIP(aus:UserCode)               & '"' )
                    END !IF

                    CASE aus:Type
                    OF 'JOB'
                        !WriteDebug('C) CheckStatusChanges(CASE aus:Type = "JOB")')

                        CASE SUB(aus:OldStatus, 1, 3)
                        !F ExchangeStatusDespatchToRRC
                        !F ExchangeStatusReceivedAtRRC
                        !F StatusARCReceivedQuery

                        !F StatusDespatchedToARC
                        OF StatusDespatchedToARC
                            !Job Despatched To ARC
                           !WriteDebug('D) CheckStatusChanges StatusDespatchedToARC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Despatched to ARC')

                            tmp:WhereIsJob = JobInTransit ! 2
                        
                        OF StatusDespatchedToRRC
                            !Job Despatched To ARC
                           !WriteDebug('E) CheckStatusChanges StatusDespatchedToRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Despatched to ARC')

                            tmp:WhereIsJob = JobInTransit ! 2

                        OF StatusReceivedAtARC
                            !Job has arrived at ARC
                           !WriteDebug('F) CheckStatusChanges StatusReceivedAtARC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Arrived at ARC')

                            tmp:WhereIsJob = JobAtARC ! 1
                            tmp:ARC        = True

                        OF StatusReceivedAtRRC
                            !Job has Arrived at RRC
                            !WriteDebug('G) CheckStatusChanges StatusReceivedAtRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Arrived at RRC')
                            IF tmp:WhereIsJob = JobInTransit
                              tmp:WhereIsJob = JobAtRRC ! 0
                            END
                            !tmp:ARC        = FALSE
                        !F StatusSendToARC
                        !F StatusSendToRRC
                        OF '705'
                            IF jobe:webjob
                              If tmp:WhereisJob = JobAtARC
                                !Ignore
                              ELSE
                                Complete_job = TRUE
                              END
                            ELSE
                              Complete_job = TRUE
                            END

                        OF '520' ! Estimate Sent
                            PreviousEstLocation = tmp:WhereIsJob 
                            tmp:WhereIsJob = JobInTransit

                        OF '535' OROF '540'
                            IF PreviousEstLocation <> ''
                                tmp:WhereIsJob = PreviousEstLocation
                            END

                        OF '410' ! Despatched To 3rd Party
                           !WriteDebug('H) CheckStatusChanges StatusReceivedAtRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" 410')

                            PreviousLocation = tmp:WhereIsJob 
                            tmp:WhereIsJob   = JobAt3rdParty

                        ELSE
                           !WriteDebug('I) CheckStatusChanges StatusReceivedAtRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" ELSE')

                            IF (tmp:WhereIsJob = JobAt3rdParty)
                                !-----------------------------------------
                                ! 06 Dec 2002 John
                                ! *** TESTING *** TESTING *** TESTING *** TESTING *** TESTING *** TESTING ***
                                ! Status 520 Received from 3rd party not being found in audstats
                                !   assume any status sets location back at RRC
                                !
                                !-----------------------------------------
                                tmp:WhereIsJob = PreviousLocation
                            END !IF 
                            !---------------------------------------------
                        END !CASE
                        !-------------------------------------------------
                        ! When status sorted out calculate customer perspective here
                        CASE SUB(aus:NewStatus, 1, 3)
                        !OF 810 ! READY TO DESPATCH
                        !    !---------------------------------------------
                        !    ! Customer Perspective
                        !
                        !    tmp:CustomerRepairDate   = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:00:01', @T2), DEFORMAT('23:59:59', @T2), tmp:CustomerRepairTime)
                        !    WriteDebug('j) CheckStatusChanges Customer Perspective(tmp:CustomerRepairDate=[' & tmp:CustomerRepairDate & '],tmp:CustomerRepairDate=[' & FORMAT(tmp:CustomerRepairTime, @T1) & '])')
                        !    !---------------------------------------------
    !!                    OF 810 OROF 901 OROF 905 OROF 910
    !!                        IF tmp:CustomerRepairDate = 0
    !!                            tmp:CustomerRepairDate   = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:00:01', @T2), DEFORMAT('23:59:59', @T2), tmp:CustomerRepairTime)
    !!                            WriteDebug('K) CheckStatusChanges Customer Perspective(tmp:CustomerRepairDate=[' & tmp:CustomerRepairDate & '],tmp:CustomerRepairDate=[' & FORMAT(tmp:CustomerRepairTime, @T1) & '])')
    !!                        END !IF

                        OF '905' OROF '910'
                            IF CustomerPerspective_Complete = False
                                IF (tmp:CustomerRepairDate < aus:DateChanged)
                                    tmp:CustomerRepairDate = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:01:00', @T4), DEFORMAT('23:59:00', @T4), tmp:CustomerRepairTime)
                                    !WriteDebug('L) CheckStatusChanges Customer Perspective(aus:NewStatus="' & CLIP(aus:NewStatus) & '", tmp:CustomerRepairDate=[' & tmp:CustomerRepairDate & '],tmp:CustomerRepairDate=[' & FORMAT(tmp:CustomerRepairTime, @T1) & '])')

                                ELSIF (tmp:CustomerRepairDate = aus:DateChanged) AND (tmp:CustomerRepairTime < aus:TimeChanged)
                                    ! OR
                                    tmp:CustomerRepairDate = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:01:00', @T4), DEFORMAT('23:59:00', @T4), tmp:CustomerRepairTime)
                                    !WriteDebug('M) CheckStatusChanges Customer Perspective(aus:NewStatus="' & CLIP(aus:NewStatus) & '", tmp:CustomerRepairDate=[' & tmp:CustomerRepairDate & '],tmp:CustomerRepairDate=[' & FORMAT(tmp:CustomerRepairTime, @T1) & '])')

                                END !IF
                            END !IF

                        OF '901' ! Exchange Despatched
                            CustomerPerspective_Complete = True

                            tmp:CustomerRepairDate   = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:01:00', @T4), DEFORMAT('23:59:00', @T4), tmp:CustomerRepairTime)
                            !WriteDebug('901') CheckStatusChanges Customer Perspective(aus:NewStatus="' & CLIP(aus:NewStatus) & '", tmp:CustomerRepairDate=[' & tmp:CustomerRepairDate & '],tmp:CustomerRepairDate=[' & FORMAT(tmp:CustomerRepairTime, @T1) & '])')

                        OF '705'
                            !---------------------------------------------
                            !Has the job been competed?

                            !WriteDebug('N) CheckStatusChanges 705-COMPLETED Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" COMPLETED')
                            !---------------------------------------------
                            ! Where is the job when it was completed?

                            If  tmp:WhereIsJob = JobInTransit ! 2
                                IF haQ:AccountName = HeadAccount
                                   !WriteDebug('M) CheckStatusChanges 705 Completed Status While in transit = choosing ARC')

                                    tmp:WhoCompleted = JobAtARC
                                ELSE
                                   !WriteDebug('O) CheckStatusChanges 705 Completed Status While in transit = choosing RRC')

                                    tmp:WhoCompleted = JobAtRRC ! 0
                                END !IF

                            Else !If tmp:WhereIsJob = 2
                               !WriteDebug('P) CheckStatusChanges 705 Completed Status')

                                tmp:WhoCompleted = tmp:WhereIsJob
                            End !If tmp:WhereIsJob = 2
                            !---------------------------------------------
                            IF tmp:WhoCompleted = JobAtARC
                               !WriteDebug('Q) CheckStatusChanges 705 Completed ARC')
                                                           
                                tmp:ARCCompletedDate = tmp:StartClockDate ! tmp:EndDate
                                tmp:ARCCompletedTime = tmp:StartClockTime ! tmp:EndTime
                                !Complete_job = TRUE
                               ! BREAK
                            ELSE
                               !WriteDebug('R) CheckStatusChanges 705 Completed RRC')
                                tmp:RRCCompletedDate = tmp:StartClockDate ! tmp:EndDate
                                tmp:RRCCompletedTime = tmp:StartClockTime ! tmp:EndTime
                                !Complete_job = TRUE
                            END !IF
                            !---------------------------------------------

                        ELSE
                            !---------------------------------------------
                            ! NULL
                            !---------------------------------------------
                        END !CASE
                    !-----------------------------------------------------
                    !When the loan is issued, then work out how long the job has been
                    !at that point.
                    OF  'LOA'
                       !WriteDebug('S) CheckStatusChanges(CASE aus:Type = "LOA")')

                        IF aus:OldStatus = 'NOT ISSUED'
                            ! SKIP
                            !WriteDebug('EXChange NOT ISSUED SKIP')
                            !
                        ELSIF tmp:LoanDate = 0 !and tmp:LoanTime = 0
                            !Is loan unit ready to despatch, or ready for qa
                            If Sub(aus:OldStatus,1,3) = '105' Or Sub(aus:OldStatus,1,3) = '605'
                                tmp:DateLoanAttached = tmp:StartClockDate 
                                tmp:TimeLoanAttached = tmp:StartClockTime

                                If jobe:WebJob
                                    tmp:LoanDate    = tmp:RRCDate
                                    tmp:LoanTime    = tmp:RRCTime
                                Else !If jobe:WebJob
                                    tmp:LoanDate    = tmp:ARCDate
                                    tmp:LoanTime    = tmp:ARCTime
                                End !If jobe:WebJob
                            End !If Sub(aus:NewStatus,1,3) = 106 Or Sub(aus:NewStatus,1,3) = 605
                        END !IF

                    OF  'EXC'

                        IF aus:OldStatus = 'NOT ISSUED'
                            ! SKIP
                            !WriteDebug('EXChange NOT ISSUED SKIP')
                        ELSE
                          IF (SUB(aus:OldStatus, 1, 3) = '110')
                            ! 125 EXCH UNIT DESPATCH
                            If jobe:WebJob
                              IF tmp:WhereIsJob = JobAtARC OR tmp:WhereIsJob = JobAt3rdParty
                                Tmp:WhereisJob = JobInTransit
                              END
                            ELSE
                              Complete_job = TRUE
                            END
                          END
                        END

                        IF aus:OldStatus = 'NOT ISSUED'
                            ! SKIP
                            !WriteDebug('EXChange NOT ISSUED SKIP')
                        ELSE
                            IF CLIP(SUB(aus:OldStatus, 1, 3)) = CLIP(ExchangeStatusReceivedAtRRC)
                                !Job Despatched To ARC    ExchangeStatusReceivedAtRRC
                               !WriteDebug('E) CheckStatusChanges StatusDespatchedToRRC Job="' & job:Ref_Number & '" Date="' & FORMAT(aus:DateChanged, @d8) & '", status="' & CLIP(aus:NewStatus) & '" Despatched to ARC')
                               If jobe:WebJob
                                 IF tmp:WhereIsJob = JobInTransit OR tmp:WhereIsJob = JobAtARC OR tmp:WhereIsJob = JobAt3rdParty
                                   tmp:WhereIsJob = JobAtRRC ! 2
                                 END
                               END
                            END
                        END
                        IF aus:OldStatus = 'NOT ISSUED'
                            ! SKIP
                            !WriteDebug('EXChange NOT ISSUED SKIP')

                        ELSIF (tmp:CustomerRepairDate = 0)
                            IF (SUB(aus:NewStatus, 1, 3) = '125')
                                ! 125 EXCH UNIT DESPATCH
                                tmp:CustomerRepairDate   = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:01:00', @T4), DEFORMAT('23:59:00', @T4), tmp:CustomerRepairTime)
                                !WriteDebug('EXChange 110 DESPATCH EXCH. UNIT Filling in tmp:CustomerRepairDate/Time')
                            END !IF
                            IF (SUB(aus:NewStatus, 1, 3) = '458')
                                ! 125 EXCH UNIT DESPATCH
                                tmp:CustomerRepairDate   = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:01:00', @T4), DEFORMAT('23:59:00', @T4), tmp:CustomerRepairTime)
                                !WriteDebug('EXChange 110 DESPATCH EXCH. UNIT Filling in tmp:CustomerRepairDate/Time')
                            END !IF
                            IF (SUB(aus:NewStatus, 1, 3) = '901')
                                ! 125 EXCH UNIT DESPATCH
                                tmp:CustomerRepairDate   = DaysTimeBetween(  job:Date_Booked, job:Time_Booked, aus:DateChanged, aus:TimeChanged, DEFORMAT('00:01:00', @T4), DEFORMAT('23:59:00', @T4), tmp:CustomerRepairTime)
                                !WriteDebug('EXChange 110 DESPATCH EXCH. UNIT Filling in tmp:CustomerRepairDate/Time')
                            END !IF
                        END !IF


                    ELSE
                      !WriteDebug('T) CheckStatusChanges(CASE aus:Type = ELSE"' & clip(aus:Type) & '")')
                        ! SKIP Empty For first entry

                    END !CASE aus:Type
                    !-----------------------------------------------------
                    IF Complete_job = FALSE
                        Case tmp:WhereIsJob
                        Of JobAtRRC !At RRC
                           WriteDebug('U) CountStatusTime(JobAtRRC)')

                            tmp:RRCDate        += tmp:EndDate
                            tmp:RRCTime        += tmp:EndTime
                            
                            IF NOT jobe:WebJob
                                tmp:OOCARCDate += tmp:EndDate
                                tmp:OOCARCTime += tmp:EndTime
                            END !IF jobe:WebJob

                        Of JobAtARC !At ARC
                           WriteDebug('V) CountStatusTime(JobAtARC)')

                            tmp:ARCDate        += tmp:EndDate
                            tmp:ARCTime        += tmp:EndTime

                            IF jobe:WebJob
                                tmp:OOCRRCDate += tmp:EndDate
                                tmp:OOCRRCTime += tmp:EndTime
                            END !IF jobe:WebJob

                        Of JobInTransit !Travelling
                           WriteDebug('W) CountStatusTime(Travelling)')

                            IF jobe:WebJob
                                tmp:OOCRRCDate += tmp:EndDate
                                tmp:OOCRRCTime += tmp:EndTime
                            ELSE
                                tmp:OOCARCDate += tmp:EndDate
                                tmp:OOCARCTime += tmp:EndTime
                            END !IF jobe:WebJob

                        OF JobAt3rdParty
                           WriteDebug('X) CountStatusTime(JobAt3rdParty)')

                            IF jobe:WebJob AND previouslocation <> JobAtARC
                                tmp:OOCRRCDate += tmp:EndDate
                                tmp:OOCRRCTime += tmp:EndTime
                            ELSE
                                tmp:OOCARCDate += tmp:EndDate
                                tmp:OOCARCTime += tmp:EndTime
                            END !IF jobe:WebJob

                        ELSE
                           !WriteDebug('Y) CountStatusTime(ELSE)')
                        End !Case tmp:WhereIsJob
                    END
                    !-----------------------------------------------------
                    !Has the time gone over 24hrs?
                    !WriteDebug('Z1) CountStatusTime NormalisedateTime RRC')
                    NormaliseDateTime(haQ:WorkingHours, tmp:RRCDate,    tmp:RRCTime)

                    !WriteDebug('Z2) CountStatusTime NormalisedateTime ARC')
                    NormaliseDateTime(haQ:WorkingHours, tmp:ARCDate,    tmp:ARCTime)

                    !WriteDebug('Z3) CountStatusTime NormalisedateTime OOCRRC')
                    NormaliseDateTime(haQ:WorkingHours, tmp:OOCRRCDate, tmp:OOCRRCTime)

                    !WriteDebug('Z4) CountStatusTime NormalisedateTime OOCARC')
                    NormaliseDateTime(haQ:WorkingHours, tmp:OOCARCDate, tmp:OOCARCTime)
                    !-----------------------------------------------------
    !                WriteDebug('CheckStatusChanges(AA1) job:Ref_Number  ="' & job:Ref_Number                       & '", Type ="' & CLIP(aus:Type) & '", NewStatus="' & CLIP(aus:NewStatus) & '", OldStatus ="' & CLIP(aus:OldStatus) & '"')
    !                WriteDebug('CheckStatusChanges(AA2) EndDate         ="' & tmp:EndDate            & '", Time ="' & FORMAT(           tmp:EndTime, @T01) & '")')
    !                WriteDebug('CheckStatusChanges(AA3) ARCDate         ="' & tmp:ARCDate            & '", Time ="' & FORMAT(           tmp:ARCTime, @T01) & '")')
    !                WriteDebug('CheckStatusChanges(AA4) RRCDate         ="' & tmp:RRCDate            & '", Time ="' & FORMAT(           tmp:RRCTime, @T01) & '")')
    !                WriteDebug('CheckStatusChanges(AA5) OOCARCDate      ="' & tmp:OOCARCDate         & '", Time ="' & FORMAT(        tmp:OOCARCTime, @T01) & '")')
    !                WriteDebug('CheckStatusChanges(AA6) OCRRCDate       ="' & tmp:OOCARCDate         & '", Time ="' & FORMAT(        tmp:OOCRRCTime, @T01) & '")')
    !                WriteDebug('CheckStatusChanges(AA7) tmp:CustomerDate="' & tmp:CustomerRepairDate & '", Time ="' & FORMAT(tmp:CustomerRepairTime, @T01) & '")')


                    tmp:StartClockDate = aus:DateChanged
                    tmp:StartClockTime = aus:TimeChanged
                    !-----------------------------------------------------
                END !Loop
            Access:AUDSTATS.RestoreFile(Save_aus_ID)
            !-------------------------------------------------------------
        debug:Active = Entry_debugActive
        !-------------------------------------------------------------
    EXIT
!------------------------------------------------------------------------
SummarySheet        Routine
    DATA
AccumRow LONG
    CODE
    StartRow# = tmp:StartRow

    tmp:TotalCompleted  = 0
    tmp:TotalBooked     = 0
    tmp:TotalOpen       = 0
    tmp:TotalLoaned     = 0

    Open(TurnReport)
    Set(TurnReport,0)
    Loop
        Next(TurnReport)
        If Error()
            Break
        End !If Error()
        Do GetNextRecord2

        ExcelSelectRange('A' & StartRow#)

!-----------------------------------------------------------------
!-----------------------------------------------------------------
        ExcelCell(turn:AccountName,0)
        ExcelCell(turn:Completed,0)

        tmp:TotalCompleted += turn:Completed

        ExcelCell(turn:Booked,0)

        tmp:TotalBooked += turn:Booked

        ExcelCell(turn:Opened,0)

        tmp:TotalOpen += turn:Opened
        ExcelCell(turn:Loaned,0)

        tmp:TotalLoaned += turn:Loaned

!-----------------------------------------------------------------
!-----------------------------------------------------------------
        ExcelCell('',0)

!-----------------------------------------------------------------
!-----------------------------------------------------------------

!1 Hr
!Completed
        ExcelCell(turn:Completed1,0)

!%Completed
        !If zero,stop the "divide by zero" errors
        !I want to keep the calculated cells if possible. itmakes the spreadsheet more flexiable
        If turn:Completed1 = 0
            ExcelCell('0',0)
        Else !If turn:Completed1 = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(G' & StartRow# & '/B' & StartRow# & ')',0)
        End !If turn:Completed1 = 0

!Loans
        ExcelCell(turn:Loaned1,0)
!Satisfied
        !ExcelCell('=(G' & StartRow# & '+I' & StartRow# & ')',0)
        ExcelCell('=(G' & StartRow# & ')',0)
!%Satisfied
        If turn:Loaned1 + turn:Completed1 = 0
            ExcelCell('0',0)
        Else !If turn:Loaned1 = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(J' & StartRow# & '/B' & StartRow# & ')',0)
        End !If turn:Loaned1 = 0
        
!-----------------------------------------------------------------
        ExcelCell('',0)
!-----------------------------------------------------------------

!24 Hr
!Completed
        ExcelCell(turn:Completed24,0)

!%Completed
        If (turn:Completed24) = 0
            ExcelCell('0',0)
        Else !If (turn:Completed1 + turn:Completed 24) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(M' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Completed1 + turn:Completed 24) = 0

!Loans
        ExcelCell(turn:Loaned24,0)
!Satisfied
        !ExcelCell('=(M' & StartRow# & '+O' & StartRow# & ')',0)
        ExcelCell('=(M' & StartRow# & ')',0)
!%Satisfied
        If turn:Loaned24 + turn:Completed24  = 0
            ExcelCell('0',0)
        Else !If (turn:Loaned1 + turn:Loaned24) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(P' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Loaned1 + turn:Loaned24) = 0
        
!-----------------------------------------------------------------
        ExcelCell('',0)
!-----------------------------------------------------------------

!48 Hr
!Completed
        ExcelCell(turn:Completed48,0)
!%Completed
        If (turn:Completed48) = 0
            ExcelCell('0',0)
        Else !If (turn:Completed1 + turn:Completed24 + turn:Completed48) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(S' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Completed1 + turn:Completed24 + turn:Completed48) = 0
!Loans
        ExcelCell(turn:Loaned48,0)
!Satisfied
        !ExcelCell('=(S' & StartRow# & '+U' & StartRow# & ')',0)
        ExcelCell('=(S' & StartRow# & ')',0)
!%Satisfied
        If (turn:Loaned48 + turn:Completed48) = 0
            ExcelCell('0',0)
        Else !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(V' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48) = 0
        
!-----------------------------------------------------------------
        ExcelCell('',0)
!-----------------------------------------------------------------

!72 Hr
!Completed
        ExcelCell(turn:Completed72,0)
!%Completed
        If (turn:Completed72) = 0
            ExcelCell('0',0)
        Else !If (turn:Completed1 + turn:Completed24 + turn:Completed48 + turn:Completed72) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(Y' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Completed1 + turn:Completed24 + turn:Completed48 + turn:Completed72) = 0
!Loans
        ExcelCell(turn:Loaned72,0)
!Satisfied
        !ExcelCell('=(Y' & StartRow# & '+AA' & StartRow# & ')',0)
        ExcelCell('=(Y' & StartRow# & ')',0)
!%Satisfied
        If (turn:Loaned72 + turn:Completed72) = 0
            ExcelCell('0',0)
        Else !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48 + turn:Loaned72) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AB' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48 + turn:Loaned72) = 0
        
!-----------------------------------------------------------------
        ExcelCell('',0)
!-----------------------------------------------------------------

!168 Hr
!Completed
        ExcelCell(turn:Completed168,0)
!%Completed
        If (turn:Completed168) = 0
            Excelcell('0',0)
        Else !If (turn:Completed1 + turn:Completed24 + turn:Completed48 + turn:Completed72 + turn:Completed168) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AE' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Completed1 + turn:Completed24 + turn:Completed48 + turn:Completed72 + turn:Completed168) = 0
!Loans
        ExcelCell(turn:Loaned168,0)
!Satisfied
        !ExcelCell('=(AE' & StartRow# & '+AG' & StartRow# & ')',0)
        ExcelCell('=(AE' & StartRow# & ')',0)
!%Satsified
        If (turn:Loaned168 + turn:Completed168) = 0
            ExcelCell('0',0)
        Else !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48 + turn:Loaned72 + turn:Loaned168) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AH' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48 + turn:Loaned72 + turn:Loaned168) = 0
        
!-----------------------------------------------------------------
        ExcelCell('',0)
!-----------------------------------------------------------------

!Over 168 Hr
!Completed
        ExcelCell(turn:CompletedOver168,0)
!%Completed
        If (turn:CompletedOver168) = 0
            ExcelCell('0',0)
        Else !If (turn:Completed1 + turn:Completed24 + turn:Completed48 + turn:Completed72 + turn:Completed168 + turn:CompletedOver168) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AK' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Completed1 + turn:Completed24 + turn:Completed48 + turn:Completed72 + turn:Completed168 + turn:CompletedOver168) = 0
!Loans
        ExcelCell(turn:LoanedOver168,0)
!Satsified
        !ExcelCell('=(AK' & StartRow# & '+AM' & StartRow# & ')',0)
        ExcelCell('=(AK' & StartRow# & ')',0)
!%Satisfied
        If (turn:LoanedOver168 + turn:CompletedOver168) = 0
            ExcelCell('0',0)
        Else !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48 + turn:Loaned72 + turn:Loaned168 + turn:LoanedOver168) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AN' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:Loaned1 + turn:Loaned24 + turn:Loaned48 + turn:Loaned72 + turn:Loaned168 + turn:LoanedOver168) = 0
        
!-----------------------------------------------------------------
!--- Accumlative Row ---------------------------------------------
!-----------------------------------------------------------------

        AccumRow = StartRow# + 1

!-----------------------------------------------------------------
        !1 Hr
        ExcelSelectRange('G' & AccumRow)
        ExcelHorizontal('Right')

        ExcelCell('=G' & StartRow#,                     1) ! G
        ExcelCell('',                                   0) ! H
        ExcelCell('',                                   0) ! I
        ExcelCell('',                                   0) ! J
        ExcelFormatCell('#0.00%')
        ExcelCell('=G' & AccumRow & '/$B$' & StartRow#, 1) ! K ! 26 Mar 2003 John ExcelCell('Accumulative Totals', 1)

!-----------------------------------------------------------------
        ExcelCell('', 0) ! L
!-----------------------------------------------------------------

        !24 Hr
        ExcelSelectRange('M' & StartRow# + 1)
        ExcelCell('=(M' & StartRow# & '+G'   & StartRow# & ')', 1) ! M
        ExcelCell('=(N' & StartRow# & '+H'   & StartRow# & ')', 1) ! N
        ExcelCell('=(O' & StartRow# & '+I'   & StartRow# & ')', 1) ! O
        ExcelCell('=(P' & StartRow# & '+J'   & StartRow# & ')', 1) ! P
        ExcelFormatCell('#0.00%')
        ExcelCell('=(P' & AccumRow  & '/$B$' & StartRow# & ')', 1) ! Q

!-----------------------------------------------------------------
        ExcelCell('',0) ! R
!-----------------------------------------------------------------

        !48Hr
        ExcelCell('=(S' & StartRow# & '+M'   & AccumRow  & ')', 1) ! S
        ExcelCell('=(T' & StartRow# & '+N'   & AccumRow  & ')', 1) ! T
        ExcelCell('=(U' & StartRow# & '+O'   & AccumRow  & ')', 1) ! U
        ExcelCell('=(V' & StartRow# & '+P'   & AccumRow  & ')', 1) ! V
        ExcelFormatCell('#0.00%')
        ExcelCell('=(V' & AccumRow  & '/$B$' & StartRow# & ')', 1) ! W

!-----------------------------------------------------------------
        ExcelCell('',0) ! X
!-----------------------------------------------------------------

        !72Hr
        ExcelCell('=(Y'  & StartRow# & '+S'   & AccumRow  & ')', 1) ! Y
        ExcelCell('=(Z'  & StartRow# & '+T'   & AccumRow  & ')', 1) ! Z
        ExcelCell('=(AA' & StartRow# & '+U'   & AccumRow  & ')', 1) ! AA
        ExcelCell('=(AB' & StartRow# & '+V'   & AccumRow  & ')', 1) ! AB
        ExcelFormatCell('#0.00%')
        ExcelCell('=(AB' & AccumRow  & '/$B$' & StartRow# & ')', 1) ! AC

!-----------------------------------------------------------------
        ExcelCell('',0) ! AD
!-----------------------------------------------------------------

        !168Hr
        ExcelCell('=(AE' & StartRow# & '+Y'   & AccumRow  & ')', 1) ! AE
        ExcelCell('=(AF' & StartRow# & '+Z'   & AccumRow  & ')', 1) ! AF
        ExcelCell('=(AG' & StartRow# & '+AA'  & AccumRow  & ')', 1) ! AG
        ExcelCell('=(AH' & StartRow# & '+AB'  & AccumRow  & ')', 1) ! AH
        ExcelFormatCell('#0.00%')
        ExcelCell('=(AH' & AccumRow  & '/$B$' & StartRow# & ')', 1) ! AI

!-----------------------------------------------------------------
        ExcelCell('',0) ! AJ
!-----------------------------------------------------------------

        !Over168Hr
        ExcelCell('=(AK' & StartRow# & '+AE'  & AccumRow  & ')', 1) ! AK
        ExcelCell('=(AL' & StartRow# & '+AF'  & AccumRow  & ')', 1) ! AL
        ExcelCell('=(AM' & StartRow# & '+AG'  & AccumRow  & ')', 1) ! AM
        ExcelCell('=(AN' & StartRow# & '+AH'  & AccumRow  & ')', 1) ! AN
        ExcelFormatCell('#0.00%')
        ExcelCell('=(AN' & AccumRow  & '/$B$' & StartRow# & ')', 1) ! AO

!-----------------------------------------------------------------

        ExcelGrid('G'  & AccumRow & ':K'  & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid('M'  & AccumRow & ':Q'  & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid('S'  & AccumRow & ':W'  & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid('Y'  & AccumRow & ':AC' & AccumRow, 1, 1, 1, 1 ,15)
        ExcelGrid('AE' & AccumRow & ':AI' & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid('AK' & AccumRow & ':AO' & AccumRow, 1, 1, 1, 1, 15)

!-----------------------------------------------------------------

        StartRow# += 2
    End !Loop

    tmp:FinishRow = StartRow#

    Close(TurnReport)
SummarySheetCustomer        Routine
    DATA
AccumRow LONG
    CODE

    StartRow# = tmp:StartRow + 1

    Open(TurnReport)
    Set(TurnReport,0)
    Loop
        Next(TurnReport)
        If Error()
            Break
        End !If Error()
        Do GetNextRecord2

        ExcelSelectRange('A' & StartRow#)

        ExcelCell(turn:AccountName,0)
        ExcelCell(turn:Completed,0)
        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)

        ExcelCell('',0)

        !1 Hr
        ExcelCell(turn:CustCompleted1,0)

        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)
        !If zero,stop the "divide by zero" errors
        !I want to keep the calculated cells if possible. itmakes the spreadsheet more flexiable
        If turn:CustCompleted1 = 0
            ExcelCell('0',0)
        Else !If turn:CustCompleted1 = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(G' & StartRow# & '/B' & StartRow# & ')',0)
        End !If turn:CustCompleted1 = 0
        
        ExcelCell('',0)

        !24 Hr
        ExcelCell(turn:CustCompleted24,0)
        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)

        If (turn:CustCompleted1 + turn:CustCompleted24) = 0
            ExcelCell('0',0)
        Else !If (turn:CustCompleted1 + turn:CustCompleted 24) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(M' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:CustCompleted1 + turn:CustCompleted 24) = 0

        ExcelCell('',0)

        !48 Hr
        ExcelCell(turn:CustCompleted48,0)

        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)

        If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48) = 0
            ExcelCell('0',0)
        Else !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(S' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48) = 0
        
        ExcelCell('',0)

        !72 Hr
        ExcelCell(turn:CustCompleted72,0)

        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)

        If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72) = 0
            ExcelCell('0',0)
        Else !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(Y' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72) = 0
        
        ExcelCell('',0)

        !168 Hr
        ExcelCell(turn:CustCompleted168,0)

        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)

        If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72 + turn:CustCompleted168) = 0
            Excelcell('0',0)
        Else !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72 + turn:CustCompleted168) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AE' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72 + turn:CustCompleted168) = 0
        
        ExcelCell('',0)

        !Over 168 Hr
        ExcelCell(turn:CustCompletedOver168,0)

        ExcelCell('',0)
        ExcelCell('',0)
        ExcelCell('',0)

        If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72 + turn:CustCompleted168 + turn:CustCompletedOver168) = 0
            ExcelCell('0',0)
        Else !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72 + turn:CustCompleted168 + turn:CustCompletedOver168) = 0
            !ExcelFormatCell('##00.00')
            ExcelFormatCell('#0.00%')
            ExcelCell('=(AK' & StartRow# & '/B' & StartRow# & ')',0)
        End !If (turn:CustCompleted1 + turn:CustCompleted24 + turn:CustCompleted48 + turn:CustCompleted72 + turn:CustCompleted168 + turn:CustCompletedOver168) = 0
        
        ExcelCell('',0)
!-----------------------------------------------------------------
!--- Accumlative Row ---------------------------------------------
!-----------------------------------------------------------------
        AccumRow = StartRow# + 1
!-----------------------------------------------------------------
        !1 Hr
        ExcelSelectRange('G' & AccumRow)
        ExcelHorizontal('Right')

        ExcelCell('=G' & StartRow#,                     1) ! G
        ExcelCell('',                                   0) ! H
        ExcelCell('',                                   0) ! I
        ExcelCell('',                                   0) ! J
        ExcelFormatCell('#0.00%')
        ExcelCell('=G' & AccumRow & '/$B$' & StartRow#, 1) ! K ! 26 Mar 2003 John ExcelCell('Accumulative Totals', 1)

!-----------------------------------------------------------------
        ExcelCell('',0) ! L
!-----------------------------------------------------------------

        !24 Hr
        ExcelCell('=(M' & StartRow# & '+G' & StartRow# & ')',1) ! M
        ExcelCell('',                                        0) ! N
        ExcelCell('',                                        0) ! O
        ExcelCell('',                                        0) ! P
        ExcelFormatCell('#0.00%')
        ExcelCell('=M' & AccumRow & '/$B$' & StartRow#,      1) ! Q ! 26 Mar 2003 John

!-----------------------------------------------------------------
        ExcelCell('',0) ! R
!-----------------------------------------------------------------

        !48Hr
        ExcelCell('=(S' & StartRow# & '+M' & AccumRow & ')',1) ! S
        ExcelCell('',                                       0) ! T
        ExcelCell('',                                       0) ! U
        ExcelCell('',                                       0) ! V
        ExcelFormatCell('#0.00%')
        ExcelCell('=S' & AccumRow & '/$B$' & StartRow#,     1) ! W ! 26 Mar 2003 John

!-----------------------------------------------------------------
        ExcelCell('',0) ! X
!-----------------------------------------------------------------

        !72Hr
        ExcelCell('=(Y' & StartRow# & '+S' & AccumRow & ')', 1) ! Y
        ExcelCell('',                                        0) ! Z
        ExcelCell('',                                        0) ! AA
        ExcelCell('',                                        0) ! AB
        ExcelFormatCell('#0.00%')
        ExcelCell('=Y' & AccumRow & '/$B$' & StartRow#,      1) ! AC ! 26 Mar 2003 John

!-----------------------------------------------------------------
        ExcelCell('',0) ! AD
!-----------------------------------------------------------------

        !168Hr
        ExcelCell('=(AE' & StartRow# & '+Y' & AccumRow & ')', 1) ! AE
        ExcelCell('',                                         0) ! AF
        ExcelCell('',                                         0) ! AG
        ExcelCell('',                                         0) ! AH
        ExcelFormatCell('#0.00%')
        ExcelCell('=AE' & AccumRow & '/$B$' & StartRow#,      1) ! AI ! 26 Mar 2003 John

!-----------------------------------------------------------------
        ExcelCell('',0) ! AJ
!-----------------------------------------------------------------

        !Over168Hr
        ExcelCell('=(AK' & StartRow# & '+AE' & AccumRow & ')', 1) ! AK
        ExcelCell('',                                          0) ! AL
        ExcelCell('',                                          0) ! AM
        ExcelCell('',                                          0) ! AN
        ExcelFormatCell('#0.00%')
        ExcelCell('=AK' & AccumRow & '/$B$' & StartRow#,       1) ! AO

!-----------------------------------------------------------------
        ExcelCell('',0) ! AP
!-----------------------------------------------------------------

        ExcelGrid( 'G' & StartRow# + 1 &  ':K' & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid( 'M' & StartRow# + 1 &  ':Q' & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid( 'S' & StartRow# + 1 &  ':W' & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid( 'Y' & StartRow# + 1 & ':AC' & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid('AE' & StartRow# + 1 & ':AI' & AccumRow, 1, 1, 1, 1, 15)
        ExcelGrid('AK' & StartRow# + 1 & ':AO' & AccumRow, 1, 1, 1, 1, 15)

        StartRow# += 2
    End !Loop

    tmp:FinishRow = StartRow#
    Close(TurnReport)
!CountStatusTime     Routine
!Data
!loc:TheTime     Long
!loc:TheDate     Long
!Code
!    loc:TheDate = 0
!    loc:TheTime = 0
!
!    If tmp:CurrentStatus <> ''
!        !Has the status been changed? If so work out how long
!        !it was at that status.
!        If tmp:CurrentStatus = Sub(aus:OldStatus,1,3)
!
!            If tmp:StartClockDate = aus:DateChanged
!                !Status changed, same day
!                !Just add the time difference
!                loc:TheTime = (aus:TimeChanged - tmp:StartClockTime)                    
!                Case tmp:WhereIsJob
!                    Of 0 !At RRC
!                        tmp:RRCTime    += loc:TheTime
!                        tmp:OOCARCTime += loc:TheTime
!                    Of 2 !Travelling
!                        tmp:OOCARCTime += loc:TheTime
!                        tmp:OOCRRCTime += loc:TheTime
!                    Of 1 !At ARC
!                        tmp:ARCTime += loc:TheTime
!                        tmp:OOCRRCTime += loc:TheTime
!                End !Case tmp:WhereIsJob
!                
!            Else !If tmp:DateStart = aus:DateChanged
!                !Status changed on a different day
!                loc:TheDate = (aus:DateChanged - tmp:StartClockDate)                    
!
!                Case tmp:WhereIsJob
!                    Of 0 !At RRC
!                        tmp:RRCDate    += loc:TheDate
!                        tmp:OOCARCDate += loc:TheDate
!                    Of 2 !Travelling
!                        tmp:OOCARCDate += loc:TheDate
!                        tmp:OOCRRCDate += loc:TheDate
!                    Of 1 !At ARC
!                        tmp:ARCDate    += loc:TheDate
!                        tmp:OOCRRCDate += loc:TheDate
!                End !Case tmp:WhereIsJob
!
!                loc:TheTime = (aus:TimeChanged - tmp:StartClockTime)
!                !Is the time before or after the start time
!                If aus:TimeChanged > tmp:StartClockTime
!                    !Time is after, just add the difference
!                    Case tmp:WhereIsJob
!                        Of 0
!                            tmp:RRCtime += loc:TheTime
!                            tmp:OOCARCTime += loc:TheTime
!                        Of 2
!                            tmp:OOCARCTime += loc:TheTime
!                            tmp:OOCRRCTime += loc:TheTime
!                        Of 1
!                            tmp:ARCTime += loc:TheTime
!                            tmp:OOCRRCTime += loc:TheTime
!                    End !Case tmp:WhereIsJob
!                    
!                End !If aus:TimeChange > tmp:TimeStart
!
!                !If the time is before the start time, then go back a day
!                !and take 24hours off the time count
!                If aus:TimeChanged < tmp:StartClockTime
!                    loc:TheTime = 8640000 - (tmp:StartClockTime - aus:TimeChanged)
!
!                    Case tmp:WhereIsJob
!                        Of 0
!                            tmp:RRCTime += loc:TheTime
!                            tmp:RRCDate -= 1
!                            tmp:OOCARCTime += loc:TheTime
!                            tmp:OOCARCDate -= 1
!                        Of 2
!                            tmp:OOCARCTime += loc:TheTime
!                            tmp:OOCARCDate -= 1
!                            tmp:OOCRRCTime += loc:TheTime
!                            tmp:OOCRRCDate -= 1
!                        Of 1
!                            tmp:ARCTime += loc:TheTime
!                            tmp:ARCDate -= 1
!                            tmp:OOCRRCTime += loc:TheTime
!                            tmp:OOCRRCDate -= 1
!                    End !Case tmp:WhereIsJob
!                End !If aus:TimeChange < tmp:TimeStart
!            End !If tmp:DateStart = aus:DateChanged
!
!            !Reset the clock
!            tmp:CurrentStatus = ''
!        End !If tmp:CurrentStatus = Sub(aus:OldStatus,1,3)
!    End !If tmp:CurrentStatus <> ''
!    !Has the time gone over 24hrs?
!
!    If tmp:CurrentStatus2 <> ''
!        !Has the status been changed? If so work out how long
!        !it was at that status.
!        If tmp:CurrentStatus2 = Sub(aus:OldStatus,1,3)
!
!            If tmp:StartClockDate = aus:DateChanged
!                !Status changed, same day
!                !Just add the time difference
!                loc:TheTime = (aus:TimeChanged - tmp:StartClockTime)                    
!                Case tmp:WhereIsJob
!                    Of 1
!                        tmp:OOCARCTime += loc:TheTime
!                    Of 2 !Travelling
!                        tmp:OOCARCTime += loc:TheTime
!                        tmp:OOCRRCTime += loc:TheTime
!                    Of 0
!                        tmp:OOCRRCTime += loc:TheTime
!                End !Case tmp:WhereIsJob
!                
!            Else !If tmp:DateStart = aus:DateChanged
!                !Status changed on a different day
!                loc:TheDate = (aus:DateChanged - tmp:StartClockDate)                    
!
!                Case tmp:WhereIsJob
!                    Of 1
!                        tmp:OOCARCDate += loc:TheDate
!                    Of 2 !Travelling
!                        tmp:OOCARCDate += loc:TheDate
!                        tmp:OOCRRCDate += loc:TheDate
!                    Of 0
!                        tmp:OOCRRCDate += loc:TheDate
!                End !Case tmp:WhereIsJob
!
!                loc:TheTime = (aus:TimeChanged - tmp:StartClockTime)
!                !Is the time before or after the start time
!                If aus:TimeChanged > tmp:StartClockTime
!                    !Time is after, just add the difference
!                    Case tmp:WhereIsJob
!                        Of 1
!                            tmp:OOCARCTime += loc:TheTime
!                        Of 2
!                            tmp:OOCARCTime += loc:TheTime
!                            tmp:OOCRRCTime += loc:TheTime
!                        Of 0
!                            tmp:OOCRRCTime += loc:TheTime
!                    End !Case tmp:WhereIsJob
!                    
!                End !If aus:TimeChange > tmp:TimeStart
!
!                !If the time is before the start time, then go back a day
!                !and take 24hours off the time count
!                If aus:TimeChanged < tmp:StartClockTime
!                    loc:TheTime = 8640000 - (tmp:StartClockTime - aus:TimeChanged)
!
!                    Case tmp:WhereIsJob
!                        Of 1
!                            tmp:OOCARCTime += loc:TheTime
!                            tmp:OOCARCDate -= 1
!                        Of 2
!                            tmp:OOCARCTime += loc:TheTime
!                            tmp:OOCARCDate -= 1
!                            tmp:OOCRRCTime += loc:TheTime
!                            tmp:OOCRRCDate -= 1
!                        Of 0
!                            tmp:OOCRRCTime += loc:TheTime
!                            tmp:OOCRRCDate -= 1
!                    End !Case tmp:WhereIsJob
!                End !If aus:TimeChange < tmp:TimeStart
!            End !If tmp:DateStart = aus:DateChanged
!
!            !Reset the clock
!            tmp:CurrentStatus2 = ''
!        End !If tmp:CurrentStatus = Sub(aus:OldStatus,1,3)
!
!    End !If tmp:CurrentStatus2 <> ''
!
!    If tmp:RRCTime > 8640000
!        tmp:RRCDate += 1
!        tmp:RRCTime -= 8640000
!    End !If tmp:Time > 864000
!
!    If tmp:ARCTime > 8640000
!        tmp:ARCDate += 1
!        tmp:ARCTime -= 8640000
!    End !If tmp:Time > 864000
!
!    If tmp:OOCRRCTime > 8640000
!        tmp:OOCRRCDate += 1
!        tmp:OOCRRCTime -= 8640000
!    End !If tmp:Time > 864000
!
!    If tmp:OOCARCTime > 8640000
!        tmp:OOCARCDate += 1
!        tmp:OOCARCTime -= 8640000
!    End !If tmp:Time > 864000
!
!CountStatusTimeLoan     Routine
!Data
!loc:TheTime     Long
!loc:TheDate     Long
!Code
!    loc:TheDate = 0
!    loc:TheTime = 0
!
!    If tmp:CurrentStatus <> ''
!        !Has the status been changed? If so work out how long
!        !it was at that status.
!        If tmp:CurrentStatus = Sub(aus:OldStatus,1,3)
!
!            If tmp:StartClockDate = aus:DateChanged
!                !Status changed, same day
!                !Just add the time difference
!                loc:TheTime = (aus:TimeChanged - tmp:StartClockTime)                    
!
!                tmp:LoanTime += loc:TheTime
!                
!            Else !If tmp:DateStart = aus:DateChanged
!                !Status changed on a different day
!                loc:TheDate = (aus:DateChanged - tmp:StartClockDate)                    
!
!                tmp:LoanDate += loc:TheDate
!
!                loc:TheTime = (aus:TimeChanged - tmp:StartClockTime)
!                !Is the time before or after the start time
!                If aus:TimeChanged > tmp:StartClockTime
!                    !Time is after, just add the difference
!                    tmp:LoanTime += loc:TheTime
!                    
!                End !If aus:TimeChange > tmp:TimeStart
!
!                !If the time is before the start time, then go back a day
!                !and take 24hours off the time count
!                If aus:TimeChanged < tmp:StartClockTime
!                    loc:TheTime = 8640000 - (tmp:StartClockTime - aus:TimeChanged)
!
!                    tmp:LoanTime += loc:TheTime
!                End !If aus:TimeChange < tmp:TimeStart
!            End !If tmp:DateStart = aus:DateChanged
!
!            !Reset the clock
!            tmp:CurrentStatus = ''
!        End !If tmp:CurrentStatus = Sub(aus:OldStatus,1,3)
!    End !If tmp:CurrentStatus <> ''
!    !Has the time gone over 24hrs?
!
!
!    If tmp:LoanTime > 8640000
!        tmp:LoanDate += 1
!        tmp:LoanTime -= 8640000
!    End !If tmp:Time > 864000
!
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
      end
    end
    Display()

endprintrun         routine
    progress:thermometer = 100
    display()
!------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020609'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TurnaroundTimeSummaryReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:LOAN.Open
  Relate:STATUS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  LOC:ProgramName = 'Turnaround Time Summary Report'
  
      HeadAccount = GETINI('BOOKING', 'HeadAccount', , CLIP(PATH()) & '\SB2KDEF.INI')
  
  tmp:CriteriaEndDate   = Today()
  tmp:CriteriaStartDate = DATE(MONTH(tmp:CriteriaEndDate),1, YEAR(tmp:CriteriaEndDate))
  IF (tmp:CriteriaEndDate - tmp:CriteriaStartDate) < 7
     tmp:CriteriaStartDate +=  -7
  END !IF
  
      debug:Active       = False
  
        SET(defaults)
        access:defaults.next()
  
  
          !-----------------------------------------------
          ! 23 Oct 2002 John
          ! R003, berrjo :Insert the report exe name EG VODR56.V01
          ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
          ! Make this standard on all reports sent for correction.
          ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
          !
          !LOC:Version           = '3.1.0002'
          !-----------------------------------------------
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:CriteriaStartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:CriteriaEndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,tra:Account_Number_Key)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,tra:Account_Number,1,BRW7)
  BRW7.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = HeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('tmp:tag',tmp:tag)
  BIND('HeadAccount',HeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:tag,BRW7.Q.tmp:tag)
  BRW7.AddField(tra:Account_Number,BRW7.Q.tra:Account_Number)
  BRW7.AddField(tra:Company_Name,BRW7.Q.tra:Company_Name)
  BRW7.AddField(tra:RemoteRepairCentre,BRW7.Q.tra:RemoteRepairCentre)
  BRW7.AddField(HeadAccount,BRW7.Q.HeadAccount)
  BRW7.AddField(tra:RecordNumber,BRW7.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:LOAN.Close
    Relate:STATUS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020609'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020609'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020609'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:CriteriaStartDate = TINCALENDARStyle1(tmp:CriteriaStartDate)
          Display(?tmp:CriteriaStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:CriteriaEndDate = TINCALENDARStyle1(tmp:CriteriaEndDate)
          Display(?tmp:CriteriaEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
          ?Progress:Thermometer{Prop:Hide} = False
          DO OKButton_Pressed
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:CriteriaStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:CriteriaEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!------------------------------------------------------------------------
CoerceToWorkingWeek     PROCEDURE( IN:StartTime, IN:End:Time, IN:IncludeSaturday, IN:IncludeSunday, INOUT:Date, INOUT:Time )
    CODE
        !---------------------------------------------------------------------
        LOOP
            CASE  (INOUT:Date % 7)
            OF 0 ! Sunday
                If IN:IncludeSunday
                    IF INOUT:Time < IN:StartTime
                        INOUT:Time  = IN:StartTime
                        BREAK

                    ELSIF INOUT:Time > IN:End:Time
                        INOUT:Date += 1
                        INOUT:Time  = IN:StartTime
                    ELSE
                        BREAK
                    END !IF
                ELSE
                    INOUT:Date += 1
                    INOUT:Time  = IN:StartTime
                End !If

            OF 6 ! Saturday
                If IN:IncludeSaturday
                    IF INOUT:Time < IN:StartTime
                        INOUT:Time  = IN:StartTime
                        BREAK

                    ELSIF INOUT:Time > IN:End:Time
                        INOUT:Date += 1
                        INOUT:Time  = IN:StartTime
                    ELSE
                        BREAK
                    END !IF
                ELSE
                    INOUT:Date += 1
                    INOUT:Time  = IN:StartTime
                End !If

            ELSE
                IF INOUT:Time < IN:StartTime
                    INOUT:Time  = IN:StartTime
                    BREAK

                ELSIF INOUT:Time > IN:End:Time
                    INOUT:Date += 1
                    INOUT:Time  = IN:StartTime
                ELSE
                    BREAK
                END !IF

            END !CASE
        END !LOOP
        !---------------------------------------------------------------------
CountStatusTime     PROCEDURE()
loc:TheTime     LONG
loc:TheDate     Long
    Code
        !---------------------------------------------------------------------
        loc:TheDate = 0
        loc:TheTime = 0

        !oc:TheDate = WorkingTimeBetween(      IN:StartDate,      IN:EndDate,       IN:StartTime,      IN:EndTime, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, loc:TheTime)
        loc:TheDate = WorkingTimeBetween(tmp:StartClockDate, aus:DateChanged, tmp:StartClockTime, aus:TimeChanged, haQ:StartWorkHours, haQ:EndWorkHours, haQ:IncludeSaturday, haQ:IncludeSunday, loc:TheTime)

       !WriteDebug('CountStatusTime(loc:TheDate="' & FORMAT(loc:TheDate, @d8) & '", loc:TheTime="' & FORMAT(loc:TheTime,@t1) & '")')
        !---------------------------------------------------------------------
        Case tmp:WhereIsJob
        Of JobAtRRC !At RRC
           !WriteDebug('CountStatusTime(JobAtRRC)')

            tmp:RRCDate        += loc:TheDate
            tmp:RRCTime        += loc:TheTime
            
            IF NOT jobe:WebJob
                tmp:OOCARCDate += loc:TheDate
                tmp:OOCARCTime += loc:TheTime
            END !IF jobe:WebJob

        Of JobAtARC !At ARC
           !WriteDebug('CountStatusTime(JobAtARC)')

            tmp:ARCDate        += loc:TheDate
            tmp:ARCTime        += loc:TheTime

            IF jobe:WebJob
                tmp:OOCRRCDate += loc:TheDate
                tmp:OOCRRCTime += loc:TheTime
            END !IF jobe:WebJob

        Of JobInTransit !Travelling
           !WriteDebug('CountStatusTime(Travelling)')

            IF jobe:WebJob
                tmp:OOCRRCDate += loc:TheDate
                tmp:OOCRRCTime += loc:TheTime
            ELSE
                tmp:OOCARCDate += loc:TheDate
                tmp:OOCARCTime += loc:TheTime
            END !IF jobe:WebJob

        OF JobAt3rdParty
           !WriteDebug('CountStatusTime(JobAt3rdParty)')

            IF jobe:WebJob
                tmp:OOCRRCDate += loc:TheDate
                tmp:OOCRRCTime += loc:TheTime
            ELSE
                tmp:OOCARCDate += loc:TheDate
                tmp:OOCARCTime += loc:TheTime
            END !IF jobe:WebJob

        ELSE
           !WriteDebug('CountStatusTime(ELSE)')
        End !Case tmp:WhereIsJob
        !---------------------------------------------------------------------
        !Has the time gone over 24hrs?
       !WriteDebug('CountStatusTime NormalisedateTime RRC')
        NormaliseDateTime(haQ:WorkingHours, tmp:RRCDate,    tmp:RRCTime)

       !WriteDebug('CountStatusTime NormalisedateTime ARC')
        NormaliseDateTime(haQ:WorkingHours, tmp:ARCDate,    tmp:ARCTime)

       !WriteDebug('CountStatusTime NormalisedateTime OOCRRC')
        NormaliseDateTime(haQ:WorkingHours, tmp:OOCRRCDate, tmp:OOCRRCTime)

       !WriteDebug('CountStatusTime NormalisedateTime OOCARC')
        NormaliseDateTime(haQ:WorkingHours, tmp:OOCARCDate, tmp:OOCARCTime)

       !WriteDebug('CountStatusTime(EXIT)')
        !---------------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
DaysTimeBetween PROCEDURE  (IN:StartDate, IN:StartTime, IN:EndDate, IN:EndTime, IN:StartWorkHours, IN:EndWorkHours, INOUT:Time ) ! DATE

TempDate      DATE
TempTime      TIME

StartDate      LONG
EndDate        LONG
StartTime      LONG
EndTime        LONG

StartWorkHours TIME
EndWorkHours   TIME

DaysBetween   LONG
Hours         LONG
    CODE
        !-------------------------------------------
        WriteDebug('DaysTimeBetween(' & DateTostring(IN:StartDate) & ', ' & FORMAT(IN:StartTime, @t1) & ', ' & DateToString(IN:EndDate) & ', ' & FORMAT(IN:EndTime, @T1) & ', ' & FORMAT(IN:StartWorkHours, @T1) & ', ' & FORMAT(IN:EndWorkHours, @T1) & ', ' & FORMAT(INOUT:Time, @T1) & ')' )
        !-------------------------------------------
        StartDate      = IN:StartDate
        EndDate        = IN:EndDate

        StartTime      = IN:StartTime
        EndTime        = IN:EndTime

        StartWorkHours = IN:StartWorkHours
        EndWorkHours   = IN:EndWorkHours
        !-------------------------------------------
        IF EndDate < StartDate
            TempDate  = EndDate
            EndDate   = StartDate
            StartDate = TempDate
        END !IF

        IF EndWorkHours < StartWorkHours
            TempTime       = EndWorkHours
            EndWorkHours   = StartWorkHours
            StartWorkHours = TempTime
        END !IF
        !-------------------------------------------
        DaysBetween  = EndDate - StartDate
        Hours        = 0

        IF DaysBetween = 0
            Hours = EndTime - StartTime
        ELSE
            DaysBetween -= 1

            !IF EndWorkHours < StartTime
            !    Hours += 0
            !ELSE
                Hours += EndWorkHours - StartTime
            !END !IF

            !IF StartWorkHours < EndTime
            !    Hours += 0
            !ELSE
                Hours += EndTime !- EndTime
            !END !IF

        END !IF

        !NormaliseDateTime(DEFORMAT('24:00', @T1), DaysBetween, Hours)
        IF Hours > Deformat('23:59:59',@t4)
          LOOP WHILE Hours => Deformat('23:59:59',@t4)
           !WriteDebug('NormaliseDateTime(LOOP)')

            DaysBetween += 1
            Hours -= Deformat('23:59:59',@t4)
          END
        END !LOOP

        INOUT:Time = Hours

        WriteDebug('RESULT DaysTimeBetween(' & DaysBetween & ', ' & FORMAT(INOUT:Time, @T1) & ')' )

        RETURN DaysBetween
        !-------------------------------------------
FormatDaysHoursMins         PROCEDURE( IN:Days, IN:Time )! STRING
Hrs       REAL
Hundreths LONG(0) ! 86400 secs per day ! 8640000 100ths per day ! 3600 secs per hour ! 360000 100ths per hour
TempTime  STRING(5)
    CODE
        !-----------------------------------------------------------------
        !RETURN IN:Days & ':'  & Format(IN:Time, @t01) ! Clashes with excel format, Excel assumes [h]:mm:ss
        !RETURN IN:Days & 'd ' & Format(IN:Time, @t01) ! Excel doesn't recpgnise format so cannont add cells together
        !-----------------------------------------------------------------
        ! Change to MS Variant Date Time type
        !NormaliseDateTime(haQ:WorkingHours, IN:Days, IN:Time)

        TempTime = FORMAT(IN:Time, @T01)

        Hundreths += (TempTime[1:2] * 360000)
        Hundreths += (TempTime[4:5] *   6000)
        Hrs        = Hundreths / 8640000


        RETURN IN:Days + hrs
        !-----------------------------------------------------------------
!        NormaliseDateTime(haQ:WorkingHours, IN:Days, IN:Time)
!
!        TempTime = FORMAT(IN:Time, @T01)
!
!        hrs = TempTime[1:2]
!        hrs = hrs + (haQ:WorkingHours / 360000) * IN:Days
!        RETURN hrs & ':' & TempTime[4:5]
        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            !WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')NOT FOUND - ADD')
            CLEAR(HeadAccount_Queue)
                haq:AccountNumber                  = IN:AccountNumber

                IF LoadTRADEACC(IN:AccountNumber)
                    haq:AccountName                = tra:Company_Name
                    haq:BranchIdentification       = tra:BranchIdentification

                    IF tra:UseTimingsFrom = 'DEF'
                        IF def:Include_Saturday    = 'YES'
                            haQ:IncludeSaturday    = True
                        ELSE
                            haQ:IncludeSaturday    = False
                        END !IF

                        IF def:Include_Sunday = 'YES'
                            haQ:IncludeSunday      = True
                        ELSE
                            haQ:IncludeSunday      = False
                        END !IF

                        haQ:StartWorkHours         = def:Start_Work_Hours
                        haQ:EndWorkHours           = def:End_Work_Hours

                    ELSE
                        IF tra:IncludeSaturday = 'YES'
                            haQ:IncludeSaturday    = True
                        ELSE
                            haQ:IncludeSaturday    = False
                        END !IF

                        IF tra:IncludeSunday = 'YES'
                            haQ:IncludeSunday      = True
                        ELSE
                            haQ:IncludeSunday      = False
                        END !IF

                        haQ:StartWorkHours         = tra:StartWorkHours
                        haQ:EndWorkHours           = tra:EndWorkHours

                    END !IF
                 
                ELSE
                    haq:AccountName                = IN:AccountNumber
                    haq:BranchIdentification       = ''

                    haQ:StartWorkHours             = def:Start_Work_Hours
                    haQ:EndWorkHours               = def:End_Work_Hours
                    
                END !IF

                haQ:WorkingHours                   = haQ:EndWorkHours - haQ:StartWorkHours
            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            tmp:Stop = True
        END !IF

        !WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')EXIT')
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------  
        saQ:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF saQ:AccountNumber  = IN:AccountNumber
                ! Found
                RETURN
            ELSE
                ! Partial Match - ADD
                DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True

        ELSE
            tmp:Stop = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        IF DoAdd
            CLEAR(SubAccount_Queue)
                saQ:AccountNumber            = IN:AccountNumber

                IF LoadSUBTRACC(IN:AccountNumber)
                    GetHeadAccount(wob:HeadAccountNumber)

                    saQ:AccountName          = sub:Company_Name
                    saQ:HeadAccountNumber    = sub:Main_Account_Number
                    saQ:HeadAccountName      = haQ:AccountName
                    saQ:BranchIdentification = haQ:BranchIdentification

!                    If haQ:InvoiceSubAccounts = True
!                        saQ:PartsVATCode         = sub:Parts_VAT_Code
!                        saQ:LabourVATCode        = sub:Labour_VAT_Code
!                        saQ:RetailVATCode        = sub:Retail_Discount_Code
!
!                        saQ:PartsVATRate         = GetVATRate(sub:Parts_VAT_Code)
!                        saQ:LabourVATRate        = GetVATRate(sub:Labour_VAT_Code)
!                        saQ:RetailVATRate        = GetVATRate(sub:Retail_Discount_Code)
!                    Else
!                        saQ:PartsVATCode         = haQ:PartsVATCode
!                        saQ:LabourVATCode        = haQ:LabourVATCode
!                        saQ:RetailVATCode        = haQ:RetailVATCode
!
!                        saQ:PartsVATRate         = haQ:PartsVATRate
!                        saQ:LabourVATRate        = haQ:LabourVATRate
!                        saQ:RetailVATRate        = haQ:RetailVATRate
!                    End!If tra:use_sub_accounts = 'YES'

                ELSE
                    saq:AccountName          = ''
                    saq:HeadAccountNumber    = ''
                    saQ:HeadAccountName      = ''
                    saQ:BranchIdentification = ''

!                    saQ:PartsVATCode         = ''
!                    saQ:LabourVATCode        = ''
!                    saQ:RetailVATCode        = ''
!
!                    saQ:PartsVATRate         = 0.00
!                    saQ:LabourVATRate        = 0.00
!                    saQ:RetailVATRate        = 0.00
                END !IF
            ADD(SubAccount_Queue, +saq:AccountNumber)
        END !IF
        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
LoadAUDSTATS        PROCEDURE( IN:JobNumber, IN:DateBooked, INOUT:First )! LONG ! BOOL
    CODE
        IF INOUT:First = True
           !WriteDebug('LoadAUDSTATS(First)')
            INOUT:First = False
            
            Access:AUDSTATS.ClearKey(aus:StatusDateKey)
                aus:RefNumber   = IN:JobNumber
                aus:DateChanged = IN:DateBooked
                !aus:TimeChanged = IN:TimeBooked
            Set(aus:StatusDateKey,aus:StatusDateKey)
        END !IF

        IF NOT Access:AUDSTATS.NEXT() = Level:Benign
           !WriteDebug('LoadAUDSTATS(EOF)')
            RETURN False
        END !IF

        IF NOT aus:RefNumber   = IN:JobNumber
           !WriteDebug('LoadAUDSTATS(EOI IN:JobNumber)')
            RETURN False
        END !IF

        !IF aus:DateChanged < IN:DateBooked
        !   !WriteDebug('LoadAUDSTATS(EOI IN:DateBooked)')
        !    RETURN False
        !END !IF

       !WriteDebug('LoadAUDSTATS(OK)')
        RETURN True
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSTATUS          PROCEDURE(IN:NewStatus)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
            sts:Ref_Number = SUB(IN:NewStatus, 1, 3)
        SET(sts:Ref_Number_Only_Key, sts:Ref_Number_Only_Key)

        If NOT Access:STATUS.NEXT() = Level:Benign
           !WriteDebug('LoadSTATUS(' & CLIP(IN:NewStatus) & ')FAIL EOF')

            RETURN False
        END !IF

        If NOT sts:Ref_Number = SUB(IN:NewStatus, 1, 3)
           !WriteDebug('LoadSTATUS(' & CLIP(IN:NewStatus) & ')FAIL EOI')

            RETURN False
        END !IF

       !WriteDebug('LoadSTATUS(' & CLIP(IN:NewStatus) & ')OK')
        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = IN:JobNumber
        SET(wob:RefNumberKey, wob:RefNumberKey)

        IF Access:WEBJOB.NEXT() <> Level:Benign
            !WriteDebug('LoadWEBJOB(' & IN:JobNumber & ')EOF')

            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            !WriteDebug('LoadWEBJOB(' & IN:JobNumber & ')EOI')

            RETURN False
        END !IF

        !WriteDebug('LoadWEBJOB(' & IN:JobNumber & ')OK')
        RETURN True
        !-----------------------------------------------
NormaliseDateTime       PROCEDURE(IN:WorkingHours, INOUT:Date, INOUT:Time)
    CODE
        !-------------------------------------------
        !WriteDebug('NormaliseDateTime("' & FORMAT(IN:WorkingHours, @t1) & '", "' & FORMAT(INOUT:Date,@d8) & '", "' & FORMAT(INOUT:Time,@T1) & '") IN')

        LOOP WHILE INOUT:Time => IN:WorkingHours
           !WriteDebug('NormaliseDateTime(LOOP)')

            INOUT:Date += 1
            INOUT:Time -= IN:WorkingHours
        END !LOOP

       !WriteDebug('NormaliseDateTime("' & FORMAT(INOUT:Date,@d8) & '", "' & FORMAT(INOUT:Time,@T1) & '") OUT')
        !-------------------------------------------
SetSheetTo              PROCEDURE( IN:AccountName )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('A) SetSheetTo("' & CLIP(IN:AccountName) & '")')

        CLEAR(Sheet_Queue)
            shQ:AccountName = CLIP(IN:AccountName)
        GET(Sheet_Queue, +shQ:AccountName)

        CASE ERRORCODE()
        OF 00
            IF shQ:AccountName = CLIP(IN:AccountName)
                ! Found
                !
                ExcelSelectSheet(CLIP(shQ:SheetName))
                !WriteDebug('B) SetSheetTo(Found  IN:AccountName="' & CLIP(IN:AccountName) & '", shQ:AccountName="' & CLIP(shQ:AccountName) & '", shQ:SheetName="' & CLIP(shQ:SheetName) & '") ActiveSheet.Name="' & CLIP(Excel{'ActiveSheet.Name'}) & '"')

                RETURN
            ELSE
                ! NOT Found, Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        ELSE
            tmp:Stop = True

            RETURN
        END !CASE
        !-----------------------------------------------
        IF DoADD = True
            !----------------------------------------------------
            ExcelMakeSheet()
            ExcelSheetName(CLIP(IN:AccountName))
            !----------------------------------------------------
            ExcelSelectRange('A1')
            ExcelFontSize(14)
            ExcelCellWidth(43)
            ExcelCell('Turnaround Report (' & Clip(IN:AccountName) & ')', 1)

            ExcelSelectRange('A3')
                ExcelCellWidth(25)
                ExcelCell('Jobs Completed From:',0)
                ExcelCell(Format(tmp:CriteriaStartDate,@d06),1)

            ExcelSelectRange('A4')
                ExcelCell('Jobs Completed To:',0)
                ExcelCell(Format(tmp:CriteriaEndDate,@d06),1)

            ExcelSelectRange('A5')
                ExcelCell('Date Created:',0)
                ExcelCell(Format(Today(),@d06),1)
            !----------------------------------------------------
            ExcelSelectRange('A8')
            ExcelCell('SB Job No',1)
            ExcelCellWidth(20)
            ExcelCell('Branch Specific No',1)
            ExcelCellWidth(25)
            ExcelCell('Head Account',1)
            ExcelCellWidth(25)
            ExcelCell('Sub Account',1)
            ExcelCellWidth(25)
            ExcelCell('Manufacturer',1)
            ExcelCellWidth(25)
            ExcelCell('Model Number',1)
            ExcelCellWidth(9)
            ExcelCell('Warranty',1)
            ExcelCellWidth(15)
            ExcelCell('Date',1) !Jobs Booked In  Col H
            ExcelCellWidth(5)
            ExcelCell('Time',1) ! Col I
            ExcelCellWidth(15)
            ExcelCell('Date',1) !Jobs Completed Col J
            ExcelCellWidth(5)
            ExcelCell('Time',1) ! Col K

            ExcelCellWidth(8)
            ExcelCell('Days',1) !Total Repair Time  Col L

            ExcelCellWidth(8)
            ExcelCell('HH:MM',1) !Total Repair Time  Col M


            ExcelCellWidth(8)
            ExcelCell('Days',1) !Customers Perspective Col N

            ExcelCellWidth(8)
            ExcelCell('HH:MM',1) !Customers Perspective Col O


            ExcelCellWidth(8)
            ExcelCell('Days',1) !In Control Col P

            ExcelCellWidth(8)
            ExcelCell('HH:MM',1) !In Control Col Q

            ExcelCellWidth(8)
            ExcelCell('Days',1) !Out Control Col R

            ExcelCellWidth(8)
            ExcelCell('HH:MM',1) !Out Control  Col S

            ExcelCellWidth(12)
            ExcelCell('Loan Issued',1)
            !------------------------------------------------------------
            ExcelSelectRange('H7')

            ExcelCell('Jobs Booked In',01)  !H7
            ExcelCell('',0)

            ExcelCell('Jobs Completed',1)   !J7
            ExcelCell('',0)

            ExcelCell('Customer Perspective',1)!L7
            ExcelCell('',0)

            ExcelCell('Total Repair Time',1)!N7
            ExcelCell('',0)

            ExcelCell('In Control',1) !P7
            ExcelCell('',0)

            ExcelCell('Out Control',1) !R7
            ExcelCell('',0)
            !------------------------------------------------------------
            ExcelSelectRange('D4')
            ExcelCell('Total Jobs',1)

            ExcelSelectRange('E3')
            ExcelCell('In Report',1)
            ExcelCell('Shown',1)

            ExcelSelectRange('K4')
            ExcelCell('Total',1)

            ExcelGrid('A1:t1',1,1,1,1,15) ! 2 Apr 2003 John, LastCol = T
            ExcelGrid('A3:t5',1,1,1,1,15) ! 2 Apr 2003 John, LastCol = T
            ExcelGrid('A7:T8',1,1,1,1,15) ! 2 Apr 2003 John, LastCol = T
            !----------------------------------------------------
            CLEAR(Sheet_Queue)
                shQ:AccountName = CLIP(IN:AccountName)
                shQ:SheetName   = Excel{'ActiveSheet.Name'}
            ADD(Sheet_Queue, +shQ:AccountName)

            !WriteDebug('C) SetSheetTo(NOT Found IN:AccountName="' & CLIP(IN:AccountName) & '", shQ:AccountName="' & CLIP(shQ:AccountName) & '", shQ:SheetName="' & CLIP(shQ:SheetName) & '") ActiveSheet.Name="' & CLIP(Excel{'ActiveSheet.Name'}) & '"')

            RETURN
        END !IF
        !-----------------------------------------------
        !WriteDebug('D) SetSheetTo(Unreachable ERROR "' & CLIP(IN:AccountNumber) & '")"' & CLIP(Excel{'ActiveSheet.Name'}) & '"')
        !-----------------------------------------------
WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
Days         LONG(0)
TempDate1    LONG
    CODE
        !-------------------------------------------
        WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
        !-------------------------------------------
        IF (IN:StartDate = 0)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
            RETURN 0
        ELSIF (IN:EndDate = 0)
            !WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
            RETURN 0
        ELSIF (IN:StartDate = IN:EndDate)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
            RETURN 0
        ELSIF (IN:StartDate > IN:EndDate)
            !WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        count# = In:EndDate - In:StartDate
        
        LOOP g# = 1 TO count# !TempDate1 = IN:StartDate TO IN:EndDate ! WHILE TempDate1 < IN:EndDate
            CASE (TempDate1 % 7)
            OF 0
                WriteDebug('sunday' & format(tempdate1,@d8))
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 6
                 WriteDebug('saturday' & format(tempdate1,@d8))
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1

            END !CASE

            WriteDebug('Date = ' & format(tempdate1,@d8))
            TempDate1 += 1
        END !LOOP
        !-------------------------------------------
        WriteDebug('WorkingDaysBetween = ' & Days)
        RETURN Days
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (IN:StartDate, IN:EndDate, IN:StartTime, IN:EndTime, IN:StartWorkHours, IN:EndWorkHours, IN:IncludeSaturday, IN:IncludeSunday ) ! LONG
DaysBetween   LONG
WorkingHours  LONG
Hours         LONG
    CODE
        !-------------------------------------------
        DaysBetween  = WorkingDaysBetween(IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        WorkingHours = (IN:EndWorkHours - IN:StartWorkHours)
        Hours        = 0

        IF DaysBetween = 0
            Hours += HoursBetween(IN:StartTime, IN:EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(IN:StartTime,            IN:EndWorkHours)
            Hours += HoursBetween(IN:StartWorkHours, IN:EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * WorkingHours

            Hours += HoursBetween(IN:StartTime,      IN:EndWorkHours)
            Hours += HoursBetween(IN:StartWorkHours, IN:EndTime     )
        END !IF

        RETURN Hours
        !-------------------------------------------
WorkingTimeBetween PROCEDURE  (IN:StartDate, IN:EndDate, IN:StartTime, IN:EndTime, IN:StartWorkHours, IN:EndWorkHours, IN:IncludeSaturday, IN:IncludeSunday, INOUT:Time ) ! LONG
DaysBetween   LONG
WorkingHours  LONG

EndTime      LONG
StartTime    LONG
    CODE
        !-------------------------------------------
        ! 23 Nov 2002 John
        ! Return noralised number of days between start and end
        ! Side effect set INOUT:Time to the amount of time between start and end
        !-------------------------------------------
        ! Ignore out of hours working
        !
        IF    IN:StartTime < IN:StartWorkHours
            StartTime  =  IN:StartWorkHours
            !WriteDebug('StartTime = "' & FORMAT(StartTime, @t1) & '"')

        ELSIF IN:StartTime > IN:EndWorkHours
            StartTime  =  IN:EndWorkHours
            !WriteDebug('StartTime = "' & FORMAT(StartTime, @t1) & '"')
        ELSE
            StartTime  =  IN:StartTime
        END !IF
        !------
        IF    IN:EndTime < IN:StartWorkHours
            EndTime    =  IN:StartWorkHours
            !WriteDebug('EndTime = "' & FORMAT(EndTime, @t1) & '"')

        ELSIF IN:EndTime > IN:EndWorkHours
            EndTime    =  IN:EndWorkHours
            !WriteDebug('EndTime = "' & FORMAT(EndTime, @t1) & '"')
        ELSE
            EndTime    =  IN:EndTime
        END !IF
        !-------------------------------------------
!        If job:Ref_number = 18130
!            Stop(Format(in:StartDate,@d6) & '<13,10>' & Format(in:EndDate,@d6))
!        End !If job:Ref_number = 18130

        DaysBetween  = WorkingDaysBetween(IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        WriteDebug('DaysBetween1 = "' & DaysBetween & '"')

        WorkingHours = (IN:EndWorkHours - IN:StartWorkHours)
        WriteDebug('WorkingHours = "' & FORMAT(WorkingHours, @t1) & '"')
        WriteDebug('StartTime = "' & FORMAT(StartTime, @t1) & '"')
        WriteDebug('EndTime = "' & FORMAT(EndTime, @t1) & '"')
        INOUT:Time   = 0

        IF DaysBetween = 0
            INOUT:Time  += EndTime - StartTime

        ELSE
            DaysBetween += -1

            INOUT:Time  += IN:EndWorkHours - StartTime
            INOUT:Time  += EndTime         - IN:StartWorkHours
        END !IF
        WriteDebug('Inout:Time = "' & FORMAT(INOUT:Time, @t1) & '"')
        NormaliseDateTime(WorkingHours, DaysBetween,    INOUT:Time)

        WriteDebug('DaysBetween1 = "' & DaysBetween & '"')
        WriteDebug('Inout:Time = "' & FORMAT(INOUT:Time, @t1) & '"')

        !-------------------------------------------
       !WriteDebug('WorkingTimeBetween(' & CLIP(aus:NewStatus) & ' ' & |
!                   '<' & DateToString(IN:StartDate) & ' ' & FORMAT(IN:StartTime,      @T1) & '>, ' & |
!                   '<' & DateToString(IN:EndDate)   & ' ' & FORMAT(IN:EndTime,        @t1) & '>, ' & |
!                   'Between[' & FORMAT(IN:StartWorkHours, @t01) & '-' & FORMAT(IN:EndWorkHours, @t01) & ']' & |
!                   ') = (' & DaysBetween & 'd ' & FORMAT(INOUT:Time, @t01) & ')' )

        RETURN DaysBetween
        !-------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
!------------------------------------------------------------------------
Local.AddToSummary        Procedure(String func:AccountNumber,String func:AddType,Byte func:ARC,Long func:ClockDate,Long func:ClockTime)
    Code
        !----------------------------------------------------------------------------------
        !WriteDebug('Local.AddToSummary("' & CLIP(func:AccountNumber) & '", func:AddType="' & CLIP(func:AddType) & '", func:ARC="' & func:ARC & '", func:ClockDate=#' & |
        !             FORMAT(func:ClockDate, @d8) & '#, func:ClockTime=<' & FORMAT(func:ClockTime, @t1) & '>)')
        !----------------------------------------------------------------------------------
        Clear(turn:Record)
            turn:AccountNumber  = func:AccountNumber
            Get(TurnReport, turn:AccountNumberKey)

            IF ERROR()
                CLEAR(TurnReport)
                    Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
                    tra_ali:Account_Number  = func:AccountNumber
                    If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                        !Found

                    Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign

                    turn:AccountNumber      = func:AccountNumber
                    turn:AccountName        = tra_ali:Company_Name
                    turn:ARCAccount         = func:ARC
                Add(TurnReport)
            END !IF
            !----------------------------------------------------------------------------------
            Case func:AddType
            Of 'COMP'
                turn:Completed          += 1
            Of 'BOOK'
                turn:Booked             += 1
            Of 'OPEN'
                turn:Opened             += 1
            Of 'LOAN'   
                turn:Loaned             += 1
            Of 'TURN'
                NormaliseDateTime(haQ:WorkingHours, func:ClockDate, func:ClockTime)

                CASE func:ClockDate
                OF 0
                    If func:ClockTime <= 360000  !DEFORMAT('01:00', @T1)
                        turn:Completed1  += 1
                    Else
                        turn:Completed24 += 1
                    End !If tmp:ClockTime <= 360000
                OF 1
                    turn:Completed48     += 1
                OF 2
                    turn:Completed72     += 1
                OF 3 To 6
                    turn:Completed168    += 1
                ELSE
                    turn:CompletedOver168    += 1
                END !CASE

            Of 'TURNLOAN'
                NormaliseDateTime(haQ:WorkingHours, func:ClockDate, func:ClockTime)

                CASE func:ClockDate
                OF 0
                    If func:ClockTime <= 360000  !DEFORMAT('01:00', @T1)
                        turn:Loaned1  += 1
                    Else
                        turn:Loaned24 += 1
                    End !If tmp:ClockTime <= 360000
                OF 1
                    turn:Loaned48     += 1
                OF 2
                    turn:Loaned72     += 1
                OF 3 TO 6
                    turn:Loaned168    += 1
                ELSE
                    turn:LoanedOver168    += 1
                END !CASE


            Of 'CUST'
                !NormaliseDateTime(haQ:WorkingHours, func:ClockDate, func:ClockTime)

                CASE func:ClockDate
                OF 0
                    If func:ClockTime <> 0
                      If func:ClockTime <= 360000  !DEFORMAT('01:00', @T1)
                          turn:CustCompleted1  += 1
                      Else
                          turn:CustCompleted24 += 1
                      End !If tmp:ClockTime <= 360000
                    ELSE
                      !no hours or minutes or anything!
                      turn:CustCompletedOver168    += 1
                    END

                OF 1
                    turn:CustCompleted48     += 1
                OF 2
                    turn:CustCompleted72     += 1
                OF 3 To 6
                    turn:CustCompleted168    += 1
                ELSE
                    turn:CustCompletedOver168    += 1
                END !CASE


            End !Case func:AddType

        Put(TurnReport)
        !----------------------------------------------------------------------------------
!Local.AddToSummary        Procedure(String func:AccountNumber,String func:AddType,Byte func:ARC,Long func:ClockDate,Long func:ClockTime)
!Code
!    
!    Clear(turn:Record)
!    turn:AccountNumber  = func:AccountNumber
!    Get(TurnReport,turn:AccountNumberKey)
!    If Error()
!        Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
!        tra_ali:Account_Number  = func:AccountNumber
!        If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!            !Error
!        End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
!        turn:AccountName        = tra_ali:Company_Name
!        turn:AccountNumber      = func:AccountNumber
!        turn:ARCAccount         = func:ARC
!
!        Case func:AddType
!            Of 'COMP'
!                turn:Completed          = 1
!            Of 'BOOK'
!                turn:Booked             = 1
!            Of 'OPEN'
!                turn:Opened             = 1
!            Of 'LOAN'   
!                turn:Loaned             = 1
!
!            Of 'TURN'
!
!                If func:ClockTime > 8640000
!                    func:ClockDate += 1
!                End !If func:ClockTime > 8460000
!
!                If func:ClockDate = 0
!                    If func:ClockTime <= 360000
!                        turn:Completed1 = 1
!                    Else
!                        turn:Completed24 = 1
!                    End !If tmp:ClockTime <= 360000
!                End !If tmp:ClockDate = 0
!
!                If func:ClockDate = 1
!                    turn:Completed48 = 1
!                End !If tmp:ClockDate = 1
!                If func:ClockDate = 2
!                    turn:Completed72 = 1
!                End !If tmp:ClockDate = 2
!                If func:ClockDate = 3
!                    turn:Completed168 = 1
!                End !If tmp:ClockDate = 3
!                If func:ClockDate > 3
!                    turn:CompletedOver168 = 1
!                End !If tmp:ClockDate > 3
!
!
!                If (job:Date_Completed - job:Date_Booked) = 0
!                    If (job:Time_Completed - job:Time_Booked) <= 360000
!                        turn:CustCompleted1 = 1
!                    Else
!                        turn:CustCompleted24 = 1
!                    End !If tmp:ClockTime <= 360000
!                End !If tmp:ClockDate = 0
!
!                If (job:Date_Completed - job:Date_Booked) = 1
!                    turn:CustCompleted48 = 1
!                End !If tmp:ClockDate = 1
!                If (job:Date_Completed - job:Date_Booked) = 2
!                    turn:CustCompleted72 = 1
!                End !If tmp:ClockDate = 2
!                If (job:Date_Completed - job:Date_Booked) = 3
!                    turn:CustCompleted168 = 1
!                End !If tmp:ClockDate = 3
!                If (job:Date_Completed - job:Date_Booked) > 3
!                    turn:CustCompletedOver168 = 1
!                End !If tmp:ClockDate > 3
!
!            Of 'TURNLOAN'
!
!                If func:ClockTime > 8640000
!                    func:ClockDate += 1
!                End !If func:ClockTime > 8460000
!
!                If func:ClockDate = 0
!                    If func:ClockTime <= 360000
!                        turn:Loaned1 = 1
!                    Else
!                        turn:Loaned24 = 1
!                    End !If tmp:ClockTime <= 360000
!                End !If tmp:ClockDate = 0
!
!                If func:ClockDate = 1
!                    turn:Loaned48 = 1
!                End !If tmp:ClockDate = 1
!                If func:ClockDate = 2
!                    turn:Loaned72 = 1
!                End !If tmp:ClockDate = 2
!                If func:ClockDate = 3
!                    turn:Loaned168 = 1
!                End !If tmp:ClockDate = 3
!                If func:ClockDate > 3
!                    turn:LoanedOver168 = 1
!                End !If tmp:ClockDate > 3
!
!
!        End !Case func:AddType
!       
!        
!
!        Add(TurnReport)
!    Else !If Error()
!        Case func:AddType
!            Of 'COMP'
!                turn:Completed          += 1
!            Of 'BOOK'
!                turn:Booked             += 1
!            Of 'OPEN'
!                turn:Opened             += 1
!            Of 'LOAN'   
!                turn:Loaned             += 1
!            Of 'TURN'
!                If func:ClockTime > 8640000
!                    func:ClockDate += 1
!                End !If func:ClockTime > 8460000
!
!                If func:ClockDate = 0
!                    If func:ClockTime <= 360000
!                        turn:Completed1 += 1
!                    Else
!                        turn:Completed24 += 1
!                    End !If tmp:ClockTime <= 360000
!                End !If tmp:ClockDate = 0
!
!                If func:ClockDate = 1
!                    turn:Completed48 += 1
!                End !If tmp:ClockDate = 1
!                If func:ClockDate = 2
!                    turn:Completed72 += 1
!                End !If tmp:ClockDate = 2
!                If func:ClockDate = 3
!                    turn:Completed168 += 1
!                End !If tmp:ClockDate = 3
!                If func:ClockDate > 3
!                    turn:CompletedOver168 += 1
!                End !If tmp:ClockDate > 3
!
!                If (job:Date_Completed - job:Date_Booked) = 0
!                    If (job:Time_Completed - job:Time_Booked) <= 360000
!                        turn:CustCompleted1 += 1
!                    Else
!                        turn:CustCompleted24 += 1
!                    End !If tmp:ClockTime <= 360000
!                End !If tmp:ClockDate = 0
!
!                If (job:Date_Completed - job:Date_Booked) = 1
!                    turn:CustCompleted48 += 1
!                End !If tmp:ClockDate = 1
!                If (job:Date_Completed - job:Date_Booked) = 2
!                    turn:CustCompleted72 += 1
!                End !If tmp:ClockDate = 2
!                If (job:Date_Completed - job:Date_Booked) = 3
!                    turn:CustCompleted168 += 1
!                End !If tmp:ClockDate = 3
!                If (job:Date_Completed - job:Date_Booked) > 3
!                    turn:CustCompletedOver168 += 1
!                End !If tmp:ClockDate > 3
!
!            Of 'TURNLOAN'
!                If func:ClockTime > 8640000
!                    func:ClockDate += 1
!                End !If func:ClockTime > 8460000
!
!                If func:ClockDate = 0
!                    If func:ClockTime <= 360000
!                        turn:Loaned1 += 1
!                    Else
!                        turn:Loaned24 += 1
!                    End !If tmp:ClockTime <= 360000
!                End !If tmp:ClockDate = 0
!
!                If func:ClockDate = 1
!                    turn:Loaned48 += 1
!                End !If tmp:ClockDate = 1
!                If func:ClockDate = 2
!                    turn:Loaned72 += 1
!                End !If tmp:ClockDate = 2
!                If func:ClockDate = 3
!                    turn:Loaned168 += 1
!                End !If tmp:ClockDate = 3
!                If func:ClockDate > 3
!                    turn:LoanedOver168 += 1
!                End !If tmp:ClockDate > 3
!
!        End !Case func:AddType
!
!        Put(TurnReport)
!    End !If Error()
!        
Local.LoanIssued        Procedure()
loc:FoundLoan       Byte
Code
    loc:FoundLoan = 0
    Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type       = 'LOA'
            aud:Action     = 'LOAN UNIT ATTACHED TO JOB'
        Set(aud:TypeActionKey,aud:TypeActionKey)

        LOOP WHILE Access:AUDIT.NEXT() = Level:Benign
            If aud:Ref_Number <> job:Ref_Number      |
            Or aud:Type       <> 'LOA'      |
            Or aud:Action     <> 'LOAN UNIT ATTACHED TO JOB'      |
                Then Break.  ! End If
            loc:FoundLoan = 1
            Break
        End !Loop
    Access:AUDIT.RestoreFile(Save_aud_ID)

   !WriteDebug('Local.LoanIssued(' & loc:FoundLoan & ')')

    Return loc:FoundLoan
!------------------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !(tra:RemoteRepairCentre = 1 OR tra:Account_Number = HeadAccount) AND (tra:Account_Number <> 'XXXRRC')
  !
  !IF (tra:Account_Number = 'XXXRRC')
  !    Return Record:Filtered
  !ELSIF tra:BranchIdentification = ''
  !    Return Record:Filtered
  !End !tra:BranchIdentification = ''
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0

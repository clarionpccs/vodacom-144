

   MEMBER('smstext.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('SMSTE001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
LastDateUsed         DATE
Found_Flag           BYTE
LocalSMSType         STRING(1)
X                    LONG
Y                    LONG
LocalSortOutText     STRING(200)
SMR_DoneQueue        QUEUE,PRE(SDQ)
RecordNo             LONG
AccountNumber        STRING(15)
JobRefNo             LONG
SMSType              STRING(1)
                     END
  CODE
   Relate:SMSMAIL.Open
   Relate:JOBS.Open
   Relate:SMSRECVD.Open
   Relate:TRADEACC.Open
   Relate:WEBJOB.Open
   Relate:AUDIT.Open
   Relate:SMSRECVD_ALIAS.Open
   Relate:TRADEAC2.Open
   Relate:JOBSE.Open
!send daily emails and look for incomming

    LastDateUsed = getini('ESTIMATES','LASTRUN','',clip(path())&'\SMSText.ini')

    if LastDateUsed <> today() then do DailySendOfEstimates.

    Do CheckReceivedSMS

    post(event:closewindow)

!================================================================================

DailySendOfEstimates     Routine

    Access:jobs.clearkey(job:By_Status)
    job:Current_Status = '520 ESTIMATE SENT'
    SET(job:By_Status,job:By_Status)
    LOOP
        if access:jobs.next() then break.
        if job:Current_Status <> '520 ESTIMATE SENT' then break.

        SendSMSText('J','Y','N')     !job, auto, not a special case - the procedure will do all the sorting

    END !loop through jobs by status

    Putini('ESTIMATES','LASTRUN',today(),clip(path())&'\SMSText.ini')

    EXIT


CheckReceivedSMS        Routine

    Access:SMSRecvd.clearkey(SMR:KeyJob_Ref_Number)
    SMR:Job_Ref_Number = 0      !ie one we have not managed to identify before
    set(SMR:KeyJob_Ref_Number,SMR:KeyJob_Ref_Number)
    Loop

        if access:SMSRecvd.next() then break.
        if SMR:Job_Ref_Number <> 0 then break.

        if SMR:SMSType = 'X' then cycle. !this one has been done before

        !reset local variables
        Found_Flag = false
        LocalSMSType = 'X'      !default - may be changed
        LocalSortOutText = ''

        !need to find the job:refno and copy it here
        Access:SMSMail.clearkey(sms:KeyMSISDN_DateDec)
        sms:MSISDN      = SMR:MSISDN
        sms:DateSMSSent = today() + 1
        set(sms:KeyMSISDN_DateDec,sms:KeyMSISDN_DateDec)
        Loop
            if access:SMSmail.next() then break.
            if sms:MSISDN <> SMR:MSISDN then break.

            !found one it should be the last one used
            Found_Flag = true
            BREAK

        END !loop through sent sms by MSISND and date decending

        If Found_Flag then   !SEND THE EMAIL TO THREE PEOPLE on the trade account
            !Message('Found job from SMS as '&clip(sms:RefNumber))
            !look up the job
            Access:jobs.clearkey(job:Ref_Number_Key)
            job:Ref_Number = sms:RefNumber
            if access:jobs.fetch(job:Ref_Number_Key)
                !error
                !message('Job not found from '&sms:RefNumber)
                LocalSMSType = 'X'
            ELSE

                !whilst here take a note of the SMStype to be used later
                Case Job:Current_Status[1:3]
                    of '520'
                        LocalSMSType = 'E'  !estimate
                    of '901' orof '902' orof '905' orof '910'
                        LocalSMSType = 'C'  !CSI
                    ELSE
                        LocalSMSType = 'N'  !normal
                END !case job:CurrentStatus

                !Web Job Number
                access:webjob.clearkey(wob:refnumberkey)
                wob:refnumber = job:ref_number
                if access:webjob.fetch(wob:refnumberkey)
                  !error
                  !message('WebJob not found from '&job:ref_number)
                  LocalSMSType = 'X'
                ELSE
                    access:tradeacc.clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    if access:tradeacc.fetch(tra:Account_Number_Key)
                        !eror
                        !message('Trade Acc not found from '&wob:HeadAccountNumber)

                        LocalSMSType = 'X'
                    ELSE
                        !yippee found it - no changes needed let LocalSMSType stand as it was calculted
                        access:tradeac2.clearkey(TRA2:KeyAccountNumber)
                        tra2:Account_Number = wob:HeadAccountNumber
                        if access:tradeac2.fetch(TRA2:KeyAccountNumber)
                            !no second or third email can be used
                        END !if tradeac2 found
                    END !if trade account found

                END !if webjob found
            END !If error on jobs fetch
        ELSE
            !not found make sure the update is for zero
            job:Ref_number = 0
        END !if previous sms found

        !Special bit if this is an estimate reply but a duplicate reply?
        if LocalSMSType <> 'X' and LocalSMSType <> 'E'
            !not something wrong nor an estimae
            if upper(SMR:TextReceived[1:6]) = 'ACCEPT' or upper(SMR:TextReceived[1:7]) = 'DECLINE'
                !accept or decline received, but not an estimate - is this a duplicate?
                SendSMSText('J','Y','D')
                LocalSMSType = 'D'
            END !if start accept or delcine
        END !if not X = something wrong

        !create the email
        ACCESS:SmsMail.primerecord()
        sms:RefNumber         = job:Ref_number
        sms:EmailAddress      = tra:EmailAddress
        sms:EmailAddress2     = tra2:SMS_Email2
        sms:EmailAddress3     = tra2:SMS_Email3
        sms:SendToSMS         = 'F'
        sms:SendToEmail       = 'T'
        sms:DateInserted      = today()
        sms:TimeInserted      = clock()
        sms:EmailSent         = 'NO'
        Case LocalSMSType

            of 'E'  !estimate
                Do SortOutTheEstimate
                !message('Back from sort out the estimate - sortouttext is '&clip(LocalSortOutText))
                if clip(LocalSortOutText) = 'OVERCOUNT'      !used as a flag
                    !do nothing
                    !message('Got overcount')
                    LocalSMSType = 'Z'      !so it does not show in the browse of SMS received
                ELSE
                    !changes will have been made
                    !message('Estimate - sending email')
                    Access:jobs.update()
                    sms:SMSType           = 'E'
                    sms:EmailSubject      = 'SMS RECEIVED ESTIMATE ON JOB NUMBER '&clip(job:Ref_number)
                    sms:MSG               = 'The following SMS has been received from a customer in response to Estimate<13,10>'&|
                                            clip(SMR:TextReceived)&'<13,10>'&clip(LocalSortOutText)&|
                                            'Job number :'&clip(job:Ref_number)&'<13,10>Receivied from:'&clip(SMR:MSISDN)
                END !if not over count
            of 'C'
                !message('CSI Type')
                Do CountTheCSI
                if LocalSortOutText = 'OVERCOUNT' then
                    !three or more already received - ignore everything
                    LocalSMSType = 'Z'      !so it does not show in the browse of SMS received
                END

                sms:SMSType           = 'C'
                sms:EmailSubject      = 'SMS RECEIVED CSI ON JOB NUMBER '&clip(job:Ref_number)
                sms:MSG               = 'The following SMS has been received from a customer in response to CSI<13,10>'&|
                                         clip(SMR:TextReceived)&'<13,10>'&|
                                        'Job number :'&clip(job:Ref_number)&'<13,10>Receivied from:'&clip(SMR:MSISDN)

            of 'N'  !normal
                !message('Normal type email')
                sms:SMSType           = 'N'
                sms:EmailSubject      = 'SMS RECEIVED ON JOB NUMBER '&clip(job:Ref_number)
                sms:MSG               = 'The following SMS has been received from a customer in response to previous texts<13,10>'&|
                                        clip(SMR:TextReceived)&'<13,10>'&|
                                        'Job number :'&cliP(job:Ref_number)&'<13,10>Receivied from:'&clip(SMR:MSISDN)

            of 'D'  !dupicate estimate

                Do CountTheEstimates
                if LocalSortOutText = 'OVERCOUNT' then
                    !message('Duplicate Overcount')
                    !three or more already received - ignore everything
                    LocalSMSType = 'Z'      !so it does not show in the browse of SMS received
                END

                sms:SMSType           = 'N'
                sms:EmailSubject      = 'SMS RECEIVED ON JOB NUMBER '&clip(job:Ref_number)
                sms:MSG               = 'The following SMS has been received from a customer in response to previous texts<13,10>'&|
                                        clip(SMR:TextReceived)&|
                                        '<13,10>This appears to be a duplicate estimate text and the relevant reply text has been sent<13,10>'&|
                                        'Job number :'&cliP(job:Ref_number)&'<13,10>Receivied from:'&clip(SMR:MSISDN)

            of 'X'  !cannot trace details
                sms:SMSType           = 'N'
                sms:EmailSubject      = 'SMS REPLY RECEIVED UNTRACKABLE ON JOB NUMBER '&clip(job:Ref_number)
                sms:MSG               = 'The following SMS has been received from a customer in response to previous texts<13,10>'&|
                                        clip(SMR:TextReceived)&'<13,10>'&|
                                        'It has not been possible to track this SMS to a specific job<13,10>Receivied from:'&clip(SMR:MSISDN)

            ELSE
                sms:SMSType           = 'N'
                sms:EmailSubject      = 'SMS REPLY RECEIVED  '&clip(job:Ref_number)
                sms:MSG               = 'The following SMS has been received from a customer in response to previous texts<13,10>'&|
                                        clip(SMR:TextReceived)&'<13,10>'&|
                                        'It has not been possible to track this SMS to a specific type<13,10>Receivied from:'&clip(SMR:MSISDN)

        END !Case SMR:SMSType

        if LocalSMSType = 'Z' then
            !send no mails
            Access:SMSMAIL.CancelAutoInc()
        ELSE
            Access:SmsMail.update()
        END

        !make a note of what changes need to be made
        SDQ:RecordNo = SMR:RecordNo
        SDQ:JobRefNo = job:Ref_number
        SDQ:AccountNumber = job:Account_Number
        SDQ:SMSType  = LocalSMSType
        Add(SMR_DoneQueue)

    END !loop through SMSRecvd by job number = 0

    !now to copy the details to the original received record
    Loop X = 1 to records(SMR_DoneQueue)
        get(SMR_DoneQueue,x)
        !copy the job ref number so it shows on the job
        Access:SMSRecvd.clearkey(SMR:KeyRecordNo)
        SMR:RecordNo = SDQ:RecordNo
        if access:SMSRecvd.fetch(SMR:KeyRecordNo)
            !error??
        ELSE
            SMR:Job_Ref_Number = SDQ:JobRefNo
            SMR:AccountNumber = SDQ:AccountNumber
            smr:SMSType = SDQ:SMSType
            Access:SMSRecvd.update()
        END
    END !loop through the queue

    EXIT

SortOutTheEstimate      Routine

    Do CountTheEstimates
    if LocalSortOutText = 'OVERCOUNT' then
        !message('Estimate overcount')
        EXIT
    END

    !Text will begin 'ACCEPT' (6Chars) or DECLINE (7 Chars) anything else cannot be sorted automatically
    CASE upper(SMR:TextReceived[1:6])
        of 'ACCEPT'
            Do AcceptEstimate
        of 'DECLIN'
            if upper(SMR:TextReceived[7]) = 'E' then
                Do DeclineEstimate
            ELSE
                LocalSortOutText = '<13,10>System cannot determine if this is ACCEPT or DECLINE, no action has been taken.<13,10>'
            END
        ELSE
            LocalSortOutText = '<13,10>System cannot determine if this is ACCEPT or DECLINE, no action has been taken.<13,10>'
    END !case textrecevied

    EXIT

AcceptEstimate  routine


    !SetTheJob needs webjob and jobse open
    Do OpenFiles

    case EstimatePartsInUse()

        of 0    !no problems

            SetTheJobStatus(535,'JOB') !estimate accepted
            !message('Accepted estimate - changed status to 535')
            !details will have changed
            Access:Webjob.update()

            job:COurier_Cost = job:Courier_Cost_Estimate

            ConvertEstimateParts()

            job:Labour_Cost = job:Labour_Cost_Estimate
            job:Parts_Cost  = job:Parts_Cost_Estimate
            job:Ignore_Chargeable_Charges = job:Ignore_Estimate_Charges
            Job:Estimate_Accepted = 'YES'

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                jobe:RRCCLabourCost = jobe:RRCELabourCost
                jobe:RRCCPartsCost  = jobe:RRCEPartsCost
                jobe:IgnoreRRCChaCosts = jobe:IgnoreRRCEstCosts
                Access:JOBSE.Update()
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        of 1    !Stock not found
           LocalSortOutText = '<13,10>Acceptance recognised but stock record not found. This needs sorting out manually. Estimate parts have not been allocated<13,10>'
        of 2    !stock record in use
           LocalSortOutText = '<13,10>Acceptance recognised but stock record currently in use. This needs sorting out manually. Estimate parts have not been allocated<13,10>'
    END !case EstimatePartsInUse()

    aud:notes      = 'COMMUNICATION METHOD: SMS Reply'&|
                     '<13,10>DETAILS: ' & SMR:TextReceived[ 7 : Len(clip(SMR:TextReceived)) ]
    aud:ref_number = job:ref_number
    aud:date       = Today()
    aud:time       = CLock()
    aud:user       = ''
    aud:action     = 'ESTIMATE ACCEPTED FROM: ' & Clip(SMR:MSISDN)
    aud:Type       = 'JOB'
    access:audit.insert()

    EXIT

DeclineEstimate    Routine

    !SetTheJob needs webjob and jobse open
    Do OpenFiles

    SetTheJobStatus(540,'JOB') !estimate refused
    !message('Declined - status changed to 540')
    Access:webjob.update()

    job:Estimate_Rejected = 'YES'

    aud:notes      = 'COMMUNICATION METHOD: SMS Reply '& |
                     '<13,10>REASON: ' & SMR:TextReceived[ 8 : Len(clip(SMR:TextReceived)) ]
    aud:ref_number = job:ref_number
    aud:date       = Today()
    aud:time       = CLock()
    aud:user       = ''
    aud:action     = 'ESTIMATE REJECTED FROM: ' & Clip(SMR:MSISDN)
    aud:Type       = 'JOB'
    access:audit.insert()

    EXIT

CountTheEstimates       Routine

    !Message('In count of estimates job number '&clip(Job:Ref_number))
    !if we have recived more than three  sms about estimates then ignore any new ones
    X = 0
    Access:SMSRECVD_ALIAS.clearkey(SMR1:KeyJob_Ref_Number)
    SMR1:Job_Ref_Number = job:Ref_Number
    Set(SMR1:KeyJob_Ref_Number,SMR1:KeyJob_Ref_Number)
    Loop
        if access:SMSRECVD_ALIAS.next() then break.
        if SMR1:Job_Ref_Number <> job:Ref_Number then break.
        !Message('In the loop at record no '&clip(SMR1:RecordNo))
        if SMR1:SMSType = 'E' or SMR1:SMSType = 'D' then
            !estimate or duplicate estimate
            X += 1
        END

    END

    !Message('Out of loop with count = '&X&'|Have to check queue of '&records(SMR_DoneQueue))
    !now to check if there are any for this job in the queue
    loop y = 1 to records(SMR_DoneQueue)
        get(SMR_DoneQueue,Y)
        !copy the job ref number so it shows on the job
        !Message('Found line with type '& SDQ:SMSType)
        if  SDQ:JobRefNo = job:Ref_Number   !same job
            if SDQ:SMSType = 'E' or SDQ:SMSType = 'D' then
                !estimate or duplicate estimate
                X += 1
            END
        END
    END !loop through the queue
    !Message('out of queue with count now '&clip(x))
    If X > 2 then
        !three or more
        !Message('Three or more found')
        LocalSortOutText = 'OVERCOUNT'      !used as a flag
    END

    EXIT


CountTheCSI             Routine
    X = 0
    Access:SMSRECVD_ALIAS.clearkey(SMR1:KeyJob_Ref_Number)
    SMR1:Job_Ref_Number = job:Ref_Number
    Set(SMR1:KeyJob_Ref_Number,SMR1:KeyJob_Ref_Number)
    Loop
        if access:SMSRECVD_ALIAS.next() then break.
        if SMR1:Job_Ref_Number <> job:Ref_Number then break.
        !Message('In the loop at record no '&clip(SMR1:RecordNo))
        if SMR1:SMSType = 'C'  then
            !CSI Reply
            X += 1
        END

    END

    !now to check if there are any for this job in the queue
    loop y = 1 to records(SMR_DoneQueue)
        get(SMR_DoneQueue,Y)
        !copy the job ref number so it shows on the job
        if  SDQ:JobRefNo = job:Ref_Number   !same job
            if SDQ:SMSType = 'C' then
                !CSI reply
                X += 1
            END
        END
    END !loop through the queue

    If X > 2 then
        !three or more
        !Message('Three or more found')
        LocalSortOutText = 'OVERCOUNT'      !used as a flag
    END

    EXIT


OpenFiles       Routine

    !open webjob and jobse at the right place for an update
    Access:Webjob.clearkey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    if Access:Webjob.fetch(wob:RefNumberKey)
        !error
    END

    Access:jobse.clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    if access:jobse.fetch(jobe:RefNumberKey)
        !error
    END


    EXIT
   Relate:SMSMAIL.Close
   Relate:JOBS.Close
   Relate:SMSRECVD.Close
   Relate:TRADEACC.Close
   Relate:WEBJOB.Close
   Relate:AUDIT.Close
   Relate:SMSRECVD_ALIAS.Close
   Relate:TRADEAC2.Close
   Relate:JOBSE.Close

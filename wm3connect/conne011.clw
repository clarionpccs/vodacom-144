

   MEMBER('connect.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CONNE011.INC'),ONCE        !Local module procedure declarations
                     END


UpdateServer PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::SRV:Record  LIKE(SRV:RECORD),STATIC
QuickWindow          WINDOW('Update the Server File'),AT(,,252,126),FONT('Arial',8,,,CHARSET:ANSI),COLOR(COLOR:White),CENTER,IMM,HLP('UpdateServer'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,244,120),USE(?CurrentTab),SPREAD
                         TAB('Server Details'),USE(?Tab:1)
                           PROMPT('Record No:'),AT(8,20),USE(?SRV:RecordNo:Prompt)
                           ENTRY(@n-14),AT(80,20,64,10),USE(SRV:RecordNo),SKIP,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('IP Addrress:'),AT(8,34),USE(?SRV:IPAddrress:Prompt)
                           ENTRY(@s20),AT(80,34,84,10),USE(SRV:IPAddrress),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Port:'),AT(8,48),USE(?SRV:Port:Prompt)
                           ENTRY(@n-7),AT(80,48,40,10),USE(SRV:Port),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(68,60,172,16),USE(SRV:MainServerFlag)
                             RADIO('Yes'),AT(80,63),USE(?SRV:MainServerFlag:Radio1),TRN,VALUE('Y')
                             RADIO('No'),AT(139,63),USE(?SRV:MainServerFlag:Radio2),TRN,VALUE('N')
                           END
                           STRING('Main Server'),AT(8,63),USE(?String1)
                           PROMPT('Program Path:'),AT(8,76),USE(?SRV:ProgramPath:Prompt)
                           ENTRY(@s40),AT(80,76,164,10),USE(SRV:ProgramPath),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Priority:'),AT(8,90),USE(?SRV:Priority:Prompt)
                           ENTRY(@n-14),AT(80,90,64,10),USE(SRV:Priority),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('OK'),AT(132,104,45,14),USE(?OK),FLAT,LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(184,104,56,16),USE(?Cancel),FLAT,LEFT,ICON('cancel.gif')
                       BUTTON('Help'),AT(12,104,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a Server Record'
  OF ChangeRecord
    ActionMessage = 'Changing a Server Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateServer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SRV:RecordNo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(SRV:Record,History::SRV:Record)
  SELF.AddHistoryField(?SRV:RecordNo,1)
  SELF.AddHistoryField(?SRV:IPAddrress,2)
  SELF.AddHistoryField(?SRV:Port,3)
  SELF.AddHistoryField(?SRV:MainServerFlag,4)
  SELF.AddHistoryField(?SRV:ProgramPath,5)
  SELF.AddHistoryField(?SRV:Priority,6)
  SELF.AddUpdateFile(Access:Server)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:Server.Open
  Relate:ServerAlias.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Server
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Server.Close
    Relate:ServerAlias.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?SRV:MainServerFlag
      !check we have only one Main Server
      if SRV:MainServerFlag = 'Y'
          access:ServerAlias.clearkey(SRA:KeyRecordNo)
          SRA:RecordNo = 1
          set(SRA:KeyRecordNo,SRA:KeyRecordNo)
          loop
              if Access:ServerAlias.next() then break.
              if SRA:MainServerFlag = 'Y' then
                  if SRV:RecordNo <> SRA:RecordNo
                      Message('There can only be one main server|A record already exists as main server|you must deselect that one first.','ServiceBase 2000')
                      SRV:MainServerFlag = 'N'
                      break
                  ENd !Same recrod
              END !If mainserver
          END !Loop
      end !If flag set to 'Y'
      
      display()
    OF ?SRV:Priority
      !check we have only one Server this priority
      access:serverAlias.clearkey(SRA:KeyRecordNo)
      SRA:RecordNo = 1
      set(SRA:KeyRecordNo,SRA:KeyRecordNo)
      Loop
          If access:ServerAlias.next() then break.
          if SRA:Priority = SRV:Priority
              if SRA:RecordNo <> SRV:RecordNo
                  Message('There can only be one server at any priority|A record already exists as this priority|You must change that one first.','ServiceBase 2000')
                  SRV:Priority = 0
                  break
              END !same record
          END !matching priorities
      END !Loop
      
      !thisform.update
      display()
    OF ?OK
      !final checks
      error"= ''
      if SRV:IPAddrress = '' then error"= 'IP Address missing'.
      if SRV:Port = 0 then error" = 'Port missing'.
      if SRV:Priority = 0 then error" = 'Prioity missing'.
      
      !check we have only one Main Server
      if error" = '' and SRV:MainServerFlag = 'Y'
          access:ServerAlias.clearkey(SRA:KeyRecordNo)
          SRA:RecordNo = 1
          set(SRA:KeyRecordNo,SRA:KeyRecordNo)
          loop
              if Access:ServerAlias.next() then break.
              if SRA:MainServerFlag = 'Y' then
                  if SRV:RecordNo <> SRA:RecordNo
                      Error" = 'There can only be one main server|A record already exists as main server|you must deselect that one first.'
                      SRV:MainServerFlag = 'N'
                      break
                  ENd !Same recrod
              END !If mainserver
              if SRV:Priority = SRA:Priority
                  if SRV:RecordNo <> SRA:RecordNo
                      Error" = 'There is already a server with this priority.|You must change that one first'
                  END !same record
              END
          END !Loop
      end !If flag set to 'Y'
      
      if error" <> '' then
          message(clip(error"),'ServiceBase 2000')
          cycle
      END !if error" exists
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


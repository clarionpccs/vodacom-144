

   MEMBER('vodr0089.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABEIP.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


SentToARCReport PROCEDURE                                  ! Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::17:TAGFLAG         BYTE(0)
DASBRW::17:TAGMOUSE        BYTE(0)
DASBRW::17:TAGDISPSTATUS   BYTE(0)
DASBRW::17:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::20:TAGFLAG         BYTE(0)
DASBRW::20:TAGMOUSE        BYTE(0)
DASBRW::20:TAGDISPSTATUS   BYTE(0)
DASBRW::20:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)                            !Version Number
save_locatlog_id     USHORT,AUTO                           !
tmp:Clipboard        ANY                                   !
StatusQueue          QUEUE,PRE(staque)                     !
StatusMessage        STRING(60)                            !Status Message
                     END                                   !
TempFilePath         CSTRING(255)                          !
tmp:StartDate        DATE                                  !Jobs Completed From
tmp:EndDate          DATE                                  !Jobs Completed To
tmp:UserName         STRING(70)                            !User Name
tmp:HeadAccountNumber STRING(30)                           !Head Account Number
tmp:ARCLocation      STRING(30)                            !ARC Location
Progress:Thermometer BYTE(0)                               !Moving Bar
tmp:ARCAccount       BYTE(0)                               !Reporting on ARC?
tmp:ARCCompanyName   STRING(30)                            !Company Name
tmp:ARCUser          BYTE(0)                               !ARC User
tmp:Tag              STRING(1)                             !
tmp:TagMan           STRING(1)                             !
tmp:TagMod           STRING(1)                             !
tmp:Blank            STRING(1)                             !
tmp:SelectManufacturer BYTE(0)                             !Select Manufacturer
tmp:Manufacturer     STRING(30)                            !Manufacturer
SummaryQueue         QUEUE,PRE(que)                        !
AccountNumber        STRING(30)                            !Account Number
AccountName          STRING(30)                            !Account Name
Booked               LONG                                  !Booked
SentToARC            LONG                                  !Sent To ARC
Exclusions           LONG                                  !Exclusions
BookedLessExclusions LONG                                  !Booked Less Exclusions
SentLessExclusions   LONG                                  !Sent Less Exclusions
                     END                                   !
BRW12::View:Browse   VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW16::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:TagMan             LIKE(tmp:TagMan)               !List box control field - type derived from local data
tmp:TagMan_Icon        LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW18::View:Browse   VIEW(MODELPART)
                       PROJECT(model:ModelNumber)
                       PROJECT(model:PartNumber1)
                       PROJECT(model:PartNumber2)
                       PROJECT(model:PartNumber3)
                       PROJECT(model:PartNumber4)
                       PROJECT(model:PartNumber5)
                       PROJECT(model:Manufacturer)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:TagMod             LIKE(tmp:TagMod)               !List box control field - type derived from local data
tmp:TagMod_Icon        LONG                           !Entry's icon ID
model:ModelNumber      LIKE(model:ModelNumber)        !List box control field - type derived from field
model:PartNumber1      LIKE(model:PartNumber1)        !List box control field - type derived from field
model:PartNumber2      LIKE(model:PartNumber2)        !List box control field - type derived from field
model:PartNumber3      LIKE(model:PartNumber3)        !List box control field - type derived from field
model:PartNumber4      LIKE(model:PartNumber4)        !List box control field - type derived from field
model:PartNumber5      LIKE(model:PartNumber5)        !List box control field - type derived from field
tmp:Blank              LIKE(tmp:Blank)                !List box control field - type derived from local data
model:Manufacturer     LIKE(model:Manufacturer)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB21::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Sent To ARC Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,57,552,308),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Received At ARC Start Date'),AT(238,74),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(350,74,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           PROMPT('Recevied At ARC End Date'),AT(238,95),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(350,95,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           PROMPT(''),AT(238,106),USE(?StatusText)
                           PROMPT('Accounts'),AT(209,114),USE(?Prompt8),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(210,122,264,188),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)|J@s1@69L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(481,174),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(241,174,1,1),USE(?DASREVTAG),HIDE
                           BUTTON,AT(481,218),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('sho&W tags'),AT(265,215,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(481,260),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON,AT(419,71),USE(?Calendar),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(419,90),USE(?Calendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('Manufacturers'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manufacturers'),AT(252,119),USE(?Prompt9),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select which manufacturers to exclude.'),AT(255,77),USE(?Prompt10),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('If you wish to exclude ALL Model Numbers for a selected Manufacturer, select the' &|
   ' Manufacturer(s) you require below, but do NOT select any Model Numbers from the' &|
   ' next screen.'),AT(94,95,492,20),USE(?Prompt11),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(252,129,172,176),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)|J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(436,180),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(312,178,1,1),USE(?DASREVTAG:2),HIDE
                           BUTTON,AT(436,225),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('sho&W tags'),AT(304,207,1,1),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(436,269),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Model Numbers'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select which Model Numbers / Part Numbers to exclude.'),AT(219,74),USE(?Prompt13),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select Manufacturer'),AT(69,90),USE(tmp:SelectManufacturer),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select Manufacturer'),TIP('Select Manufacturer'),VALUE('1','0')
                           COMBO(@s30),AT(169,90,124,10),USE(tmp:Manufacturer),IMM,HIDE,VSCROLL,FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Model Numbers'),AT(69,106),USE(?Prompt12),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(137,106,64,10),USE(model:ModelNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Model Number')
                           PROMPT('(Double click on entry to insert/edit Part Number(s)'),AT(336,106),USE(?Prompt14),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('Edit Part Number'),AT(333,162,42,12),USE(?Change),HIDE
                           BUTTON('&Rev tags'),AT(153,202,1,1),USE(?DASREVTAG:3),HIDE
                           BUTTON,AT(545,286),USE(?DASUNTAGALL:3),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON('sho&W tags'),AT(161,231,1,1),USE(?DASSHOWTAG:3),HIDE
                           LIST,AT(69,119,544,158),USE(?List:3),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('12C|J@s1@98L|_M~Model Number~C@s30@98L(2)|_M~Part Number 1~@s30@86L(2)|_M~Part N' &|
   'umber 2~@s30@86L(2)|_M~Part Number 3~@s30@86L(2)|_M~Part Number 4~@s30@120L(2)|_' &|
   'M~Part Number 5~@s30@86L(2)|_M~Something else~@s30@'),FROM(Queue:Browse:2)
                           BUTTON,AT(405,286),USE(?DASTAG:3),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(477,286),USE(?DASTAGAll:3),TRN,FLAT,ICON('tagallp.jpg')
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(68,366),USE(?VSBackButton),TRN,FLAT,HIDE,LEFT,ICON('backp.jpg')
                       BUTTON,AT(136,366),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       PROMPT('Report Version'),AT(68,348),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Ensure Excel is NOT running before you begin!'),AT(238,374,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
WriteLine               Procedure()
            End ! local       Class
tmp:TempFilePath    CString(255)

!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                       String(2000)
                        End
                    End
ARCExportFile    File,Driver('BASIC'),Pre(arcexp),Name(glo:ARCExportFile),Create,Bindable,Thread
Record                  Record

                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
progressThermometer Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.UserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***
Wizard14         CLASS(FormWizardClass)                    ! FormWizardButtonClass (ABC Free)
TakeNewSelection        PROCEDURE,VIRTUAL                  ! FormWizardButtonClass (ABC Free)
TakeBackEmbed           PROCEDURE,VIRTUAL                  ! FormWizardButtonClass (ABC Free)
TakeNextEmbed           PROCEDURE,VIRTUAL                  ! FormWizardButtonClass (ABC Free)
Validate                PROCEDURE(),LONG,VIRTUAL           ! FormWizardButtonClass (ABC Free)
                   END                                     ! FormWizardButtonClass (ABC Free)
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW12                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW12::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW16                CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW16::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW18                CLASS(BrowseClass)                    ! Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM) ! Method added to host embed code
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW18::Sort0:Locator EntryLocatorClass                     ! Default Locator
BRW18::Sort1:Locator EntryLocatorClass                     ! Conditional Locator - tmp:SelectManufacturer = 1
BRW18::EIPManager    BrowseEIPManager                      ! Browse EIP Manager for Browse using ?List:3
EditInPlace::tmp:TagMod EditEntryClass                     ! Edit-in-place class for field tmp:TagMod
EditInPlace::model:ModelNumber EditEntryClass              ! Edit-in-place class for field model:ModelNumber
EditInPlace::model:PartNumber1 EditEntryClass              ! Edit-in-place class for field model:PartNumber1
EditInPlace::model:PartNumber2 EditEntryClass              ! Edit-in-place class for field model:PartNumber2
EditInPlace::model:PartNumber3 EditEntryClass              ! Edit-in-place class for field model:PartNumber3
EditInPlace::model:PartNumber4 EditEntryClass              ! Edit-in-place class for field model:PartNumber4
EditInPlace::model:PartNumber5 EditEntryClass              ! Edit-in-place class for field model:PartNumber5
EditInPlace::tmp:Blank EditEntryClass                      ! Edit-in-place class for field tmp:Blank
FDCB21               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED ! Method added to host embed code
                     END

Calendar15           CalendarClass
Calendar24           CalendarClass
MyProg PW ! ProgressBarLocal(BryanTemplate)

MyProg:ProgressWindow      WINDOW('Progress...'),AT(,,210,64),CENTER,FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),DOUBLE
                        PROGRESS, AT(17,15,175,12), USE(?MyProg:ProgressThermometer), RANGE(0,100),SMOOTH
                        STRING(''), AT(0,3,211,10), USE(?MyProg:UserString), CENTER
                        STRING(''), AT(0,30,208,10), USE(?MyProg:PercentString), CENTER
                        BUTTON('Cancel'), AT(80,44,50,15), USE(?MyProg:ProgressCancel)
                        END

    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW12.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::17:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW16.UpdateBuffer
   glo:Queue2.Pointer2 = man:Manufacturer
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:TagMan = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:TagMan = ''
  END
    Queue:Browse:1.tmp:TagMan = tmp:TagMan
  IF (tmp:TagMan = '*')
    Queue:Browse:1.tmp:TagMan_Icon = 2
  ELSE
    Queue:Browse:1.tmp:TagMan_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::17:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW16.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW16::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::17:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW16.Reset
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::17:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::17:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::17:QUEUE = glo:Queue2
    ADD(DASBRW::17:QUEUE)
  END
  FREE(glo:Queue2)
  BRW16.Reset
  LOOP
    NEXT(BRW16::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::17:QUEUE.Pointer2 = man:Manufacturer
     GET(DASBRW::17:QUEUE,DASBRW::17:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = man:Manufacturer
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::17:DASSHOWTAG Routine
   CASE DASBRW::17:TAGDISPSTATUS
   OF 0
      DASBRW::17:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::17:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::17:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW16.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::20:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW18.UpdateBuffer
   glo:Queue3.Pointer3 = model:ModelNumber
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = model:ModelNumber
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:TagMod = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:TagMod = ''
  END
    Queue:Browse:2.tmp:TagMod = tmp:TagMod
  IF (tmp:TagMod = '*')
    Queue:Browse:2.tmp:TagMod_Icon = 2
  ELSE
    Queue:Browse:2.tmp:TagMod_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::20:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW18.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW18::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = model:ModelNumber
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW18.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::20:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW18.Reset
  SETCURSOR
  BRW18.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::20:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::20:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::20:QUEUE = glo:Queue3
    ADD(DASBRW::20:QUEUE)
  END
  FREE(glo:Queue3)
  BRW18.Reset
  LOOP
    NEXT(BRW18::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::20:QUEUE.Pointer3 = model:ModelNumber
     GET(DASBRW::20:QUEUE,DASBRW::20:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = model:ModelNumber
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW18.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::20:DASSHOWTAG Routine
   CASE DASBRW::20:TAGDISPSTATUS
   OF 0
      DASBRW::20:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::20:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::20:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW18.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:RecordCount       Long()
local:IMEINumber        String(30)
local:HeadAccountNumber String(30)
local:BounceDate       Date()
local:RefNumber         Long()
local:Desktop           Cstring(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'Sent To ARC Report'
    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & '','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & '','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot get access to the selected document:'&|
                '|'&|
                '|' & Clip(excel:Filename) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Calculating Booked Jobs...')

    Loop x# = 1 To Records(glo:Queue)
        Get(glo:Queue,x#)

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        Count# = 0
        Hub# = 0
        Access:WEBJOB.ClearKey(wob:DateBookedKey)
        wob:HeadAccountNumber = glo:Pointer
        wob:DateBooked        = tmp:StartDate
        Set(wob:DateBookedKey,wob:DateBookedKey)
        Loop
            If Access:WEBJOB.NEXT()
               Break
            End !If
            If wob:HeadAccountNumber <> glo:Pointer      |
            Or wob:DateBooked        > tmp:EndDate       |
                Then Break.  ! End If
            ! Inserting (DBH 27/09/2007) # 9370 - Don't count VCP jobs here
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = wob:RefNumber
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
                If job:Who_Booked = 'WEB'
                    Cycle
                End ! If job:Who_Booked <> 'WEB'
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            ! End (DBH 27/09/2007) #9370

!            !TB13273 - J - 24/04/14 - don't count the cancelled jobs
!            if job:Cancelled = 'YES' then cycle.
!            !Commented until this is approved by Vodacom

            ! DBH #10544 - Exclude Liquid Damage Jobs
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                IF (jobe:Booking48HourOption = 4)
                    CYCLE
                END
            END


            Count# += 1
            If SentToHub(wob:RefNumber)
                Hub# += 1
            End ! If SentToHub(job:Ref_Number) = False
        End !Loop

        que:AccountNumber = glo:Pointer

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = que:AccountNumber
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            que:AccountName = tra:Company_Name
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        que:Booked = Count#
        que:SentToARC = Hub#
        Add(SummaryQueue)
        local.UpdateProgressWindow(Clip(que:AccountNumber) & ' - ' & Clip(que:AccountName) & ': ' & Count#)
    End ! Loop x# = 1 To Records(glo:Queue)

    ! Inserting (DBH 27/09/2007) # Count VCP jobs by the time they were RECEIVED at the Franchise - 9370
    local.UpdateProgressWindow('Calculating "Booked" VCP Jobs...')
    Access:LOCATLOG.Clearkey(lot:DateNewLocationKey)
    lot:NewLocation = 'AT FRANCHISE'
    lot:TheDate = tmp:StartDate
    Set(lot:DateNewLocationKey,lot:DateNewLocationKey)
    Loop ! Begin Loop
        If Access:LOCATLOG.Next()
            Break
        End ! If Access:LOCATLOG.Next()
        If lot:NewLocation <> 'AT FRANCHISE'
            Break
        End ! If lot:NewLocation <> 'AT FRANCHISE'
        If lot:TheDate > tmp:EndDate
            Break
        End ! If lot:TheDate > tmp:EndDate
        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        ! Only count VCP jobs. (DBH: 27/09/2007)
        If lot:PreviousLocation <> 'IN TRANSIT FROM PUP'
            Cycle
        End ! If lot:PreviousLocation <> 'IN TRANSIT FROM PUP'
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = lot:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
            If job:Who_Booked <> 'WEB'
                Cycle
            End ! If job:Who_Booked <> 'WEB'

            ! DBH #10544 - Exclude Liquid Damage Jobs
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                IF (jobe:Booking48HourOption = 4)
                    CYCLE
                END
            END

        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        glo:Pointer = wob:HeadAccountNumber
        Get(glo:Queue,glo:Pointer)
        If Error()
            Cycle
        End ! If Error()
        que:AccountNumber = wob:HeadAccountNumber
        Get(SummaryQueue,que:AccountNumber)
        If Error()
            que:AccountNumber = wob:HeadAccountNumber

            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = que:AccountNumber
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                que:AccountName = tra:Company_Name
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

            que:Booked = 1
            If SentToHub(job:Ref_Number)
                que:SentToARC = 1
            End ! If SentToHub(job:Ref_Number)
            Add(SummaryQueue)
        Else ! If Error()
            que:Booked += 1
            If SentToHub(job:Ref_Number)
                que:SentToARC += 1
            End ! If SentToHub(job:Ref_Number)
            Put(SummaryQueue)
        End ! If Error()
    End ! Loop
    ! End (DBH 27/09/2007) #Count VCP jobs by the time they were RECEIVED at the Franchise


    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Calculating "Sent To ARC" Jobs...')

    glo:ExportFile = Clip(local:LocalPath) & 'ARC' & Clock() & '.CSV'
    Remove(glo:ExportFile)
    Create(ExportFile)
    Open(ExportFile)
    Access:LOCATLOG.ClearKey(lot:DateNewLocationKey)
    lot:NewLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
    lot:TheDate     = tmp:StartDate
    Set(lot:DateNewLocationKey,lot:DateNewLocationKey)
    Loop
        If Access:LOCATLOG.NEXT()
           Break
        End !If
        If lot:NewLocation <> GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')      |
        Or lot:TheDate     > tmp:EndDate       |
            Then Break.  ! End If

        ! Inserting (DBH 03/08/2006) # N/A - Don't count if the unit has come back from 3rd Party
        If lot:PreviousLocation = GETINI('RRC','RTMLocation',,Clip(Path()) & '\SB2KDEF.INI')
            Cycle
        End ! If lot:PreviousLocation = GETINI('RRC','RTMLocation',,Clip(Path()) & '\SB2KDEF.INI')
        ! End (DBH 03/08/2006) #N/A

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = lot:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
            !DBH #10544 - Exclude Liquid Damage Jobs
            IF (jobe:Booking48HourOption = 4)
                CYCLE
            END
        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

        ! Criteria____________________________________________________________________________________

        If job:Date_Completed = ''
            Cycle
        End ! If job:Date_Completed = ''


        ! Has this account been selected? (DBH: 26/07/2006)
        glo:Pointer = wob:HeadAccountNumber
        Get(glo:Queue,glo:Pointer)
        If Error()
            Cycle
        End ! If Error()

        ! Exclude whole manufacturer (DBH: 26/07/2006)
        glo:Pointer2 = job:Manufacturer
        Get(glo:Queue2,glo:Pointer2)
        If ~Error()
            Cycle
        End ! If ~Error()

        glo:Pointer3 = job:Model_Number
        Get(glo:Queue3,glo:Pointer3)
        If ~Error()
            Access:MODELPART.ClearKey(model:ModelNumberKey)
            model:ModelNumber = job:Model_Number
            If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
                !Found
                If Clip(model:PartNumber1) = '' And Clip(model:PartNumber2) = '' And Clip(model:PartNumber3) = '' And Clip(model:PartNumber4) = '' And Clip(model:PartNumber5) = ''
                    Cycle
                Else ! If Clip(model:PartNumber1) = '' And Clip(model:PartNumber2) = '' And Clip(model:PartNumber3) = ''
                    ! Check the parts on the job (DBH: 26/07/2006)
                    FoundPart# = False
                    If job:Chargeable_Job = 'YES'
                        Access:PARTS.ClearKey(par:Part_Number_Key)
                        par:Ref_Number  = job:Ref_Number
                        Set(par:Part_Number_Key,par:Part_Number_Key)
                        Loop
                            If Access:PARTS.NEXT()
                               Break
                            End !If
                            If par:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            Access:STOCK.ClearKey(sto:Ref_Number_Key)
                            sto:Ref_Number = par:Part_Ref_Number
                            If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                ! Was the part attached by the ARC and matching one of the exclusions? (DBH: 26/07/2006)
                                If sto:Location = 'AA20 ADVANCED REPAIR CENTRE'
                                    If sto:Part_Number = model:PartNumber1 Or |
                                        sto:Part_Number = model:PartNumber2 Or |
                                        sto:Part_Number = model:PartNumber3 Or |
                                        sto:Part_Number = model:PartNumber4 Or |
                                        sto:Part_Number = model:PartNumber5
                                        FoundPart# = True
                                        Break
                                    End ! sto:Part_Number = model:PartNumber3
                                End ! If sto:Location = 'AA20 ADVANCED REPAIR CENTRE'
                            Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                            End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        End !Loop
                    End ! If job:Chargeable_Job = 'YES'

                    If job:Warranty_Job = 'YES' And FoundPart# = False
                        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                        wpr:Ref_Number  = job:Ref_Number
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.NEXT()
                               Break
                            End !If
                            If wpr:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            Access:STOCK.ClearKey(sto:Ref_Number_Key)
                            sto:Ref_Number = wpr:Part_Ref_Number
                            If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                ! Was the part attached by the ARC and matching one of the exclusions? (DBH: 26/07/2006)
                                If sto:Location = 'AA20 ADVANCED REPAIR CENTRE'
                                    If sto:Part_Number = model:PartNumber1 Or |
                                        sto:Part_Number = model:PartNumber2 Or |
                                        sto:Part_Number = model:PartNumber3 Or |
                                        sto:Part_Number = model:PartNumber4 Or |
                                        sto:Part_Number = model:PartNumber5
                                        FoundPart# = True
                                        Break
                                    End ! sto:Part_Number = model:PartNumber3
                                End ! If sto:Location = 'AA20 ADVANCED REPAIR CENTRE'
                            Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                            End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        End !Loop
                    End ! If job:Chargeable_Job = 'YES'

                    If FoundPart# = True
                        Cycle
                    End ! If FoundPart# = True
                End ! If Clip(model:PartNumber1) = '' And Clip(model:PartNumber2) = '' And Clip(model:PartNumber3) = ''
            Else ! If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
                !Error
            End ! If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
        End ! If ~Error()

        ! Don't count OBF validated jobs (DBH: 26/07/2006)
        If jobe:OBFvalidated
            Cycle
        End ! If jobe:OBFvalidated

        If job:Warranty_Job = 'YES' And job:Warranty_Charge_Type = 'WARRANTY (OBF)'
            Cycle
        End ! If job:Warranty_Job = 'YES' And job:Warranty_Charge_Type = 'WARRANTY (OBF)'

        ! Don't count jobs exchanged at the RRC, unless it's a 48 hour one (DBH: 26/07/2006)
        If job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC = True And jobe:Engineer48HourOption <> 1
            Cycle
        End ! If job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC = True

        !Don't count jobs where the model number is excluded from RRC Repair
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = job:Model_Number
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            If mod:ExcludedRRCRepair = True
                Cycle
            End ! If mod:ExcludedRRCRepair = True
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign

        ! Check for bouncers (DBH: 26/07/2006)
        local:IMEINumber = job:ESN
        local:HeadAccountNumber = wob:HeadAccountNumber
        local:RefNumber         = job:Ref_Number
        local:BounceDate        = job:Date_Booked

        Found# = False
        Access:JOBS.Clearkey(job:ESN_Key)
        job:ESN = local:IMEINumber
        Set(job:ESN_Key,job:ESN_Key)
        Loop ! Begin Loop
            If Access:JOBS.Next()
                Break
            End ! If Access:JOBS.Next()
            If job:ESN <> local:IMEINumber
                Break
            End ! If job:ESN <> local:IMEINumber
            If job:Ref_Number => local:RefNumber
                Cycle
            End ! If job:Ref_Number <> local:JobNumber

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
                !DBH #10544 - Exclude Liquid Damage Jobs
                IF (jobe:Booking48HourOption = 4)
                    CYCLE
                END
            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

            If wob:HeadAccountNumber = local:HeadAccountNumber
                ! Job was previous booked by same RRC (DBH: 26/07/2006)
                Save_LOCATLOG_ID = Access:LOCATLOG.SaveFile()
                Hub# = SentToHub(job:Ref_Number)
                Access:LOCATLOG.RestoreFile(Save_LOCATLOG_ID)
                If Hub# = False
                    Cycle
                End ! If Hub# = False
            Else ! If wob:HeadAccountNumber = local:HeadAccountNumber
                If jobe:WebJob = True
                    ! Original job was booked by another RRC (DBH: 26/07/2006)
                    Found# = True
                    Break
                Else
                    ! Original job was booked by the ARC. Was it within the last 3 months? (DBH: 26/07/2006)
                End ! If jobe:WebJob = True
            End ! If wob:HeadAccountNumber = local:HeadAccountNumber

            ! Look for the despatch date (assume, depsatch to customer) (DBH: 31/07/2006)
            Access:JOBSCONS.ClearKey(joc:DateKey)
            joc:RefNumber = job:Ref_Number
            Set(joc:DateKey,joc:DateKey)
            Loop
                If Access:JOBSCONS.NEXT()
                   Break
                End !If
                If joc:RefNumber <> job:Ref_Number      |
                    Then Break.  ! End If
                If joc:DespatchTo = 'CUSTOMER'
                    If joc:TheDate > local:BounceDate - 90
                        Found# = True
                        Break
                    End ! If joc:TheDate > loc:BouncerDate - 90
                End ! If joc:DespatchTo = 'CUSTOMER'
            End !Loop
            If Found# = True
                Break
            End ! If Found# = True
        End ! Loop

        If Found# = True
            Cycle
        End ! If Found# = True

        ! Reget the current record (DBH: 26/07/2006)
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = lot:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
            !DBH #10544 - Exclude Liquid Damage Jobs
            IF (jobe:Booking48HourOption = 4)
                CYCLE
            END
        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

        que:AccountNumber = wob:HeadAccountNumber
        Get(SummaryQueue,que:AccountNumber)
        If ~Error()
            que:BookedLessExclusions += 1
            Put(SummaryQueue)
        End ! If ~Error()

        If jobe:HubRepair = False
            Cycle
        End ! If jobe:HubRepair = False

        ! Only count jobs that have been sent to the ARC (DBH: 26/07/2006)
        Save_LOCATLOG_ID = Access:LOCATLOG.SaveFile()
        Hub# = SentToHub(job:Ref_Number)
        Access:LOCATLOG.RestoreFile(Save_LOCATLOG_ID)
        If Hub# = False
            Cycle
        End ! If Hub# = False

        Local.WriteLine()
        local:RecordCount += 1

        ! Update the summary (DBH: 31/07/2006)
        que:AccountNumber = wob:HeadAccountNumber
        Get(SummaryQueue,que:AccountNumber)
        If ~Error()
            que:SentLessExclusions += 1
            Put(SummaryQueue)
        End ! If ~Error()
    End !Loop TRADEACC
    Close(ExportFile)

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding your Excel document.'&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If E1.Init(0,0,1) = 0

        SetClipBoard('')
        E1.OpenWorkBook(glo:ExportFile)
        E1.Copy('A1','R' & local:RecordCount)
        tmp:Clipboard = ClipBoard()
        SetClipBoard('Blank')
        E1.CloseWorkBook(3)

        E1.NewWorkBook()
        E1.SelectCells('A12')
        SetClipBoard(tmp:ClipBoard)
        E1.Paste()
        SetClipBoard('Blank')
        E1.RenameWorkSheet('Sent To ARC')

        Do DrawDetailTitle

        local.DrawBox('A9','Q9','A9','Q9',color:Silver)
        local.DrawBox('A11','Q11','A11','Q11',color:Silver)

        E1.WriteToCell('Section Name','A9')
        E1.WriteToCell('Sent To ARC Report','B9')
        E1.WriteToCell('Total Records','E9')
        E1.WriteToCell(local:RecordCount,'F9')
        E1.WriteToCell('Showing','G9')
        E1.WriteToCell('=SUBTOTAL(2,A12:A' & local:RecordCount + 12,'H9')

        E1.WriteToCell('SB Job Number','A11')
        E1.WriteToCell('Franchise Branch Number','B11')
        E1.WriteToCell('Franchise Job Number','C11')
        E1.WriteToCell('Head Account No','D11')
        E1.WriteToCell('Head Account Name','E11')
        E1.WriteToCell('Chargeable Charge Type','F11')
        E1.WriteToCell('Warranty Charge Type','G11')
        E1.WriteToCell('Action Date','H11')
        E1.WriteToCell('Waybill Number','I11')
        E1.WriteToCell('Make','J11')
        E1.WriteToCell('Model Number','K11')
        E1.WriteToCell('IMEI','L11')
        E1.WriteToCell('MSN','M11')
        E1.WriteToCell('Date Received At ARC','N11')
        E1.WriteToCell('Date Completed At ARC','O11')
        E1.WriteToCell('Repair Type','P11')
        E1.WriteToCell('Manufacturer Repair','Q11')

        E1.SetCellFontName('Tahoma','A9','Q' & local:RecordCount + 12)
        E1.SetCellFontSize(8,'A1','Q' & local:RecordCount + 12)
        E1.SetCellFontStyle('Bold','A11','R11')
        E1.SetCellFontStyle('Bold','A9')
        E1.SetCellFontStyle('Bold','F9')
        E1.SetCellFontStyle('Bold','H9')
        E1.SetCellBackgroundColor(color:Silver,'A9','Q9')
        E1.SetCellBackgroundColor(color:Silver,'A11','Q11')

        E1.SetColumnWidth('A','Q')
        E1.SelectCells('B12')
        E1.FreezePanes()

        E1.InsertWorksheet()
        E1.RenameWorkSheet('Summary')

        Do DrawDetailTitle

        local.DrawBox('A9','H9','A9','H9',color:Silver)
        local.DrawBox('A10','H10','A10','H10',color:Silver)

        E1.WriteToCell('Summary','A9')
        E1.WriteToCell('Head Account Number','A10')
        E1.WriteToCell('Head Account Name','B10')
        E1.WriteToCell('Booked','C10')
        E1.WriteToCell('Sent To ARC','D10')
        E1.WriteToCell('Booked Exclusions','E10')
        E1.WriteToCell('Send To ARC Exclusions','F10')
! Deleting (DBH 27/09/2007) # 9370 - Remove
!        E1.WriteToCell('Booked Less Exclusions','F10')
! End (DBH 27/09/2007) #9370
        E1.WriteToCell('Sent Less Exclusions','G10')
        E1.WriteToCell('Sent %','H10')

        E1.SelectCells('A11')
        local:RecordCount = 11
        Sort(SummaryQueue,que:AccountNumber)
        Loop x# = 1 To Records(SummaryQueue)
            Get(SummaryQueue,x#)
            E1.WriteToCell(Clip(que:AccountNumber),'A' & local:RecordCount)
            E1.WriteToCell(Clip(que:AccountName),'B' & local:RecordCount)
            E1.WriteToCell(Clip(que:Booked),'C' & local:RecordCount)
            E1.WriteToCell(Clip(que:SentToARC),'D' & local:RecordCount)
            E1.WriteToCell(Clip(que:Booked - que:BookedLessExclusions),'E' & local:RecordCount)
            E1.WriteToCell(Clip(que:SentToARC - que:SentLessExclusions),'F' & local:RecordCount)
! Deleting (DBH 27/09/2007) # 9370 - Remove
!            E1.WriteToCell(Clip(que:BookedLessExclusions),'G' & local:RecordCount)
! End (DBH 27/09/2007) #9370
            E1.WriteToCell(Clip(que:SentLessExclusions),'G' & local:RecordCount)
            ! Inserting (DBH 01/09/2006) # 8215 - Don't show the equation if it's going to be blank
            If que:BookedLessExclusions = 0 Or que:SentLessExclusions = 0
                E1.WriteToCell('','H' & local:RecordCount)
            Else ! If que:Booked = 0 Or que:SentToARC = 0
            ! End (DBH 01/09/2006) #8215
                E1.WriteToCell('=G' & local:RecordCount & '/C' & local:RecordCount,'H' & local:RecordCount)
            End ! If que:Booked = 0 Or que:SentToARC = 0

            local:RecordCount += 1
        End ! Loop x# = 1 To Records(SummaryQueue)

        E1.WriteToCell('Total Jobs','B' & local:RecordCount)
        E1.WriteToCell('=SUM(C11:C' & local:RecordCount - 1 & ')','C' & local:RecordCount)
        E1.WriteToCell('=SUM(D11:D' & local:RecordCount - 1 & ')','D' & local:RecordCount)
        E1.WriteToCell('=SUM(E11:E' & local:RecordCount - 1 & ')','E' & local:RecordCount)
        E1.WriteToCell('=SUM(F11:F' & local:RecordCount - 1 & ')','F' & local:RecordCount)
        E1.WriteToCell('=SUM(G11:G' & local:RecordCount - 1 & ')','G' & local:RecordCount)
        E1.WriteToCell('=SUM(H11:H' & local:RecordCount - 1 & ')','H' & local:RecordCount)
! Deleting (DBH 27/09/2007) # 9370 - Remove
!        E1.WriteToCell('=SUM(I11:I' & local:RecordCount - 1 & ')','I' & local:RecordCount)
! End (DBH 27/09/2007) #9370
        E1.SetCellNumberFormat(oix:NumberFormatPercentage,,2,,'H11','H' & local:RecordCount)
        local.DrawBox('A' & local:RecordCount,'H' & local:RecordCount,'A' & local:RecordCount,'H' & local:RecordCount,color:Silver)
        E1.SetCellFontName('Tahoma','A9','H' & local:RecordCount)
        E1.SetCellFontStyle('Bold','A9','H10')
        E1.SetCellFontStyle('Bold','A' & local:RecordCount,'G' & local:RecordCount)
        E1.SetCellFontSize(8,'A1','H' & local:RecordCount)
        E1.SetColumnWidth('A','H')
        E1.SelectCells('A11')

        E1.SaveAs(excel:FileName)!,oix:xlWorkbookDefault)
        E1.CloseWorkBook(3)
        E1.Kill()

        ! Use alternative method to assign autofilters (DBH: 31/07/2006)
        ExcelSetup(0)
        ExcelOpenDoc(excel:FileName)
        ExcelSelectSheet('Sent To ARC')
        ExcelAutoFilter('A11:Q11')
        ExcelAutoFit('A:Q')
        ExcelSelectRange('B12')
        ExcelSelectSheet('Summary')
        ExcelAutoFilter('A10:H10')
        ExcelAutoFit('A:H')
        ExcelSelectRange('A11')
        ExcelSaveWorkBook(excel:FileName)
        ExcelClose()

        local.UpdatePRogressWindow('Finishing Off Formatting..')


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    Else
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawSummaryTitle        Routine   !Set Summary Screen (DBH: 22-03-2004)
    E1.SetColumnWidth('A','','20')
    E1.SetColumnWidth('B','Z','15')
    E1.WriteToCell('** Summary Title **')
    E1.SetCellFontStyle('Bold','A1')
    E1.SetCellFontSize(12,'A1')

    E1.WriteToCell('Jobs Completed From','A3')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B3')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B3')
    E1.WriteToCell('Jobs Completed To','A4')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')
    E1.WriteTocell('Date Created','A5')
    E1.WriteToCell(Format(Today(),@d18),'B5')
    E1.SetCellFontStyle('Bold','B3','E5')

    !Colour
    Local.DrawBox('A1','E1','A1','E1',oix:Color15)
    Local.DrawBox('A3','E3','A5','E5',oix:Color15)

    E1.WriteToCell('** EXE NAME ** - ' & loc:Version  ,'D3')
DrawDetailTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(Clip(excel:ProgramName), 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A7','D7',color:Silver)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')


getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Do you want to finish off building the excel document with the data you''ve compled so far, or just quit now?','ServiceBase',|
            Icon:Question,'&Finish Off|&Quit Now|&Cancel',3)
        Of 1 ! &Finish Off Button
            tmp:cancel = 2
        Of 2 ! &Quit Now Button
            tmp:cancel = 1
        Of 3 ! &Cancel Button
        End!Case Message
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          Access:SRNTExt.Clearkey(srn:KeySRN)
          srn:SRNumber = '020654'&'0'
          If Access:SRNText.Fetch(srn:KeySRN)
              !Error
              SRN:TipText = 'Press F1 to access detailed help'
          End !Access:SRNText.Fetch(stn:KeySRN) = Level:Benign
          ?SRNNumber{Prop:Text}= 'SRN:020654'&'0'
          if clip(SRN:TipText)='' then SRN:TipText = 'Press F1 to access detailed help'.
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    MyProg.PWProgressWindow &= MyProg:ProgressWindow
    MyProg.CancelButtonFEQ = ?MyProg:ProgressCancel
    MyProg.UserStringFEQ = ?MyProg:UserString
    MyProg.ThermometerFEQ = ?MyProg:ProgressThermometer
    MyProg.PercentStringFEQ = ?MyProg:PercentString
   
      glo:FileName = 'MOD' & Clock() & '.tmp'
      If GetTempPathA(255,tmp:TempFilePath)
          If Sub(tmp:TempFilePath,-1,1) = '\'
              glo:FileName = Clip(tmp:TempFilePath) & Clip(glo:FileName)
          Else !If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(tmp:TempFilePath) & '\' & Clip(glo:FileName)
          End !If Sub(tmp:TempFilePath,-1,1) = '\'
      End
  
      Remove(glo:FileName)
      Access:MODELNUM.Open()
      Access:MODELNUM.UseFile()
      Create(MODELPART)
      Open(MODELPART)
      Stream(MODELPART)
  
      MyProg.init(RECORDS(MODELNUM))
  
      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
      mod:Model_Number = ''
      Set(mod:Model_Number_Key,mod:Model_Number_Key)
      Loop ! Begin Loop
          If Access:MODELNUM.Next()
              Break
          End ! If Access:MODELNUM.Next()
          if (MyProg.updatePRogressBar('Compiling Criteria. Please wait..'))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('You cannot cancel this process.','ServiceBase',|
                  Icon:Hand,'&OK',1)
              Of 1 ! &OK Button
              End!Case Message
          End ! If Prog.InsideLoop()
          Clear(model:Record)
          model:Manufacturer = mod:Manufacturer
          model:ModelNumber = mod:Model_Number
          Add(MODELPART)
      End ! Loop
  
      Flush(MODELPART)
      Close(MODELPART)
      Relate:MODELNUM.Close()
      MyProg.kill()
  GlobalErrors.SetProcedureName('SentToARCReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('tmp:HeadAccountNumber',tmp:HeadAccountNumber)      ! Added by: BrowseBox(ABC)
  BIND('tmp:Tag',tmp:Tag)                                  ! Added by: BrowseBox(ABC)
  BIND('tmp:TagMan',tmp:TagMan)                            ! Added by: BrowseBox(ABC)
  BIND('tmp:TagMod',tmp:TagMod)                            ! Added by: BrowseBox(ABC)
  BIND('tmp:Blank',tmp:Blank)                              ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNText.Open()
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:MODELPART.Open                                    ! File MODELPART used by this procedure, so make sure it's RelationManager is open
  Relate:REPSCHAC.SetOpenRelated()
  Relate:REPSCHAC.Open                                     ! File REPSCHAC used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MODELNUM.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOMODEL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATLOG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSCONS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOCK.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHCR.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHED.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHLG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHMA.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHMP.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:TRADEACC,SELF) ! Initialize the browse manager
  BRW16.Init(?List:2,Queue:Browse:1.ViewPosition,BRW16::View:Browse,Queue:Browse:1,Relate:MANUFACT,SELF) ! Initialize the browse manager
  BRW18.Init(?List:3,Queue:Browse:2.ViewPosition,BRW18::View:Browse,Queue:Browse:2,Relate:MODELPART,SELF) ! Initialize the browse manager
  SELF.Open(window)                                        ! Open window
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5009'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List{Prop:LineHeight} = 0
  ?List:2{Prop:LineHeight} = 0
  ?List:3{Prop:LineHeight} = 10
    Wizard14.Init(?Sheet1, |                               ! Sheet
                     ?VSBackButton, |                      ! Back button
                     ?VSNextButton, |                      ! Next button
                     ?Print, |                             ! OK button
                     ?Cancel, |                            ! Cancel button
                     1, |                                  ! Skip hidden tabs
                     1, |                                  ! OK and Next in same location
                     1)                                    ! Validate before allowing Next button to be pressed
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      Alert(0078H) !F9
      Alert(0070H) !F1
  BRW12.Q &= Queue:Browse
  BRW12.AddSortOrder(,tra:Account_Number_Key)              ! Add the sort order for tra:Account_Number_Key for sort order 1
  BRW12.AddLocator(BRW12::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW12::Sort0:Locator.Init(,tra:Account_Number,1,BRW12)   ! Initialize the browse locator using  using key: tra:Account_Number_Key , tra:Account_Number
  BRW12.SetFilter('(Upper(tra:BranchIdentification) <<> '''' And Upper(tra:Account_Number) <<> Upper(tmp:HeadAccountNumber))') ! Apply filter expression to browse
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tmp:Tag,BRW12.Q.tmp:Tag)                  ! Field tmp:Tag is a hot field or requires assignment from browse
  BRW12.AddField(tra:Account_Number,BRW12.Q.tra:Account_Number) ! Field tra:Account_Number is a hot field or requires assignment from browse
  BRW12.AddField(tra:Company_Name,BRW12.Q.tra:Company_Name) ! Field tra:Company_Name is a hot field or requires assignment from browse
  BRW12.AddField(tra:RecordNumber,BRW12.Q.tra:RecordNumber) ! Field tra:RecordNumber is a hot field or requires assignment from browse
  BRW16.Q &= Queue:Browse:1
  BRW16.AddSortOrder(,man:Manufacturer_Key)                ! Add the sort order for man:Manufacturer_Key for sort order 1
  BRW16.AddLocator(BRW16::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW16::Sort0:Locator.Init(,man:Manufacturer,1,BRW16)     ! Initialize the browse locator using  using key: man:Manufacturer_Key , man:Manufacturer
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW16.AddField(tmp:TagMan,BRW16.Q.tmp:TagMan)            ! Field tmp:TagMan is a hot field or requires assignment from browse
  BRW16.AddField(man:Manufacturer,BRW16.Q.man:Manufacturer) ! Field man:Manufacturer is a hot field or requires assignment from browse
  BRW16.AddField(man:RecordNumber,BRW16.Q.man:RecordNumber) ! Field man:RecordNumber is a hot field or requires assignment from browse
  BRW18.Q &= Queue:Browse:2
  BRW18.AddSortOrder(,model:ManufacturerKey)               ! Add the sort order for model:ManufacturerKey for sort order 1
  BRW18.AddRange(model:Manufacturer,tmp:Manufacturer)      ! Add single value range limit for sort order 1
  BRW18.AddLocator(BRW18::Sort1:Locator)                   ! Browse has a locator for sort order 1
  BRW18::Sort1:Locator.Init(?model:ModelNumber,model:ModelNumber,1,BRW18) ! Initialize the browse locator using ?model:ModelNumber using key: model:ManufacturerKey , model:ModelNumber
  BRW18.AddSortOrder(,model:ModelNumberKey)                ! Add the sort order for model:ModelNumberKey for sort order 2
  BRW18.AddLocator(BRW18::Sort0:Locator)                   ! Browse has a locator for sort order 2
  BRW18::Sort0:Locator.Init(?model:ModelNumber,model:ModelNumber,1,BRW18) ! Initialize the browse locator using ?model:ModelNumber using key: model:ModelNumberKey , model:ModelNumber
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW18.AddField(tmp:TagMod,BRW18.Q.tmp:TagMod)            ! Field tmp:TagMod is a hot field or requires assignment from browse
  BRW18.AddField(model:ModelNumber,BRW18.Q.model:ModelNumber) ! Field model:ModelNumber is a hot field or requires assignment from browse
  BRW18.AddField(model:PartNumber1,BRW18.Q.model:PartNumber1) ! Field model:PartNumber1 is a hot field or requires assignment from browse
  BRW18.AddField(model:PartNumber2,BRW18.Q.model:PartNumber2) ! Field model:PartNumber2 is a hot field or requires assignment from browse
  BRW18.AddField(model:PartNumber3,BRW18.Q.model:PartNumber3) ! Field model:PartNumber3 is a hot field or requires assignment from browse
  BRW18.AddField(model:PartNumber4,BRW18.Q.model:PartNumber4) ! Field model:PartNumber4 is a hot field or requires assignment from browse
  BRW18.AddField(model:PartNumber5,BRW18.Q.model:PartNumber5) ! Field model:PartNumber5 is a hot field or requires assignment from browse
  BRW18.AddField(tmp:Blank,BRW18.Q.tmp:Blank)              ! Field tmp:Blank is a hot field or requires assignment from browse
  BRW18.AddField(model:Manufacturer,BRW18.Q.model:Manufacturer) ! Field model:Manufacturer is a hot field or requires assignment from browse
  IF ?tmp:SelectManufacturer{Prop:Checked} = True
    UNHIDE(?tmp:Manufacturer)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = False
    HIDE(?tmp:Manufacturer)
  END
  FDCB21.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB21::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB21.Q &= Queue:FileDropCombo
  FDCB21.AddSortOrder(man:Manufacturer_Key)
  FDCB21.AddField(man:Manufacturer,FDCB21.Q.man:Manufacturer) !List box control field - type derived from field
  FDCB21.AddField(man:RecordNumber,FDCB21.Q.man:RecordNumber) !Primary key field - type derived from field
  FDCB21.AddUpdateField(man:Manufacturer,tmp:Manufacturer)
  ThisWindow.AddItem(FDCB21.WindowComponent)
  FDCB21.DefaultFill = 0
  BRW12.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW16.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW18.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
  
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              If rpc:AllAccounts
                  DO DASBRW::13:DASTAGALL
              Else ! If rpc:AllAccounts
                  Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
                  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
                  Loop ! Begin Loop
                      If Access:REPSCHAC.Next()
                          Break
                      End ! If Access:REPSCHAC.Next()
                      If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                      glo:Pointer = rpa:AccountNumber
                      Add(glo:Queue)
                  End ! Loop
              End ! If rpc:AllAccounts
  
  
              If rpc:AllManufacturers
                  Do DASBRW::17:DASTAGALL
              Else ! If rpc:AllManufacturers
                  Access:REPSCHMA.Clearkey(rpu:ManufacturerKey)
                  rpu:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpu:ManufacturerKey,rpu:ManufacturerKey)
                  Loop ! Begin Loop
                      If Access:REPSCHMA.Next()
                          Break
                      End ! If Access:REPSCHMA.Next()
                      If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
                      glo:Pointer2 = rpu:Manufacturer
                      Add(glo:Queue2)
                  End ! Loop
              End ! If rpc:AllManufacturers
  
              Access:REPSCHMP.Clearkey(rpo:ModelNumberKey)
              rpo:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpo:ModelNumberKey,rpo:ModelNumberKey)
              Loop ! Begin Loop
                  If Access:REPSCHMP.Next()
                      Break
                  End ! If Access:REPSCHMP.Next()
                  If rpo:REPSCHCRREcordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpo:REPSCHCRREcordNumber <> rpc:RecordNumber
                  glo:Pointer3 = rpo:ModelNumber
                  Add(glo:Queue3)
                  Access:MODELPART.ClearKey(model:ModelNumberKey)
                  model:ModelNumber = rpo:ModelNumber
                  If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
                      !Found
                      model:PartNumber1 = rpo:PartNumber1
                      model:PartNumber2 = rpo:PartNumber2
                      model:PartNumber3 = rpo:PartNumber3
                      model:PartNumber4 = rpo:PartNumber4
                      model:PartNumber5 = rpo:PartNumber5
                      Access:MODELPART.TryUpdate()
                  Else ! If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
                      !Error
                  End ! If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
              End ! Loop
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do Reporting
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  Relate:SRNText.Close()
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:MODELPART.Close
    Relate:REPSCHAC.Close
    Relate:SRNTEXT.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  Remove(glo:FileName)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateModelPart
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard14.Validate()                                ! FormWizardButtonClass (ABC Free)
        DISABLE(Wizard14.NextControl())                    ! FormWizardButtonClass (ABC Free)
     ELSE                                                  ! FormWizardButtonClass (ABC Free)
        ENABLE(Wizard14.NextControl())                     ! FormWizardButtonClass (ABC Free)
     END                                                   ! FormWizardButtonClass (ABC Free)
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?VSNextButton
          unhide(?VSBackButton)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Calendar
      ThisWindow.Update
      Calendar15.SelectOnClose = True
      Calendar15.Ask('Select a Date',tmp:StartDate)
      IF Calendar15.Response = RequestCompleted THEN
      tmp:StartDate=Calendar15.SelectedDate
      DISPLAY(?tmp:StartDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar24.SelectOnClose = True
      Calendar24.Ask('Select a Date',tmp:EndDate)
      IF Calendar24.Response = RequestCompleted THEN
      tmp:EndDate=Calendar24.SelectedDate
      DISPLAY(?tmp:EndDate)
      END
      ThisWindow.Reset(True)
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::17:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::17:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::17:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::17:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::17:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:SelectManufacturer
      IF ?tmp:SelectManufacturer{Prop:Checked} = True
        UNHIDE(?tmp:Manufacturer)
      END
      IF ?tmp:SelectManufacturer{Prop:Checked} = False
        HIDE(?tmp:Manufacturer)
      END
      ThisWindow.Reset
      BRW18.ResetSort(1)
    OF ?tmp:Manufacturer
      BRW18.ResetSort(1)
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::20:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::20:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::20:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::20:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::20:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      ModelError# = False
      Access:MODELPART.ClearKey(model:ModelNumberKey)
      model:ModelNumber = ''
      Set(model:ModelNumberKey,model:ModelNumberKey)
      Loop
          If Access:MODELPART.NEXT()
             Break
          End !If
          If Clip(model:PartNumber1) <> ''
              Access:STOMODEL.ClearKey(stm:Location_Part_Number_Key)
              stm:Model_Number = model:ModelNumber
              stm:Location     = 'MAIN STORE'
              stm:Part_Number  = model:PartNumber1
              If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
      
              Else ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Message('Error! There is a Model / Part Number mismatch.'&|
                        '|'&|
                        '|Model Number: ' & Clip(model:ModelNumber) & ''&|
                        '|Part Number1: ' & Clip(model:PartNumber1),'ServiceBase',|
                        Icon:Hand,'&OK',1)
                    Of 1 ! &OK Button
                    End!Case Message
                  ModelError# = True
                  Break
              End ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
          End ! If modal:PartNumber1 <> ''
          If Clip(model:PartNumber2) <> ''
              Access:STOMODEL.ClearKey(stm:Location_Part_Number_Key)
              stm:Model_Number = model:ModelNumber
              stm:Location     = 'MAIN STORE'
              stm:Part_Number  = model:PartNumber2
              If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
      
              Else ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Message('Error! There is a Model / Part Number mismatch.'&|
                        '|'&|
                        '|Model Number: ' & Clip(model:ModelNumber) & ''&|
                        '|Part Number2: ' & Clip(model:PartNumber2),'ServiceBase',|
                        Icon:Hand,'&OK',1)
                    Of 1 ! &OK Button
                    End!Case Message
                  ModelError# = True
                  Break
              End ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
          End ! If modal:PartNumber1 <> ''
          If Clip(model:PartNumber3) <> ''
              Access:STOMODEL.ClearKey(stm:Location_Part_Number_Key)
              stm:Model_Number = model:ModelNumber
              stm:Location     = 'MAIN STORE'
              stm:Part_Number  = model:PartNumber3
              If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
      
              Else ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Message('Error! There is a Model / Part Number mismatch.'&|
                        '|'&|
                        '|Model Number: ' & Clip(model:ModelNumber) & ''&|
                        '|Part Number3: ' & Clip(model:PartNumber3),'ServiceBase',|
                        Icon:Hand,'&OK',1)
                    Of 1 ! &OK Button
                    End!Case Message
                  ModelError# = True
                  Break
              End ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
          End ! If modal:PartNumber1 <> ''
          If Clip(model:PartNumber4) <> ''
              Access:STOMODEL.ClearKey(stm:Location_Part_Number_Key)
              stm:Model_Number = model:ModelNumber
              stm:Location     = 'MAIN STORE'
              stm:Part_Number  = model:PartNumber4
              If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
      
              Else ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Message('Error! There is a Model / Part Number mismatch.'&|
                        '|'&|
                        '|Model Number: ' & Clip(model:ModelNumber) & ''&|
                        '|Part Number4: ' & Clip(model:PartNumber4),'ServiceBase',|
                        Icon:Hand,'&OK',1)
                    Of 1 ! &OK Button
                    End!Case Message
                  ModelError# = True
                  Break
              End ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
          End ! If modal:PartNumber1 <> ''
      
          If Clip(model:PartNumber5) <> ''
              Access:STOMODEL.ClearKey(stm:Location_Part_Number_Key)
              stm:Model_Number = model:ModelNumber
              stm:Location     = 'MAIN STORE'
              stm:Part_Number  = model:PartNumber5
              If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
      
              Else ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Message('Error! There is a Model / Part Number mismatch.'&|
                        '|'&|
                        '|Model Number: ' & Clip(model:ModelNumber) & ''&|
                        '|Part Number5: ' & Clip(model:PartNumber5),'ServiceBase',|
                        Icon:Hand,'&OK',1)
                    Of 1 ! &OK Button
                    End!Case Message
                  ModelError# = True
                  Break
              End ! If Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign
          End ! If modal:PartNumber1 <> ''
      
      
      End !Loop
      
      If ModelError# = True
          Cycle
      End ! If ModelError# = True
      
      Do Reporting
    OF ?VSBackButton
      ThisWindow.Update
         Wizard14.TakeAccepted()                           ! FormWizardButtonClass (ABC Free)
    OF ?VSNextButton
      ThisWindow.Update
         Wizard14.TakeAccepted()                           ! FormWizardButtonClass (ABC Free)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020654'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020654'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020654'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::17:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::20:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
  !Of Event:AlertKey
      If KeyCode() = 0078H
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020654'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020654'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020654'&'0')
      ***
      End !If KeyCode() F9
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WriteLine         Procedure()
Code
    !SB Job No
    exp:Line1   = '"' & job:Ref_Number
    !Franchise Branch NO
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(tra:BranchIdentification)
    !Franchise Job Number
    exp:Line1   = Clip(exp:Line1) & '","' & wob:JobNumber
    !Head Account Number
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(tra:Account_Number)
    !Head Account Name
    exp:Line1   = Clip(exp:Line1) & '","' & CLip(tra:Company_Name)
    !Chargeable Charge Type
    If job:Chargeable_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Charge_Type)
    Else ! If job:Chargeable_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If job:Chargeable_Job = 'YES'
    !Warranty Charge Type
    If job:Warranty_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Warranty_Charge_Type)
    Else ! If job:Warranty_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If job:Warranty_Job = 'YES'
    !Action Date
    exp:Line1   = Clip(exp:Line1) & '","' & Format(Day(jobe:HubRepairDate),@n02) & Format(Month(jobe:HubRepairDate),@n02) & Year(jobe:HubRepairDate)
    !Waybill Number
    FoundWaybill# = False
    Access:JOBSCONS.Clearkey(joc:DateKey)
    joc:RefNumber   = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop ! Begin Loop
        If Access:JOBSCONS.Next()
            Break
        End ! If Access:JOBCONS.Next()
        If joc:RefNumber <> job:Ref_Number
            Break
        End ! If joc:RefNumber <> job:Ref_Number
        If joc:TheDate > lot:TheDate
            Cycle
        End ! If joc:TheDate > lot:TheDate
        If joc:DespatchFrom = 'RRC'
            If joc:DespatchTo = 'ARC'
                exp:Line1   = Clip(exp:Line1) & '","' & Clip(joc:ConsignmentNumber)
                FoundWaybill# = True
                Break
            End ! If job:DespatchTo = 'ARC'
        End ! If job:DespatchFrom = 'RRC'
    End ! Loop

    If FoundWaybill# = False
        exp:Line1 = Clip(exp:Line1) & '","'
    End ! If FoundWaybill# = False
    !Make
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Manufacturer)
    !Model
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Model_Number)
    !IMEI
    exp:Line1   = Clip(exp:Line1) & '","''' & Clip(job:ESN)
    !MSN
    If job:MSN <> 'N/A'
        exp:Line1   = Clip(exp:Line1) & '","''' & Clip(job:MSN)
    Else ! If job:MSN <> 'N/A'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If job:MSN <> 'N/A'
    !Date Received At ARC
    exp:Line1   = Clip(exp:Line1) & '","' & Format(Day(lot:TheDate),@n02) & Format(Month(lot:TheDate),@n02) & Year(lot:TheDate)
    !Date Completed At ARC
    exp:Line1   = Clip(exp:Line1) & '","' & Format(Day(job:Date_Completed),@n02) & Format(Month(job:Date_Completed),@n02) & Year(job:Date_Completed)
    !Repair Type
    If job:Warranty_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Repair_Type_Warranty)
    Else ! If job:Warranty_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Repair_Type)
    End ! If job:Warranty_Job = 'YES'
    !Manufacturer Repair
    If job:Third_Party_Site <> ''
        exp:Line1   = Clip(exp:Line1) & '","Yes'
    Else ! If job:Third_Party_Site <> ''
        exp:Line1   = Clip(exp:Line1) & '","No'
    End ! If job:Third_Party_Site <> ''
    exp:Line1   = Clip(exp:Line1) & '"'
    Add(ExportFile)
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
Window
Prog.ProgressSetup        Procedure(Long func:Records)
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
Code
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}
        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.percentText = '0% Completed'

Prog.InsideLoop         Procedure(<String func:String>)
Code
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
    Prog.UserText = Clip(func:String)
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWinow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Prog.ProgressThermometer = 100
    Prog.PercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
    Code
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0


BRW12.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2                                ! Set icon from icon list
  ELSE
    SELF.Q.tmp:Tag_Icon = 1                                ! Set icon from icon list
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue


BRW16.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW16.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:TagMan = ''
    ELSE
      tmp:TagMan = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (tmp:TagMan = '*')
    SELF.Q.tmp:TagMan_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.tmp:TagMan_Icon = 1                             ! Set icon from icon list
  END


BRW16.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW16.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW16::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW16::RecordStatus=ReturnValue
  IF BRW16::RecordStatus NOT=Record:OK THEN RETURN BRW16::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::17:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW16::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW16::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW16::RecordStatus
  RETURN ReturnValue


BRW18.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW18.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.EIP &= BRW18::EIPManager                            ! Set the EIP manager
  SELF.AddEditControl(EditInPlace::tmp:TagMod,1)
  SELF.AddEditControl(EditInPlace::model:ModelNumber,3)
  SELF.AddEditControl(EditInPlace::model:PartNumber1,4)
  SELF.AddEditControl(EditInPlace::model:PartNumber2,5)
  SELF.AddEditControl(EditInPlace::model:PartNumber3,6)
  SELF.AddEditControl(EditInPlace::model:PartNumber4,7)
  SELF.AddEditControl(EditInPlace::model:PartNumber5,8)
  SELF.AddEditControl(EditInPlace::tmp:Blank,9)
  SELF.DeleteAction = EIPAction:Always
  SELF.ArrowAction = EIPAction:Default+EIPAction:Remain+EIPAction:RetainColumn
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.ChangeControl=?Change
  END


BRW18.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:SelectManufacturer = 1
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW18.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = model:ModelNumber
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:TagMod = ''
    ELSE
      tmp:TagMod = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (tmp:TagMod = '*')
    SELF.Q.tmp:TagMod_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.tmp:TagMod_Icon = 1                             ! Set icon from icon list
  END


BRW18.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW18.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW18::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW18::RecordStatus=ReturnValue
  IF BRW18::RecordStatus NOT=Record:OK THEN RETURN BRW18::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = model:ModelNumber
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::20:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW18::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW18::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW18::RecordStatus
  RETURN ReturnValue


FDCB21.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

GreenBarIndex   LONG
  CODE
  ReturnValue = PARENT.ResetQueue(Force)
  RETURN ReturnValue

Wizard14.TakeNewSelection PROCEDURE                        ! FormWizardButtonClass (ABC Free)
   CODE                                                    ! FormWizardButtonClass (ABC Free)
    PARENT.TakeNewSelection()                              ! FormWizardButtonClass (ABC Free)

    IF NOT(BRW12.Q &= NULL) ! Has Browse Object been initialized?
       BRW12.ResetQueue(Reset:Queue)                       ! FormWizardButtonClass (ABC Free)
    END                                                    ! FormWizardButtonClass (ABC Free)
    IF NOT(BRW16.Q &= NULL) ! Has Browse Object been initialized?
       BRW16.ResetQueue(Reset:Queue)                       ! FormWizardButtonClass (ABC Free)
    END                                                    ! FormWizardButtonClass (ABC Free)
    IF NOT(BRW18.Q &= NULL) ! Has Browse Object been initialized?
       BRW18.ResetQueue(Reset:Queue)                       ! FormWizardButtonClass (ABC Free)
    END                                                    ! FormWizardButtonClass (ABC Free)

Wizard14.TakeBackEmbed PROCEDURE                           ! FormWizardButtonClass (ABC Free)
   CODE                                                    ! FormWizardButtonClass (ABC Free)

Wizard14.TakeNextEmbed PROCEDURE                           ! FormWizardButtonClass (ABC Free)
   CODE                                                    ! FormWizardButtonClass (ABC Free)

Wizard14.Validate PROCEDURE                                ! FormWizardButtonClass (ABC Free)
   CODE                                                    ! FormWizardButtonClass (ABC Free)
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False                                           ! FormWizardButtonClass (ABC Free)
UpdateModelPart PROCEDURE                                  ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::model:Record LIKE(model:RECORD),THREAD
QuickWindow          WINDOW('Update the MODELPART File'),AT(,,196,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateModelPart'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,188,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?model:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,20,124,10),USE(model:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                           PROMPT('Model Number'),AT(8,34),USE(?model:ModelNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,34,124,10),USE(model:ModelNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Model Number'),TIP('Model Number'),UPR
                           PROMPT('Part Number 1'),AT(8,48),USE(?model:PartNumber1:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,48,124,10),USE(model:PartNumber1),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 1'),TIP('Part Number 1'),UPR
                           PROMPT('Part Number 2'),AT(8,62),USE(?model:PartNumber2:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,62,124,10),USE(model:PartNumber2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 2'),TIP('Part Number 2'),UPR
                           PROMPT('Part Number 3'),AT(8,76),USE(?model:PartNumber3:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,76,124,10),USE(model:PartNumber3),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 3'),TIP('Part Number 3'),UPR
                         END
                       END
                       BUTTON('OK'),AT(49,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(98,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(147,94,45,14),USE(?Help),STD(STD:Help)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage                   ! Display status message in title bar
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateModelPart')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?model:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(model:Record,History::model:Record)
  SELF.AddHistoryField(?model:Manufacturer,1)
  SELF.AddHistoryField(?model:ModelNumber,2)
  SELF.AddHistoryField(?model:PartNumber1,3)
  SELF.AddHistoryField(?model:PartNumber2,4)
  SELF.AddHistoryField(?model:PartNumber3,5)
  SELF.AddUpdateFile(Access:MODELPART)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:MODELPART.Open                                    ! File MODELPART used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MODELPART
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELPART.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

XFiles PROCEDURE                                           ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

SentToHub            PROCEDURE  (func:RefNumber)           ! Declare Procedure
Bryan       Class
CompFieldColour       Procedure()
            End
  CODE
    If jobe:HubRepair Or jobe:HubRepairDate <> ''
        Return Level:Fatal
    Else !If jobe:HubRepair
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = func:RefNumber
        lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
            Return Level:Fatal
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
    End !If jobe:HubRepair

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
Bryan       Class
CompFieldColour       Procedure()
            End
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
Bryan       Class
CompFieldColour       Procedure()
            End
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()



   MEMBER('vodr0018.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

CourierReportByManufacturer PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_job_id          USHORT,AUTO
tmp:VersionNumber    STRING(30)
save_joc_id          USHORT,AUTO
ParameterStuff       GROUP,PRE(LOC)
Courier              STRING(30)
Count                LONG
Today                DATE
EndDate              DATE
StartDate            DATE
                     END
Excel_Group          GROUP,PRE()
Excel                CSTRING(20)
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
WorkSheet_Group      GROUP,PRE(sheet)
HeadLastCol          STRING('E')
HeadSummaryRow       LONG(9)
TempLastCol          STRING('D')
DataLastCol          STRING('L')
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
ProgressBar_Group    GROUP,PRE()
Progress:Text        STRING(100)
progress:NextCancelCheck TIME
RecordCount          LONG
                     END
Couier_Queue         QUEUE,PRE(couQ)
Courier              STRING(30)
Count                LONG
                     END
Local_Stuff          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CommentText          STRING(100)
CompanyName          STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
JobNumber            LONG
LastColumn           STRING('L')
LineCost             DECIMAL(7,2)
Path                 STRING('C:\ {252}')
ProgramName          STRING(100)
ScanType             STRING(3)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
VATCode              STRING(2)
Version              STRING('3.1.000 {23}')
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
Result               BYTE
AccountChange        BYTE
tmp:FirstModel       STRING(30)
StockOK              BYTE
InvoiceOK            BYTE
WebJOB_OK            BYTE
                     END
SummaryQueue         QUEUE,PRE(sq)
Manufacturer         STRING(30),NAME('sq_Manufacturer')
ModelNumber          STRING(30),NAME('sq_ModelNumber')
Courier              STRING(30),NAME('sq_Courier')
WarrantyCount        LONG
ChargeableCount      LONG
TotalUnitsConsigned  LONG
                     END
EngineerQueue        QUEUE,PRE(eq)
Engineer             STRING(3)
Name                 STRING(100)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTimeOut         LONG
DoAll                STRING(1)
LocalTag             STRING(1)
tmp:ExchangeIMEI     STRING(20)
DespatchStage        BYTE
tmp:ExportFile       STRING(255),STATIC
JobsQueue            QUEUE,PRE(jobque)
JobNumber            LONG,NAME('jq:JobNumber')
AccountNumber        STRING(30),NAME('jq:AccountNumber')
DespatchFrom         STRING(3),NAME('jq:DespatchFrom')
                     END
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Courier Report By Manufacturer Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Despatch Start Date'),AT(245,114),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(284,214,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           ENTRY(@d6),AT(345,114,64,10),USE(LOC:StartDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest booking date'),REQ
                           BUTTON,AT(413,110),USE(?StartPopCalendar),IMM,SKIP,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           PROMPT('Despatch End Date'),AT(245,136),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(345,136,64,10),USE(LOC:EndDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest booking date'),REQ
                           BUTTON,AT(413,132),USE(?EndPopCalendar),IMM,SKIP,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(292,230,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON,AT(496,246),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,280),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,316),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('All Repair Centres'),AT(192,156),USE(DoAll),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(432,156),USE(excel:Visible),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,168,296,184),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
ExportFile    File,Driver('TOPSPEED'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
AccountNumberKey        Key(expfil:AccountNumber,expfil:DespatchFrom,expfil:JobNumber),NOCASE,DUP
DespatchFromKey         Key(expfil:DespatchFrom,expfil:JobNumber),NOCASE,DUP
Record                  Record
AccountNumber           String(30)
JobNumber               Long
DespatchFrom            String(30)
                        End
                    End

TempFilePath         CSTRING(255)
!-----------------------------------------------
    MAP
DateToString        PROCEDURE( DATE ), STRING
GetHeadAccount PROCEDURE( STRING )
GetSubAccount PROCEDURE( STRING )
GetWebJobNumber PROCEDURE( LONG ), STRING
GetExchangeIMEI PROCEDURE( LONG ), STRING
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
LoadJOBSE      PROCEDURE( LONG ), LONG, PROC
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
WriteColumn PROCEDURE( STRING, LONG=False )
    END !MAP
!-----------------------------------------------
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
! Despatch Equates

Stage:Exit            EQUATE(0)
Stage:SentToARC       EQUATE(1)
Stage:SentTo3rdParty  EQUATE(2)
Stage:SentToRRC       EQUATE(3)
Stage:SentToCustomer  EQUATE(4)
Stage:Unknown         EQUATE(5)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
OKButton_Pressed                ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
    !_____________________________________________________________________

        !Create export file with job numbers in -  (DBH: 12-08-2003)
        If GetTempPathA(255,TempFilePath)
            If Sub(TempFilePath,-1,1) = '\'
                tmp:ExportFile = Clip(TempFilePath) & 'COURIERREPORT' & Clock() & '.TMP'
            Else !If Sub(TempFilePath,-1,1) = '\'
                tmp:ExportFile = Clip(TempFilePath) & '\COURIERREPORT' & Clock() & '.TMP'
            End !If Sub(TempFilePath,-1,1) = '\'
        End

        Remove(ExportFile)
        Create(ExportFile)
        Open(ExportFile)
        If Error()
            Stop(Error())
        End !If Error()

        Prog.ProgressSetup(Records(JOBSCONS))
        Prog.ProgressText('Initializing Report..')

        Count# = 0
        Skip# = 0

        Save_joc_ID = Access:JOBSCONS.SaveFile()
        Access:JOBSCONS.ClearKey(joc:DateOnlyKey)
        joc:TheDate   = loc:StartDate
        Set(joc:DateOnlyKey,joc:DateOnlyKey)
        Loop
            If Access:JOBSCONS.NEXT()
               Break
            End !If
            If joc:TheDate   > loc:EndDate      |
                Then Break.  ! End If

            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()

!            Count# += 1
!            Skip# += 1
!            If Skip# > 200
!                Prog.ProgressText('Entries Found: ' & Count#)
!                Skip# = 0
!            End ! If Skip# > 200

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = joc:RefNumber
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            Clear(JobsQueue)
            jobque:AccountNumber = wob:HeadAccountNumber
            jobque:JobNumber = joc:RefNumber
            jobque:DespatchFrom = joc:DespatchFrom
            Get(JobsQueue,'jq:AccountNumber,jq:JobNumber,jq:DespatchFrom')
            If Error()
                jobque:AccountNumber = wob:HeadAccountNumber
                jobque:JobNumber = joc:RefNumber
                jobque:DespatchFrom = joc:DespatchFrom
                Add(JobsQueue)
            End ! If Error()
!
!            Clear(expfil:Record)
!            expfil:AccountNumber    = wob:HeadAccountNumber
!            expfil:JobNumber        = joc:RefNumber
!            expfil:DespatchFrom     = joc:DespatchFrom
!            Get(ExportFile,expfil:AccountNumberKey)
!            !Add an entry for the job. Once for ARC, once for RRC -  (DBH: 12-08-2003)
!            If Error()
!                Clear(expfil:Record)
!                expfil:AccountNumber    = wob:HeadAccountNumber
!                expfil:JobNumber        = joc:RefNumber
!                expfil:DespatchFrom     = joc:DespatchFrom
!                Add(ExportFile)
!                If Error()
!                    Stop(Error())
!                End ! If Error()
!            End !If Error()
        End !Loop
        Access:JOBSCONS.RestoreFile(Save_joc_ID)

        Prog.ProgressFinish()

        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)

        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            IF tra_ali:Account_Number <> LocalHeadAccount THEN
                 IF tra_ali:RemoteRepairCentre = 0 THEN CYCLE.
            END !IF

            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.

            IF DoAll <> 'Y' THEN
                 glo:Queue2.Pointer2 = tra_ali:RecordNumber
                 GET(glo:Queue2,glo:Queue2.Pointer2)
                 IF ERROR() THEN CYCLE.
            END !IF

            LOC:FileName = ''
            DO ExportSetup
            IF LOC:FileName = '' THEN CYCLE.

            DO ExportBody
            DO ExportFinalize

        END !LOOP

        IF NOT CancelPressed
            POST(Event:CloseWindow)
        END !IF
        Close(ExportFile)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False

        IF LOC:StartDate > LOC:EndDate
            Case Missive('Invalid date range.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            CancelPressed = True

            EXIT
        END !IF

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !-----------------------------------------------

        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO ProgressBar_Setup
           !-----------------------------------------------------------------
           DO XL_Setup

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        FREE(SummaryQueue)
        CLEAR(SummaryQueue)
        FREE(EngineerQueue)
        CLEAR(EngineerQueue)
        FREE(HeaderQueue)
        CLEAR(HeaderQueue)
        FREE(HeadAccount_Queue)
        CLEAR(HeadAccount_Queue)
        FREE(SubAccount_Queue)
        CLEAR(SubAccount_Queue)
        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!ExportFinalize                          ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
!        DO ResetClipboard
!
!        IF CancelPressed
!            DO XL_DropAllWorksheets
!            DO XL_Finalize
!
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        !
!        Excel{'Sheets("Sheet3").Select'}
!        Excel{'ActiveWindow.SelectedSheets.Delete'}
!
!        Excel{'Sheets("Summary").Select'}
!        Excel{'Range("A1").Select'}
!
!        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
!        Excel{'Application.ActiveWorkBook.Close()'}
!        !-----------------------------------------------------------------
!        DO XL_Finalize
!
!        DO ProgressBar_Finalise
!        !-----------------------------------------------------------------
!        SETCURSOR()
!
!        Case MessageEx('Export Completed.', |
!                     LOC:ApplicationName,   |
!                     'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!        Of 1 ! &OK Button
!        End!Case MessageEx
!        !-----------------------------------------------------------------
!    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:B").ColumnWidth'}                         = 18
        Excel{'ActiveSheet.Columns("C:' & sheet:HeadLastCol & '").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
        DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        Progress:Text    = 'Checking Jobs'
        RecordsToProcess = 10000
        RecordsProcessed = 0
        RecordCount      = 0

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        DO InitColumns

        LOC:SectionName = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        LOC:Text        = LEFT(CLIP(LOC:ProgramName), 30)
        LOC:CommentText = ''
        DO CreateSectionHeader


        !It's either go through jobs in date booked order
        !or in trade order. Both will be slow until I can add a key -  (DBH: 09-07-2003)

    !_____________________________________________________________________

        Sort(JobsQueue,'jq:DespatchFrom,jq:AccountNumber,jq:JobNumber')
        Loop x# = 1 To Records(JobsQueue)
            Get(JobsQueue,x#)
            If tra_ali:Account_Number = LocalHeadAccount
                If jobque:DespatchFrom <> 'ARC'
                    Cycle
                End ! If jobque:DespatchFrom <> 'ARC'
            Else ! If tra_ali:Account_Number = LocalHeadAccount
                If jobque:DespatchFrom <> 'RRC'
                    Cycle
                End ! If jobque:DespatchFrom <> 'RRC'
                If jobque:AccountNumber <> tra_ali:Account_Number
                    Cycle
                End ! If jobque:AccountNumber <> tra_ali:Account_Number
            End ! If tra_ali:Account_Number = LocalHeadAccount

!
!        Clear(expfil:Record)
!        If tra_ali:Account_Number = LocalHeadAccount
!            !ARC -  (DBH: 12-08-2003)
!            expfil:DespatchFrom     = 'ARC'
!            Set(expfil:DespatchFromKey,expfil:DespatchFromKey)
!        Else !If tra_ali:AccountNumber = LocalHeadAccount
!            !RRC -  (DBH: 12-08-2003)
!            expfil:AccountNumber    = tra_ali:Account_Number
!            expfil:DespatchFrom     = 'RRC'
!            Set(expfil:AccountNumberKey,expfil:AccountNumberKey)
!        End !If tra_ali:AccountNumber = LocalHeadAccount
!        Loop
!            Next(ExportFile)
!            If Error()
!                Break
!            End !If Error()
!            If tra_ali:Account_Number = LocalHeadAccount
!                If expfil:DespatchFrom <> 'ARC'
!                    Break
!                End !If expfil:DespatchFrom <> 'ARC'
!            Else !If tra_ali:Account_Number = LocalHeadAccount
!                If expfil:AccountNumber <> tra_ali:Account_Number
!                    Break
!                End !If expfil:AccountNumber <> tra_ali:AccountNumber
!                If expfil:DespatchFrom <> 'RRC'
!                    Break
!                End !If expfil:DespatchFrom <> 'RRC'
!            End !If tra_ali:Account_Number = LocalHeadAccount

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = jobque:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found

            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            Save_joc_ID = Access:JOBSCONS.SaveFile()
            Access:JOBSCONS.ClearKey(joc:DateKey)
            joc:RefNumber = job:Ref_Number
            joc:TheDate   = loc:StartDate
            Set(joc:DateKey,joc:DateKey)
            Loop
                If Access:JOBSCONS.NEXT()
                   Break
                End !If
                If joc:RefNumber <> job:Ref_Number      |
                Or joc:TheDate   > loc:EndDate      |
                    Then Break.  ! End If
                If tra_ali:Account_Number = LocalHeadAccount
                    !ARC. Looking at all jobs -  (DBH: 09-07-2003)
                    If joc:DespatchFrom <> 'ARC'
                        Cycle
                    End !If job:DespatchFrom <> 'ARC'
                    If wob:HeadAccountNumber = LocalHeadAccount
                        !Booked at ARC
                        !Include ARC to 3rd Party and ARC to Client -  (DBH: 09-07-2003)
                        If joc:DespatchTo = 'CUSTOMER' Or |
                            joc:DespatchTo = '3RD PARTY'
                            Do WriteColumns
                        End !joc:DespatchTo = '3RD PARTY'
                    Else !If wob:HeadAccountNumber = LocalHeadAccount
                        !Not booked at ARC
                        !Include ARC to RRC & ARC to 3rd Party -  (DBH: 09-07-2003)
                        If joc:DespatchTo = 'CUSTOMER' Or |
                            joc:DespatchTo = '3RD PARTY' Or |
                            joc:DespatchTo = 'RRC'
                            Do WriteColumns
                        End !joc:DespatchTo = 'RRC'
                    End !If wob:HeadAccountNumber = LocalHeadAccount
                Else !If tra_ali:Account_Number = LocalHeadAccount
                    !Booked at RRC
                    !Include RRC to ARC and RRC to Customer -  (DBH: 09-07-2003)
                    If joc:DespatchFrom <> 'RRC'
                        Cycle
                    End !If joc:DespatchFrom <> 'RRC'

                    If joc:DespatchTo = 'CUSTOMER' Or |
                        joc:DespatchTo = 'ARC'
                        Do WriteColumns
                    End !joc:DespatchTo = 'ARC'
                End !If tra_ali:Account_Number = LocalHeadAccount
            End !Loop
            Access:JOBSCONS.RestoreFile(Save_joc_ID)

        End !Loop


!        !Now go through Consignment History file
!        !in date order -  (DBH: 12-08-2003)
!
!        Save_joc_ID = Access:JOBSCONS.SaveFile()
!        Access:JOBSCONS.ClearKey(joc:DespatchFromDateKey)
!
!        If tra_ali:Account_Number = LocalHeadAccount
!            !ARC -  (DBH: 12-08-2003)
!            joc:DespatchFrom = 'ARC'
!        Else !If tra_ali:Account_Number = LocalHeadAccount
!            !RRC -  (DBH: 12-08-2003)
!            joc:DespatchFrom = 'RRC'
!        End !If tra_ali:Account_Number = LocalHeadAccount
!
!        joc:TheDate      = loc:StartDate
!        Set(joc:DespatchFromDateKey,joc:DespatchFromDateKey)
!        Loop
!            If Access:JOBSCONS.NEXT()
!               Break
!            End !If
!            If tra_ali:Account_Number = LocalHeadAccount
!                !ARC -  (DBH: 12-08-2003)
!                If joc:DespatchFrom <> 'ARC'
!                    Break
!                End !If joc:DespatchFrom <> 'ARC'
!            Else !If tra_ali:Account_Number = LocalHeadAccount
!                !RRC -  (DBH: 12-08-2003)
!                If joc:DespatchFrom <> 'RRC'
!                    Break
!                End !If joc:DespatchFrom <> 'RRC'
!            End !If tra_ali:Account_Number = LocalHeadAccount
!
!            If joc:TheDate      > loc:EndDate       |
!                Then Break.  ! End If
!
!
!            DO ProgressBar_Loop
!
!            IF CancelPressed
!                BREAK
!            END !IF
!
!            Access:JOBS.Clearkey(job:Ref_Number_Key)
!            job:Ref_Number  = joc:RefNumber
!            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!                !Found
!
!            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!                !Error
!                Cycle
!            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!
!            Access:WEBJOB.Clearkey(wob:RefNumberKey)
!            wob:RefNumber = job:Ref_Number
!            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                !Found
!
!            Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                !Error
!                Cycle
!            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!
!            !Is this an RRC? -  (DBH: 09-07-2003)
!            If tra_ali:Account_Number <> LocalHeadAccount
!                !RRC. Only include jobs that were booked by this ARC -  (DBH: 09-07-2003)
!                If wob:HeadAccountNumber <> tra_ali:Account_Number
!                    Cycle
!                End !If wob:HeadAccountNumber <> tra_ali:Account_Number
!            End !If tra_ali:Account_Number = LocalHeadAccount
!
!            If tra_ali:Account_Number = LocalHeadAccount
!                !ARC. Looking at all jobs -  (DBH: 09-07-2003)
!                If joc:DespatchFrom <> 'ARC'
!                    Cycle
!                End !If job:DespatchFrom <> 'ARC'
!                If wob:HeadAccountNumber = LocalHeadAccount
!                    !Booked at ARC
!                    !Include ARC to 3rd Party and ARC to Client -  (DBH: 09-07-2003)
!                    If joc:DespatchTo = 'CUSTOMER' Or |
!                        joc:DespatchTo = '3RD PARTY'
!                        Do WriteColumns
!                    End !joc:DespatchTo = '3RD PARTY'
!                Else !If wob:HeadAccountNumber = LocalHeadAccount
!                    !Not booked at ARC
!                    !Include ARC to RRC & ARC to 3rd Party -  (DBH: 09-07-2003)
!                    If joc:DespatchTo = 'CUSTOMER' Or |
!                        joc:DespatchTo = '3RD PARTY' Or |
!                        joc:DespatchTo = 'RRC'
!                        Do WriteColumns
!                    End !joc:DespatchTo = 'RRC'
!                End !If wob:HeadAccountNumber = LocalHeadAccount
!            Else !If tra_ali:Account_Number = LocalHeadAccount
!                !Booked at RRC
!                !Include RRC to ARC and RRC to Customer -  (DBH: 09-07-2003)
!                If joc:DespatchFrom <> 'RRC'
!                    Cycle
!                End !If joc:DespatchFrom <> 'RRC'
!
!                If joc:DespatchTo = 'CUSTOMER' Or |
!                    joc:DespatchTo = 'ARC'
!                    Do WriteColumns
!                End !joc:DespatchTo = 'ARC'
!            End !If tra_ali:Account_Number = LocalHeadAccount
!        End !Loop
!        Access:JOBSCONS.RestoreFile(Save_joc_ID)
!
!!        Save_job_ID = Access:JOBS.SaveFile()
!!        Access:JOBS.ClearKey(job:Date_Booked_Key)
!!        job:date_booked = loc:StartDate
!!        Set(job:Date_Booked_Key,job:Date_Booked_Key)
!!        Loop
!!            If Access:JOBS.NEXT()
!!               Break
!!            End !If
!!            If job:date_booked > loc:EndDate      |
!!                Then Break.  ! End If
!!
!!            DO ProgressBar_Loop
!!
!!            IF CancelPressed
!!                BREAK
!!            END !IF
!!
!!            Access:WEBJOB.Clearkey(wob:RefNumberKey)
!!            wob:RefNumber = job:Ref_Number
!!            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!                !Found
!!
!!            Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!                !Error
!!            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!!
!!            !Is this an RRC? -  (DBH: 09-07-2003)
!!            If tra_ali:Account_Number <> LocalHeadAccount
!!                !RRC. Only include jobs that were booked by this ARC -  (DBH: 09-07-2003)
!!                If wob:HeadAccountNumber <> tra_ali:Account_Number
!!                    Cycle
!!                End !If wob:HeadAccountNumber <> tra_ali:Account_Number
!!            End !If tra_ali:Account_Number = LocalHeadAccount
!!
!!            !Let's see if something has been despatched and to where -  (DBH: 09-07-2003)
!!            Save_joc_ID = Access:JOBSCONS.SaveFile()
!!            Access:JOBSCONS.ClearKey(joc:DateKey)
!!            joc:RefNumber = job:Ref_Number
!!            Set(joc:DateKey,joc:DateKey)
!!            Loop
!!                If Access:JOBSCONS.NEXT()
!!                   Break
!!                End !If
!!                If joc:RefNumber <> job:Ref_Number      |
!!                    Then Break.  ! End If
!!
!!                If tra_ali:Account_Number = LocalHeadAccount
!!                    !ARC. Looking at all jobs -  (DBH: 09-07-2003)
!!                    If joc:DespatchFrom <> 'ARC'
!!                        Cycle
!!                    End !If job:DespatchFrom <> 'ARC'
!!                    If wob:HeadAccountNumber = LocalHeadAccount
!!                        !Booked at ARC
!!                        !Include ARC to 3rd Party and ARC to Client -  (DBH: 09-07-2003)
!!                        If joc:DespatchTo = 'CUSTOMER' Or |
!!                            joc:DespatchTo = '3RD PARTY'
!!                            Do WriteColumns
!!                        End !joc:DespatchTo = '3RD PARTY'
!!                    Else !If wob:HeadAccountNumber = LocalHeadAccount
!!                        !Not booked at ARC
!!                        !Include ARC to RRC & ARC to 3rd Party -  (DBH: 09-07-2003)
!!                        If joc:DespatchTo = 'CUSTOMER' Or |
!!                            joc:DespatchTo = '3RD PARTY' Or |
!!                            joc:DespatchTo = 'RRC'
!!                            Do WriteColumns
!!                        End !joc:DespatchTo = 'RRC'
!!                    End !If wob:HeadAccountNumber = LocalHeadAccount
!!                Else !If tra_ali:Account_Number = LocalHeadAccount
!!                    !Booked at RRC
!!                    !Include RRC to ARC and RRC to Customer -  (DBH: 09-07-2003)
!!                    If joc:DespatchFrom <> 'RRC'
!!                        Cycle
!!                    End !If joc:DespatchFrom <> 'RRC'
!!
!!                    If joc:DespatchTo = 'CUSTOMER' Or |
!!                        joc:DespatchTo = 'ARC'
!!                        Do WriteColumns
!!                    End !joc:DespatchTo = 'ARC'
!!                End !If tra_ali:Account_Number = LocalHeadAccount
!!            End !Loop
!!            Access:JOBSCONS.RestoreFile(Save_joc_ID)
!!        End !Loop
!!        Access:JOBS.RestoreFile(Save_job_ID)

        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}                      = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}                  = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}                      = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}                 = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}   = 'VODR0018 - '&CLIP(LOC:Version)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8

        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = 'Criteria'
        !-----------------------------------------------
        Excel{'Range("A4:' & sheet:TempLastCol & '7").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A4").Select'}
            Excel{'ActiveCell.Formula'}          = 'Booking Date From:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = DateToString(LOC:StartDate)
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A5").Select'}
            Excel{'ActiveCell.Formula'}          = 'Booking Date To:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = DateToString(LOC:EndDate)
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A6").Select'}
            Excel{'ActiveCell.Formula'}          = 'Created By:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LOC:UserName
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A7").Select'}
            Excel{'ActiveCell.Formula'}          = 'Created Date:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 18.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

       head:ColumnName       = 'Franchise Branch Name'
           head:ColumnWidth  = 30.00
           head:NumberFormat = chr(64)
           ADD(HeaderQueue)

        head:ColumnName       = 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'IMEI Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Booked'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Delivered To'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Company Name'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Delivery Address Line 1'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Delivery Address Line 2'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Delivery Suburb'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Delivery Postcode'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Courier'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Courier Waybill Number'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Waybill Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Consignment Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = '3rd Party Consignment Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Despatched'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange Unit'
            head:ColumnWidth  = 10.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Current Job Status' ! was 'Current Status'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Current Exchange Status'
            head:ColumnWidth  = 30.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Despatch Type'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Job Type'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
    !_____________________________________________________________________

        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
    !_____________________________________________________________________

    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !PutIniCount += 1
        !PUTINI(CLIP(LOC:ProgramName),PutIniCount, 'SetColumns','C:\' & LOC:ApplicationName & '_Debug.INI')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                      ROUTINE
    DATA
local:ExchangeIMEI      String(30)
local:DespatchIMEI      String(30)
local:LoanIMEI          String(30)
local:BookingAccountName    String(30)
local:DeliveredTo           String(100)
local:Courier           String(30)
    CODE
        IF CancelPressed
            EXIT
        END !IF

        RecordCount += 1

    !_____________________________________________________________________


        !What is the Exchange IMEI -  (DBH: 09-07-2003)
        If job:Exchange_Unit_Number <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                local:ExchangeIMEI  = xch:ESN
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        End !If job:Exchange_Unit_Number <> 0

        !What is the Loan IMEI -  (DBH: 09-07-2003)
        If job:Loan_Unit_Number <> 0
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                local:LoanIMEI  = loa:ESN
            Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
        End !If job:Loan_Unit_Number <> 0

        !Get account to find the Booking Account Name -  (DBH: 09-07-2003)
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            local:BookingAccountName    = tra:Company_Name
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

        !Show the courier unless it's third party.
        !Then show the courier record on the Third Party Account -  (DBH: 09-07-2003)
        If joc:DespatchTo = '3RD PARTY'
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = job:Third_Party_Site
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                local:Courier   = trd:Courier
            Else !If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                local:Courier   = 'THIRD PARTY'
            End !If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        Else !If joc:DespatchTo = '3RD PARTY'
            local:Courier   = joc:Courier
        End !If joc:DespatchTo = '3RD PARTY'

    !_____________________________________________________________________

        sq:Manufacturer = job:Manufacturer
        sq:ModelNumber  = job:Model_Number
        sq:Courier      = local:Courier
        Get(SummaryQueue,'sq_Manufacturer,sq_ModelNumber,sq_Courier')
        If Error()
            sq:Manufacturer = job:Manufacturer
            sq:ModelNumber  = job:Model_Number
            sq:Courier      = local:Courier
            sq:TotalUnitsConsigned  = 1
            Add(SummaryQueue)
        Else !If Error()
            sq:TotalUnitsConsigned += 1
            Put(SummaryQueue)
        End !If Error()

    !_____________________________________________________________________


        !Job Number
        WriteColumn(job:Ref_Number)
        !Franchise Branch Name
        WriteColumn(local:BookingAccountName)
        !Manufacturer
        WriteColumn(job:Manufacturer)
        !Model Number
        WriteColumn(job:Model_Number)
        !I.M.E.I. Number
        Case joc:DespatchType
            Of 'JOB'
                local:DespatchIMEI  = job:ESN
            Of 'LOA'
                local:DespatchIMEI  = local:LoanIMEI
            Of 'EXC'
                local:DespatchIMEI  = local:ExchangeIMEI
        End !Case joc:DespatchType
        WriteColumn(local:DespatchIMEI)
        !Date Booked
        WriteColumn(DateToString(job:Date_Booked))
        !Delivered To
            !Show booking account, followed by job account number
            !Then the destination in brackets, but only if ARC -  (DBH: 09-07-2003)
            !Delivered to should be in the format FROM-TO - L827 (DBH: 17-07-2003)
        Case joc:DespatchFrom
            Of 'ARC'
                Case joc:DespatchTo
                    Of 'RRC'
                        local:DeliveredTo = Clip(LocalHeadAccount) & ' - ' & Clip(wob:HeadAccountNumber)
                    Of 'CUSTOMER'
                        local:DeliveredTo = Clip(LocalHeadAccount) & ' - ' & Clip(job:Account_Number)
                    Of '3RD PARTY'
                        local:DeliveredTo = Clip(LocalHeadACcount) & ' - ' & Clip(job:Third_Party_Site)
                End !Case joc:DespatchTo
            Of 'RRC'
                Case joc:DespatchTo
                    Of 'ARC'
                        local:DeliveredTo = Clip(wob:HeadAccountNumber) & ' - ' & Clip(LocalHeadAccount)
                    Of 'CUSTOMER'
                        local:DeliveredTo = Clip(wob:HeadAccountNumber) & ' - ' & Clip(job:Account_Number)
                End !Case joc:DespatchTo
        End !Case joc:DespatchFrom
        WriteColumn(Clip(local:DeliveredTo))
        ! Inserting (DBH 25/01/2008) # 9612 - Show delivery address
        !Delivery Address
        WriteColumn(job:Company_Name_Delivery)
        WriteColumn(job:Address_Line1_Delivery)
        WriteColumn(job:Address_Line2_Delivery)
        WriteColumn(job:Address_line3_Delivery)
        WriteColumn(job:Postcode_Delivery)
        ! End (DBH 25/01/2008) #9612

        !Courier
        WriteColumn(local:Courier)
        ! Inserting (DBH 25/01/2008) # 9612 - Show courier waybill number
        !Courier Waybill Number
        Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
            WriteColumn(jobe2:CourierWaybillNumber)
        Else ! If Access:JOBSE2.TryFtch(jobe2:RefNumberKey) = Level:Benign
            WriteColumn('')
        End ! If Access:JOBSE2.TryFtch(jobe2:RefNumberKey) = Level:Benign
        ! End (DBH 25/01/2008) #9612
        !Waybill Number
            !Does this courier print waybills? -  (DBH: 09-07-2003)
        Access:COURIER.Clearkey(cou:Courier_Key)
        cou:Courier = joc:Courier
        If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
            !Found
            If cou:PrintWaybill
                WriteColumn(joc:ConsignmentNumber)
            Else !If cou:PrintWaybill
                WriteColumn('')
            End !If cou:PrintWaybill
        Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
            !Error
            WriteColumn('')
        End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Consignment Number
        !3rd Party Consignment Number
        If joc:DespatchTo <> '3RD PARTY'
            WriteColumn(joc:ConsignmentNumber)
            WriteColumn('')
        Else !If joc:DespatchTo <> '3RD PARTY'
            WriteColumn('')
            WriteColumn(joc:ConsignmentNumber)
        End !If joc:DespatchTo <> '3RD PARTY'
        !Date Despatched
        WriteColumn(DateToString(joc:TheDate))
        !Exchange Unit
        If job:Exchange_Unit_Number <> 0
            WriteColumn('YES')
        Else !If job:Exchange_Unit_Number <> 0
            WriteColumn('NO')
        End !If job:Exchange_Unit_Number <> 0
        !Current Job Status
        WriteColumn(job:Current_Status)
        !Current Exchange Status
        If job:Exchange_Unit_Number <> 0
            WriteColumn(job:Exchange_Status)
        Else !If job:Exchange_Unit_Number <> 0
            WriteColumn('')
        End !If job:Exchange_Unit_Number <> 0
        !Despatch Type
        WriteColumn(joc:DespatchType)
        !Job Type
        If job:Chargeable_Job = 'YES'
            If job:Warranty_Job = 'YES'
                WriteColumn('Warranty/Chargeable')
            Else !If job:Warranty_Job = 'YES'
                WriteColumn('Chargeable')
            End !If job:Warranty_Job = 'YES'
        Else !If job:Chargeable_Job = 'YES'
            If job:Warranty_Job = 'YES'
                WriteColumn('Warranty')
            Else !If job:Warranty_Job = 'YES'
                WriteColumn('')
            End !If job:Warranty_Job = 'YES'
        End !If job:Chargeable_Job = 'YES'

    !_____________________________________________________________________


        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
            !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_Courier                                ROUTINE ! Courier
    DATA
Temp LIKE(job:Courier)
    CODE
        !-----------------------------------------------
        CASE LOC:ScanType
        OF 'Exc'
            Temp = job:Exchange_Courier
        OF 'Job'
            case DespatchStage
                of Stage:SentToARC
                    Temp = job:Incoming_Courier
                else
                    Temp = job:Courier
            end
        OF 'Loa'
            Temp = job:Loan_Courier
        END !CASE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ConsignmentNumber                             ROUTINE ! Consignment Number
    DATA
Temp LIKE(job:Consignment_Number)
    CODE
        !-----------------------------------------------
        CASE LOC:ScanType
        OF 'Exc'
            Temp = job:Exchange_Consignment_Number
        OF 'Job'
            Temp = job:Consignment_Number
        OF 'Loa'
            Temp = job:Loan_Consignment_Number
        END !CASE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DateDespatched                                 ROUTINE ! Date Despatched
    DATA
Temp STRING(20)
    CODE
        !-----------------------------------------------
        CASE LOC:ScanType
        OF 'Exc'
            Temp = DateToString(job:Exchange_Despatched)
        OF 'Job'
            case DespatchStage
                of Stage:SentToARC
                    Temp = DateToString(wob:DateJobDespatched)
                of Stage:SentTo3rdParty
                    Temp = DateToString(job:ThirdPartyDateDesp)
                else
                    Temp = DateToString(job:Date_Despatched)
            end
            
        OF 'Loa'
            Temp = DateToString(job:Loan_Despatched)
        END !CASE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ExchangeUnit                               ROUTINE ! Exchange Unit
    DATA
Temp STRING(3)
    CODE
        !-----------------------------------------------
        IF LOC:ScanType = 'Exc'
            Temp = 'YES'
        ELSE
            Temp = 'NO'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT


WriteColumn_CurrentExchangeStatus                           ROUTINE ! Current Exchange Status'
    DATA
Temp LIKE(job:Current_Status)
    CODE
        !-----------------------------------------------
        CASE LOC:ScanType
        OF 'Exc'
            Temp = job:Exchange_Status
        OF 'Job'
            Temp = job:Current_Status
        OF 'Loa'
            Temp = job:Loan_Status
        END !CASE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT


WriteColumn_JobType                                         ROUTINE ! Job Type
    DATA
Temp STRING('Chargable/Warranty')
    CODE
        !-----------------------------------------------
        IF job:Warranty_Job = 'YES'
            IF job:Chargeable_Job = 'YES'
                Temp = 'Chargable/Warranty'
            ELSE
                Temp = 'Warranty'
            END !IF
        ELSE
            !-------------------------------------------
            ! 24 Oct 2002 John
            ! MICHALAN: CHARGEABLE spelt incorrectly in this colum
            Temp = 'Chargeable'
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_WayBillNumber                                ROUTINE ! WayBillNumber
    DATA
Temp STRING(10)
    CODE
        !-----------------------------------------------
        IF WEBJOB_OK = True
            CASE LOC:ScanType
            OF 'Exc'
                if DespatchStage = Stage:SentToRRC
                    Temp = job:Exchange_Consignment_Number
                else
                    Temp = wob:ExcWayBillNumber
                end
            OF 'Job'
                case DespatchStage
                    of Stage:SentToARC
                        Temp = job:Incoming_Consignment_Number
                    of Stage:SentToRRC
                        Temp = job:Consignment_Number
                    of Stage:SentTo3rdParty
                        Temp = ''   ! No waybill
                    of Stage:SentToCustomer
                        Temp = wob:JobWayBillNumber
                    else
                        Temp = job:Consignment_Number
                end
            OF 'Loa'
                Temp = wob:LoaWayBillNumber
            END !CASE
        ELSE
            CASE LOC:ScanType
            OF 'Exc'
                Temp = job:Exchange_Consignment_Number
            OF 'Job'
                Temp = job:Consignment_Number
            OF 'Loa'
                Temp = job:Loan_Consignment_Number
            END !CASE
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DeliveredTo                         ROUTINE
        !-----------------------------------------------
    DATA
deliveredTo STRING(40)                                           ! LIKE(job:Company_Name_Delivery) + 10 chars
    CODE
        ! 24 Oct 2002 John
        ! MICHALAN: Insert a column before Courier indicating the delivery address ie. Where the unit was delivered to
        ! 08 Nov 2002 Gary
        ! MICHALAN : Currently it only shows the customer address but does not show if the unit was sent to ARC for repairs
        case DespatchStage
            of Stage:SentToARC
                deliveredTo = CLIP(job:Company_Name_Delivery) & ' (ARC)'
            of Stage:SentTo3rdParty
                deliveredTo = CLIP(job:Third_Party_Site)
            of Stage:SentToRRC
                deliveredTo = CLIP(saq:AccountNumber)
            else
                deliveredTo = CLIP(job:Company_Name_Delivery)
        end

        WriteColumn( deliveredTo                               ) ! Delivered To
!-----------------------------------------------
WriteDataSummary                                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary details
        !
        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'No Exchange Jobs Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}                      = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}                      = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.NumberFormat'}                 = '0'
            Excel{'ActiveCell.Formula'}                      = '=SUBTOTAL(2, a' & sheet:DataHeaderRow+1 & ':a' & sheet:DataHeaderRow+RecordCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
QueueCount LONG
QueueIndex           LONG

TopRow     LONG
CurrentRow LONG
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        DO XL_ColFirst
        !-----------------------------------------------------------------
        !
        TopRow = Excel{'ActiveCell.Row'}

        Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Manufacturer'
        Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Model'
        Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Courier'
        Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Total Units Consigned'

        Excel{'Range("A' & TopRow & ':' & sheet:HeadLastCol & TopRow & '").Select'}
                DO XL_SetColumnHeader

        Excel{'Range("A' & TopRow+1 & '").Select'}
        !-----------------------------------------------------------------
        QueueCount = RECORDS(SummaryQueue)
        IF QueueCount < 1
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'No Jobs Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        SORT(SummaryQueue, '+sq_Manufacturer, +sq_Courier')
        LOOP x# = 1 TO QueueCount
            !-------------------------------------------------------------
            GET(SummaryQueue, x#)
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0, 1).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = sq:Manufacturer
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = sq:ModelNumber
            Excel{'ActiveCell.Offset(0, 2).Formula'}      = sq:Courier
            Excel{'ActiveCell.Offset(0, 3).Formula'}      = sq:TotalUnitsConsigned
            !-------------------------------------------------------------
            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("a' & TopRow & ':' & sheet:HeadLastCol & (TopRow+QueueCount) & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        CurrentRow = TopRow+QueueCount+1

        Excel{'Range("a' & CurrentRow & ':' & sheet:HeadLastCol & (CurrentRow) & '").Select'}
            DO XL_SetTitle
            DO XL_SetGrid
        !-----------------------------------------------------------------
        Excel{'Range("a' & CurrentRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Offset(0, 0).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0, 1).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
!            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '#,##0'
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Totals'
!            Excel{'ActiveCell.Offset(0, 2).Formula'} = '=SUBTOTAL(9, C' & TopRow & ':C' & TopRow+QueueCount & ')'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = '=SUBTOTAL(9, D' & TopRow & ':D' & TopRow+QueueCount & ')'
!            Excel{'ActiveCell.Offset(0, 4).Formula'} = '=SUBTOTAL(9, E' & TopRow & ':E' & TopRow+QueueCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & CurrentRow+2 & '").Select'}

!        DO WriteCourierSummary
        !-----------------------------------------------------------------
    EXIT

!WriteCourierSummary                                              ROUTINE
!    DATA
!QueueCount LONG
!QueueIndex           LONG
!
!TopRow     LONG
!CurrentRow LONG
!    CODE
!        !-----------------------------------------------------------------
!        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
!        DO XL_ColFirst
!        !-----------------------------------------------------------------
!        FREE(Courier_Queue)
!        LOOP x# = 1 TO RECORDS(SummaryQueue)
!            GET(SummaryQueue, x#)
!            !UpdateCourierQueue(sq:Courier, sq:WarrantyCount+sq:ChargeableCount)
!        END !LOOP
!        !-----------------------------------------------------------------
!    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
GetEngineer                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------  
        eq:Engineer  = job:Engineer
        GET(EngineerQueue, +eq:Engineer)

        IF NOT ERRORCODE()
            ! In queue - return
            EXIT !
        END !IF
        !-----------------------------------------------------------------
        ! Not in queue - ADD
        !-----------------------------------------------------------------
        DO LoadEngineer
        IF Result
            eq:Name = CLIP(use:Surname) & ', ' & CLIP(use:Forename)
        ELSE
            eq:Name = job:Engineer
        END !IF

        eq:Engineer  = job:Engineer
        ADD(EngineerQueue, +eq:Engineer)
        !-----------------------------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Courier Report By Manufacturer'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!               ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(tra_ali:Account_Number) & '  VODR0018 ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0018 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG', |
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
UpdateSummaryQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        sq:Manufacturer  = job:Manufacturer
        sq:Courier       = job:Exchange_Courier
        sq:ModelNumber   = job:Model_Number
        GET(SummaryQueue, '+sq_Manufacturer, +sq_Courier')
        CASE ERRORCODE()
        OF 00 ! Found
            IF    (sq:Manufacturer = job:Manufacturer) AND (sq:Courier = job:Exchange_Courier)
                ! NULL
            ELSE
                DO UpdateSummaryQueue_ADD ! Only partial match
            END !IF

        OF 30 ! NOT Found
            DO UpdateSummaryQueue_ADD

        ELSE
            CancelPressed = True

            EXIT
        END !IF
        !-----------------------------------------------------------------
        IF job:Warranty_Job = 'YES'
            IF job:Chargeable_Job = 'YES'
                sq:ChargeableCount += 1
                sq:WarrantyCount   += 1

            ELSE
                sq:WarrantyCount += 1

            END !IF
        ELSE
            sq:ChargeableCount += 1

        END !IF

        sq:TotalUnitsConsigned += 1

        PUT(SummaryQueue, '+sq_Manufacturer, +sq_Courier')
        !-----------------------------------------------------------------
    EXIT
UpdateSummaryQueue_ADD                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! Not in queue - ADD
        CLEAR(SummaryQueue)
            sq:Manufacturer        = job:Manufacturer
            sq:Courier             = job:Exchange_Courier
        ADD(SummaryQueue, '+sq_Manufacturer, +sq_Courier')
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
LoadEngineer                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = job:Engineer

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> job:Engineer
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadSTOMODEL                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:STOMODEL.ClearKey(stm:Model_Number_Key)
        stm:Ref_Number    = shi:Ref_Number
        !stm:manufacturer = sto:manufacturer
        set(stm:Model_Number_Key, stm:Model_Number_Key)

        IF Access:STOMODEL.NEXT()
            EXIT
        END !IF
!        IF Access:STOMODEL.TryFetch(stm:Model_Number_Key)
!            EXIT
!        END !IF

        IF stm:Ref_Number <> shi:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadWARPARTS                                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number

        IF Access:WARPARTS.TryFetch(wpr:Part_Number_Key)
            EXIT
        END !IF

        IF wpr:Ref_Number <> job:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncINVOICE                                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result    = False
        InvoiceOK = False

        IF job:Invoice_Number = 0
            EXIT
        END !IF

        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number
        set(inv:Invoice_Number_Key, inv:Invoice_Number_Key)

        IF Access:INVOICE.NEXT()
            EXIT
        END !IF

        IF inv:Invoice_Number <> job:Invoice_Number
            EXIT
        END !IF

        Result    = True
        InvoiceOK = True
        !-----------------------------------------------
    EXIT
SyncSTOCK                                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number

        IF Access:STOCK.TryFetch(sto:Ref_Number_Key)
            EXIT
        END !IF

        IF sto:Ref_Number <> wpr:Part_Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
Load_StockHistory                                             ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
! !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
!        IF CancelPressed
!            EXIT
!        END !IF
!        !------------------------------------------
!        !
!        Access:STOHIST.ClearKey(shi:DateKey)
!        shi:Date = LOC:StartDate
!        SET(shi:DateKey, shi:DateKey)
!        !------------------------------------------
!        Progress:Text    = 'Loading Stock History Details'
!        RecordsToProcess = RECORDS(STOHIST)
!        RecordsProcessed = 0
!        RecordCount      = 0
!
!        DO ProgressBar_LoopPre
! !message('Start of main loop')
!        !-----------------------------------------------------------------
!        LOOP UNTIL Access:STOHIST.Next()
!            !-------------------------------------------------------------
!            DO ProgressBar_Loop
!
!            IF CancelPressed
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            IF shi:Date < LOC:StartDate
!                CYCLE
!            ELSIF shi:Date > LOC:EndDate
!                BREAK
!            END !IF
!
!            DO SaveStockDetailsToQueue
!            !-------------------------------------------------------------
!        END !LOOP
! !message('End of main loop')
!
!        IF CancelPressed
!            EXIT
!        END !IF
!
!        DO ProgressBar_LoopPost
!        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'

        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***

            Do ProgressBar_EndPrintRun
            close(progresswindow)

        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mstop.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False  ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A10").Select'}
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT

XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0 ! Default/error value
        OperatingSystem     = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020601'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CourierReportByManufacturer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOMODEL.UseFile
  Access:JOBS.UseFile
  Access:INVOICE.UseFile
  Access:WARPARTS.UseFile
  Access:TRADEACC.UseFile
  Access:PARTS.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:COURIER.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  Access:LOAN.UseFile
  Access:TRDPARTY.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ============ Set Version Number ================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = Clip(tmp:versionNumber) & '5002'
  loc:Version = tmp:VersionNumber
  ?ReportVersion{prop:Text} = 'Report Version: ' & loc:Version
      LOC:ProgramName         = 'Courier Report By Manufacturer' !            Job1278         Cust=N18
  
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
      excel:Visible      = False
  
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
  
      IF GUIMode = 1 THEN
         LocalTimeOut = 500
         DoAll = 'Y'
         MainWindow{PROP:ICONIZE} = TRUE
      END !IF
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020601'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020601'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020601'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            haq:AccountNumber            = IN:AccountNumber

            IF LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = tra:Company_Name
                haq:BranchIdentification = tra:BranchIdentification

!                IF tra:Invoice_Sub_Accounts = 'YES'
!                    haQ:InvoiceSubAccounts = True
!                ELSE
!                    haQ:InvoiceSubAccounts = False
!                END !IF
!
!                haQ:PartsVATCode         = tra:Parts_VAT_Code
!                haQ:LabourVATCode        = tra:Labour_VAT_Code
!                haQ:RetailVATCode        = tra:Retail_Discount_Code
!
!                haQ:PartsVATRate         = GetVATRate(tra:Parts_VAT_Code)
!                haQ:LabourVATRate        = GetVATRate(tra:Labour_VAT_Code)
!                haQ:RetailVATRate        = GetVATRate(tra:Retail_Discount_Code)
            ELSE
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'

!                haQ:PartsVATCode         = ''
!                haQ:LabourVATCode        = ''
!                haQ:RetailVATCode        = ''
!
!                haQ:PartsVATRate         = 0.00
!                haQ:LabourVATRate        = 0.00
!                haQ:RetailVATRate        = 0.00
            END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            saq:AccountNumber            = IN:AccountNumber

            IF LoadSUBTRACC(job:Account_Number)
                GetHeadAccount(sub:Main_Account_Number)

                saq:AccountName          = sub:Company_Name
                saQ:HeadAccountNumber    = sub:Main_Account_Number
                saQ:HeadAccountName      = haQ:AccountName
                saQ:BranchIdentification = haQ:BranchIdentification

!                If haQ:InvoiceSubAccounts = True
!                    saQ:PartsVATCode         = sub:Parts_VAT_Code
!                    saQ:LabourVATCode        = sub:Labour_VAT_Code
!                    saQ:RetailVATCode        = sub:Retail_Discount_Code
!
!                    saQ:PartsVATRate         = GetVATRate(sub:Parts_VAT_Code)
!                    saQ:LabourVATRate        = GetVATRate(sub:Labour_VAT_Code)
!                    saQ:RetailVATRate        = GetVATRate(sub:Retail_Discount_Code)
!                Else
!                    saQ:PartsVATCode         = saQ:PartsVATCode
!                    saQ:LabourVATCode        = saQ:LabourVATCode
!                    saQ:RetailVATCode        = saQ:RetailVATCode
!
!                    saQ:PartsVATRate         = haQ:PartsVATRate
!                    saQ:LabourVATRate        = haQ:LabourVATRate
!                    saQ:RetailVATRate        = haQ:RetailVATRate
!                End!If tra:use_sub_accounts = 'YES'

            ELSE
                saq:AccountName          = ''
                saq:HeadAccountNumber    = ''
                saQ:HeadAccountName      = ''
                saQ:BranchIdentification = ''

!                haQ:PartsVATCode         = ''
!                haQ:LabourVATCode        = ''
!                haQ:RetailVATCode        = ''
!
!                haQ:PartsVATRate         = 0.00
!                haQ:LabourVATRate        = 0.00
!                haQ:RetailVATRate        = 0.00
            END !IF

            ADD(SubAccount_Queue, +saq:AccountNumber)
        ELSE
            CancelPressed = True
        END !CASE
        !-----------------------------------------------------------------
GetWebJobNumber PROCEDURE( IN:JobNumber )! STRING
    CODE
        !-----------------------------------------------
        IF NOT LoadWEBJOB( job:Ref_Number)
            RETURN ''
        END !IF

        RETURN IN:JobNumber & '-' & saQ:BranchIdentification & wob:JobNumber
        !-----------------------------------------------
GetExchangeIMEI PROCEDURE( IN:ExchangeUnitNo )! STRING
    CODE
        !-----------------------------------------------
        IF IN:ExchangeUnitNo = 0
            RETURN ''
        END

        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = IN:ExchangeUnitNo
        If Access:EXCHANGE.Fetch(xch:Ref_Number_Key) = Level:Benign
            RETURN xch:ESN
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            RETURN ''
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBSE      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = IN:JobNumber

        IF Access:JOBSE.Fetch(jobe:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
!-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 200
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0

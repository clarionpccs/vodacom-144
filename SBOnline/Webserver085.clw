

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER085.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
saveJob              PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    do openFiles

    Access:WEBJOB.Clearkey(wob:refNumberKey)
    wob:refNumber    = p_web.GSV('wob:ref_Number')
    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(WEBJOB)
        access:WEBJOB.tryUpdate()
    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

    Access:JOBS.Clearkey(job:ref_Number_Key)
    job:ref_Number    = p_web.GSV('wob:RefNumber')
    if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBS)
        access:JOBS.tryupdate()
    else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE)
        access:JOBSE.tryupdate()
    else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)


    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE2)
        access:JOBSE2.tryupdate()
    else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)


    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBNOTES)
        if access:JOBNOTES.tryupdate()
        end
    else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS.Close
     Access:JOBSE.Close
     Access:JOBNOTES.Close
     Access:JOBSE2.Close
     Access:WEBJOB.Close
     FilesOpened = False
  END



   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER397.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Add script line to packet string (trailing ; added)
!!! </summary>
CreateScript         PROCEDURE  (*STRING pPacket,STRING pScript) ! Declare Procedure
packetString STRING(16384)

  CODE
        packetString = pPacket
        pPacket = CLIP(pPacket) & '<script>' & CLIP(pScript) & ';</script>'        



   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER182.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CheckParts           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locReturnValue       BYTE                                  !
FilesOpened     BYTE(0)

  CODE
    p_web.SSV('CheckParts:ReturnValue',0)

    do openFiles

    do CheckParts

    do closeFiles

    p_web.SSV('CheckParts:ReturnValue',locReturnValue)
checkParts    routine
    Data
tmp:FoundReceived   Byte()
    Code
!Return (1) = Pending Order
!Return (2) = On Order
!Return (3) = Back Order Spares
!Return (4) = Parts Recevied
!Return (0) = Received Order Or No Parts With Anything To Do WIth Orders
    tmp:FoundReceived = False
    locReturnValue = 0
    Case p_web.GSV('CheckParts:Type')
        Of 'C'
            If p_web.GSV('job:Chargeable_Job') = 'YES'
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = p_web.GSV('job:ref_number')
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = par:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = par:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If par:order_number <> ''
                        If par:date_received = ''
                            locReturnValue = 2
                            exit

                        Else
                            tmp:FoundReceived = True
                        End!If par:date_received = ''
                    End!If par:order_number <> ''
                end !loop
            End ! If p_web.GSV('job:Chargeable_Job = 'YES'

        Of 'W'
            If p_web.GSV('job:Warranty_Job') = 'YES'
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = p_web.GSV('job:ref_number')
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If wpr:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = wpr:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = wpr:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)

                    If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If wpr:order_number <> ''
                        If wpr:date_received = ''
                            locReturnValue = 2
                            exit
                        Else!If wpr:date_recieved = ''
                            tmp:FoundReceived = True
                        End!If wpr:date_recieved = ''
                    End!If wpr:order_number <> ''
                end !loop

            End ! If p_web.GSV('job:Warranty_Job = 'YES'

    End!Case f_type

    If tmp:FoundReceived = True
        !All parts received - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 4
        exit
    Else ! If tmp:FoundReceived = True
        !No parts attached - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 0
    End ! If tmp:FoundReceived = True


!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:STOCK.Close
     FilesOpened = False
  END

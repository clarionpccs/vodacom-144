

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER493.INC'),ONCE        !Local module procedure declarations
                     END


TagInternalLocations PROCEDURE  (NetWebServerWorker p_web)
tagField             STRING(1)                             !
BookingSiteLocation  STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(LOCINTER)
                      Project(loi:Location)
                      Project(loi:Location)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
SBO_GenericTagFile::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('TagInternalLocations')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('TagInternalLocations:NoForm')
      loc:NoForm = p_web.GetValue('TagInternalLocations:NoForm')
      loc:FormName = p_web.GetValue('TagInternalLocations:FormName')
    else
      loc:FormName = 'TagInternalLocations_frm'
    End
    p_web.SSV('TagInternalLocations:NoForm',loc:NoForm)
    p_web.SSV('TagInternalLocations:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('TagInternalLocations:NoForm')
    loc:FormName = p_web.GSV('TagInternalLocations:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('TagInternalLocations') & '_' & lower(loc:parent)
  else
    loc:divname = lower('TagInternalLocations')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(LOCINTER,loi:Location_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TAGF:TAGGED') then p_web.SetValue('TagInternalLocations_sort','3')
    ElsIf (loc:vorder = 'LOI:LOCATION') then p_web.SetValue('TagInternalLocations_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('TagInternalLocations:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('TagInternalLocations:LookupFrom','LookupFrom')
    p_web.StoreValue('TagInternalLocations:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('TagInternalLocations:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('TagInternalLocations:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('TagInternalLocations_sort',net:DontEvaluate)
  p_web.SetSessionValue('TagInternalLocations_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'tagf:Tagged','-tagf:Tagged')
    Loc:LocateField = 'tagf:Tagged'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loi:Location)','-UPPER(loi:Location)')
    Loc:LocateField = 'loi:Location'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(loi:Location)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tagf:Tagged')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('TagInternalLocations_LocatorPic','@n-14')
  Of upper('loi:Location')
    loc:SortHeader = p_web.Translate('Location')
    p_web.SetSessionValue('TagInternalLocations_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('TagInternalLocations:LookupFrom')
  End!Else
  loc:formaction = 'TagInternalLocations'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="TagInternalLocations:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="TagInternalLocations:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('TagInternalLocations:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="LOCINTER"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="loi:Location_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagInternalLocations',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2TagInternalLocations',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="TagInternalLocations.locate(''Locator2TagInternalLocations'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'TagInternalLocations.cl(''TagInternalLocations'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="TagInternalLocations_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="TagInternalLocations_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','TagInternalLocations','','Click here to sort by ',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by ')&'">'&p_web.Translate('')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','TagInternalLocations','Location','Click here to sort by Location',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Location')&'">'&p_web.Translate('Location')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('loi:location',lower(Thisview{prop:order}),1,1) = 0 !and LOCINTER{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'loi:Location'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('loi:Location'),p_web.GetValue('loi:Location'),p_web.GetSessionValue('loi:Location'))
      loc:FilterWas = 'UPPER(loi:Location_Available) = ''YES'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagInternalLocations',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('TagInternalLocations_Filter')
    p_web.SetSessionValue('TagInternalLocations_FirstValue','')
    p_web.SetSessionValue('TagInternalLocations_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,LOCINTER,loi:Location_Key,loc:PageRows,'TagInternalLocations',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If LOCINTER{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(LOCINTER,loc:firstvalue)
              Reset(ThisView,LOCINTER)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If LOCINTER{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(LOCINTER,loc:lastvalue)
            Reset(ThisView,LOCINTER)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(loi:Location)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'TagInternalLocations.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'TagInternalLocations.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'TagInternalLocations.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'TagInternalLocations.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagInternalLocations',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('TagInternalLocations_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('TagInternalLocations_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1TagInternalLocations',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="TagInternalLocations.locate(''Locator1TagInternalLocations'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'TagInternalLocations.cl(''TagInternalLocations'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('TagInternalLocations_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('TagInternalLocations_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'TagInternalLocations.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'TagInternalLocations.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'TagInternalLocations.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'TagInternalLocations.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
        ! Set Queue Record
    Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
    tagf:SessionID = p_web.SessionID
    tagf:TagType    = kInternalLocation
    tagf:TaggedValue = loi:Location 
    IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged))
        tagf:Tagged = 0
    END ! IF
    
    loc:field = loi:Location
    p_web._thisrow = p_web._nocolon('loi:Location')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('TagInternalLocations:LookupField')) = loi:Location and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((loi:Location = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="TagInternalLocations.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If LOCINTER{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(LOCINTER)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If LOCINTER{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(LOCINTER)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','loi:Location',clip(loc:field),,loc:checked,,,'onclick="TagInternalLocations.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','loi:Location',clip(loc:field),,'checked',,,'onclick="TagInternalLocations.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tagf:Tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loi:Location
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="TagInternalLocations.omv(this);" onMouseOut="TagInternalLocations.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var TagInternalLocations=new browseTable(''TagInternalLocations'','''&clip(loc:formname)&''','''&p_web._jsok('loi:Location',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('loi:Location')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'TagInternalLocations.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'TagInternalLocations.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2TagInternalLocations')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1TagInternalLocations')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1TagInternalLocations')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2TagInternalLocations')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(LOCINTER)
  p_web._CloseFile(SBO_GenericTagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(LOCINTER)
  Bind(loi:Record)
  Clear(loi:Record)
  NetWebSetSessionPics(p_web,LOCINTER)
  p_web._OpenFile(SBO_GenericTagFile)
  Bind(tagf:Record)
  NetWebSetSessionPics(p_web,SBO_GenericTagFile)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('loi:Location',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(LOCINTER)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tagf:Tagged')
    do Validate::tagf:Tagged
  End
  p_web._CloseFile(LOCINTER)
! ----------------------------------------------------------------------------------------
Validate::tagf:Tagged  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  loi:Location = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(LOCINTER,loi:Location_Key)
  p_web.FileToSessionQueue(LOCINTER)
  loc:was = tagf:Tagged
  tagf:Tagged = Choose(p_web.GetValue('value') = 1,1,0)
        ! Before EIP For Field
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType    = kInternalLocation
        tagf:TaggedValue = loi:Location 
        IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged))
            tagf:Tagged = CHOOSE(p_web.GetValue('value') = 1,TRUE,FALSE)
            Access:SBO_GenericTagFile.TryInsert()
        ELSE
            tagf:Tagged = CHOOSE(p_web.GetValue('value') = 1,TRUE,FALSE)
            Access:SBO_GenericTagFile.TryUpdate()
        END ! IF
  ! Validation goes here, if fails set loc:invalid & loc:alert
  ! Automatic Dictionary Validation
    If tagf:Tagged <> 1 and tagf:Tagged <> 0
      loc:Invalid = 'tagf:Tagged'
      loc:Alert = clip('') & ' ' & clip(p_web.site.OneOfText) & ' ' & 1 & ' / ' & 0
    End
  do CheckForDuplicate
  If loc:invalid = '' ! save record
      p_web._updatefile(SBO_GenericTagFile)
      p_web.FileToSessionQueue(SBO_GenericTagFile)
  End
  do SendAlert
  if loc:alert <> ''
    tagf:Tagged = loc:was
    do Value::tagf:Tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tagf:Tagged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tagf:Tagged_'&loi:Location,,net:crc)
      loc:extra = Choose(tagf:Tagged = 1,'checked','')
      packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tagf:Tagged'&loi:Location),clip(1),,loc:extra & ' ' & loc:disabled,,,'onclick="TagInternalLocations.eip(this,'''&p_web._jsok('tagf:Tagged')&''','''&p_web._jsok(loc:viewstate)&''');"',,) & '<13,10>'
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loi:Location   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loi:Location_'&loi:Location,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loi:Location,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(SBO_GenericTagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_GenericTagFile)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(loi:Location_Key)
    loc:Invalid = 'loi:Location'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Location_Key --> '&clip('Location')&' = ' & clip(loi:Location)
  End
PushDefaultSelection  Routine
  loc:default = loi:Location

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('loi:Location',loi:Location)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('loi:Location',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('loi:Location'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('loi:Location'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CallSiebelExt        PROCEDURE  (NetwebServerWorker p_web) ! Declare Procedure
    MAP
trace	PROCEDURE(STRING pText)	
    END
!region Local File and Variables Setup
!Output file from Brian's programme - pipe deliniated
SFileName                   STRING(255), STATIC
LFileName                   STRING(255), STATIC
SiebelFile                  FILE, DRIVER('BASIC', '/COMMA=124 /ALWAYSQUOTE=OFF'), PRE(SBF), NAME(SFileName), CREATE, BINDABLE, THREAD
SFRec                           RECORD
SFField1                            STRING(80)
SFField2                            STRING(80)
SFField3                            STRING(80)
SFField4                            STRING(80)
SFField5                            STRING(80)
SFField6                            STRING(80)
SFField7                            STRING(80)
SFField8                            STRING(80)
SFField9                            STRING(250)    !large to hold the loyalty status
                                END ! record
                            END ! file

!logging file for errors
LoggingFile                 FILE, DRIVER('BASIC'), PRE(LOUT), NAME(LFileName), CREATE
LFRec                           RECORD
LFField1                            STRING(80)
LFField2                            STRING(80)
LFField3                            STRING(80)
LFField4                            STRING(80)
LFField5                            STRING(80)
LFField6                            STRING(80)
LFField7                            STRING(80)
LFField8                            STRING(80)
                                END ! record
                            END ! file
locErrorCode                LONG	
locErrorDescription         STRING(255)
locErrorCodeDescription     STRING(80)

FMWUserName                 STRING(100)
FMWPassword                 STRING(100)
CustomerLookup              STRING(1)
IdentifyStack               STRING(1)
CustomerExceptionFolder     STRING(255)
CallUsing                   STRING(10)
PathToSiebel                STRING(255)
PathToSiebel2               STRING(255)
PathToExe                   STRING(255)
SB2KDEFINI                  STRING(255)

ReadField1                  String(80)
ReadField2                  String(80)
ReadField3                  String(80)
ReadField4                  String(80)
ReadField5                  String(80)
ReadField6                  String(80)
ReadField7                  String(80)
ReadField8                  String(80)
ReadField9                  String(255)

locSoapAction               STRING(80)

SiebelRefNo                 STRING(30) ! id to reference the Siebel entries (can't use job no)
!endregion

  CODE
!region ProcessedCode

        trace('CallSiebel')
	
        Relate:C3DMONIT.Open()
        Relate:SOAPERRS.Open()
        Relate:SMSMAIL.Open()
        Relate:SUBURB.Open()
	
        SB2KDEFINI = PATH() & '\SB2KDEF.INI'
	
        CustomerLookup = GETINI('C3D','CustomerLookup','N',SB2KDEFINI)
		
        p_web.SSV('SiebelRefNo','') ! Clear Var incase it fails
		
        IF (CustomerLookup = 'N')
            
        ELSE ! IF (CustomerLookup = 'N')
            SiebelRefno = RANDOM(1,1000000) ! Hopefully this is random enough
            p_web.SSV('SiebelRefNo',SiebelRefNo) ! Store for retrieving when job no is known
				
            FMWUserName             = GETINI('C3D','FMWUserName',    '',    SB2KDEFINI)
            FMWPassword             = GETINI('C3D','FMWPassword',    '',    SB2KDEFINI)
            IdentifyStack           = GETINI('C3D','IdentifyStack',  '',    SB2KDEFINI)
            CustomerExceptionFolder = GETINI('C3D','ExceptionFolder','',    SB2KDEFINI)
            CallUsing               = GETINI('C3D','CallUsing',      '',    SB2KDEFINI)
            PathToSiebel            = GETINI('C3D','PathToSiebel',   '',    SB2KDEFINI)
            PathToExe               = GETINI('C3D','PathToExe',      '',    SB2KDEFINI)
            PathToSiebel2           = GETINI('C3D','PathToSiebel2',  '',    SB2KDEFINI)			
		
			! Check everything is filled in
            locErrorCode = 0
            locErrorDescription = ''
            IF (CLIP(FMWUserName) 				= '') THEN locErrorCode += 1.
            IF (CLIP(FMWPassword) 				= '') THEN locErrorCode += 2.
            IF (CLIP(CustomerLookup) 			= '') THEN locErrorCode += 4.
            IF (CLIP(IdentifyStack) 			= '') THEN locErrorCode += 8.
            IF (CLIP(CustomerExceptionFolder) 	= '') THEN locErrorCode += 16.
            IF (CLIP(CallUsing) 				= '') THEN locErrorCode += 32.
            IF (CLIP(PathToSiebel) 				= '') THEN locErrorCode += 64.
            IF (CLIP(PathToExe) 				= '') THEN locErrorCode += 128.
			
            IF (~EXISTS(PathToExe)) 				  THEN locErrorCode += 256.
            IF (~EXISTS(CLIP(PathToExe) & '\SiebelCom.exe')) THEN locErrorCode += 512.
			
            IF (locErrorCode > 0)
                locErrorCodeDescription = 'SETUP'
                locErrorDescription = 'CODE =' & locErrorCode
            ELSE ! IF (locErrorCode > 0)
                rtn# = RUNDOS(CLIP(PathToExe)&'\SiebelCom.exe '&|
                    '  "Username:' & CLIP(FMWUserName) & |
                    '" "Password:' & CLIP(FMWPassword) & |
                    '" "URL:'      & CLIP(PathToSiebel) & |
                    '" "URL2:'     & CLIP(PathToSiebel2) & |
                    '" "MSISDN:'   & p_web.GSV('job:Mobile_Number') & |
                    '" "Output:'& CLIP(PathToExe) & '\S1' & CLIP(SiebelRefno) & '.in"',1)
								
                trace('After SiebelCom. SiebelRefNo = ' & SiebelRefNo)
								
				! Look for incoming data file
                SFileName = CLIP(PathToExe) & '\S1' & CLIP(SiebelRefno) & '.in'
				
                IF (~EXISTS(SFileName))
                    locErrorCodeDescription = 'SYSTEM'
                    locErrorDescription = 'RETURNING FILE NOT FOUND ' & CLIP(SFilename)
                ELSE ! IF (~EXISTS(SFileName)
				
                    OPEN(SiebelFile)
                    SET(SiebelFile)
                    NEXT(SiebelFile)
                    IF (ERRORCODE())
						! Could not get first record
                        locErrorCodeDescription = 'SYSTEM'
                        locErrorDescription = 'UNABLE TO READ RETURNED FILE'
                    ELSE ! IF (ERRORCODE())	
                        IF (UPPER(CLIP(SBF:SFField1)) = 'ERROR')
							! Returned an error
                            NEXT(SiebelFile)
							! Read error from next line
                            IF CLIP(SBF:SFField1) = '' THEN SBF:SFField1 = 'SYSTEM'.
                            locErrorCodeDescription = SBF:SFField1
                            locErrorDescription = SBF:SFField2
                        ELSE ! IF (UPPER(CLIP(SBF:SFField1)) = 'ERROR')
                            NEXT(SiebelFile) ! Move to send line
                            IF (ERRORCODE())
								! Could not get second record
                                locErrorCodeDescription = 'SYSTEM2'
                                locErrorDescription = 'UNABLE TO READ SECOND LINE'
                            ELSE ! IF (ERRORCODE())
                                locErrorCodeDescription = 'OK'
								
                                ReadField1 = SBF:SFField1       !Migration Status
                                ReadField2 = SBF:SFField2       !Account ID
                                ReadField3 = ''                  !'TITLE'
                                ReadField4 = SBF:SFField4       !'FORENAME'
                                ReadField5 = SBF:SFField5       !'SURNAME'
                                ReadField6 = SBF:SFField6       !'LINE1'
                                ReadField7 = SBF:SFField7       !'LINE2'
                                ReadField8 = SBF:SFField8       !'LINE3'
                                ReadField9 = SBF:SFField9		! Loyalty Status
                            END ! IF (ERRORCODE())
                        END ! IF (UPPER(CLIP(SBF:SFField1)) = 'ERROR')
                    END ! IF (ERRORCODE())
					
                    CLOSE(SiebelFile)
                    REMOVE(SiebelFile)
					
                    IF (locErrorCodeDescription = '' OR locErrorCodeDescription = 'OK') ! aside: I don't think a blank is possible
                        IF (ReadField1 = 'N')
                            locErrorCodeDescription = 'NOT MIGRATED'
                        END ! IF
                    END ! IF
					
                    IF (ReadField9 <> '')
						! If Loyalty Status Filled
                        p_web.SSV('jobe3:LoyaltyStatus',ReadField9)
                    END ! IF
                END ! IF (~EXISTS(SFileName)
            END ! IF (locErrorCode > 0)
			
            trace('Add To C3DMonit: ' & CLIP(locErrorCodeDescription))
			
			! Always write to C3DMONIT ?? Maybe do this at the end
            IF (Access:C3DMONIT.PrimeRecord() = Level:Benign)
                C3D:RefNumber = SiebelRefNo
                C3D:MSISDN    = p_web.GSV('job:Mobile_Number')
                C3D:CallDate  = TODAY()
                C3D:CallTime  = CLOCK()
                C3D:ErrorCode = locErrorCodeDescription
                IF (Access:C3DMONIT.TryInsert())
                    Access:C3DMONIT.CancelAutoInc()
                END ! IF
            END ! IF
			
			!look up the required action AUTO/MANUAL/MANUAL PLUS/RETRY
            Access:SOAPERRS.ClearKey(sop:KeyErrorCode)
            sop:ErrorCode = locErrorCodeDescription
            IF (Access:SOAPERRS.TryFetch(sop:KeyErrorCode))
                sop:Action = 'ERROR NOT FOUND'
            END ! IF
			
            trace('Soap Action: ' & CLIP(sop:Action))
			
			!All "errors" must be logged in an exception file together with the MSISDN that was queried. The log file will contain the following information:
            IF (locErrorCodeDescription <> 'OK')
                LFileName = CLIP(CustomerExceptionFolder) & '\MSISDN_Query_Exception_' &  FORMAT(TODAY(),@d10-) & '.csv'
				
                IF (~EXISTS(LFileName))
                    CREATE(LoggingFile)
                    SHARE(LoggingFile)
                    Lout:LFField1 = 'DATE'
                    Lout:LFField2 = 'TIME'
                    Lout:LFField3 = 'MOBILE'
                    Lout:LFField4 = 'ERROR'
                    Lout:LFField5 = 'FAULT'
                    Lout:LFField6 = 'TYPE'
                    Lout:LFField7 = 'DESCRIPTION'
                    Lout:LFField8 = 'DETAILS'
                    ADD(LoggingFile)						
                ELSE ! IF
                    SHARE(LoggingFile) ! File already exists. Open it and add to it
                END ! IF
				
                Lout:LFField1 = FORMAT(TODAY(),@d06)
                Lout:LFField2 = FORMAT(CLOCK(),@t04)
                Lout:LFField3 = p_web.GSV('job:Mobile_Number')
                Lout:LFField4 = CLIP(locErrorCodeDescription)
                Lout:LFField5 = CLIP(SOP:SoapFault)
                Lout:LFField6 = CLIP(SOP:ErrorType)
                Lout:LFField7 = CLIP(SOP:Description)
                Lout:LFField8 = locErrorDescription
                ADD(LoggingFile)
                CLOSE(LoggingFile)					
                
                trace('Error: ' & CLIP(locErrorDescription))
				
				!for some transactions failed an email must be sent to servicebase.support@vodacom.co.za with a copy of exception file.
                IF (sop:Action = 'MANUAL PLUS')
                    IF (Access:SMSMAIL.PrimeRecord() = Level:Benign)
                        sms:SendToSMS       = 'F'
                        sms:SendToEmail     = 'T'
                        sms:SMSSent         = 'F'
                        sms:EmailSent       = 'F'
                        sms:DateInserted    = TODAY()
                        sms:TimeInserted    = CLOCK()
                        sms:EmailSubject    = CLIP(SOP:Subject)
                        sms:EmailAddress    = 'servicebase.support@vodacom.co.za'
                        sms:PathToEstimate  = CLIP(LFileName)
                        sms:MSG             = CLIP(SOP:Body) &|
                            '<13,10>Date: '&FORMAT(TODAY(),@D06)&|
                            '<13,10>Time: '&FORMAT(CLOCK(),@t04)							
                        IF (Access:SMSMAIL.TryInsert())
                            Access:SMSMAIL.CancelAutoInc()
                        END ! IF
                    END ! IF
                END ! IF
				
            END ! IF (locErrorCodeDescription <> 'OK')
			
            IF (sop:Action = 'AUTO')
                p_web.SSV('job:Initial',ReadField4)
                p_web.SSV('job:Surname',ReadField5)
                p_web.SSV('job:Company_Name',CLIP(ReadField4) & ' ' & CLIP(ReadField5))
                p_web.SSV('job:Address_Line1',ReadField6)
                p_web.SSV('job:Address_Line2',ReadField7)
                p_web.SSV('job:Address_Line3',ReadField8)
				
                Access:SUBURB.ClearKey(sur:SuburbKey)
                sur:Suburb = p_web.GSV('job:Address_Line3')
                IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
                    p_web.SSV('job:Postcode',sur:Postcode)
                    p_web.SSV('jobe2:HubCustomer',sur:Hub)
                END ! IF					
            END ! IF
        END ! IF (CustomerLookup = 'N')
		
        Relate:SUBURB.Close()
        Relate:SMSMAIL.Close()
        Relate:SOAPERRS.Close()
        Relate:C3DMONIT.Close()
!endregion
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled

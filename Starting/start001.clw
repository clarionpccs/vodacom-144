

   MEMBER('starting.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('START001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

progress:thermometer BYTE
ThisWindowHandle     LONG
TRNColorRef          GROUP,PRE()
highorder            BYTE
blue                 BYTE
green                BYTE
red                  BYTE
                     END
window               WINDOW('ServiceBase 3g'),AT(,,308,107),FONT('Tahoma',8,010101H,),COLOR(COLOR:White,09A6A7CH,COLOR:White),CENTER,IMM,ICON('Cellular3g.ico'),TILED,TIMER(1),GRAY
                       IMAGE('sblogo.jpg'),AT(12,22),USE(?Image2)
                       PROMPT('Please wait...'),AT(168,42),USE(?Prompt1),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White)
                       PANEL,AT(8,2,292,14),USE(?Panel2),BEVEL(-1,-1)
                       PANEL,AT(8,94,293,4),USE(?Panel1)
                       PROGRESS,USE(progress:thermometer),AT(168,52,132,12),RANGE(0,100),COLOR(COLOR:White,COLOR:White,09A6A7CH)
                       PROMPT('Loading ServiceBase...'),AT(4,3,300,14),USE(?LoadingText),TRN,CENTER,FONT('Tahoma',12,COLOR:Gray,),COLOR(COLOR:White)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
      end
    end
    Display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  PUTINI('STARTING','Run',False,)
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
  
      recordstoprocess    = 500
  
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          If Command() <> ''
              ?LoadingText{Prop:Text} = 'Loading ' & Clip(Command() & '...')
          Else ! Command() <> ''
              Run('CELLRUN.EXE')
          End ! Command() <> ''
    OF EVENT:LoseFocus
      Error#    = SetWindowPos(0{Prop:Handle},-1,0,0,0,0,3)
    OF EVENT:Timer
              If GETINI('STARTING','Run',,) = True
                  Post(Event:CloseWindow)
              End ! If GETINI('STARTING','Run',,) = True
              Do GetNextRecord2
              If recordsprocessed >= 500
                  recordsprocessed        = 0
                  percentprogress         = 0
                  progress:thermometer    = 0
              End ! If recordsprocessed >= 2000
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue




   MEMBER('ImpExchValue.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('IMPEX001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
locManufacturer     STRING(30)

locSavePath         CSTRING(255)
locImportFile       CSTRING(255),STATIC
ImportFile          File,DRIVER('BASIC'),PRE(impfil),Name(locImportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
ModelNumber                 STRING(30)
SellingPrice                REAL()
                        END
                    END
locExportFile       CSTRING(255),STATIC
ExportFile          File,DRIVER('BASIC'),PRE(expfil),Name(locExportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
ModelNumber                 STRING(30)
SellingPrice                String(30)
ExceptionReason             STRING(255)
                        END
                    END

myProg        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
myProg:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(myProg.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(myProg.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(myProg.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?myProg:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
myProg:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(myProg.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?myProg:CNPrompt),FONT(,14,,FONT:bold)
     END
***

  CODE
    ! Command Line Check
    ! Manufacturer will be passed as the command line
    
    man# = INSTRING('!',COMMAND(),1,1)
    
    IF (man# = 0)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Message('This exe cannot be run from here.','ServiceBase',|
            Icon:Hand,'&OK',1) 
        Of 1 ! &OK Button
        End!Case Message
        RETURN
    ELSE
        locManufacturer = CLIP(SUB(COMMAND(),man# + 1,30))
    END
    
    
   Relate:MODELNUM.Open
    ! Processing
    locSavePath = Path()
    locImportFile = ''
    if (fileDialog('Choose File',locImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
        !Found
        setPath(locSavePath)
        
    else ! if (fileDialog)
        !Error
        setPath(locSavePath)
        RETURN
    end ! if (fileDialog)
        
    ! Set the exceptions file, if needed
    locExportFile = BHAddBackSlash(GETINI('EXCEPTION','ExchangeRate',,CLIP(PATH()) & '\SB2KDEF.INI')) & BHGetFileNoExtension(locImportFile) & FORMAT(TODAY(),@D012) & FORMAT(CLOCK(),@t05) & '.csv'
    REMOVE(locExportFile)
        
        
    OPEN(ImportFile)
    IF ERROR()
        STOP(ERROR())
        RETURN
    END
        
    ! Count Records
    SETCURSOR(CURSOR:Wait)
    countRec# = 0
    SET(ImportFile,0)
    LOOP
        NEXT(ImportFile)
        IF ERROR()
            BREAK
        END
        countRec# += 1
    END
    SETCURSOR()
    
    exceptionFound# = 0
    firstLine# = 1
        
    myProg.INIT(countRec#)
    SET(ImportFile,0)
    LOOP
        NEXT(ImportFile)
        IF (ERROR())
            BREAK
        END
        IF (myProg.Update())
            BREAK
        END
        
        ! Ignore First Line
        IF (firstLine# = 1)
            firstLine# = 0
            CYCLE
        END
            
        Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
        mod:Manufacturer = locManufacturer
        mod:Model_Number = UPPER(impfil:ModelNumber)
        IF (Access:MODELNUM.Tryfetch(mod:Manufacturer_Key) = Level:Benign)
            mod:ExchReplaceValue = impfil:SellingPrice
            Access:MODELNUM.TryUpdate()
        ELSE
            IF (exceptionFound# = 0)
                CREATE(ExportFile)
                OPEN(ExportFile)
                exceptionFound# = 1
            END
            expfil:ModelNumber = impfil:ModelNumber
            expfil:SellingPrice = impfil:SellingPrice
            expfil:ExceptionReason = 'Model Number Not Found'
            ADD(ExportFile)
        END
    END
        
    IF (exceptionFound#)
        CLOSE(ExportFile)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Message('Some errors have occurred. The exceptions can be viewed in the following file:||' & CLIP(locExportFile),'ServiceBase',|
            Icon:Hand,'&OK',1) 
        Of 1 ! &OK Button
        End!Case Message
    END
    
    CLOSE(ImportFile)
    
   Relate:MODELNUM.Close
myProg.Init                 PROCEDURE(LONG func:Records)
    CODE
        myProg.ProgressSetup(func:Records)
myProg.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Source
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        myProg.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(myProg:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        myProg.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.ResetProgress      Procedure(Long func:Records)
CODE

    myProg.recordsToProcess = func:Records
    myProg.recordsprocessed = 0
    myProg.percentProgress = 0
    myProg.progressThermometer = 0
    myProg.CNprogressThermometer = 0
    myProg.skipRecords = 0
    myProg.userText = ''
    myProg.CNuserText = ''
    myProg.percentText = '0% Completed'
    myProg.CNpercentText = myProg.percentText


myProg.Update      Procedure(<String func:String>)
    CODE
        RETURN (myProg.InsideLoop(func:String))
myProg.InsideLoop     Procedure(<String func:String>)
CODE

    myProg.SkipRecords += 1
    If myProg.SkipRecords < 20
        myProg.RecordsProcessed += 1
        Return 0
    Else
        myProg.SkipRecords = 0
    End
    if (func:String <> '')
        myProg.UserText = Clip(func:String)
        myProg.CNUserText = myProg.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        myProg.NextRecord()
        if (myProg.CancelLoop())
            return 1
        end ! if (myProg.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

myProg.ProgressText        Procedure(String    func:String)
CODE

    myProg.UserText = Clip(func:String)
    myProg.CNUserText = myProg.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.Kill     Procedure()
    CODE
        myProg.ProgressFinish()
myProg.ProgressFinish     Procedure()
CODE

    myProg.ProgressThermometer = 100
    myProg.CNProgressThermometer = 100
    myProg.PercentText = '100% Completed'
    myProg.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(myProg:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.NextRecord      Procedure()
CODE
    Yield()
    myProg.RecordsProcessed += 1
    !If myProg.percentprogress < 100
        myProg.percentprogress = (myProg.recordsprocessed / myProg.recordstoprocess)*100
        If myProg.percentprogress > 100 or myProg.percentProgress < 0
            myProg.percentprogress = 0
        End
        If myProg.percentprogress <> myProg.ProgressThermometer then
            myProg.ProgressThermometer = myProg.percentprogress
            myProg.PercentText = format(myProg:percentprogress,@n3) & '% Completed'
        End
    !End
    myProg.CNPercentText = myProg.PercentText
    myProg.CNProgressThermometer = myProg.ProgressThermometer
    Display()

myProg.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?myProg:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0

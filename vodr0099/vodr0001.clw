

   MEMBER('vodr0099.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


FinanceWarrantyIncomeReport PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::16:TAGFLAG         BYTE(0)
DASBRW::16:TAGMOUSE        BYTE(0)
DASBRW::16:TAGDISPSTATUS   BYTE(0)
DASBRW::16:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
tmp:Clipboard        ANY
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
TempFilePath         CSTRING(255)
tmp:FileName1        STRING(255)
tmp:FileName2        STRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:LastDetailColumn STRING(2)
tmp:LastSummaryColumn STRING(2)
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
Count                LONG
FileName             STRING(255)
                     END
TempFileQueue2       QUEUE,PRE(tmpque2)
AccountNumber        STRING(30)
Count                LONG
FileName             STRING(255)
                     END
tmp:ChargeTag        STRING(1)
tmp:Yes              STRING('YES')
tmp:ManufacturerTag  STRING(1)
tmp:Pending          BYTE(0)
tmp:Approved         BYTE(0)
tmp:Rejected         BYTE(0)
tmp:Query            BYTE(0)
tmp:OnTechnicalReport BYTE(0)
tmp:SubmittedToMFTR  BYTE(0)
tmp:MFTRPaid         BYTE(0)
tmp:SelectAll        BYTE(1)
tmp:DateRangeType    BYTE(0)
tmp:SupressZeros     BYTE(0)
tmp:AllChargeTypes   BYTE(1)
tmp:AllManufacturers BYTE(1)
ManufacturerQueue    QUEUE,PRE(manque)
Manufacturer         STRING(30)
JobNumber            LONG
FirstSecondYear      BYTE(0)
                     END
TotalsGroup          GROUP,PRE()
total:HandlingFee    REAL
total:Parts          REAL
total:PartsSelling   REAL
total:Labour         REAL
total:VAT            REAL
total:SubTotal       REAL
total:Total          REAL
total:Jobs           LONG
                     END
GrandTotalsGroup     GROUP,PRE()
grand:Jobs           LONG
grand:HandlingFee    REAL
grand:Parts          REAL
grand:PartsSelling   REAL
grand:Labour         REAL
grand:SubTotal       REAL
grand:VAT            REAL
grand:Total          REAL
                     END
tmp:FirstYear        BYTE(0)
tmp:SecondYear       BYTE(0)
BRW12::View:Browse   VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:ChargeTag          LIKE(tmp:ChargeTag)            !List box control field - type derived from local data
tmp:ChargeTag_Icon     LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Finance Warranty Income Report'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,102,552,258),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Select Charge Types'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(352,124),USE(tmp:AllChargeTypes),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           PROMPT('Select Charge Types'),AT(244,124),USE(?Prompt8),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(244,137,184,200),USE(?List),IMM,DISABLE,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(436,242),USE(?DASTAG),DISABLE,TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(436,276),USE(?DASTAGAll),DISABLE,TRN,FLAT,HIDE,ICON('tagallp.jpg')
                           BUTTON('&Rev tags'),AT(282,228,50,13),USE(?DASREVTAG)
                           BUTTON('sho&W tags'),AT(286,257,70,13),USE(?DASSHOWTAG)
                           BUTTON,AT(436,308),USE(?DASUNTAGALL),DISABLE,TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Select Manufacturer'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Manufacturer'),AT(244,124),USE(?Prompt9),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Manufacturers'),AT(348,124),USE(tmp:AllManufacturers),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                           LIST,AT(244,137,184,200),USE(?List:2),IMM,DISABLE,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(436,238),USE(?DASTAG:2),DISABLE,TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(436,274),USE(?DASTAGAll:2),DISABLE,TRN,FLAT,HIDE,ICON('tagallp.jpg')
                           BUTTON('&Rev tags'),AT(294,234,50,13),USE(?DASREVTAG:2)
                           BUTTON,AT(436,310),USE(?DASUNTAGALL:2),DISABLE,TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON('sho&W tags'),AT(294,262,70,13),USE(?DASSHOWTAG:2)
                         END
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(324,218),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(400,218,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(468,216),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           OPTION('Date Range Type'),AT(164,212,140,44),USE(tmp:DateRangeType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Completed Date Range'),AT(172,225),USE(?tmp:DateRangeType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Claim Submitted Date Range'),AT(172,241),USE(?tmp:DateRangeType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           GROUP('Select Warranty Status'),AT(164,134,328,50),USE(?Group:WarrantyStatus),DISABLE,BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Pending'),AT(173,147),USE(tmp:Pending),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Pending'),TIP('Pending'),VALUE('1','0')
                             CHECK('Approved'),AT(240,147),USE(tmp:Approved),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Approved'),TIP('Approved'),VALUE('1','0')
                             CHECK('Rejected'),AT(173,163),USE(tmp:Rejected),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Rejected'),TIP('Rejected'),VALUE('1','0')
                             CHECK('Query'),AT(328,147),USE(tmp:Query),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Query'),TIP('Query'),VALUE('1','0')
                             CHECK('On Technical Report'),AT(392,147),USE(tmp:OnTechnicalReport),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('On Technical Report'),TIP('On Technical Report'),VALUE('1','0')
                             CHECK('Submitted To MFTR'),AT(240,163),USE(tmp:SubmittedToMFTR),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Submitted To MFTR'),TIP('Submitted To MFTR'),VALUE('1','0')
                             CHECK('MFTR Paid'),AT(392,163),USE(tmp:MFTRPaid),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('MFTR Paid'),TIP('MFTR Paid'),VALUE('1','0')
                           END
                           CHECK('Select All'),AT(172,120),USE(tmp:SelectAll),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select All'),TIP('Select All'),VALUE('1','0')
                           PROMPT('End Date'),AT(324,242),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(400,242,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           BUTTON,AT(468,240),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT(''),AT(324,250),USE(?StatusText)
                           CHECK('Supress Zeros'),AT(324,270),USE(tmp:SupressZeros),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Supress Zeros'),TIP('Supress Zeros'),VALUE('1','0')
                           BUTTON,AT(476,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                           PROMPT('Ensure Excel is NOT running before you begin!'),AT(238,340,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(244,366),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(312,366),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       PROMPT('Report Version'),AT(68,348),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
AddToExportFile      Procedure(Byte f:FirstSecondYear)
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                   String(2000)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Wizard14         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW12.UpdateBuffer
   glo:Queue.Pointer = cha:Charge_Type
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = cha:Charge_Type
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:ChargeTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:ChargeTag = ''
  END
    Queue:Browse.tmp:ChargeTag = tmp:ChargeTag
  IF (tmp:ChargeTag = '*')
    Queue:Browse.tmp:ChargeTag_Icon = 2
  ELSE
    Queue:Browse.tmp:ChargeTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = cha:Charge_Type
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = cha:Charge_Type
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = cha:Charge_Type
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::16:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW15.UpdateBuffer
   glo:Queue2.Pointer2 = man:Manufacturer
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:ManufacturerTag = ''
  END
    Queue:Browse:1.tmp:ManufacturerTag = tmp:ManufacturerTag
  IF (tmp:ManufacturerTag = '*')
    Queue:Browse:1.tmp:ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW15.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW15.Reset
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::16:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::16:QUEUE = glo:Queue2
    ADD(DASBRW::16:QUEUE)
  END
  FREE(glo:Queue2)
  BRW15.Reset
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::16:QUEUE.Pointer2 = man:Manufacturer
     GET(DASBRW::16:QUEUE,DASBRW::16:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = man:Manufacturer
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASSHOWTAG Routine
   CASE DASBRW::16:TAGDISPSTATUS
   OF 0
      DASBRW::16:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::16:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::16:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW15.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:RecordCount       Long()
local:Manufacturer      String(30)
local:LineCount         Long()
local:Desktop           CString(255)
local:SavePath          CString(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'Finance Warranty Income Report'

    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    tmp:FileName1 = Clip(local:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & ' 1st Yr'!.xls'
    tmp:FileName2 = Clip(local:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & ' 2nd Yr'!.xls'

    If tmp:AllChargeTypes = 1
        tmp:FirstYear = 1
        tmp:SecondYear = 1
    Else ! If tmp:AllChargeTypes
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty = 'YES'
            cha:Charge_Type = glo:Pointer
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
                If cha:SecondYearWarranty
                    tmp:SecondYear = 1
                Else ! If cha:SecondYearWarranty
                    tmp:FirstYear = 1
                End ! If cha:SecondYearWarranty
            Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
            End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign

        End ! Loop x# = 1 To Records(glo:Queue)
    End ! If tmp:AllChargeTypes

    If Exists(tmp:FileName1) And tmp:FirstYear
        Remove(tmp:FileName1)
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End ! If Exists(tmp:FileName1)

    If Exists(tmp:FileName2) And tmp:SecondYear
        Remove(tmp:FileName2)
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End ! If Exists(tmp:FileName1)


    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')


    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = ''
    Set(tra:Account_Number_Key,tra:Account_Number_Key)
    Loop ! Begin Loop
        If Access:TRADEACC.Next()
            Break
        End ! If Access:TRADEACC.Next()

        If Clip(tra:BranchIdentification) = ''
            Cycle
        End ! If tra:BranchIdentification = ''

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        local.UpdateProgressWindow('Searching Account: ' & Clip(tra:Account_Number) & ' ' & Clip(tra:Company_Name))

        local:RecordCount = 0

        Free(ManufacturerQueue)
        Clear(ManufacturerQueue)

        If tra:Account_Number = tmp:HeadAccountNumber

            ! --- For ARC show all jobs sent to hub ----
            ! Inserting:  DBH 08/12/2008 #10568
            Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
            tra_ali:Account_Number = ''
            Set(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)
            ! End: DBH 08/12/2008 #10568
            ! -----------------------------------------
            Loop ! Begin Loop
                If Access:TRADEACC_ALIAS.Next()
                    Break
                End ! If Access:TRADEACC_ALIAS.Next()
                Case tmp:DateRangeType
                Of 0
                    Access:WEBJOB.Clearkey(wob:DateCompletedKey)
                    wob:HeadAccountNumber = tra_ali:Account_Number
                    wob:DateCompleted = tmp:StartDate
                    Set(wob:DateCompletedKey,wob:DateCompletedKey)
                    Loop ! Begin Loop
                        If Access:WEBJOB.Next()
                            Break
                        End ! If Access:WEBJOB.Next()
                        If wob:HeadAccountNumber <> tra_ali:Account_Number
                            Break
                        End ! If wob:HeadAccountNumber <> tra_ali:Account_Number
                        If wob:DateCompleted > tmp:EndDate
                            Break
                        End ! If wob:Date_Completed > tmp:EndDate
                        Do GetNextRecord2
                        Do CancelCheck
                        If tmp:Cancel
                            Break
                        End !If tmp:Cancel = 1

                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = wob:RefNumber
                        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                            !Found
                        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign


                        If ~SentToHub(wob:RefNumber)
                            Cycle
                        End ! If SentToHub(wob:RefNumber) <> 1

                        Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                        jow:RefNumber = wob:RefNumber
                        If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                            !Found
                        Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign

                        Do AddToQueue

                    End ! Loop

                Of 1
                    Access:JOBSWARR.Clearkey(jow:SubmittedBranchKey)
                    jow:BranchID = tra_ali:BranchIdentification
                    jow:ClaimSubmitted = tmp:StartDate
                    Set(jow:SubmittedBranchKey,jow:SubmittedBranchKey)
                    Loop ! Begin Loop
                        If Access:JOBSWARR.Next()
                            Break
                        End ! If Access:JOBSWARR.Next()
                        If jow:BranchID <> tra_ali:BranchIdentification
                            Break
                        End ! If jow:BranchID <> tra:BranchIdentification
                        If jow:ClaimSubmitted > tmp:EndDate
                            Break
                        End ! If jow:ClaimSubmitted > tmp:EndDate
                        Do GetNextRecord2
                        Do CancelCheck
                        If tmp:Cancel
                            Break
                        End !If tmp:Cancel = 1

                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = jow:RefNumber
                        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                            !Found
                        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                        If ~SentToHub(jow:RefNumber)
                            Cycle
                        End ! If SentToHub(wob:RefNumber) <> 1

                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = jow:RefNumber
                        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                            !Found
                        Else ! If Access:OWEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

                        Do AddToQueue
                    End ! Loop

                End ! Case tmp:DateRangeType
                If tmp:Cancel
                    Break
                End ! If tmp:Cancel
            End ! Loop

        Else ! If tra:ACcount_Number = tmp:ARCAccount
            Case tmp:DateRangeType
            Of 0 ! Date Completed Date Range
                Access:WEBJOB.Clearkey(wob:DateCompletedKey)
                wob:HeadAccountNumber = tra:Account_Number
                wob:DateCompleted = tmp:StartDate
                Set(wob:DateCompletedKey,wob:DateCompletedKey)
                Loop ! Begin Loop
                    If Access:WEBJOB.Next()
                        Break
                    End ! If Access:WEBJOB.Next()
                    If wob:HeadAccountNumber <> tra:Account_Number
                        Break
                    End ! If wob:HeadAccountNumber <> tra:Account_Number
                    If wob:DateCompleted > tmp:EndDate
                        Break
                    End ! If wob:DateCompleted > tmp:EndDate

                    Do GetNextRecord2
                    Do CancelCheck
                    If tmp:Cancel
                        Break
                    End !If tmp:Cancel = 1

                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = wob:RefNumber
                    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                    If SentToHub(wob:RefNumber)
                        Cycle
                    End ! If SentToHub(wob:RefNumber) <> 1


                    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                    jow:RefNumber = wob:RefNumber
                    If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign

                    Do AddToQueue

                End ! Loop
            Of 1 ! Claim Submitted Date Range
                Access:JOBSWARR.Clearkey(jow:SubmittedBranchKey)
                jow:BranchID = tra:BranchIdentification
                jow:ClaimSubmitted = tmp:StartDate
                Set(jow:SubmittedBranchKey,jow:SubmittedBranchKey)
                Loop ! Begin Loop
                    If Access:JOBSWARR.Next()
                        Break
                    End ! If Access:JOBSWARR.Next()
                    If jow:BranchID <> tra:BranchIdentification
                        Break
                    End ! If jow:BranchID <> tra:BranchIdentification
                    If jow:ClaimSubmitted > tmp:EndDate
                        Break
                    End ! If jow:ClaimSubmitted > tmp:EndDate
                    Do GetNextRecord2
                    Do CancelCheck
                    If tmp:Cancel
                        Break
                    End !If tmp:Cancel = 1

                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = jow:RefNumber
                    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                    If SentToHub(jow:RefNumber)
                        Cycle
                    End ! If SentToHub(wob:RefNumber) <> 1

                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = jow:RefNumber
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:OWEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                        Cycle
                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

                    Do AddToQueue
                End ! Loop

            End ! Case tmp:DateRangeType
        End ! If tra:ACcount_Number = tmp:ARCAccount


        If tmp:Cancel
            Break
        End ! If tmp:Cancel

        Found1# = 0
        Found2# = 0
        Loop x# = 1 To Records(ManufacturerQueue)
            Get(ManufacturerQueue,x#)
            If tmp:FirstYear
                If manque:FirstSecondYear = 0
                    Found1# = 1
                End ! If manque:FirstSecondYear = 0
            End ! If tmp:FirstYear
            If tmp:SecondYear
                If manque:FirstSecondYear = 1
                    Found2# = 1
                End ! If manque:FirstSecondYear = 1
            End ! If tmp:SecondYear
        End ! Loop x# = 1 To Records(ManufacturerQueue)


        If tmp:FirstYear = 1 And Found1# > 0 
            local.UpdateProgressWindow('Reading Account (1st): ' & Clip(tra:Account_Number) & ' ' & Clip(tra:Company_Name))

            glo:ExportFile = Clip(local:LocalPath) & Clip(tra:RecordNumber) & Clock() & '_1.CSV'
            Remove(glo:ExportFile)
            Create(ExportFile)
            Open(ExportFile)

            local.AddToExportFile(0)

        End ! If Records(ManufacturerQueue)

        If tmp:SecondYear = 1 And Found2# > 0
            local.UpdateProgressWindow('Reading Account (2nd): ' & Clip(tra:Account_Number) & ' ' & Clip(tra:Company_Name))

            glo:ExportFile = Clip(local:LocalPath) & Clip(tra:RecordNumber) & Clock() & '_2.CSV'
            Remove(glo:ExportFile)
            Create(ExportFile)
            Open(ExportFile)

            local.AddToExportFile(1)

        End ! If tmp:SecondYear = 1 And Found2# > 0
    End !Loop TRADEACC
    

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
        local.UpdateProgressWindow('')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________


        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        tmp:LastDetailColumn = 'AB'

        If tmp:FirstYear And REcords(TempFileQueue)
            local.UpdateProgressWindow('Creating 1st Year Excel Sheets...')
            local.UpdateProgressWindow('')

            E1.NewWorkBook()
            E1.SaveAs(tmp:FileName1)
            E1.CloseWorkBook(3)

            Sort(TempFileQueue,-tmpque:AccountNumber)

            Loop x# = 1 To Records(TempFileQueue)
                Get(TempFileQueue,x#)
                Do GetNextRecord2

                SetClipBoard('')
                E1.OpenWorkBook(tmpque:FileName)
                E1.Copy('A1',Clip(tmp:LastDetailColumn) & tmpque:Count)
                tmp:Clipboard = ClipBoard()
                Loop Until Clipboard() = ''
                    SetClipboard('')
                End ! Loop Until Clipboard() = ''
                E1.CloseWorkBook(3)
                Remove(tmpque:FileName)
                E1.OpenWorkBook(tmp:FileName1)
                E1.InsertWorkSheet()
                E1.RenameWorkSheet(tmpque:AccountNumber)
                E1.SelectCells('A1')
                SetClipBoard(tmp:ClipBoard)
                E1.Paste()
                Loop Until Clipboard() = ''
                    SetClipboard('')
                End ! Loop Until Clipboard() = ''

                Loop row# = 2 To tmpque:Count
                    Do GetNextRecord2
                    If E1.ReadCell('A' & row#) = 'Totals'
                        E1.SetPaletteColor(33,13434879)
                        E1.InterpretClarionColorEquates = 0
                        E1.SetCellBackgroundColor(33,'A' & row#,tmp:LastDetailColumn & row#)
                        E1.SetCellFontStyle('Bold','A' & row#,tmp:LastDetailColumn & row#)
                    End ! If E1.ReadCell('A' & tmpque:Count) = 'Totals'
                End ! Loop row# = 2 To tmpque:Count

                E1.SetCellFontName('Tahoma','A1','AB' & tmpque:Count)
                E1.SetCellFontSize('8','A1','AB' & tmpque:Count)
                E1.SetPaletteColor(34,13421772)
                E1.InterpretClarionColorEquates = 0
                E1.SetCellBackgroundColor(34,'A1',tmp:LastDetailColumn & '1')
                E1.SetCellFontStyle('Bold','A1','AB1')
                E1.SetPaletteColor(35,10079487)
                E1.InterpretClarionColorEquates = 0
                E1.SetCellBackgroundColor(35,'A' & tmpque:Count,tmp:LastDetailColumn & tmpque:Count)
                E1.SetCellFontStyle('Bold','A' & tmpque:Count,'AB' & tmpque:Count)
                E1.SetColumnWidth('A','AB')
                E1.SelectCells('B2')
                E1.FreezePanes(1)
                E1.SelectCells('A2')
                E1.Save()
                E1.CloseWorkBook(3)
                local.UpdateProgressWindow(Clip(tmpque:AccountNumber) & ': Sheet Created.')
                local.UpdateProgressWindow('')
            End ! Loop x# = 1 To Records(TempFileQueue)


        End ! If tmp:FirstYear

        If tmp:SecondYear And Records(TempFileQueue2)
            local.UpdateProgressWindow('Creating 2nd Year Excel Sheets...')
            local.UpdateProgressWindow('')

            E1.NewWorkBook()
            E1.SaveAs(tmp:FileName2)
            E1.CloseWorkBook(3)

            Sort(TempFileQueue2,-tmpque2:AccountNumber)

            Loop x# = 1 To Records(TempFileQueue2)
                Get(TempFileQueue2,x#)
                Do GetNextRecord2

                SetClipBoard('')
                E1.OpenWorkBook(tmpque2:FileName)
                E1.Copy('A1',Clip(tmp:LastDetailColumn) & tmpque2:Count)
                tmp:Clipboard = ClipBoard()
                Loop Until Clipboard() = ''
                    SetClipboard('')
                End ! Loop Until Clipboard() = ''
                E1.CloseWorkBook(3)
                Remove(tmpque2:FileName)
                E1.OpenWorkBook(tmp:FileName2)
                E1.InsertWorkSheet()
                E1.RenameWorkSheet(tmpque2:AccountNumber)
                E1.SelectCells('A1')
                SetClipBoard(tmp:ClipBoard)
                E1.Paste()
                Loop Until Clipboard() = ''
                    SetClipboard('')
                End ! Loop Until Clipboard() = ''

                Loop row# = 2 To tmpque:Count
                    Do GetNextRecord2
                    If E1.ReadCell('A' & row#) = 'Totals'
                        E1.SetPaletteColor(33,13434879)
                        E1.InterpretClarionColorEquates = 0
                        E1.SetCellBackgroundColor(33,'A' & row#,tmp:LastDetailColumn & row#)
                        E1.SetCellFontStyle('Bold','A' & row#,tmp:LastDetailColumn & row#)
                    End ! If E1.ReadCell('A' & tmpque:Count) = 'Totals'
                End ! Loop row# = 2 To tmpque:Count

                E1.SetCellFontName('Tahoma','A1','AB' & tmpque2:Count)
                E1.SetCellFontSize('8','A1','AB' & tmpque2:Count)
                E1.SetPaletteColor(34,13421772)
                E1.InterpretClarionColorEquates = 0
                E1.SetCellBackgroundColor(34,'A1',tmp:LastDetailColumn & '1')

                E1.SetCellFontStyle('Bold','A1','AB1')
                E1.SetPaletteColor(35,10079487)
                E1.InterpretClarionColorEquates = 0
                E1.SetCellBackgroundColor(35,'A' & tmpque:Count,tmp:LastDetailColumn & tmpque:Count)

                E1.SetCellFontStyle('Bold','A' & tmpque2:Count,'AB' & tmpque2:Count)
                E1.SetColumnWidth('A','AB')
                E1.SelectCells('B2')
                E1.FreezePanes(1)

                E1.SelectCells('A2')
                E1.Save()
                E1.CloseWorkBook(3)
                local.UpdateProgressWindow(Clip(tmpque2:AccountNumber) & ': Sheet Created.')
                local.UpdateProgressWindow('')
            End ! Loop x# = 1 To Records(TempFileQueue)
        End ! If tmp:SecondYear

        E1.Kill()

        local.UpdatePRogressWindow('Finishing Off Formatting..')


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
AddToQueue      Routine

    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_number = wob:RefNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Error
        Exit
    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

    If job:Warranty_Job <> 'YES'
        Exit
    End ! If job:Warranty_Job <> 'YES'

    If ~tmp:AllChargeTypes
        Sort(glo:Queue,glo:Pointer)
        glo:Pointer    = job:Warranty_Charge_Type
        Get(glo:Queue,glo:Pointer)
        If Error()
            Exit
        End !If Error()
    End !If ~tmp:AllChargeTypes


    If ~tmp:FirstYear And jow:FirstSecondYear = 0
        Exit
    End ! If tmp:FirstYear And jow:FirstSecondYear = 1

    If ~tmp:SecondYear And jow:FirstSecondYear = 1
        Exit
    End ! If tmp:FirstYear And jow:FirstSecondYear = 1

    If ~tmp:AllManufacturers
        Sort(glo:Queue2,glo:Pointer2)
        glo:Pointer2    = job:Manufacturer
        Get(glo:Queue2,glo:Pointer2)
        If Error()
            Exit
        End !If Error()
    End !If ~tmp:AllManufacturers

    If ~tmp:SelectAll

        Case jow:Status
        Of 'NO'
            Case jobe:WarrantyClaimStatus
            Of 'ON TECHNICAL REPORT'
                If ~tmp:OnTechnicalReport
                    Exit
                End ! If ~tmp:OnTechnicalReport
            Of 'SUBMITTED TO MFTR'
                If ~tmp:SubmittedToMFTR
                    Exit
                End ! If ~tmp:SubmittedToMFTR
            Else
                If ~tmp:Pending
                    Exit
                End ! If ~tmp:Pending
            End ! Case jobe:WarrantyClaimStatus
        Of 'YES'
            If ~tmp:Approved
                Exit
            End ! If ~tmp:Approved
        Of 'EXC'
            If ~tmp:Rejected
                Exit
            End ! If ~tmp:Rejected
        Of 'QUE'
            If ~tmp:Query
                Exit
            End ! If ~tmp:Query
        Of 'REJ'
            Exit
        Else
            Exit
        End ! Case jow:Status
    End ! If ~tmp:SelectAll

    manque:Manufacturer = job:Manufacturer
    manque:JobNumber = job:Ref_Number
    manque:FirstSecondYear = jow:FirstSecondYear
    Add(ManufacturerQueue)
DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(excel:ProgramName, 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A7','D7',color:Silver)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')



WriteLine         Routine
Data
Costs               Group(),Pre()
local:HandlingFee       Real
local:PartsSelling      Real
local:Labour            Real
local:VAT               Real
local:Total             Real
local:Parts             Real
                    End
local:RejectionBy      String(65)
local:TradeAccount      String(30)
Code

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign


    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
    jow:RefNumber = job:Ref_Number
    If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign

    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADE_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADE_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign



    Clear(exp:Record)
    ! Job Number
    exp:Line1 = '"' & job:Ref_Number
    ! Branch ID
    exp:Line1 = Clip(exp:Line1) & '","' & tra_ali:BranchIdentification
    ! Webjob Number
    exp:Line1 = Clip(exp:Line1) & '","' & wob:JobNumber
    ! Completed Date
    exp:Line1 = Clip(exp:Line1) & '","' & Format(job:Date_Completed,@d06)
    ! Claim Submitted
    exp:Line1 = Clip(exp:Line1) & '","' & Format(jow:ClaimSubmitted,@d06)
    ! Account Number
    exp:Line1 = Clip(exp:Line1) & '","' & tra_ali:Account_Number
    ! Account Name
    exp:Line1 = Clip(exp:Line1) & '","' & tra_ali:Company_Name
    ! Job Account Number
    exp:Line1 = Clip(exp:Line1) & '","' & job:Account_Number
    ! Job Company Name
    exp:Line1 = Clip(exp:Line1) & '","' & BHStripReplace(job:Company_Name,'","',' ')
    ! Manufacturer
    exp:Line1 = Clip(exp:Line1) & '","' & job:Manufacturer
    ! Model Number
    exp:Line1 = Clip(exp:Line1) & '","' & job:Model_Number
    ! Charge Type
    exp:Line1 = Clip(exp:Line1) & '","' & job:Warranty_Charge_Type
    ! Repair Type
    exp:Line1 = Clip(exp:Line1) & '","' & job:Repair_Type_Warranty
    ! Engineer
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = job:Engineer
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        exp:Line1 = Clip(exp:Line1) & '","' & BHStripReplace(Clip(use:Forename) & ' ' & Clip(use:Surname),'","',' ')
    Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        exp:Line1 = Clip(exp:Line1) & '","'
    End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    ! Repair
    exp:Line1 = Clip(exp:Line1) & '","' & jow:RepairedAt
    ! Exchanged
    If job:Exchange_Unit_Number = 0
        exp:Line1 = Clip(exp:Line1) & '","'
    Else ! If job:Exchange_Unit_Number = 0
        If jobe:ExchangedAtRRC
            exp:Line1   = Clip(exp:Line1) & '","RRC'
        Else ! If jobe:ExchangedAtRRC
            exp:Line1   = Clip(exp:Line1) & '","ARC'
        End ! If jobe:ExchangedAtRRC
    End ! If job:Exchange_Unit_Number = 0

    Clear(Costs)
    If job:Invoice_Number_Warranty > 0
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number_Warranty
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            local:PartsSelling = job:WInvoice_Parts_Cost
            local:Labour = jobe:InvoiceClaimValue
            local:VAT = job:WInvoice_Parts_Cost * inv:Vat_Rate_Labour/100 + |
                                    jobe:InvoiceClaimValue * inv:Vat_Rate_Parts/100 + |
                                    job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
            local:Total    = local:PartsSelling + local:Labour + local:VAT + job:WInvoice_Courier_Cost
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

    Else ! If job:Invoice_Number_Warranty > 0
        local:PartsSelling    = job:Parts_Cost_Warranty
        local:Labour   = jobe:ClaimValue
        local:VAT      = job:Parts_Cost_Warranty * VatRate(job:Account_Number,'L')/100 + |
                            jobe:ClaimValue * VatRate(job:Account_Number,'P')/100 + |
                            job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
        local:Total    = local:PartsSelling + local:Labour + local:VAT + job:Courier_Cost_Warranty
    End ! If job:Invoice_Number_Warranty > 0

    If tmp:SupressZeros And local:Total = 0
        Exit
    End ! If tmp:SupressZeros And local:Total = 0

    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop ! Begin Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number
        If wpr:Correction
            Cycle
        End ! If wpr:Correction
        local:Parts += (wpr:AveragePurchaseCost * wpr:Quantity)
    End ! Loop

    ! Handling Fee
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:HandlingFee,@n_10.2)
    ! Parts Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Parts,@n_10.2)
    ! Parts Selling Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:PartsSelling,@n_10.2)
    ! Labour Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Labour,@n_10.2)
    ! Sub Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Labour + local:PartsSelling,@n_10.2)
    ! VAT
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:VAT,@n_10.2)
    ! Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Total,@n_10.2)

    total:HandlingFee += local:HandlingFee
    total:Parts += local:Parts
    total:PartsSelling += local:PartsSelling
    total:Labour += local:Labour
    total:SubTotal += local:Labour + local:PartsSelling
    total:VAT += local:VAT
    total:Total += local:Total

    ! Warranty Status
    local:RejectionBy = ''
    Case wob:EDI
    Of 'XXX'
        exp:Line1   = Clip(exp:Line1) & '","EXCLUDED'
    Of 'NO'
        exp:Line1   = Clip(exp:Line1) & '","PENDING'
    Of 'YES'
        exp:Line1   = Clip(exp:Line1) & '","IN PROGRESS'
    Of 'EXC'
        exp:Line1   = Clip(exp:Line1) & '","REJECTED'
    Of 'PAY'
        exp:Line1   = Clip(exp:Line1) & '","PAID'
    Of 'APP'
        exp:Line1   = Clip(exp:Line1) & '","APPROVED'
    Of 'AAJ'
        exp:Line1   = Clip(exp:Line1) & '","ACCEPTED-REJECTION'

        Access:AUDIT.Clearkey(aud:TypeActionKey)
        aud:Ref_Number = job:Ref_Number
        aud:Type       = 'JOB'
        aud:Action     = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop ! Begin Loop
            If Access:AUDIT.Next()
                Break
            End ! If Access:AUDIT.Next()
            If aud:Ref_Number <> job:Ref_Number
                Break
            End ! If aud:Ref_Number <> job:Ref_Number
            If aud:Type <> 'JOB'
                Break
            End ! If aud:Type <> 'JOB'
            If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
                Break
            End ! If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = aud:User
            If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Found
                local:RejectionBy = Clip(use:Forename) & ' ' & Clip(use:Surname)
                Break
            Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Error
            End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        End ! Loop
    Of 'REJ'
        exp:Line1   = Clip(exp:Line1) & '","FINAL REJECTION'
    End ! Case wob:EDI

    ! Rejection Reasons
    CountReasons# = 0
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = job:Ref_Number
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM REJECTED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> job:Ref_Number
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type <> 'JOB'
        If aud:Action <> 'WARRANTY CLAIM REJECTED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        CountReasons# += 1
        If CountReasons# > 3
            Break
        End ! If CountReasons# > 3

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
    End ! Loop

    CountReasons# += 1
    If CountReasons# <= 3
        Loop loop# = CountReasons# To 3
            exp:Line1   = Clip(exp:Line1) & '","'
        End ! Loop loop# = CountReasons# To 3
    End ! If CountReasons# <= 3

    ! Rejection By
    exp:Line1   = Clip(exp:Line1) & '","' & local:RejectionBy

    ! Da End
    exp:Line1   = Clip(exp:Line1) & '"'

    Add(ExportFile)

    total:Jobs += 1

getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020721'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('FinanceWarrantyIncomeReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:JOBSWARR.UseFile
  Access:USERS.UseFile
  Access:WARPARTS.UseFile
  Access:INVOICE.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:CHARTYPE,SELF)
  BRW15.Init(?List:2,Queue:Browse:1.ViewPosition,BRW15::View:Browse,Queue:Browse:1,Relate:MANUFACT,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard14.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Print, |                        ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW12.Q &= Queue:Browse
  BRW12.AddSortOrder(,cha:Warranty_Key)
  BRW12.AddRange(cha:Warranty,tmp:Yes)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,cha:Charge_Type,1,BRW12)
  BIND('tmp:ChargeTag',tmp:ChargeTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tmp:ChargeTag,BRW12.Q.tmp:ChargeTag)
  BRW12.AddField(cha:Charge_Type,BRW12.Q.cha:Charge_Type)
  BRW12.AddField(cha:Warranty,BRW12.Q.cha:Warranty)
  BRW15.Q &= Queue:Browse:1
  BRW15.AddSortOrder(,man:Manufacturer_Key)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,man:Manufacturer,1,BRW15)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(tmp:ManufacturerTag,BRW15.Q.tmp:ManufacturerTag)
  BRW15.AddField(man:Manufacturer,BRW15.Q.man:Manufacturer)
  BRW15.AddField(man:RecordNumber,BRW15.Q.man:RecordNumber)
  BRW12.AddToolbarTarget(Toolbar)
  BRW15.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard14.Validate()
        DISABLE(Wizard14.NextControl())
     ELSE
        ENABLE(Wizard14.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020721'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020721'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020721'&'0')
      ***
    OF ?tmp:AllChargeTypes
      If tmp:AllChargeTypes
          ?List{prop:Disable} = 1
          ?DasTag{prop:Disable} = 1
          ?DasTagAll{prop:Disable} = 1
          ?DasUntagAll{prop:Disable} = 1
      Else ! If tmp:AllChargeTypes
          ?List{prop:Disable} = 0
          ?DasTag{prop:Disable} = 0
          ?DasTagAll{prop:Disable} = 0
          ?DasUntagAll{prop:Disable} = 0
      End ! If tmp:AllChargeTypes
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllManufacturers
      If tmp:AllManufacturers
          ?List:2{prop:Disable} = 1
          ?DasTag:2{prop:Disable} = 1
          ?DasTagAll:2{prop:Disable} = 1
          ?DasUntagAll:2{prop:Disable} = 1
      Else ! If tmp:AllChargeTypes
          ?List:2{prop:Disable} = 0
          ?DasTag:2{prop:Disable} = 0
          ?DasTagAll:2{prop:Disable} = 0
          ?DasUntagAll:2{prop:Disable} = 0
      End ! If tmp:AllChargeTypes
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:SelectAll
      If tmp:SelectAll
          tmp:Pending = 1
          tmp:Approved = 1
          tmp:Query = 1
          tmp:OnTechnicalReport = 1
          tmp:Rejected = 1
          tmp:SubmittedtoMFTR = 1
          tmp:MFTRPaid = 1
          ?Group:WarrantyStatus{prop:Disable} = 1
      Else ! If tmp:SelectAll
          ?Group:WarrantyStatus{prop:Disable} = 0
      End ! If tmp:SelectAll
      Display()
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    OF ?VSBackButton
      ThisWindow.Update
         Wizard14.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard14.TakeAccepted()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::16:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
local.AddToExportFile      Procedure(Byte f:FirstSecondYear)
local:RecordCount   Long()
local:Manufacturer  String(30)
Code
    Clear(GrandTotalsGroup)

    Sort(ManufacturerQueue,manque:Manufacturer)

    FirstTime# = 1

    Loop x# = 1 To Records(ManufacturerQueue)
        Get(ManufacturerQueue,x#)
        If f:FirstSecondYear = 0
            If manque:FirstSecondYear = 1
                Cycle
            End ! If manque:FirstSecondYear = 1
        Else ! If f:FirstSecondYear = 0
            If manque:FirstSecondYear = 0
                Cycle
            End ! If manque:FirstSecondYear = 0
        End ! If f:FirstSecondYear = 0
        Do GetNextRecord2
        If FirstTime# = 1
            FirstTime# = 0
            local:Manufacturer = manque:Manufacturer
            Clear(TotalsGroup)
            Clear(exp:Record)
            exp:Line1 = 'SB Job No,Franchise Branch ID,Franchise Job No,Date Completed,Claim Submitted,Head Account No,Head Account Name,Account No,Account Name,Manufacturer,Model Number,Warranty Charge Type,Warranty Repair Type,Engineer,Repair,Exchange,Handling/Swap Fee,Parts Cost,Part Sale,Labour,Sub Total,VAT,Total,Warranty Status,Rejection Reason 1,Rejection Reason 2,Rejection Reason 3,Rejection Accepted By'
            Add(ExportFile)
        Else
            If local:Manufacturer <> manque:Manufacturer
                ! Do Totals
                Clear(exp:Record)
                exp:Line1   = 'Totals,,,,,,,,,' & total:Jobs & ',,,,,,,' & |
                            Format(total:HandlingFee,@n_10.2) & ',' & |
                            Format(total:Parts,@n_10.2) & ',' &  |
                            Format(total:PartsSelling,@n_10.2) & ',' &  |
                            Format(total:Labour,@n_10.2) & ',' &  |
                            Format(total:SubTotal,@n_10.2) & ',' &  |
                            Format(total:Vat,@n_10.2) & ',' &  |
                            Format(total:Total,@n_10.2)
                Add(ExportFile)
                Clear(exp:Record)
                Add(ExportFile)
                local:Manufacturer = manque:Manufacturer

                grand:Jobs += total:Jobs
                grand:HandlingFee += total:HandlingFee
                grand:Parts += total:Parts
                grand:PartsSelling += total:PartsSelling
                grand:Labour += total:Labour
                grand:Vat += total:VAT
                grand:SubTotal +=total:SubTotal
                grand:Total += total:Total

                Clear(TotalsGroup)

            End ! If local:Manufacturer <> manque:Manufacturer
        End ! If x# = 1
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = manque:JobNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        Do WriteLine
        local:RecordCount += 1
    End ! Loop x# = 1 To Records(ManufacturerQueue)
    Clear(exp:Record)
    exp:Line1   = 'Totals,,,,,,,,,' & total:Jobs & ',,,,,,,' & |
                Format(total:HandlingFee,@n_10.2) & ',' & |
                Format(total:Parts,@n_10.2) & ',' &  |
                Format(total:PartsSelling,@n_10.2) & ',' &  |
                Format(total:Labour,@n_10.2) & ',' &  |
                Format(total:SubTotal,@n_10.2) & ',' &  |
                Format(total:Vat,@n_10.2) & ',' &  |
                Format(total:Total,@n_10.2)
    Add(ExportFile)
    grand:Jobs += total:Jobs
    grand:HandlingFee += total:HandlingFee
    grand:Parts += total:Parts
    grand:PartsSelling += total:PartsSelling
    grand:Labour += total:Labour
    grand:Vat += total:VAT
    grand:SubTotal +=total:SubTotal
    grand:Total += total:Total
    Clear(exp:Record)
    Add(ExportFile)
    Clear(exp:Record)
    exp:Line1   = 'Grand Totals,,,,,,,,,' & grand:Jobs & ',,,,,,,' & |
                Format(grand:HandlingFee,@n_10.2) & ',' & |
                Format(grand:Parts,@n_10.2) & ',' &  |
                Format(grand:PartsSelling,@n_10.2) & ',' &  |
                Format(grand:Labour,@n_10.2) & ',' &  |
                Format(grand:SubTotal,@n_10.2) & ',' &  |
                Format(grand:Vat,@n_10.2) & ',' &  |
                Format(grand:Total,@n_10.2)
    Add(ExportFile)

    local.UpdateProgressWindow('Jobs Read: ' & local:RecordCount)
    local.UpdateProgressWindow('')

    LineCount# = 0
    Set(ExportFile,0)
    Loop
        Next(ExportFile)
        If Error()
            Break
        End ! If Error()
        LineCount# += 1
    End ! Loop
    Close(ExportFile)

    If f:FirstSecondYear = 0
        tmpque:AccountNumber = tra:Account_Number
        tmpque:Count = LineCount#
        tmpque:FileName = glo:ExportFile
        Add(TempFileQueue)

    Else ! If f:FirstSecondYear = 0
        tmpque2:AccountNumber = tra:Account_Number
        tmpque2:Count = LineCount#
        tmpque2:FileName = glo:ExportFile
        Add(TempFileQueue2)

    End ! If f:FirstSecondYear = 0

Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = cha:Charge_Type
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:ChargeTag = ''
    ELSE
      tmp:ChargeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ChargeTag = '*')
    SELF.Q.tmp:ChargeTag_Icon = 2
  ELSE
    SELF.Q.tmp:ChargeTag_Icon = 1
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  If cha:Charge_Type <> 'WARRANTY (MFTR)' And cha:Charge_Type <> 'WARRANTY (2ND YR)'
      Return Record:Filtered
  End ! If cha:Charge_Type <> 'WARRANTY (MFTR)' And cha:Charge_Type <> 'WARRANTY (2ND YR)'
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = cha:Charge_Type
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:ManufacturerTag = ''
    ELSE
      tmp:ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ManufacturerTag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW15::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW15::RecordStatus=ReturnValue
  IF BRW15::RecordStatus NOT=Record:OK THEN RETURN BRW15::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::16:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW15::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Wizard14.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW12.Q &= NULL) ! Has Browse Object been initialized?
       BRW12.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW15.Q &= NULL) ! Has Browse Object been initialized?
       BRW15.ResetQueue(Reset:Queue)
    END

Wizard14.TakeBackEmbed PROCEDURE
   CODE

Wizard14.TakeNextEmbed PROCEDURE
   CODE

Wizard14.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False



   MEMBER('vodr0058.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


RetailPartsAwaitingPicking PROCEDURE                  !Generated from procedure template - Window

tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE(param)
StartDate            DATE
EndDate              DATE
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG(False)
Count                LONG(0)
                     END
ExcelStuff           GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Local_Stuff          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CompanyName          STRING(30)
Courier              STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
JobNumber            LONG
LastColumn           STRING('L')
LocationName         STRING(30)
LineCost             DECIMAL(7,2)
MainStore            STRING(3)
Path                 STRING('C:\ {252}')
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
VATCode              STRING(2)
                     END
ProgressBar_Stuff    GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
Result               BYTE
AccountChange        BYTE
tmp:FirstModel       STRING(30)
StockOK              BYTE
InvoiceOK            BYTE
                     END
Worksheet_Stuff      GROUP,PRE(sheet)
TitleSummaryRow      LONG(9)
TempLastCol          STRING(1)
HeadLastCol          STRING('Q')
DataLastCol          STRING('Q')
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
LocationBrowse_Group GROUP,PRE()
LocationVal          STRING(30)
LocationTag          STRING(1)
LocationCount        LONG
LocationChange       BYTE
                     END
Calculation_Group    GROUP,PRE(calc)
PartQty              LONG
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
LocalTag             STRING(1)
LocalTimeOut         LONG
DoAll                STRING(1)
GUIMode              BYTE
Counter              LONG
MainWindow           WINDOW('Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(396,172,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       STRING(@s255),AT(252,184,140,),USE(SRN:TipText),TRN
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Retail Parts Awaiting Picking'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN
                         SHEET,AT(244,212,192,42),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                           TAB('Criteria'),USE(?Tab1)
                             CHECK('Show Excel'),AT(311,220),USE(excel:Visible),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                             PROMPT('Report Version'),AT(250,238,180,10),USE(?ReportVersion),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           TAB('Repair Categories'),USE(?Tab2)
                           END
                         END
                         BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                         BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
    MAP
DateToString PROCEDURE( DATE ), STRING
LoadLOCATION  PROCEDURE( STRING ), LONG ! BOOL
LoadORDERS  PROCEDURE( LONG ), LONG ! BOOL
LoadSTOCK  PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE(LONG), STRING
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug  PROCEDURE( STRING )
    END !MAP
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!-----------------------------------------------------------------
OKButtonPressed                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('OKButtonPressed START')

        DO ProgressBar_Setup
        DO XL_Setup
        !-----------------------------------------------------------------

        CancelPressed = False

        DO InitColumns
        !-----------------------------------------------------------------
        RecordsToProcess = RECORDS(Stock)

        Access:Location.CLEARKEY(loc:Main_Store_Key)
        loc:Main_Store = 'YES'
        SET(loc:Main_Store_Key,loc:Main_Store_Key)

        IF Access:Location.NEXT() THEN
           LOC:LocationName= ''
        ELSE
            IF loc:Main_Store <> 'YES' THEN
               LOC:LocationName = ''
            ELSE
                LOC:LocationName = loc:Location
            END !IF
        END !IF

        DO ProgressBar_LoopPre
        !-------------------------------------------------------------
        WriteDebug('OKButtonPressed LOOP(' & LOC:LocationName & ')')
        LOC:FileName = ''
        DO ExportSetup
        IF LOC:FileName <> '' THEN
           DO ExportBody
           DO ExportFinalize
        END !IF
        !-------------------------------------------------------------
        DO ProgressBar_LoopPost
        !-------------------------------------------------------------
        !-----------------------------------------------------------------
        DO XL_Finalize
        DO ProgressBar_Finalise

        WriteDebug('OKButtonPressed END')
        IF CancelPressed = False
            POST(Event:CloseWindow)
        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportSetup')

        IF CancelPressed = True
            LOC:FileName = 'CancelPressed'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        !---------------------------------------------
        ! 8 Oct 2002 John
        ! Please remove the Summary sheet - This serves no purpose.
        !DO CreateTitleSheet
        !---------------------------------------------


        DO CreateDataSheet


        !---------------------------------------------
        ! 8 Oct 2002 John
        ! Please remove the Summary sheet - This serves no purpose.
        !DO WriteHeadSummary
        !---------------------------------------------

        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportFinalize')

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------


        !---------------------------------------------
        ! 8 Oct 2002 John
        ! Please remove the Summary sheet - This serves no purpose.
        !
        Excel{'Sheets("Sheet1").Select'}

        IF Excel{'ActiveSheet.Name'} = 'Sheet1'
            Excel{'ActiveWindow.SelectedSheets.Delete'}
        END !IF
!        Excel{'Sheets("Summary").Select'}
!        Excel{'Range("A1").Select'}
        !---------------------------------------------


        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Range("A1").Select'}


        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateTitleSheet')

        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:C").ColumnWidth'} = 20
        Excel{'ActiveSheet.Columns("D:' & sheet:HeadLastCol & '").ColumnWidth'} = 10

        Excel{'ActiveSheet.Columns("E:E").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("I:I").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("N:N").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("Q:Q").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:TitleSummaryRow & ':' & sheet:HeadLastCol & sheet:TitleSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:TitleSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:TitleSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
QueueIndex LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Checking Location(' & CLIP(LOC:LocationName) & ')'
        RecordsToProcess = RECORDS(Stock)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        CLEAR(Calculation_Group)
        !-----------------------------------------------------------------
        WriteDebug('WriteLocation(' & CLIP(LOC:LocationName) & ', LocationValue="' & CLIP(LocationQueue.LocationValue) & '", loc:Location="' & CLIP(loc:Location) & '")')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        IF LoadLOCATION(LOC:LocationName)
            LOC:MainStore = loc:Main_Store
        ELSE
            LOC:MainStore = 'NO'
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
        DO XL_SetWorksheetLandscape

        LOC:SectionName   = CLIP(LOC:LocationName)
        excel:CommentText = ''
        DO CreateSectionHeader
        !-----------------------------------------------------------------
        ! Location_Key KEY(sto:Location, sto:Part_Number),DUP,NOCASE
        !
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = LOC:LocationName
        sto:Part_Number = ''
        SET(sto:Location_Key, sto:Location_Key)
        RecordCount = 0
        !------------------------------------------
        LOOP WHILE Access:STOCK.NEXT() = level:Benign
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF  sto:Location <> LOC:LocationName
                BREAK
            END !IF
            calc:PartQty = 0
            Access:RETSTOCK.CLEARKEY(res:DespatchedKey)
            res:Despatched = 'PIK'
            SET(res:DespatchedKey,res:DespatchedKey)
            LOOP
              IF Access:RETSTOCK.NEXT() THEN BREAK.
              IF res:Despatched <> 'PIK' THEN BREAK.
              IF res:Part_Ref_Number <> sto:Ref_Number THEN CYCLE.
              calc:PartQty += res:Quantity
            END !LOOP

            !IF RecordCount > 100 THEN BREAK.
            IF calc:PartQty <> 0 THEN
               RecordCount += 1
               DO WriteColumns
            END !IF
            
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary

        WriteDebug('WriteLocation END')
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text          = CLIP(LOC:SectionName)
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'

        Excel{'Range("B' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("D' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft

!        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
!            Excel{'ActiveCell.Formula'}         = 'Showing:'
!            DO XL_HorizontalAlignmentRight

!        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
!                Excel{'ActiveCell.Formula'}     = 0
!                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------
        DO CheckSheetName ! remove dodgy characters

        Excel{'ActiveSheet.Name'}                = LEFT(CLIP(LOC:Text), 30)
        !-----------------------------------------------       
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName) & ' ' & CLIP(LOC:LocationName)
        !-----------------------------------------------       
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = 'Criteria'
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Start Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(param:StartDate)
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'End Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(param:EndDate)
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'Shelf Location'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = '2nd Location'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Part Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Description'
            head:ColumnWidth  = 27.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Quantity'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}

        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
            Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('WriteColumns')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
            WriteColumn( sto:Shelf_Location, True )
            WriteColumn( sto:Second_Location      )
            WriteColumn( '''' & sto:Part_Number   )
            WriteColumn( sto:Description          )
            WriteColumn( calc:PartQty             )
        !-----------------------------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------
WriteDataSummary                                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary details
        !
        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'No Stock Found'

            EXIT
        END !IF

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
        Excel{'ActiveCell.Formula'} = '=SUBTOTAL(2,E' & sheet:DataHeaderRow+1 & ':E' & sheet:DataHeaderRow+RecordCount & ')' ! CLIP(RecordCount)

!        Excel{'Range("G9").Select'}
!        Excel{'ActiveCell.Formula'} = CLIP(RecordCount)

        DO FormatColumns
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                                               ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

TopRow       LONG
CurrentRow   LONG
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}

        DO XL_ColFirst

        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'No Stock Found'

            EXIT
        END !IF
        TopRow = Excel{'ActiveCell.Row'}

!        Excel{'ActiveCell.Offset(0, 00).Formula'} = 'Total Trade Cost'
!        Excel{'ActiveCell.Offset(1, 00).Formula'} = 'Total Retail Cost'
!        Excel{'ActiveCell.Offset(2, 00).Formula'} = 'Total Stock'
!        Excel{'ActiveCell.Offset(3, 00).Formula'} = 'Total Purchase Cost'
!        Excel{'ActiveCell.Offset(0, 01).Formula'} = calc:TotalTrade
!        Excel{'ActiveCell.Offset(1, 01).Formula'} = calc:TotalRetail
!        Excel{'ActiveCell.Offset(2, 01).Formula'} = calc:TotalStock
!        Excel{'ActiveCell.Offset(3, 01).Formula'} = calc:TotalPurchase
    EXIT
!-----------------------------------------------------------------
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Retail Parts Awaiting Picking'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        WriteDebug('GetFileName')
!        !-----------------------------------------------
!
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!        !-----------------------------------------------
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(LOC:LocationName) & ' VODR0058 ' & FORMAT(TODAY(), @D12) & '.xls'
!
!        WriteDebug('GetFileName="' & CLIP(LOC:FileName) & '"')
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG', |
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Unable to find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
CheckSheetName                                                  ROUTINE
    DATA
MaxLength LONG(30)
TextLen   LONG
    CODE
        !-----------------------------------------------------------------
        TextLen = LEN(CLIP(LOC:Text))

        IF TextLen > MaxLength
            TextLen = MaxLength
        END !IF

        LOOP x# = 1 TO TextLen
            CASE LOC:Text[x#]
            OF 'a' TO 'z'
                ! NULL
            OF 'A' TO 'Z'
                ! NULL
            OF '0' TO '9'
                ! NULL
            ELSE
                LOC:Text[x#] = '_'
            END !CASE
        END !LOOP

        LOC:Text = LEFT(LOC:Text, TextLen)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                IF NOT CancelPressed = True
                    cancel# = 1
                END !IF

                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
!    IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(LOC:LocationName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RetailPartsAwaitingPicking')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:RETSTOCK.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Parts Awaiting Picking' !          Job=1919        Cust=
  
      !---------------------------------------------
      ! 8 Oct 2002 John
      ! Also, can the name of the export be Parts Awaiting Picking with the date.
      ! LOC:ProgramName cahnged from 'Retail Parts Awaiting Picking'
      !---------------------------------------------
  
  
      param:EndDate   = TODAY()
      param:StartDate = TODAY()
  
      excel:Visible   = False
      debug:Active    = True
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
      IF GUIMode = 1 THEN
         LocalTimeOut             = 500
         MainWindow{PROP:ICONIZE} = TRUE
         DoAll = 'Y'
      END !IF
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:RETSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
 DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------
DateToString PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------
        IF IN:Date = 0
            RETURN ''
        ELSE
            RETURN LEFT(FORMAT(IN:Date, @D8))
        END !IF
        !-----------------------------------------------
LoadLOCATION  PROCEDURE( IN:Location )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadLOCATION(' & CLIP(IN:Location) & ')')

        Access:LOCATION.ClearKey(loc:Location_Key)
            loc:Location = IN:Location
        SET(loc:Location_Key, loc:Location_Key)

        IF Access:LOCATION.NEXT()
            WriteDebug('LoadLOCATION FALSE(Next))')

            RETURN False
        END !IF

        IF NOT loc:Location = IN:Location
            WriteDebug('LoadLOCATION FALSE(' & CLIP(loc:Location) & '))')

            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadORDERS  PROCEDURE( IN:OrderNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Received_Key             KEY(ord:All_Received,                              ord:Order_Number),DUP,NOCASE
        ! Order_Number_Key         KEY(                                               ord:Order_Number),NOCASE,PRIMARY
        ! Printed_Key              KEY(ord:Printed,                                   ord:Order_Number),DUP,NOCASE
        ! Supplier_Printed_Key     KEY(ord:Printed, ord:Supplier,                     ord:Order_Number),DUP,NOCASE
        ! Supplier_Received_Key    KEY(ord:Supplier,                ord:All_Received, ord:Order_Number),DUP,NOCASE
        ! Supplier_Key             KEY(ord:Supplier,                                  ord:Order_Number),DUP,NOCASE
        !
        Access:ORDERS.ClearKey(ord:Order_Number_Key)
        ord:Order_Number = IN:OrderNumber

        IF Access:ORDERS.TryFetch(ord:Order_Number_Key)
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSTOCK  PROCEDURE( IN:StockNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Ref_Number_Key KEY( sto:Ref_Number ),NOCASE,PRIMARY
        !
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = IN:StockNumber

        IF Access:STOCK.TryFetch(sto:Ref_Number_Key)
            RETURN False
        END !IF

        IF sto:Ref_Number <> IN:StockNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
NumberToColumn PROCEDURE(IN:Long)!STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:Start )
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:Start
                clip:Value = IN:String
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & IN:String
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = IN:String

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug  PROCEDURE( IN:Message )
    CODE
        !------------------------------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'c:\Debug.ini')
        !------------------------------------------------------------------
!-----------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

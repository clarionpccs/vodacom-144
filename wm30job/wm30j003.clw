

   MEMBER('wm30job.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('WM30J003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WM30J001.INC'),ONCE        !Req'd for module callout resolution
                     END


SetUp                PROCEDURE                        ! Declare Procedure
tmp:VersionNumber    STRING(30)
tmp:Error            BYTE(0)
  CODE
    x# = BindToCPU()
    ! set path and update procedure
    tmp:Error         = False
    tmp:VersionNumber = GETINI('VERSION', 'VersionNumber',, CLIP(PATH()) & '\SB2KVER.INI')

    g_path = GETINI('global', 'datapath', 'xx7fail8zz', clip(path()) & '\celweb.ini')
    ! message('g_path is ' & clip(g_path))
    If clip(g_path) = 'xx7fail8zz' then
        ! Start - Close the splash window (DBH: 25-05-2005)
        ClarioNET:CallClientProcedure('CLOSESPLASH')
        ! End   - Close the splash window (DBH: 25-05-2005)
        Beep(Beep:SystemHand)  
        Yield()
        Case Message('Error!' &                     |
                     '||INI file not found.',       |
                     'ServiceBase 2000', Icon:Hand, |
                     Button:OK, Button:OK, 0)
        Of Button:OK
        End ! CASE
        tmp:Error = True
    End ! If

    If tmp:Error = False
        setpath (clip(g_path))
        Access:DefaultV.Open()
        Access:DefaultV.UseFile()
        Set(DEFAULTV)
        Access:DEFAULTV.Next()

        If tmp:VersionNumber <> ''
            If tmp:VersionNumber <> defv:VersionNumber
                ! Start - Close the splash window (DBH: 25-05-2005)
                ClarioNET:CallClientProcedure('CLOSESPLASH')
                ! End   - Close the splash window (DBH: 25-05-2005)
                Beep(Beep:SystemExclamation)  
                Yield()
                Case Message('The current version of Webmaster does not appear to match ' &|
                             'the current version of ServiceBase.' &               |
                             '||ServiceBase version: ' &Clip(defv:VersionNumber) & |
                             '||Webmaster version:  ' &Clip(tmp:VersionNumber),    |
                             'ServiceBase 2000', Icon:Exclamation,                 |
                             Button:Ignore + Button:Cancel, Button:Ignore, 0)
                Of Button:Ignore
                Of Button:Cancel
                    tmp:Error = True
                End ! CASE
            End ! If tmp:CurrentVersion <> defv:VersionNumber
        End ! If tmp:CurrentVersion <> ''
        Access:Users.Open()
        Access:Users.UseFile()

    End ! If tmp:Error = False

    ! #12085 Make sure Site Location globals are set. (Bryan: 12/04/2011)
    Password = Upper(Password)
    Account_Number_Global = Upper(Account_Number_Global)

    Relate:TRADEACC.Open()

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = Account_Number_Global
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        Beep(Beep:SystemHand)  ;  Yield()
        Case Message('Failed to retrieve Account Details for Account Number: ' & Clip(Account_Number_Global) & '','ServiceBase',|
                       Icon:Hand,'&OK',1) 
        Of 1 ! &OK Button
        End!Case Message
        tmp:Error = 1
    END

    glo:Location = tra:SiteLocation
    glo:Default_Site_Location = tra:SiteLocation

    Relate:TRADEACC.Close()

    If tmp:Error = False

!        ! Check passed password
!        Select_Global1 = 'Y'
!
!        ! check Trade account - Also checks personal password
!        Check_Web_Password
!
!        if Select_Global1 = 'X' ! Trade account details OK, Personal wrong
!            glo:select1 = 'Y'
!            Insert_Password
!            Select_Global1 = glo:select1
!        end ! Check_Password not a success

        ! The above is no longer necessary
        ! because we don't pass the passwords
        ! from connect. Should fix blank passwords issue. Looks like it was a little necessary, see next :) (Bryan: 04/04/2011)

        glo:Select1 = 'Y'
        Insert_Password
        Select_Global1 = glo:Select1

        access:users.clearkey(use:password_key)
        use:password = glo:Password
        access:Users.fetch(use:password_key)
        if use:Location <> glo:location then
            Select_global1 = 'Y'
        end ! if

        If Select_Global1 <> 'N'
            ! Halt (0,'Logon Details Incorrect')
            ! Halt is too Harsh for clarionet and causes errors (DBH: 22-03-2005)
            tmp:Error = True
        Else ! If Select_Global1 <> 'N'
            Select_Global1 = ''

            set(defaultv)
            if access:defaultv.next()
            ! Error - leave to other programs to sort out
            Else
                ! save the version number on the client machine
                ClarioNET:CallClientProcedure('VERSIONSAVE', defv:VersionNumber)
            end ! if

            ! put something on the clipboad - browseWobs should overwrite it
            Access:Users.Close()
            Access:DefaultV.Close()

            ClarioNET:CallClientProcedure('CLIPBOARD', 'BREAK')
            glo:select1 = Account_number_global

            Browse_Wobs()
        End ! If Select_Global1 <> 'N'
    End ! If tmp:Error = False
    ! Start - Close the splash window (DBH: 25-05-2005)
    ClarioNET:CallClientProcedure('CLOSESPLASH')
    ! End   - Close the splash window (DBH: 25-05-2005)
    ClarioNETServer:Kill
    RETURN



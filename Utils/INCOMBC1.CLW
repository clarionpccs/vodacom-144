  MEMBER('incompletewarrantyjobsfix.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
INCOMBC1:DctInit    PROCEDURE
INCOMBC1:DctKill    PROCEDURE
INCOMBC1:FilesInit  PROCEDURE
  END

Hide:Access:JOBSTAMP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSTAMP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ACCESDEF CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ACCESDEF CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBACCNO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBACCNO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBRPNOT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBRPNOT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSOBF  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSOBF  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSINV  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSINV  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSCONS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSCONS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSWARR CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSWARR CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSE2   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSE2   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRDSPEC  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRDSPEC  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDPEND  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORDPEND  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOHIST  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOHIST  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:REPTYDEF CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:REPTYDEF CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PRIORITY CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PRIORITY CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSE    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSE    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRACHAR  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRACHAR  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCVALUE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOCVALUE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COURIER  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COURIER  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRDPARTY CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:TRDPARTY CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

INCOMBC1:DctInit PROCEDURE
  CODE
  Relate:JOBSTAMP &= Hide:Relate:JOBSTAMP
  Relate:ACCESDEF &= Hide:Relate:ACCESDEF
  Relate:JOBACCNO &= Hide:Relate:JOBACCNO
  Relate:JOBRPNOT &= Hide:Relate:JOBRPNOT
  Relate:JOBSOBF &= Hide:Relate:JOBSOBF
  Relate:JOBSINV &= Hide:Relate:JOBSINV
  Relate:JOBSCONS &= Hide:Relate:JOBSCONS
  Relate:JOBSWARR &= Hide:Relate:JOBSWARR
  Relate:JOBSE2 &= Hide:Relate:JOBSE2
  Relate:TRDSPEC &= Hide:Relate:TRDSPEC
  Relate:ORDPEND &= Hide:Relate:ORDPEND
  Relate:STOHIST &= Hide:Relate:STOHIST
  Relate:REPTYDEF &= Hide:Relate:REPTYDEF
  Relate:PRIORITY &= Hide:Relate:PRIORITY
  Relate:JOBSE &= Hide:Relate:JOBSE
  Relate:MANFAULT &= Hide:Relate:MANFAULT
  Relate:TRACHAR &= Hide:Relate:TRACHAR
  Relate:LOCVALUE &= Hide:Relate:LOCVALUE
  Relate:COURIER &= Hide:Relate:COURIER
  Relate:TRDPARTY &= Hide:Relate:TRDPARTY

INCOMBC1:FilesInit PROCEDURE
  CODE
  Hide:Relate:JOBSTAMP.Init
  Hide:Relate:ACCESDEF.Init
  Hide:Relate:JOBACCNO.Init
  Hide:Relate:JOBRPNOT.Init
  Hide:Relate:JOBSOBF.Init
  Hide:Relate:JOBSINV.Init
  Hide:Relate:JOBSCONS.Init
  Hide:Relate:JOBSWARR.Init
  Hide:Relate:JOBSE2.Init
  Hide:Relate:TRDSPEC.Init
  Hide:Relate:ORDPEND.Init
  Hide:Relate:STOHIST.Init
  Hide:Relate:REPTYDEF.Init
  Hide:Relate:PRIORITY.Init
  Hide:Relate:JOBSE.Init
  Hide:Relate:MANFAULT.Init
  Hide:Relate:TRACHAR.Init
  Hide:Relate:LOCVALUE.Init
  Hide:Relate:COURIER.Init
  Hide:Relate:TRDPARTY.Init


INCOMBC1:DctKill PROCEDURE
  CODE
  Hide:Relate:JOBSTAMP.Kill
  Hide:Relate:ACCESDEF.Kill
  Hide:Relate:JOBACCNO.Kill
  Hide:Relate:JOBRPNOT.Kill
  Hide:Relate:JOBSOBF.Kill
  Hide:Relate:JOBSINV.Kill
  Hide:Relate:JOBSCONS.Kill
  Hide:Relate:JOBSWARR.Kill
  Hide:Relate:JOBSE2.Kill
  Hide:Relate:TRDSPEC.Kill
  Hide:Relate:ORDPEND.Kill
  Hide:Relate:STOHIST.Kill
  Hide:Relate:REPTYDEF.Kill
  Hide:Relate:PRIORITY.Kill
  Hide:Relate:JOBSE.Kill
  Hide:Relate:MANFAULT.Kill
  Hide:Relate:TRACHAR.Kill
  Hide:Relate:LOCVALUE.Kill
  Hide:Relate:COURIER.Kill
  Hide:Relate:TRDPARTY.Kill


Hide:Access:JOBSTAMP.Init PROCEDURE
  CODE
  SELF.Init(JOBSTAMP,GlobalErrors)
  SELF.Buffer &= jos:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jos:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jos:JOBSRefNumberKey,'By RefNumber',0)
  SELF.AddKey(jos:DateTimeKey,'By Date Time',0)
  Access:JOBSTAMP &= SELF


Hide:Relate:JOBSTAMP.Init PROCEDURE
  CODE
  Hide:Access:JOBSTAMP.Init
  SELF.Init(Access:JOBSTAMP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSTAMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSTAMP &= NULL


Hide:Relate:JOBSTAMP.Kill PROCEDURE

  CODE
  Hide:Access:JOBSTAMP.Kill
  PARENT.Kill
  Relate:JOBSTAMP &= NULL


Hide:Access:ACCESDEF.Init PROCEDURE
  CODE
  SELF.Init(ACCESDEF,GlobalErrors)
  SELF.Buffer &= acd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(acd:Accessory_Key,'By Accessory',0)
  Access:ACCESDEF &= SELF


Hide:Relate:ACCESDEF.Init PROCEDURE
  CODE
  Hide:Access:ACCESDEF.Init
  SELF.Init(Access:ACCESDEF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ACCESSOR,RI:CASCADE,RI:None,acr:Model_Number_Key)
  SELF.AddRelationLink(acd:Accessory,acr:Accessory)


Hide:Access:ACCESDEF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ACCESDEF &= NULL


Hide:Relate:ACCESDEF.Kill PROCEDURE

  CODE
  Hide:Access:ACCESDEF.Kill
  PARENT.Kill
  Relate:ACCESDEF &= NULL


Hide:Access:JOBACCNO.Init PROCEDURE
  CODE
  SELF.Init(JOBACCNO,GlobalErrors)
  SELF.Buffer &= joa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joa:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joa:AccessoryNumberKey,'By Accessory Number',0)
  SELF.AddKey(joa:AccessoryNoOnlyKey,'By Accessory Number',0)
  Access:JOBACCNO &= SELF


Hide:Relate:JOBACCNO.Init PROCEDURE
  CODE
  Hide:Access:JOBACCNO.Init
  SELF.Init(Access:JOBACCNO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBACCNO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBACCNO &= NULL


Hide:Relate:JOBACCNO.Kill PROCEDURE

  CODE
  Hide:Access:JOBACCNO.Kill
  PARENT.Kill
  Relate:JOBACCNO &= NULL


Hide:Access:JOBRPNOT.Init PROCEDURE
  CODE
  SELF.Init(JOBRPNOT,GlobalErrors)
  SELF.Buffer &= jrn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jrn:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jrn:TheDateKey,'By Date',0)
  Access:JOBRPNOT &= SELF


Hide:Relate:JOBRPNOT.Init PROCEDURE
  CODE
  Hide:Access:JOBRPNOT.Init
  SELF.Init(Access:JOBRPNOT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBRPNOT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBRPNOT &= NULL


Hide:Access:JOBRPNOT.PrimeFields PROCEDURE

  CODE
  jrn:TheDate = Today()
  jrn:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBRPNOT.Kill PROCEDURE

  CODE
  Hide:Access:JOBRPNOT.Kill
  PARENT.Kill
  Relate:JOBRPNOT &= NULL


Hide:Access:JOBSOBF.Init PROCEDURE
  CODE
  SELF.Init(JOBSOBF,GlobalErrors)
  SELF.Buffer &= jof:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jof:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jof:RefNumberKey,'By Job Number',0)
  SELF.AddKey(jof:StatusRefNumberKey,'By Job Number',0)
  SELF.AddKey(jof:StatusIMEINumberKey,'By I.M.E.I. Number',0)
  SELF.AddKey(jof:HeadAccountCompletedKey,'By Date Completed',0)
  SELF.AddKey(jof:HeadAccountProcessedKey,'By Date Processed',0)
  Access:JOBSOBF &= SELF


Hide:Relate:JOBSOBF.Init PROCEDURE
  CODE
  Hide:Access:JOBSOBF.Init
  SELF.Init(Access:JOBSOBF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSOBF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSOBF &= NULL


Hide:Access:JOBSOBF.PrimeFields PROCEDURE

  CODE
  jof:Status = 0
  jof:Replacement = 0
  PARENT.PrimeFields


Hide:Relate:JOBSOBF.Kill PROCEDURE

  CODE
  Hide:Access:JOBSOBF.Kill
  PARENT.Kill
  Relate:JOBSOBF &= NULL


Hide:Access:JOBSINV.Init PROCEDURE
  CODE
  SELF.Init(JOBSINV,GlobalErrors)
  SELF.Buffer &= jov:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jov:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jov:RefNumberKey,'By Record Number',0)
  SELF.AddKey(jov:DateCreatedKey,'By Date Created',0)
  SELF.AddKey(jov:DateCreatedOnlyKey,'By Job Number',0)
  SELF.AddKey(jov:TypeRecordKey,'By Record Number',0)
  SELF.AddKey(jov:TypeSuffixKey,'By Suffix',0)
  SELF.AddKey(jov:InvoiceNumberKey,'By Invoice Number',0)
  SELF.AddKey(jov:InvoiceTypeKey,'By Invoice Number',0)
  SELF.AddKey(jov:BookingDateTypeKey,'By Type',0)
  SELF.AddKey(jov:TypeDateKey,'By Type',0)
  Access:JOBSINV &= SELF


Hide:Relate:JOBSINV.Init PROCEDURE
  CODE
  Hide:Access:JOBSINV.Init
  SELF.Init(Access:JOBSINV,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSINV.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSINV &= NULL


Hide:Access:JOBSINV.PrimeFields PROCEDURE

  CODE
  jov:DateCreated = Today()
  jov:TimeCreated = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBSINV.Kill PROCEDURE

  CODE
  Hide:Access:JOBSINV.Kill
  PARENT.Kill
  Relate:JOBSINV &= NULL


Hide:Access:JOBSCONS.Init PROCEDURE
  CODE
  SELF.Init(JOBSCONS,GlobalErrors)
  SELF.Buffer &= joc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joc:DateKey,'By Date',0)
  SELF.AddKey(joc:ConsignmentNumberKey,'By Consignment Number',0)
  SELF.AddKey(joc:DespatchFromDateKey,'By Date',0)
  SELF.AddKey(joc:DateOnlyKey,'By Date',0)
  SELF.AddKey(joc:CourierKey,'By Courier',0)
  SELF.AddKey(joc:DespatchFromCourierKey,'By Job Number',0)
  Access:JOBSCONS &= SELF


Hide:Relate:JOBSCONS.Init PROCEDURE
  CODE
  Hide:Access:JOBSCONS.Init
  SELF.Init(Access:JOBSCONS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSCONS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSCONS &= NULL


Hide:Access:JOBSCONS.PrimeFields PROCEDURE

  CODE
  joc:TheDate = Today()
  joc:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBSCONS.Kill PROCEDURE

  CODE
  Hide:Access:JOBSCONS.Kill
  PARENT.Kill
  Relate:JOBSCONS &= NULL


Hide:Access:JOBSWARR.Init PROCEDURE
  CODE
  SELF.Init(JOBSWARR,GlobalErrors)
  SELF.Buffer &= jow:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jow:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jow:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jow:StatusManFirstKey,'By Job Number',0)
  SELF.AddKey(jow:StatusManKey,'By Job Number',0)
  SELF.AddKey(jow:ClaimStatusManKey,'By Job Number',0)
  SELF.AddKey(jow:ClaimStatusManFirstKey,'By Job Number',0)
  SELF.AddKey(jow:RRCStatusKey,'By Job Number',0)
  SELF.AddKey(jow:RRCStatusManKey,'By Job Number',0)
  SELF.AddKey(jow:RRCReconciledManKey,'By Job Number',0)
  SELF.AddKey(jow:RRCReconciledKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCStatusKey,'By RRC Status',0)
  SELF.AddKey(jow:RepairedRRCStatusManKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCReconciledKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCReconManKey,'By Job Number',0)
  SELF.AddKey(jow:SubmittedRepairedBranchKey,'By Branch',0)
  SELF.AddKey(jow:SubmittedBranchKey,'By Job Number',0)
  SELF.AddKey(jow:ClaimSubmittedKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCAccManKey,'By Job Number',0)
  SELF.AddKey(jow:AcceptedBranchKey,'By Job Number',0)
  SELF.AddKey(jow:DateAcceptedKey,'By Job Number',0)
  SELF.AddKey(jow:RejectedBranchKey,'By Job Number',0)
  SELF.AddKey(jow:RejectedKey,'By Job Number',0)
  SELF.AddKey(jow:FinalRejectionBranchKey,'By Job Number',0)
  SELF.AddKey(jow:FinalRejectionKey,'By Job Number',0)
  Access:JOBSWARR &= SELF


Hide:Relate:JOBSWARR.Init PROCEDURE
  CODE
  Hide:Access:JOBSWARR.Init
  SELF.Init(Access:JOBSWARR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSWARR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSWARR &= NULL


Hide:Access:JOBSWARR.PrimeFields PROCEDURE

  CODE
  jow:FirstSecondYear = 0
  jow:FromApproved = 0
  PARENT.PrimeFields


Hide:Relate:JOBSWARR.Kill PROCEDURE

  CODE
  Hide:Access:JOBSWARR.Kill
  PARENT.Kill
  Relate:JOBSWARR &= NULL


Hide:Access:JOBSE2.Init PROCEDURE
  CODE
  SELF.Init(JOBSE2,GlobalErrors)
  SELF.Buffer &= jobe2:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jobe2:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jobe2:RefNumberKey,'By Ref Number',0)
  Access:JOBSE2 &= SELF


Hide:Relate:JOBSE2.Init PROCEDURE
  CODE
  Hide:Access:JOBSE2.Init
  SELF.Init(Access:JOBSE2,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSE2.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSE2 &= NULL


Hide:Access:JOBSE2.PrimeFields PROCEDURE

  CODE
  jobe2:Contract = 0
  jobe2:Prepaid = 0
  jobe2:XAntenna = 0
  jobe2:XLens = 0
  jobe2:XFCover = 0
  jobe2:XBCover = 0
  jobe2:XKeypad = 0
  jobe2:XBattery = 0
  jobe2:XCharger = 0
  jobe2:XLCD = 0
  jobe2:XSimReader = 0
  jobe2:XSystemConnector = 0
  jobe2:XNone = 0
  jobe2:ExchangeTerms = 0
  jobe2:SMSNotification = 0
  jobe2:EmailNotification = 0
  PARENT.PrimeFields


Hide:Relate:JOBSE2.Kill PROCEDURE

  CODE
  Hide:Access:JOBSE2.Kill
  PARENT.Kill
  Relate:JOBSE2 &= NULL


Hide:Access:TRDSPEC.Init PROCEDURE
  CODE
  SELF.Init(TRDSPEC,GlobalErrors)
  SELF.Buffer &= tsp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tsp:Short_Description_Key,'By Short Description',0)
  Access:TRDSPEC &= SELF


Hide:Relate:TRDSPEC.Init PROCEDURE
  CODE
  Hide:Access:TRDSPEC.Init
  SELF.Init(Access:TRDSPEC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDPARTY,RI:CASCADE,RI:RESTRICT,trd:Special_Instructions_Key)
  SELF.AddRelationLink(tsp:Short_Description,trd:Special_Instructions)


Hide:Access:TRDSPEC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRDSPEC &= NULL


Hide:Relate:TRDSPEC.Kill PROCEDURE

  CODE
  Hide:Access:TRDSPEC.Kill
  PARENT.Kill
  Relate:TRDSPEC &= NULL


Hide:Access:ORDPEND.Init PROCEDURE
  CODE
  SELF.Init(ORDPEND,GlobalErrors)
  SELF.Buffer &= ope:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ope:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(ope:Supplier_Key,'By Supplier',0)
  SELF.AddKey(ope:DescriptionKey,'By Description',0)
  SELF.AddKey(ope:Supplier_Name_Key,'By Supplier',0)
  SELF.AddKey(ope:Part_Ref_Number_Key,'By Stock Ref Number',0)
  SELF.AddKey(ope:Awaiting_Supplier_Key,'By Part Number',0)
  SELF.AddKey(ope:PartRecordNumberKey,'By Record Number',0)
  SELF.AddKey(ope:ReqPartNumber,'By Part Number',0)
  SELF.AddKey(ope:ReqDescriptionKey,'By Description',0)
  SELF.AddKey(ope:KeyPrevStoReqNo,'ope:KeyPrevStoReqNo',0)
  Access:ORDPEND &= SELF


Hide:Relate:ORDPEND.Init PROCEDURE
  CODE
  Hide:Access:ORDPEND.Init
  SELF.Init(Access:ORDPEND,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)


Hide:Access:ORDPEND.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDPEND &= NULL


Hide:Access:ORDPEND.PrimeFields PROCEDURE

  CODE
  ope:Awaiting_Stock = 'NO'
  PARENT.PrimeFields


Hide:Relate:ORDPEND.Kill PROCEDURE

  CODE
  Hide:Access:ORDPEND.Kill
  PARENT.Kill
  Relate:ORDPEND &= NULL


Hide:Access:STOHIST.Init PROCEDURE
  CODE
  SELF.Init(STOHIST,GlobalErrors)
  SELF.Buffer &= shi:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(shi:Ref_Number_Key,'By Ref_Number',0)
  SELF.AddKey(shi:record_number_key,'shi:record_number_key',1)
  SELF.AddKey(shi:Transaction_Type_Key,'By Transaction',0)
  SELF.AddKey(shi:DateKey,'By Date',0)
  SELF.AddKey(shi:JobNumberKey,'By Job Number',0)
  Access:STOHIST &= SELF


Hide:Relate:STOHIST.Init PROCEDURE
  CODE
  Hide:Access:STOHIST.Init
  SELF.Init(Access:STOHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOHISTE,RI:CASCADE,RI:CASCADE,stoe:SHIRecordNumberKey)
  SELF.AddRelationLink(shi:Record_Number,stoe:SHIRecordNumber)
  SELF.AddRelation(Relate:STOCK)


Hide:Access:STOHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOHIST &= NULL


Hide:Relate:STOHIST.Kill PROCEDURE

  CODE
  Hide:Access:STOHIST.Kill
  PARENT.Kill
  Relate:STOHIST &= NULL


Hide:Access:REPTYDEF.Init PROCEDURE
  CODE
  SELF.Init(REPTYDEF,GlobalErrors)
  SELF.Buffer &= rtd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(rtd:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(rtd:ManRepairTypeKey,'By Repair Type',0)
  SELF.AddKey(rtd:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(rtd:Chargeable_Key,'rtd:Chargeable_Key',0)
  SELF.AddKey(rtd:Warranty_Key,'rtd:Warranty_Key',0)
  SELF.AddKey(rtd:ChaManRepairTypeKey,'By Repair Type',0)
  SELF.AddKey(rtd:WarManRepairTypeKey,'By Repair Type',0)
  Access:REPTYDEF &= SELF


Hide:Relate:REPTYDEF.Init PROCEDURE
  CODE
  Hide:Access:REPTYDEF.Init
  SELF.Init(Access:REPTYDEF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:REPTYCAT,RI:CASCADE,RI:CASCADE,repc:RepairTypeKey)
  SELF.AddRelationLink(rtd:Repair_Type,repc:RepairType)


Hide:Access:REPTYDEF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:REPTYDEF &= NULL


Hide:Access:REPTYDEF.PrimeFields PROCEDURE

  CODE
  rtd:Chargeable = 'YES'
  rtd:Warranty = 'YES'
  rtd:CompFaultCoding = 0
  rtd:ExcludeFromEDI = 0
  rtd:ExcludeFromInvoicing = 0
  rtd:BER = 0
  rtd:ExcludeFromBouncer = 0
  rtd:PromptForExchange = 0
  rtd:ForceAdjustment = 0
  rtd:ScrapExchange = 0
  rtd:ExcludeHandlingFee = 0
  rtd:NotAvailable = 0
  PARENT.PrimeFields


Hide:Relate:REPTYDEF.Kill PROCEDURE

  CODE
  Hide:Access:REPTYDEF.Kill
  PARENT.Kill
  Relate:REPTYDEF &= NULL


Hide:Access:PRIORITY.Init PROCEDURE
  CODE
  SELF.Init(PRIORITY,GlobalErrors)
  SELF.Buffer &= pri:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(pri:Priority_Type_Key,'By Priority Type',0)
  Access:PRIORITY &= SELF


Hide:Relate:PRIORITY.Init PROCEDURE
  CODE
  Hide:Access:PRIORITY.Init
  SELF.Init(Access:PRIORITY,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:PriorityTypeKey)
  SELF.AddRelationLink(pri:Priority_Type,job:Job_Priority)


Hide:Access:PRIORITY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PRIORITY &= NULL


Hide:Relate:PRIORITY.Kill PROCEDURE

  CODE
  Hide:Access:PRIORITY.Kill
  PARENT.Kill
  Relate:PRIORITY &= NULL


Hide:Access:JOBSE.Init PROCEDURE
  CODE
  SELF.Init(JOBSE,GlobalErrors)
  SELF.Buffer &= jobe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jobe:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jobe:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jobe:WarrStatusDateKey,'By Warranty Status Date',0)
  Access:JOBSE &= SELF


Hide:Relate:JOBSE.Init PROCEDURE
  CODE
  Hide:Access:JOBSE.Init
  SELF.Init(Access:JOBSE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSE &= NULL


Hide:Access:JOBSE.PrimeFields PROCEDURE

  CODE
  jobe:JobMark = 0
  jobe:JobReceived = 0
  jobe:FailedDelivery = 0
  jobe:CConfirmSecondEntry = 0
  jobe:HubRepair = 0
  jobe:POPConfirmed = 0
  jobe:ValidPOP = 'NO'
  jobe:OriginalPackaging = 0
  jobe:OriginalBattery = 0
  jobe:OriginalCharger = 0
  jobe:OriginalAntenna = 0
  jobe:OriginalManuals = 0
  jobe:PhysicalDamage = 0
  jobe:COverwriteRepairType = 0
  jobe:WOverwriteRepairType = 0
  jobe:IgnoreClaimCosts = 0
  jobe:WebJob = 0
  jobe:ExceedWarrantyRepairLimit = 0
  jobe:ConfirmClaimAdjustment = 0
  jobe:ExchangedATRRC = 0
  jobe:ClaimColour = 0
  jobe:Ignore3rdPartyCosts = 0
  jobe:OBFProcessed = 0
  jobe:PendingClaimColour = 0
  jobe:Booking48HourOption = 0
  jobe:Engineer48HourOption = 0
  jobe:ExcReplcamentCharge = 0
  jobe:VSACustomer = 0
  PARENT.PrimeFields


Hide:Relate:JOBSE.Kill PROCEDURE

  CODE
  Hide:Access:JOBSE.Kill
  PARENT.Kill
  Relate:JOBSE &= NULL


Hide:Access:MANFAULT.Init PROCEDURE
  CODE
  SELF.Init(MANFAULT,GlobalErrors)
  SELF.Buffer &= maf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(maf:Field_Number_Key,'By Field Number',0)
  SELF.AddKey(maf:MainFaultKey,'By Main Fault',0)
  SELF.AddKey(maf:InFaultKey,'By In Fault',0)
  SELF.AddKey(maf:ScreenOrderKey,'By Screen Order',0)
  Access:MANFAULT &= SELF


Hide:Relate:MANFAULT.Init PROCEDURE
  CODE
  Hide:Access:MANFAULT.Init
  SELF.Init(Access:MANFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:MANFAULO,RI:CASCADE,RI:CASCADE,mfo:Field_Key)
  SELF.AddRelationLink(maf:Manufacturer,mfo:Manufacturer)
  SELF.AddRelationLink(maf:Field_Number,mfo:Field_Number)


Hide:Access:MANFAULT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAULT &= NULL


Hide:Access:MANFAULT.PrimeFields PROCEDURE

  CODE
  maf:Compulsory = 'NO'
  maf:Compulsory_At_Booking = 'NO'
  maf:RestrictLength = 0
  maf:ForceFormat = 0
  maf:MainFault = 0
  maf:PromptForExchange = 0
  maf:InFault = 0
  maf:CompulsoryIfExchange = 0
  maf:NotAvailable = 0
  maf:CharCompulsory = 0
  maf:CharCompulsoryBooking = 0
  maf:RestrictAvailability = 0
  maf:RestrictServiceCentre = 0
  maf:GenericFault = 0
  maf:NotCompulsoryThirdParty = 0
  maf:HideThirdParty = 0
  maf:HideRelatedCodeIfBlank = 0
  maf:FillFromDOP = 0
  maf:CompulsoryForRepairType = 0
  PARENT.PrimeFields


Hide:Relate:MANFAULT.Kill PROCEDURE

  CODE
  Hide:Access:MANFAULT.Kill
  PARENT.Kill
  Relate:MANFAULT &= NULL


Hide:Access:TRACHAR.Init PROCEDURE
  CODE
  SELF.Init(TRACHAR,GlobalErrors)
  SELF.Buffer &= tch:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tch:Account_Number_Key,'By Charge Type',0)
  Access:TRACHAR &= SELF


Hide:Relate:TRACHAR.Init PROCEDURE
  CODE
  Hide:Access:TRACHAR.Init
  SELF.Init(Access:TRACHAR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRACHAR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRACHAR &= NULL


Hide:Relate:TRACHAR.Kill PROCEDURE

  CODE
  Hide:Access:TRACHAR.Kill
  PARENT.Kill
  Relate:TRACHAR &= NULL


Hide:Access:LOCVALUE.Init PROCEDURE
  CODE
  SELF.Init(LOCVALUE,GlobalErrors)
  SELF.Buffer &= lov:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lov:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lov:DateKey,'By Date',0)
  SELF.AddKey(lov:DateOnly,'lov:DateOnly',0)
  Access:LOCVALUE &= SELF


Hide:Relate:LOCVALUE.Init PROCEDURE
  CODE
  Hide:Access:LOCVALUE.Init
  SELF.Init(Access:LOCVALUE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCATION)


Hide:Access:LOCVALUE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCVALUE &= NULL


Hide:Relate:LOCVALUE.Kill PROCEDURE

  CODE
  Hide:Access:LOCVALUE.Kill
  PARENT.Kill
  Relate:LOCVALUE &= NULL


Hide:Access:COURIER.Init PROCEDURE
  CODE
  SELF.Init(COURIER,GlobalErrors)
  SELF.Buffer &= cou:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cou:Courier_Key,'By Courier',0)
  SELF.AddKey(cou:Courier_Type_Key,'By Courier_Type',0)
  Access:COURIER &= SELF


Hide:Relate:COURIER.Init PROCEDURE
  CODE
  Hide:Access:COURIER.Init
  SELF.Init(Access:COURIER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COUBUSHR,RI:CASCADE,RI:CASCADE,cbh:TypeDateKey)
  SELF.AddRelationLink(cou:Courier,cbh:Courier)
  SELF.AddRelation(Relate:TRDPARTY,RI:CASCADE,RI:RESTRICT,trd:Courier_Only_Key)
  SELF.AddRelationLink(cou:Courier,trd:Courier)


Hide:Access:COURIER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COURIER &= NULL


Hide:Access:COURIER.PrimeFields PROCEDURE

  CODE
  cou:Courier_Type = 'NA'
  cou:DespatchClose = 'NO'
  cou:CustomerCollection = 0
  cou:AutoConsignmentNo = 0
  cou:PrintLabel = 0
  cou:PrintWaybill = 0
  cou:IncludeSaturday = 0
  cou:IncludeSunday = 0
  PARENT.PrimeFields


Hide:Relate:COURIER.Kill PROCEDURE

  CODE
  Hide:Access:COURIER.Kill
  PARENT.Kill
  Relate:COURIER &= NULL


Hide:Access:TRDPARTY.Init PROCEDURE
  CODE
  SELF.Init(TRDPARTY,GlobalErrors)
  SELF.Buffer &= trd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(trd:Company_Name_Key,'By Company Name',0)
  SELF.AddKey(trd:Account_Number_Key,'By Account_Number',0)
  SELF.AddKey(trd:Special_Instructions_Key,'By Special Instructions',0)
  SELF.AddKey(trd:Courier_Only_Key,'trd:Courier_Only_Key',0)
  SELF.AddKey(trd:DeactivateCompanyKey,'By Company Name',0)
  SELF.AddKey(trd:ASCIDKey,'By ASC ID',0)
  Access:TRDPARTY &= SELF


Hide:Relate:TRDPARTY.Init PROCEDURE
  CODE
  Hide:Access:TRDPARTY.Init
  SELF.Init(Access:TRDPARTY,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDMAN,RI:CASCADE,RI:CASCADE,tdm:ManufacturerKey)
  SELF.AddRelationLink(trd:Company_Name,tdm:ThirdPartyCompanyName)
  SELF.AddRelation(Relate:TRDMODEL,RI:CASCADE,RI:CASCADE,trm:Model_Number_Key)
  SELF.AddRelationLink(trd:Company_Name,trm:Company_Name)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Third_Party_Key)
  SELF.AddRelationLink(trd:Company_Name,job:Third_Party_Site)
  SELF.AddRelation(Relate:TRDSPEC)
  SELF.AddRelation(Relate:COURIER)


Hide:Access:TRDPARTY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRDPARTY &= NULL


Hide:Access:TRDPARTY.PrimeFields PROCEDURE

  CODE
  trd:Batch_Number = 0
  trd:Deactivate = 0
  trd:LHubAccount = 0
  trd:EVO_Excluded = 0
  PARENT.PrimeFields


Hide:Relate:TRDPARTY.Kill PROCEDURE

  CODE
  Hide:Access:TRDPARTY.Kill
  PARENT.Kill
  Relate:TRDPARTY &= NULL


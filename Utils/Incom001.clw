

   MEMBER('incompletewarrantyjobsfix.clw')            ! This is a MEMBER module

                     MAP
                       INCLUDE('INCOM001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
startDate       DATE()
window WINDOW('Incomplete Jobs In Warranty Browse'),AT(,,195,69),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White), |
         CENTER,GRAY,DOUBLE
       PROMPT('Procedure to remove incomplete jobs from warranty browses'),AT(4,4,188,18),USE(?Prompt1)
       PROMPT('Only include jobs claimed after:'),AT(4,28),USE(?Prompt2)
       ENTRY(@d06),AT(116,28,52,12),USE(startDate),FONT('MS Sans Serif',8,,FONT:bold)
       BUTTON('Start'),AT(4,48,36,14),USE(?OkButton),DEFAULT
       BUTTON('Cancel'),AT(152,48,36,14),USE(?CancelButton)
     END

progressBar LONG
progresswindow WINDOW('Progress..'),AT(,,159,31),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),CENTER, |
         WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
       PROGRESS,USE(progressBar),AT(4,8,152,12),RANGE(0,100)
     END


    MAP
UpdateJobs  PROCEDURE()
    END ! MAP
  CODE
    startDate = DEFORMAT('20/01/2015',@d06)
    OPEN(window)
    ACCEPT
    CASE EVENT()
    OF Event:Accepted
        CASE FIELD()
        OF ?OKButton
            IF (startDate <> 0)
                UpdateJobs()
                BREAK
            END ! IF
        OF ?CancelButton
            BREAK
        END! CASE
        
    END !CASE
    END ! ACCEPT
    CLOSE(window)

UpdateJobs  PROCEDURE()
countJObs       LONG
i           LONG
j           LONG
qJobs       QUEUE(),PRE(qJobs)
JobNumber       LONG
            END ! QUEUE
qAuditToDelete  QUEUE(),PRE(qAuditToDelete)
RecordNumber        LONG
            END !QUEUE
    CODE
        BEEP(BEEP:SystemQuestion)  ;  YIELD()
        CASE MESSAGE('Are you sure you want to begin?','ServiceBase',|
                       ICON:Question,'&Yes|&No',2) 
        OF 1 ! &Yes Button
        OF 2 ! &No Button
            RETURN
        END!CASE MESSAGE

        Relate:JOBS.Open()
        Relate:JOBSWARR.Open()
        Relate:JOBSE.Open()
        Relate:WEBJOB.Open()
        Relate:AUDIT.Open()
        Relate:AUDITE.Open()
        Relate:AUDIT2.Open()

        SETCURSOR(CURSOR:WAIT)

        FREE(qJobs)

        Access:JOBSWARR.ClearKey(jow:ClaimSubmittedKey)
        jow:ClaimSubmitted = startDate
        SET(jow:ClaimSubmittedKey,jow:ClaimSubmittedKey)
        LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
            IF (jow:ClaimSubmitted < startDate)
                CYCLE
            END ! IF

            qJobs.JobNumber = jow:RefNumber
            ADD(qJobs)
            ! Write to Q so I can easily delete from jobswarr later
        END ! IF

        OPEN(ProgressWindow)
        DISPLAY()
        ?progressBar{prop:rangehigh} = RECORDS(qJobs)

        LOOP i = 1 TO RECORDS(qJobs)
            GET(qJobs,i)

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = qJobs.JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END ! F

            ?progressBar{prop:Progress} = ?progressBar{prop:Progress} + 1

            IF (job:Date_Completed = '' AND |
                job:EDI <> 'XXX' AND |
                job:EDI <> '' AND |
                job:Third_Party_Site = '')

                job:EDI = 'XXX'

                IF (Access:JOBS.TryUpdate())
                    BEEP(BEEP:SystemHand)  ;  YIELD()
                    CASE MESSAGE('Unable to update job number ' & CLIP(job:Ref_Number) & '.','ServiceBase',|
                                   ICON:Hand,'&OK',1) 
                    OF 1 ! &OK Button
                    END!CASE MESSAGE
                    CYCLE
                END ! IF
                countJobs += 1

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    jobe:WarrantyClaimStatus = ''
                    Access:JOBSE.TryUpdate()
                END ! IF

                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                    wob:EDI = 'XXX'
                    Access:WEBJOB.TryUpdate()
                END ! IF
                
                !Remove Audit Entry
                FREE(qAuditToDelete)
                Access:AUDIT.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action = 'ORIGINAL SUBMISSION TO WARRANTY PROCESS SCREEN'
                aud:Date = TODAY()
                SET(aud:Action_Key,aud:Action_Key)
                LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                    IF (aud:Ref_Number <> job:Ref_Number OR |
                        aud:Action <> 'ORIGINAL SUBMISSION TO WARRANTY PROCESS SCREEN')
                        BREAK
                    END ! IF
                    qAuditToDelete.RecordNumber = aud:Record_Number
                    ADD(qAuditToDelete)
                END ! LOOP

                LOOP j = 1 TO RECORDS(qAuditToDelete)
                    GET(qAuditToDelete,j)
                    Access:AUDIT.ClearKey(aud:Record_Number_Key)
                    aud:Record_Number = qAuditToDelete.RecordNumber
                    IF (Access:AUDIT.TryFetch(aud:Record_Number_Key) = Level:Benign)
                        Relate:AUDIT.Delete(0)
                    END ! IF
                END ! LOOP

                ! Remove job from warranty table
                Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                jow:RefNumber = job:Ref_Number
                IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
                    Access:JOBSWARR.DeleteRecord(0)
                END ! IF
            END ! IF
        END ! LOOP


        CLOSE(ProgressWindow)

        SETCURSOR()

        Relate:JOBS.Close()
        Relate:JOBSWARR.Close()
        Relate:JOBSE.Close()
        Relate:WEBJOB.Close()
        Relate:AUDIT.Close()
        Relate:AUDITE.Close()
        Relate:AUDIT2.Close()

        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        CASE MESSAGE('Jobs Updated: ' & CLIP(countJobs) & '','ServiceBase',|
                       ICON:Asterisk,'&OK',1) 
        OF 1 ! &OK Button
        END!CASE MESSAGE
